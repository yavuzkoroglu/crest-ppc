# 1 "./grep.cil.c"
# 1 "/home/yavuz/crest-ppc3/benchmarks/grep-2.2/src//"
# 1 "<command-line>"
# 1 "/usr/include/stdc-predef.h" 1 3 4
# 1 "<command-line>" 2
# 1 "./grep.cil.c"
# 716 "grep.c"
void __globinit_grep(void) ;
extern void __CrestInit(void) __attribute__((__crest_skip__)) ;
extern void __CrestHandleReturn(int id , long long val ) __attribute__((__crest_skip__)) ;
extern void __CrestReturn(int id ) __attribute__((__crest_skip__)) ;
extern void __CrestCall(int id , unsigned int fid ) __attribute__((__crest_skip__)) ;
extern void __CrestBranch(int id , int bid , unsigned char b ) __attribute__((__crest_skip__)) ;
extern void __CrestApply2(int id , int op , long long val ) __attribute__((__crest_skip__)) ;
extern void __CrestApply1(int id , int op , long long val ) __attribute__((__crest_skip__)) ;
extern void __CrestClearStack(int id ) __attribute__((__crest_skip__)) ;
extern void __CrestStore(int id , unsigned long addr ) __attribute__((__crest_skip__)) ;
extern void __CrestLoad(int id , unsigned long addr , long long val ) __attribute__((__crest_skip__)) ;
# 124 "/usr/include/x86_64-linux-gnu/bits/types.h"
typedef unsigned long __dev_t;
# 125 "/usr/include/x86_64-linux-gnu/bits/types.h"
typedef unsigned int __uid_t;
# 126 "/usr/include/x86_64-linux-gnu/bits/types.h"
typedef unsigned int __gid_t;
# 127 "/usr/include/x86_64-linux-gnu/bits/types.h"
typedef unsigned long __ino_t;
# 129 "/usr/include/x86_64-linux-gnu/bits/types.h"
typedef unsigned int __mode_t;
# 130 "/usr/include/x86_64-linux-gnu/bits/types.h"
typedef unsigned long __nlink_t;
# 131 "/usr/include/x86_64-linux-gnu/bits/types.h"
typedef long __off_t;
# 132 "/usr/include/x86_64-linux-gnu/bits/types.h"
typedef long __off64_t;
# 139 "/usr/include/x86_64-linux-gnu/bits/types.h"
typedef long __time_t;
# 153 "/usr/include/x86_64-linux-gnu/bits/types.h"
typedef long __blksize_t;
# 158 "/usr/include/x86_64-linux-gnu/bits/types.h"
typedef long __blkcnt_t;
# 172 "/usr/include/x86_64-linux-gnu/bits/types.h"
typedef long __ssize_t;
# 175 "/usr/include/x86_64-linux-gnu/bits/types.h"
typedef long __syscall_slong_t;
# 183 "/usr/include/x86_64-linux-gnu/bits/types.h"
typedef char *__caddr_t;
# 88 "/usr/include/x86_64-linux-gnu/sys/types.h"
typedef __off64_t off_t;
# 109 "/usr/include/x86_64-linux-gnu/sys/types.h"
typedef __ssize_t ssize_t;
# 116 "/usr/include/x86_64-linux-gnu/sys/types.h"
typedef __caddr_t caddr_t;
# 212 "/usr/lib/gcc/x86_64-linux-gnu/4.8/include/stddef.h"
typedef unsigned long size_t;
# 120 "/usr/include/time.h"
struct timespec {
   __time_t tv_sec ;
   __syscall_slong_t tv_nsec ;
};
# 46 "/usr/include/x86_64-linux-gnu/bits/stat.h"
struct stat {
   __dev_t st_dev ;
   __ino_t st_ino ;
   __nlink_t st_nlink ;
   __mode_t st_mode ;
   __uid_t st_uid ;
   __gid_t st_gid ;
   int __pad0 ;
   __dev_t st_rdev ;
   __off_t st_size ;
   __blksize_t st_blksize ;
   __blkcnt_t st_blocks ;
   struct timespec st_atim ;
   struct timespec st_mtim ;
   struct timespec st_ctim ;
   __syscall_slong_t __glibc_reserved[3] ;
};
# 44 "/usr/include/stdio.h"
struct _IO_FILE;
# 44 "/usr/include/stdio.h"
struct _IO_FILE;
# 48 "/usr/include/stdio.h"
typedef struct _IO_FILE FILE;
# 144 "/usr/include/libio.h"
struct _IO_FILE;
# 154 "/usr/include/libio.h"
typedef void _IO_lock_t;
# 160 "/usr/include/libio.h"
struct _IO_marker {
   struct _IO_marker *_next ;
   struct _IO_FILE *_sbuf ;
   int _pos ;
};
# 245 "/usr/include/libio.h"
struct _IO_FILE {
   int _flags ;
   char *_IO_read_ptr ;
   char *_IO_read_end ;
   char *_IO_read_base ;
   char *_IO_write_base ;
   char *_IO_write_ptr ;
   char *_IO_write_end ;
   char *_IO_buf_base ;
   char *_IO_buf_end ;
   char *_IO_save_base ;
   char *_IO_backup_base ;
   char *_IO_save_end ;
   struct _IO_marker *_markers ;
   struct _IO_FILE *_chain ;
   int _fileno ;
   int _flags2 ;
   __off_t _old_offset ;
   unsigned short _cur_column ;
   signed char _vtable_offset ;
   char _shortbuf[1] ;
   _IO_lock_t *_lock ;
   __off64_t _offset ;
   void *__pad1 ;
   void *__pad2 ;
   void *__pad3 ;
   void *__pad4 ;
   size_t __pad5 ;
   int _mode ;
   char _unused2[(15UL * sizeof(int ) - 4UL * sizeof(void *)) - sizeof(size_t )] ;
};
# 23 "system.h"
typedef void *ptr_t;
# 81 "./getopt.h"
struct option {
   char const *name ;
   int has_arg ;
   int *flag ;
   int val ;
};
# 27 "grep.h"
struct matcher {
   char *name ;
   void (*compile)(char * , size_t ) ;
   char *(*execute)(char * , size_t , char ** ) ;
};
# 200 "/usr/local/include/crest.h"
extern void __CrestChar(char *x ) __attribute__((__crest_skip__)) ;
# 220 "/usr/include/x86_64-linux-gnu/sys/stat.h"
extern __attribute__((__nothrow__)) int ( __attribute__((__nonnull__(2), __leaf__)) fstat)(int __fd ,
                                                                                            struct stat *__buf ) __asm__("fstat64") ;
# 61 "/usr/include/x86_64-linux-gnu/sys/mman.h"
extern __attribute__((__nothrow__)) void *( __attribute__((__leaf__)) mmap)(void *__addr ,
                                                                             size_t __len ,
                                                                             int __prot ,
                                                                             int __flags ,
                                                                             int __fd ,
                                                                             __off64_t __offset ) __asm__("mmap64") ;
# 168 "/usr/include/stdio.h"
extern struct _IO_FILE *stdin ;
# 169 "/usr/include/stdio.h"
extern struct _IO_FILE *stdout ;
# 170 "/usr/include/stdio.h"
extern struct _IO_FILE *stderr ;
# 237 "/usr/include/stdio.h"
extern int fclose(FILE *__stream ) ;
# 283 "/usr/include/stdio.h"
extern FILE *fopen(char const * __restrict __filename , char const * __restrict __modes ) __asm__("fopen64") ;
# 356 "/usr/include/stdio.h"
extern int fprintf(FILE * __restrict __stream , char const * __restrict __format
                   , ...) ;
# 362 "/usr/include/stdio.h"
extern int printf(char const * __restrict __format , ...) ;
# 695 "/usr/include/stdio.h"
extern int puts(char const *__s ) ;
# 709 "/usr/include/stdio.h"
extern size_t fread(void * __restrict __ptr , size_t __size , size_t __n , FILE * __restrict __stream ) ;
# 715 "/usr/include/stdio.h"
extern size_t fwrite(void const * __restrict __ptr , size_t __size , size_t __n ,
                     FILE * __restrict __s ) ;
# 828 "/usr/include/stdio.h"
extern __attribute__((__nothrow__)) int ( __attribute__((__leaf__)) feof)(FILE *__stream ) ;
# 830 "/usr/include/stdio.h"
extern __attribute__((__nothrow__)) int ( __attribute__((__leaf__)) ferror)(FILE *__stream ) ;
# 149 "/usr/include/fcntl.h"
extern int ( __attribute__((__nonnull__(1))) open)(char const *__file , int __oflag
                                                   , ...) __asm__("open64") ;
# 337 "/usr/include/unistd.h"
extern __attribute__((__nothrow__)) __off64_t ( __attribute__((__leaf__)) lseek)(int __fd ,
                                                                                  __off64_t __offset ,
                                                                                  int __whence ) __asm__("lseek64") ;
# 353 "/usr/include/unistd.h"
extern int close(int __fd ) ;
# 35 "./getopt.h"
extern char *optarg ;
# 49 "./getopt.h"
extern int optind ;
# 110 "./getopt.h"
extern int getopt_long(int argc , char * const *argv , char const *shortopts ,
                       struct option const *longopts , int *longind ) ;
# 978 "/usr/include/unistd.h"
extern __attribute__((__nothrow__)) int ( __attribute__((__leaf__)) getpagesize)(void) __attribute__((__const__)) ;
# 147 "/usr/include/stdlib.h"
extern __attribute__((__nothrow__)) int ( __attribute__((__nonnull__(1), __leaf__)) atoi)(char const *__nptr ) __attribute__((__pure__)) ;
# 466 "/usr/include/stdlib.h"
extern __attribute__((__nothrow__)) void *( __attribute__((__leaf__)) malloc)(size_t __size ) __attribute__((__malloc__)) ;
# 480 "/usr/include/stdlib.h"
extern __attribute__((__nothrow__)) void *( __attribute__((__warn_unused_result__,
__leaf__)) realloc)(void *__ptr , size_t __size ) ;
# 515 "/usr/include/stdlib.h"
extern __attribute__((__nothrow__, __noreturn__)) void ( __attribute__((__leaf__)) abort)(void) ;
# 543 "/usr/include/stdlib.h"
extern __attribute__((__nothrow__, __noreturn__)) void ( __attribute__((__leaf__)) exit)(int __status ) ;
# 79 "system.h"
extern ptr_t valloc() ;
# 96 "/usr/include/string.h"
extern __attribute__((__nothrow__)) void *( __attribute__((__nonnull__(1), __leaf__)) memchr)(void const *__s ,
                                                                                               int __c ,
                                                                                               size_t __n ) __attribute__((__pure__)) ;
# 129 "/usr/include/string.h"
extern __attribute__((__nothrow__)) char *( __attribute__((__nonnull__(1,2), __leaf__)) strcpy)(char * __restrict __dest ,
                                                                                                 char const * __restrict __src ) ;
# 144 "/usr/include/string.h"
extern __attribute__((__nothrow__)) int ( __attribute__((__nonnull__(1,2), __leaf__)) strcmp)(char const *__s1 ,
                                                                                               char const *__s2 ) __attribute__((__pure__)) ;
# 263 "/usr/include/string.h"
extern __attribute__((__nothrow__)) char *( __attribute__((__nonnull__(1), __leaf__)) strrchr)(char const *__s ,
                                                                                                int __c ) __attribute__((__pure__)) ;
# 399 "/usr/include/string.h"
extern __attribute__((__nothrow__)) size_t ( __attribute__((__nonnull__(1), __leaf__)) strlen)(char const *__s ) __attribute__((__pure__)) ;
# 413 "/usr/include/string.h"
extern __attribute__((__nothrow__)) char *( __attribute__((__leaf__)) strerror)(int __errnum ) ;
# 50 "/usr/include/x86_64-linux-gnu/bits/errno.h"
extern __attribute__((__nothrow__)) int *( __attribute__((__leaf__)) __errno_location)(void) __attribute__((__const__)) ;
# 124 "/usr/include/locale.h"
extern __attribute__((__nothrow__)) char *( __attribute__((__leaf__)) setlocale)(int __category ,
                                                                                  char const *__locale ) ;
# 105 "../intl/libintl.h"
extern char *dcgettext(char const *__domainname , char const *__msgid , int __category ) ;
# 114 "../intl/libintl.h"
extern char *textdomain(char const *__domainname ) ;
# 119 "../intl/libintl.h"
extern char *bindtextdomain(char const *__domainname , char const *__dirname ) ;
# 19 "grep.h"
void fatal(char const *mesg , int errnum ) ;
# 20 "grep.h"
char *xmalloc(size_t size ) ;
# 21 "grep.h"
char *xrealloc(char *ptr , size_t size ) ;
# 27 "grep.h"
extern struct matcher matchers[] ;
# 35 "grep.h"
char *matcher ;
# 39 "grep.h"
int match_icase ;
# 40 "grep.h"
int match_words ;
# 41 "grep.h"
int match_lines ;
# 48 "grep.c"
static int show_help ;
# 51 "grep.c"
static int show_version ;
# 54 "grep.c"
static struct option long_options[26] =
# 54 "grep.c"
  { {"after-context", 1, (int *)((void *)0), 'A'},
        {"basic-regexp", 0, (int *)((void *)0), 'G'},
        {"before-context", 1, (int *)((void *)0), 'B'},
        {"byte-offset", 0, (int *)((void *)0), 'b'},
        {"context", 0, (int *)((void *)0), 'C'},
        {"count", 0, (int *)((void *)0), 'c'},
        {"extended-regexp", 0, (int *)((void *)0), 'E'},
        {"file", 1, (int *)((void *)0), 'f'},
        {"files-with-matches", 0, (int *)((void *)0), 'l'},
        {"files-without-match", 0, (int *)((void *)0), 'L'},
        {"fixed-regexp", 0, (int *)((void *)0), 'F'},
        {"fixed-strings", 0, (int *)((void *)0), 'F'},
        {"help", 0, & show_help, 1},
        {"ignore-case", 0, (int *)((void *)0), 'i'},
        {"line-number", 0, (int *)((void *)0), 'n'},
        {"line-regexp", 0, (int *)((void *)0), 'x'},
        {"no-filename", 0, (int *)((void *)0), 'h'},
        {"no-messages", 0, (int *)((void *)0), 's'},
        {"quiet", 0, (int *)((void *)0), 'q'},
        {"regexp", 1, (int *)((void *)0), 'e'},
        {"revert-match", 0, (int *)((void *)0), 'v'},
        {"silent", 0, (int *)((void *)0), 'q'},
        {"version", 0, (int *)((void *)0), 'V'},
        {"with-filename", 0, (int *)((void *)0), 'H'},
        {"word-regexp", 0, (int *)((void *)0), 'w'},
        {(char const *)0, 0, (int *)0, 0}};
# 95 "grep.c"
static char *prog ;
# 96 "grep.c"
static char *filename ;
# 97 "grep.c"
static int errseen ;
# 99 "grep.c"
static int ck_atoi(char const *str , int *out ) ;
# 100 "grep.c"
static void usage(int status ) ;
# 101 "grep.c"
static void error(char const *mesg , int errnum ) ;
# 102 "grep.c"
static int setmatcher(char *name ) ;
# 103 "grep.c"
static void reset(int fd ) ;
# 104 "grep.c"
static int fillbuf(size_t save ) ;
# 105 "grep.c"
static int grepbuf(char *beg , char *lim ) ;
# 106 "grep.c"
static void prtext(char *beg , char *lim , int *nlinesp ) ;
# 107 "grep.c"
static void prpending(char *lim ) ;
# 108 "grep.c"
static void prline(char *beg , char *lim , int sep ) ;
# 109 "grep.c"
static void nlscan(char *lim ) ;
# 110 "grep.c"
static int grep(int fd ) ;
# 114 "grep.c"
ssize_t fuzzread(int fd , char *buf , size_t count ) ;
# 114 "grep.c"
static int count___0 = 0;
# 112 "grep.c"
ssize_t fuzzread(int fd , char *buf , size_t count )
{
  char tmp1 ;
  ssize_t __retres5 ;

  {
  __CrestCall(46520, 352);
  __CrestStore(46519, (unsigned long )(& count));
  __CrestStore(46518, (unsigned long )(& fd));
  __CrestLoad(46523, (unsigned long )(& count___0), (long long )count___0);
  __CrestLoad(46522, (unsigned long )0, (long long )1);
  __CrestApply2(46521, 0, (long long )(count___0 + 1));
  __CrestStore(46524, (unsigned long )(& count___0));
# 115 "grep.c"
  count___0 ++;
  {
  __CrestLoad(46527, (unsigned long )(& count___0), (long long )count___0);
  __CrestLoad(46526, (unsigned long )0, (long long )40);
  __CrestApply2(46525, 15, (long long )(count___0 <= 40));
# 116 "grep.c"
  if (count___0 <= 40) {
    __CrestBranch(46528, 13891, 1);
# 117 "grep.c"
    __CrestChar(& tmp1);
    __CrestLoad(46530, (unsigned long )(& tmp1), (long long )tmp1);
    __CrestStore(46531, (unsigned long )buf);
# 118 "grep.c"
    *buf = tmp1;
    __CrestLoad(46532, (unsigned long )0, (long long )((ssize_t )1));
    __CrestStore(46533, (unsigned long )(& __retres5));
# 119 "grep.c"
    __retres5 = (ssize_t )1;
# 119 "grep.c"
    goto return_label;
  } else {
    __CrestBranch(46529, 13894, 0);
    __CrestLoad(46534, (unsigned long )0, (long long )((ssize_t )0));
    __CrestStore(46535, (unsigned long )(& __retres5));
# 121 "grep.c"
    __retres5 = (ssize_t )0;
# 121 "grep.c"
    goto return_label;
  }
  }
  return_label:
  {
  __CrestLoad(46536, (unsigned long )(& __retres5), (long long )__retres5);
  __CrestReturn(46537);
# 112 "grep.c"
  return (__retres5);
  }
}
}
# 126 "grep.c"
static void (*compile)(char * , size_t ) ;
# 127 "grep.c"
static char *(*execute)(char * , size_t , char ** ) ;
# 131 "grep.c"
static void error(char const *mesg , int errnum )
{
  char *tmp ;

  {
  __CrestCall(46539, 353);
  __CrestStore(46538, (unsigned long )(& errnum));
  {
  __CrestLoad(46542, (unsigned long )(& errnum), (long long )errnum);
  __CrestLoad(46541, (unsigned long )0, (long long )0);
  __CrestApply2(46540, 13, (long long )(errnum != 0));
# 136 "grep.c"
  if (errnum != 0) {
    __CrestBranch(46543, 13898, 1);
    __CrestLoad(46545, (unsigned long )(& errnum), (long long )errnum);
# 137 "grep.c"
    tmp = strerror(errnum);
    __CrestClearStack(46546);
# 137 "grep.c"
    fprintf((FILE * __restrict )stderr, (char const * __restrict )"%s: %s: %s\n",
            prog, mesg, tmp);
    __CrestClearStack(46547);
  } else {
    __CrestBranch(46544, 13899, 0);
# 139 "grep.c"
    fprintf((FILE * __restrict )stderr, (char const * __restrict )"%s: %s\n",
            prog, mesg);
    __CrestClearStack(46548);
  }
  }
  __CrestLoad(46549, (unsigned long )0, (long long )1);
  __CrestStore(46550, (unsigned long )(& errseen));
# 140 "grep.c"
  errseen = 1;

  {
  __CrestReturn(46551);
# 131 "grep.c"
  return;
  }
}
}
# 144 "grep.c"
void fatal(char const *mesg , int errnum )
{


  {
  __CrestCall(46553, 354);
  __CrestStore(46552, (unsigned long )(& errnum));
  __CrestLoad(46554, (unsigned long )(& errnum), (long long )errnum);
# 149 "grep.c"
  error(mesg, errnum);
  __CrestClearStack(46555);
  __CrestLoad(46556, (unsigned long )0, (long long )2);
# 150 "grep.c"
  exit(2);
  __CrestClearStack(46557);
  {
  __CrestReturn(46558);
# 144 "grep.c"
  return;
  }
}
}
# 154 "grep.c"
char *xmalloc(size_t size )
{
  char *result ;
  void *tmp ;
  char *tmp___0 ;

  {
  __CrestCall(46560, 355);
  __CrestStore(46559, (unsigned long )(& size));
  __CrestLoad(46561, (unsigned long )(& size), (long long )size);
# 160 "grep.c"
  tmp = malloc(size);
  __CrestClearStack(46562);
# 160 "grep.c"
  result = (char *)tmp;
  {
  __CrestLoad(46565, (unsigned long )(& size), (long long )size);
  __CrestLoad(46564, (unsigned long )0, (long long )0);
  __CrestApply2(46563, 13, (long long )(size != 0));
# 161 "grep.c"
  if (size != 0) {
    __CrestBranch(46566, 13907, 1);
    {
    __CrestLoad(46570, (unsigned long )(& result), (long long )((unsigned long )result));
    __CrestLoad(46569, (unsigned long )0, (long long )0);
    __CrestApply2(46568, 12, (long long )(result == 0));
# 161 "grep.c"
    if (result == 0) {
      __CrestBranch(46571, 13908, 1);
      __CrestLoad(46573, (unsigned long )0, (long long )5);
# 162 "grep.c"
      tmp___0 = dcgettext((char const *)((void *)0), "memory exhausted", 5);
      __CrestClearStack(46574);
      __CrestLoad(46575, (unsigned long )0, (long long )0);
# 162 "grep.c"
      fatal((char const *)tmp___0, 0);
      __CrestClearStack(46576);
    } else {
      __CrestBranch(46572, 13909, 0);

    }
    }
  } else {
    __CrestBranch(46567, 13910, 0);

  }
  }
  {
  __CrestReturn(46577);
# 163 "grep.c"
  return (result);
  }
}
}
# 167 "grep.c"
char *xrealloc(char *ptr , size_t size )
{
  char *result ;
  void *tmp ;
  void *tmp___0 ;
  char *tmp___1 ;

  {
  __CrestCall(46579, 356);
  __CrestStore(46578, (unsigned long )(& size));
  {
  __CrestLoad(46582, (unsigned long )(& ptr), (long long )((unsigned long )ptr));
  __CrestLoad(46581, (unsigned long )0, (long long )0);
  __CrestApply2(46580, 13, (long long )(ptr != 0));
# 174 "grep.c"
  if (ptr != 0) {
    __CrestBranch(46583, 13913, 1);
    __CrestLoad(46585, (unsigned long )(& size), (long long )size);
# 175 "grep.c"
    tmp = realloc((void *)ptr, size);
    __CrestClearStack(46586);
# 175 "grep.c"
    result = (char *)tmp;
  } else {
    __CrestBranch(46584, 13914, 0);
    __CrestLoad(46587, (unsigned long )(& size), (long long )size);
# 177 "grep.c"
    tmp___0 = malloc(size);
    __CrestClearStack(46588);
# 177 "grep.c"
    result = (char *)tmp___0;
  }
  }
  {
  __CrestLoad(46591, (unsigned long )(& size), (long long )size);
  __CrestLoad(46590, (unsigned long )0, (long long )0);
  __CrestApply2(46589, 13, (long long )(size != 0));
# 178 "grep.c"
  if (size != 0) {
    __CrestBranch(46592, 13916, 1);
    {
    __CrestLoad(46596, (unsigned long )(& result), (long long )((unsigned long )result));
    __CrestLoad(46595, (unsigned long )0, (long long )0);
    __CrestApply2(46594, 12, (long long )(result == 0));
# 178 "grep.c"
    if (result == 0) {
      __CrestBranch(46597, 13917, 1);
      __CrestLoad(46599, (unsigned long )0, (long long )5);
# 179 "grep.c"
      tmp___1 = dcgettext((char const *)((void *)0), "memory exhausted", 5);
      __CrestClearStack(46600);
      __CrestLoad(46601, (unsigned long )0, (long long )0);
# 179 "grep.c"
      fatal((char const *)tmp___1, 0);
      __CrestClearStack(46602);
    } else {
      __CrestBranch(46598, 13918, 0);

    }
    }
  } else {
    __CrestBranch(46593, 13919, 0);

  }
  }
  {
  __CrestReturn(46603);
# 180 "grep.c"
  return (result);
  }
}
}
# 186 "grep.c"
static int ck_atoi(char const *str , int *out )
{
  char const *p ;
  int __retres4 ;

  {
  __CrestCall(46604, 357);
# 192 "grep.c"
  p = str;
  {
# 192 "grep.c"
  while (1) {
    while_continue: ;
    {
    __CrestLoad(46607, (unsigned long )p, (long long )*p);
    __CrestLoad(46606, (unsigned long )0, (long long )0);
    __CrestApply2(46605, 13, (long long )(*p != 0));
# 192 "grep.c"
    if (*p != 0) {
      __CrestBranch(46608, 13926, 1);

    } else {
      __CrestBranch(46609, 13927, 0);
# 192 "grep.c"
      goto while_break;
    }
    }
    {
    __CrestLoad(46612, (unsigned long )p, (long long )*p);
    __CrestLoad(46611, (unsigned long )0, (long long )48);
    __CrestApply2(46610, 16, (long long )((int const )*p < 48));
# 193 "grep.c"
    if ((int const )*p < 48) {
      __CrestBranch(46613, 13929, 1);
      __CrestLoad(46615, (unsigned long )0, (long long )-1);
      __CrestStore(46616, (unsigned long )(& __retres4));
# 194 "grep.c"
      __retres4 = -1;
# 194 "grep.c"
      goto return_label;
    } else {
      __CrestBranch(46614, 13931, 0);
      {
      __CrestLoad(46619, (unsigned long )p, (long long )*p);
      __CrestLoad(46618, (unsigned long )0, (long long )57);
      __CrestApply2(46617, 14, (long long )((int const )*p > 57));
# 193 "grep.c"
      if ((int const )*p > 57) {
        __CrestBranch(46620, 13932, 1);
        __CrestLoad(46622, (unsigned long )0, (long long )-1);
        __CrestStore(46623, (unsigned long )(& __retres4));
# 194 "grep.c"
        __retres4 = -1;
# 194 "grep.c"
        goto return_label;
      } else {
        __CrestBranch(46621, 13934, 0);

      }
      }
    }
    }
# 192 "grep.c"
    p ++;
  }
  while_break: ;
  }
# 196 "grep.c"
  *out = atoi((char const *)optarg);
  __CrestHandleReturn(46625, (long long )*out);
  __CrestStore(46624, (unsigned long )out);
  __CrestLoad(46626, (unsigned long )0, (long long )0);
  __CrestStore(46627, (unsigned long )(& __retres4));
# 197 "grep.c"
  __retres4 = 0;
  return_label:
  {
  __CrestLoad(46628, (unsigned long )(& __retres4), (long long )__retres4);
  __CrestReturn(46629);
# 186 "grep.c"
  return (__retres4);
  }
}
}
# 205 "grep.c"
static char *buffer ;
# 206 "grep.c"
static size_t bufsalloc ;
# 207 "grep.c"
static size_t bufalloc ;
# 208 "grep.c"
static int bufdesc ;
# 209 "grep.c"
static char *bufbeg ;
# 210 "grep.c"
static char *buflim ;
# 213 "grep.c"
static int bufmapped ;
# 214 "grep.c"
static struct stat bufstat ;
# 215 "grep.c"
static off_t bufoffset ;
# 224 "grep.c"
static int initialized ;
# 220 "grep.c"
static void reset(int fd )
{
  int tmp___0 ;
  int tmp___1 ;
  ptr_t tmp___2 ;
  char *tmp___3 ;
  int tmp___4 ;

  {
  __CrestCall(46631, 358);
  __CrestStore(46630, (unsigned long )(& fd));
  {
  __CrestLoad(46634, (unsigned long )(& initialized), (long long )initialized);
  __CrestLoad(46633, (unsigned long )0, (long long )0);
  __CrestApply2(46632, 12, (long long )(initialized == 0));
# 226 "grep.c"
  if (initialized == 0) {
    __CrestBranch(46635, 13941, 1);
    __CrestLoad(46637, (unsigned long )0, (long long )1);
    __CrestStore(46638, (unsigned long )(& initialized));
# 228 "grep.c"
    initialized = 1;
# 230 "grep.c"
    tmp___1 = getpagesize();
    __CrestHandleReturn(46640, (long long )tmp___1);
    __CrestStore(46639, (unsigned long )(& tmp___1));
    {
    __CrestLoad(46643, (unsigned long )0, (long long )8192);
    __CrestLoad(46642, (unsigned long )(& tmp___1), (long long )tmp___1);
    __CrestApply2(46641, 14, (long long )(8192 > tmp___1));
# 230 "grep.c"
    if (8192 > tmp___1) {
      __CrestBranch(46644, 13943, 1);
      __CrestLoad(46646, (unsigned long )0, (long long )((size_t )8192));
      __CrestStore(46647, (unsigned long )(& bufsalloc));
# 230 "grep.c"
      bufsalloc = (size_t )8192;
    } else {
      __CrestBranch(46645, 13944, 0);
# 230 "grep.c"
      tmp___0 = getpagesize();
      __CrestHandleReturn(46649, (long long )tmp___0);
      __CrestStore(46648, (unsigned long )(& tmp___0));
      __CrestLoad(46650, (unsigned long )(& tmp___0), (long long )tmp___0);
      __CrestStore(46651, (unsigned long )(& bufsalloc));
# 230 "grep.c"
      bufsalloc = (size_t )tmp___0;
    }
    }
    __CrestLoad(46654, (unsigned long )0, (long long )5UL);
    __CrestLoad(46653, (unsigned long )(& bufsalloc), (long long )bufsalloc);
    __CrestApply2(46652, 2, (long long )(5UL * bufsalloc));
    __CrestStore(46655, (unsigned long )(& bufalloc));
# 234 "grep.c"
    bufalloc = 5UL * bufsalloc;
    __CrestLoad(46658, (unsigned long )(& bufalloc), (long long )bufalloc);
    __CrestLoad(46657, (unsigned long )0, (long long )1UL);
    __CrestApply2(46656, 0, (long long )(bufalloc + 1UL));
# 238 "grep.c"
    tmp___2 = valloc(bufalloc + 1UL);
    __CrestClearStack(46659);
# 238 "grep.c"
    buffer = (char *)tmp___2;
    {
    __CrestLoad(46662, (unsigned long )(& buffer), (long long )((unsigned long )buffer));
    __CrestLoad(46661, (unsigned long )0, (long long )0);
    __CrestApply2(46660, 12, (long long )(buffer == 0));
# 239 "grep.c"
    if (buffer == 0) {
      __CrestBranch(46663, 13947, 1);
      __CrestLoad(46665, (unsigned long )0, (long long )5);
# 240 "grep.c"
      tmp___3 = dcgettext((char const *)((void *)0), "memory exhausted", 5);
      __CrestClearStack(46666);
      __CrestLoad(46667, (unsigned long )0, (long long )0);
# 240 "grep.c"
      fatal((char const *)tmp___3, 0);
      __CrestClearStack(46668);
    } else {
      __CrestBranch(46664, 13948, 0);

    }
    }
# 241 "grep.c"
    bufbeg = buffer;
# 242 "grep.c"
    buflim = buffer;
  } else {
    __CrestBranch(46636, 13950, 0);

  }
  }
  __CrestLoad(46669, (unsigned long )(& fd), (long long )fd);
  __CrestStore(46670, (unsigned long )(& bufdesc));
# 244 "grep.c"
  bufdesc = fd;
  __CrestLoad(46671, (unsigned long )(& fd), (long long )fd);
# 246 "grep.c"
  tmp___4 = fstat(fd, & bufstat);
  __CrestHandleReturn(46673, (long long )tmp___4);
  __CrestStore(46672, (unsigned long )(& tmp___4));
  {
  __CrestLoad(46676, (unsigned long )(& tmp___4), (long long )tmp___4);
  __CrestLoad(46675, (unsigned long )0, (long long )0);
  __CrestApply2(46674, 16, (long long )(tmp___4 < 0));
# 246 "grep.c"
  if (tmp___4 < 0) {
    __CrestBranch(46677, 13953, 1);
    __CrestLoad(46679, (unsigned long )0, (long long )0);
    __CrestStore(46680, (unsigned long )(& bufmapped));
# 247 "grep.c"
    bufmapped = 0;
  } else {
    __CrestBranch(46678, 13954, 0);
    {
    __CrestLoad(46685, (unsigned long )(& bufstat.st_mode), (long long )bufstat.st_mode);
    __CrestLoad(46684, (unsigned long )0, (long long )61440U);
    __CrestApply2(46683, 5, (long long )(bufstat.st_mode & 61440U));
    __CrestLoad(46682, (unsigned long )0, (long long )32768U);
    __CrestApply2(46681, 13, (long long )((bufstat.st_mode & 61440U) != 32768U));
# 246 "grep.c"
    if ((bufstat.st_mode & 61440U) != 32768U) {
      __CrestBranch(46686, 13955, 1);
      __CrestLoad(46688, (unsigned long )0, (long long )0);
      __CrestStore(46689, (unsigned long )(& bufmapped));
# 247 "grep.c"
      bufmapped = 0;
    } else {
      __CrestBranch(46687, 13956, 0);
      __CrestLoad(46690, (unsigned long )0, (long long )1);
      __CrestStore(46691, (unsigned long )(& bufmapped));
# 250 "grep.c"
      bufmapped = 1;
      __CrestLoad(46692, (unsigned long )(& fd), (long long )fd);
      __CrestLoad(46693, (unsigned long )0, (long long )((__off64_t )0));
      __CrestLoad(46694, (unsigned long )0, (long long )1);
# 251 "grep.c"
      bufoffset = lseek(fd, (__off64_t )0, 1);
      __CrestHandleReturn(46696, (long long )bufoffset);
      __CrestStore(46695, (unsigned long )(& bufoffset));
    }
    }
  }
  }

  {
  __CrestReturn(46697);
# 220 "grep.c"
  return;
  }
}
}
# 269 "grep.c"
static int pagesize ;
# 260 "grep.c"
static int fillbuf(size_t save )
{
  char *nbuffer ;
  char *dp ;
  char *sp ;
  int cc ;
  caddr_t maddr ;
  ptr_t tmp ;
  char *tmp___0 ;
  char *tmp___1 ;
  char *tmp___2 ;
  size_t tmp___3 ;
  void *tmp___4 ;
  ssize_t tmp___5 ;

  {
  __CrestCall(46699, 359);
  __CrestStore(46698, (unsigned long )(& save));
  {
  __CrestLoad(46702, (unsigned long )(& pagesize), (long long )pagesize);
  __CrestLoad(46701, (unsigned long )0, (long long )0);
  __CrestApply2(46700, 12, (long long )(pagesize == 0));
# 271 "grep.c"
  if (pagesize == 0) {
    __CrestBranch(46703, 13960, 1);
# 271 "grep.c"
    pagesize = getpagesize();
    __CrestHandleReturn(46706, (long long )pagesize);
    __CrestStore(46705, (unsigned long )(& pagesize));
    {
    __CrestLoad(46709, (unsigned long )(& pagesize), (long long )pagesize);
    __CrestLoad(46708, (unsigned long )0, (long long )0);
    __CrestApply2(46707, 12, (long long )(pagesize == 0));
# 271 "grep.c"
    if (pagesize == 0) {
      __CrestBranch(46710, 13962, 1);
# 272 "grep.c"
      abort();
      __CrestClearStack(46712);
    } else {
      __CrestBranch(46711, 13963, 0);

    }
    }
  } else {
    __CrestBranch(46704, 13964, 0);

  }
  }
  {
  __CrestLoad(46715, (unsigned long )(& save), (long long )save);
  __CrestLoad(46714, (unsigned long )(& bufsalloc), (long long )bufsalloc);
  __CrestApply2(46713, 14, (long long )(save > bufsalloc));
# 274 "grep.c"
  if (save > bufsalloc) {
    __CrestBranch(46716, 13966, 1);
    {
# 276 "grep.c"
    while (1) {
      while_continue: ;
      {
      __CrestLoad(46720, (unsigned long )(& save), (long long )save);
      __CrestLoad(46719, (unsigned long )(& bufsalloc), (long long )bufsalloc);
      __CrestApply2(46718, 14, (long long )(save > bufsalloc));
# 276 "grep.c"
      if (save > bufsalloc) {
        __CrestBranch(46721, 13970, 1);

      } else {
        __CrestBranch(46722, 13971, 0);
# 276 "grep.c"
        goto while_break;
      }
      }
      __CrestLoad(46725, (unsigned long )(& bufsalloc), (long long )bufsalloc);
      __CrestLoad(46724, (unsigned long )0, (long long )2UL);
      __CrestApply2(46723, 2, (long long )(bufsalloc * 2UL));
      __CrestStore(46726, (unsigned long )(& bufsalloc));
# 277 "grep.c"
      bufsalloc *= 2UL;
    }
    while_break: ;
    }
    __CrestLoad(46729, (unsigned long )0, (long long )5UL);
    __CrestLoad(46728, (unsigned long )(& bufsalloc), (long long )bufsalloc);
    __CrestApply2(46727, 2, (long long )(5UL * bufsalloc));
    __CrestStore(46730, (unsigned long )(& bufalloc));
# 278 "grep.c"
    bufalloc = 5UL * bufsalloc;
    __CrestLoad(46733, (unsigned long )(& bufalloc), (long long )bufalloc);
    __CrestLoad(46732, (unsigned long )0, (long long )1UL);
    __CrestApply2(46731, 0, (long long )(bufalloc + 1UL));
# 279 "grep.c"
    tmp = valloc(bufalloc + 1UL);
    __CrestClearStack(46734);
# 279 "grep.c"
    nbuffer = (char *)tmp;
    {
    __CrestLoad(46737, (unsigned long )(& nbuffer), (long long )((unsigned long )nbuffer));
    __CrestLoad(46736, (unsigned long )0, (long long )0);
    __CrestApply2(46735, 12, (long long )(nbuffer == 0));
# 280 "grep.c"
    if (nbuffer == 0) {
      __CrestBranch(46738, 13976, 1);
      __CrestLoad(46740, (unsigned long )0, (long long )5);
# 281 "grep.c"
      tmp___0 = dcgettext((char const *)((void *)0), "memory exhausted", 5);
      __CrestClearStack(46741);
      __CrestLoad(46742, (unsigned long )0, (long long )0);
# 281 "grep.c"
      fatal((char const *)tmp___0, 0);
      __CrestClearStack(46743);
    } else {
      __CrestBranch(46739, 13977, 0);

    }
    }
  } else {
    __CrestBranch(46717, 13978, 0);
# 284 "grep.c"
    nbuffer = buffer;
  }
  }
# 286 "grep.c"
  sp = buflim - save;
# 287 "grep.c"
  dp = (nbuffer + bufsalloc) - save;
# 288 "grep.c"
  bufbeg = dp;
  {
# 289 "grep.c"
  while (1) {
    while_continue___0: ;
    __CrestLoad(46744, (unsigned long )(& save), (long long )save);
    __CrestStore(46745, (unsigned long )(& tmp___3));
# 289 "grep.c"
    tmp___3 = save;
    __CrestLoad(46748, (unsigned long )(& save), (long long )save);
    __CrestLoad(46747, (unsigned long )0, (long long )1UL);
    __CrestApply2(46746, 1, (long long )(save - 1UL));
    __CrestStore(46749, (unsigned long )(& save));
# 289 "grep.c"
    save --;
    {
    __CrestLoad(46752, (unsigned long )(& tmp___3), (long long )tmp___3);
    __CrestLoad(46751, (unsigned long )0, (long long )0);
    __CrestApply2(46750, 13, (long long )(tmp___3 != 0));
# 289 "grep.c"
    if (tmp___3 != 0) {
      __CrestBranch(46753, 13985, 1);

    } else {
      __CrestBranch(46754, 13986, 0);
# 289 "grep.c"
      goto while_break___0;
    }
    }
# 290 "grep.c"
    tmp___1 = dp;
# 290 "grep.c"
    dp ++;
# 290 "grep.c"
    tmp___2 = sp;
# 290 "grep.c"
    sp ++;
    __CrestLoad(46755, (unsigned long )tmp___2, (long long )*tmp___2);
    __CrestStore(46756, (unsigned long )tmp___1);
# 290 "grep.c"
    *tmp___1 = *tmp___2;
  }
  while_break___0: ;
  }
# 295 "grep.c"
  buffer = nbuffer;
  {
  __CrestLoad(46759, (unsigned long )(& bufmapped), (long long )bufmapped);
  __CrestLoad(46758, (unsigned long )0, (long long )0);
  __CrestApply2(46757, 13, (long long )(bufmapped != 0));
# 298 "grep.c"
  if (bufmapped != 0) {
    __CrestBranch(46760, 13991, 1);
    {
    __CrestLoad(46766, (unsigned long )(& bufoffset), (long long )bufoffset);
    __CrestLoad(46765, (unsigned long )(& pagesize), (long long )pagesize);
    __CrestApply2(46764, 4, (long long )(bufoffset % (long )pagesize));
    __CrestLoad(46763, (unsigned long )0, (long long )0L);
    __CrestApply2(46762, 12, (long long )(bufoffset % (long )pagesize == 0L));
# 298 "grep.c"
    if (bufoffset % (long )pagesize == 0L) {
      __CrestBranch(46767, 13992, 1);
      {
      __CrestLoad(46775, (unsigned long )(& bufstat.st_size), (long long )bufstat.st_size);
      __CrestLoad(46774, (unsigned long )(& bufoffset), (long long )bufoffset);
      __CrestApply2(46773, 1, (long long )(bufstat.st_size - bufoffset));
      __CrestLoad(46772, (unsigned long )(& bufalloc), (long long )bufalloc);
      __CrestLoad(46771, (unsigned long )(& bufsalloc), (long long )bufsalloc);
      __CrestApply2(46770, 1, (long long )(bufalloc - bufsalloc));
      __CrestApply2(46769, 17, (long long )((size_t )(bufstat.st_size - bufoffset) >= bufalloc - bufsalloc));
# 298 "grep.c"
      if ((size_t )(bufstat.st_size - bufoffset) >= bufalloc - bufsalloc) {
        __CrestBranch(46776, 13993, 1);
# 301 "grep.c"
        maddr = buffer + bufsalloc;
        __CrestLoad(46780, (unsigned long )(& bufalloc), (long long )bufalloc);
        __CrestLoad(46779, (unsigned long )(& bufsalloc), (long long )bufsalloc);
        __CrestApply2(46778, 1, (long long )(bufalloc - bufsalloc));
        __CrestLoad(46781, (unsigned long )0, (long long )3);
        __CrestLoad(46782, (unsigned long )0, (long long )18);
        __CrestLoad(46783, (unsigned long )(& bufdesc), (long long )bufdesc);
        __CrestLoad(46784, (unsigned long )(& bufoffset), (long long )bufoffset);
# 302 "grep.c"
        tmp___4 = mmap((void *)maddr, bufalloc - bufsalloc, 3, 18, bufdesc, bufoffset);
        __CrestClearStack(46785);
# 302 "grep.c"
        maddr = (caddr_t )tmp___4;
        {
        __CrestLoad(46788, (unsigned long )(& maddr), (long long )((unsigned long )maddr));
        __CrestLoad(46787, (unsigned long )0, (long long )((unsigned long )((caddr_t )-1)));
        __CrestApply2(46786, 12, (long long )((unsigned long )maddr == (unsigned long )((caddr_t )-1)));
# 304 "grep.c"
        if ((unsigned long )maddr == (unsigned long )((caddr_t )-1)) {
          __CrestBranch(46789, 13995, 1);
# 314 "grep.c"
          goto tryread;
        } else {
          __CrestBranch(46790, 13996, 0);

        }
        }
        __CrestLoad(46793, (unsigned long )(& bufalloc), (long long )bufalloc);
        __CrestLoad(46792, (unsigned long )(& bufsalloc), (long long )bufsalloc);
        __CrestApply2(46791, 1, (long long )(bufalloc - bufsalloc));
        __CrestStore(46794, (unsigned long )(& cc));
# 322 "grep.c"
        cc = (int )(bufalloc - bufsalloc);
        __CrestLoad(46797, (unsigned long )(& bufoffset), (long long )bufoffset);
        __CrestLoad(46796, (unsigned long )(& cc), (long long )cc);
        __CrestApply2(46795, 0, (long long )(bufoffset + (off_t )cc));
        __CrestStore(46798, (unsigned long )(& bufoffset));
# 323 "grep.c"
        bufoffset += (off_t )cc;
      } else {
        __CrestBranch(46777, 13998, 0);
# 298 "grep.c"
        goto tryread;
      }
      }
    } else {
      __CrestBranch(46768, 13999, 0);
# 298 "grep.c"
      goto tryread;
    }
    }
  } else {
    __CrestBranch(46761, 14000, 0);
    tryread:
    {
    __CrestLoad(46801, (unsigned long )(& bufmapped), (long long )bufmapped);
    __CrestLoad(46800, (unsigned long )0, (long long )0);
    __CrestApply2(46799, 13, (long long )(bufmapped != 0));
# 331 "grep.c"
    if (bufmapped != 0) {
      __CrestBranch(46802, 14001, 1);
      __CrestLoad(46804, (unsigned long )0, (long long )0);
      __CrestStore(46805, (unsigned long )(& bufmapped));
# 333 "grep.c"
      bufmapped = 0;
      __CrestLoad(46806, (unsigned long )(& bufdesc), (long long )bufdesc);
      __CrestLoad(46807, (unsigned long )(& bufoffset), (long long )bufoffset);
      __CrestLoad(46808, (unsigned long )0, (long long )0);
# 334 "grep.c"
      lseek(bufdesc, bufoffset, 0);
      __CrestClearStack(46809);
    } else {
      __CrestBranch(46803, 14002, 0);

    }
    }
    __CrestLoad(46810, (unsigned long )(& bufdesc), (long long )bufdesc);
    __CrestLoad(46813, (unsigned long )(& bufalloc), (long long )bufalloc);
    __CrestLoad(46812, (unsigned long )(& bufsalloc), (long long )bufsalloc);
    __CrestApply2(46811, 1, (long long )(bufalloc - bufsalloc));
# 336 "grep.c"
    tmp___5 = fuzzread(bufdesc, buffer + bufsalloc, bufalloc - bufsalloc);
    __CrestHandleReturn(46815, (long long )tmp___5);
    __CrestStore(46814, (unsigned long )(& tmp___5));
    __CrestLoad(46816, (unsigned long )(& tmp___5), (long long )tmp___5);
    __CrestStore(46817, (unsigned long )(& cc));
# 336 "grep.c"
    cc = (int )tmp___5;
  }
  }
  {
  __CrestLoad(46820, (unsigned long )(& cc), (long long )cc);
  __CrestLoad(46819, (unsigned long )0, (long long )0);
  __CrestApply2(46818, 14, (long long )(cc > 0));
# 345 "grep.c"
  if (cc > 0) {
    __CrestBranch(46821, 14005, 1);
# 346 "grep.c"
    buflim = (buffer + bufsalloc) + cc;
  } else {
    __CrestBranch(46822, 14006, 0);
# 348 "grep.c"
    buflim = buffer + bufsalloc;
  }
  }
  {
  __CrestLoad(46823, (unsigned long )(& cc), (long long )cc);
  __CrestReturn(46824);
# 349 "grep.c"
  return (cc);
  }
}
}
# 353 "grep.c"
static int out_quiet ;
# 354 "grep.c"
static int out_invert ;
# 355 "grep.c"
static int out_file ;
# 356 "grep.c"
static int out_line ;
# 357 "grep.c"
static int out_byte ;
# 358 "grep.c"
static int out_before ;
# 359 "grep.c"
static int out_after ;
# 362 "grep.c"
static size_t totalcc ;
# 363 "grep.c"
static char *lastnl ;
# 364 "grep.c"
static char *lastout ;
# 367 "grep.c"
static size_t totalnl ;
# 368 "grep.c"
static int pending ;
# 369 "grep.c"
static int done_on_match ;
# 375 "grep.c"
static void nlscan(char *lim )
{
  char *beg ;

  {
  __CrestCall(46825, 360);
# 381 "grep.c"
  beg = lastnl;
  {
# 381 "grep.c"
  while (1) {
    while_continue: ;
    {
    __CrestLoad(46828, (unsigned long )(& beg), (long long )((unsigned long )beg));
    __CrestLoad(46827, (unsigned long )(& lim), (long long )((unsigned long )lim));
    __CrestApply2(46826, 16, (long long )((unsigned long )beg < (unsigned long )lim));
# 381 "grep.c"
    if ((unsigned long )beg < (unsigned long )lim) {
      __CrestBranch(46829, 14013, 1);

    } else {
      __CrestBranch(46830, 14014, 0);
# 381 "grep.c"
      goto while_break;
    }
    }
    {
    __CrestLoad(46833, (unsigned long )beg, (long long )*beg);
    __CrestLoad(46832, (unsigned long )0, (long long )10);
    __CrestApply2(46831, 12, (long long )((int )*beg == 10));
# 382 "grep.c"
    if ((int )*beg == 10) {
      __CrestBranch(46834, 14016, 1);
      __CrestLoad(46838, (unsigned long )(& totalnl), (long long )totalnl);
      __CrestLoad(46837, (unsigned long )0, (long long )1UL);
      __CrestApply2(46836, 0, (long long )(totalnl + 1UL));
      __CrestStore(46839, (unsigned long )(& totalnl));
# 383 "grep.c"
      totalnl ++;
    } else {
      __CrestBranch(46835, 14017, 0);

    }
    }
# 381 "grep.c"
    beg ++;
  }
  while_break: ;
  }
# 384 "grep.c"
  lastnl = beg;

  {
  __CrestReturn(46840);
# 375 "grep.c"
  return;
  }
}
}
# 387 "grep.c"
static void prline(char *beg , char *lim , int sep )
{
  int *tmp ;
  char *tmp___0 ;
  int tmp___1 ;

  {
  __CrestCall(46842, 361);
  __CrestStore(46841, (unsigned long )(& sep));
  {
  __CrestLoad(46845, (unsigned long )(& out_file), (long long )out_file);
  __CrestLoad(46844, (unsigned long )0, (long long )0);
  __CrestApply2(46843, 13, (long long )(out_file != 0));
# 393 "grep.c"
  if (out_file != 0) {
    __CrestBranch(46846, 14024, 1);
    __CrestLoad(46848, (unsigned long )(& sep), (long long )sep);
# 394 "grep.c"
    printf((char const * __restrict )"%s%c", filename, sep);
    __CrestClearStack(46849);
  } else {
    __CrestBranch(46847, 14025, 0);

  }
  }
  {
  __CrestLoad(46852, (unsigned long )(& out_line), (long long )out_line);
  __CrestLoad(46851, (unsigned long )0, (long long )0);
  __CrestApply2(46850, 13, (long long )(out_line != 0));
# 395 "grep.c"
  if (out_line != 0) {
    __CrestBranch(46853, 14027, 1);
# 397 "grep.c"
    nlscan(beg);
    __CrestClearStack(46855);
    __CrestLoad(46858, (unsigned long )(& totalnl), (long long )totalnl);
    __CrestLoad(46857, (unsigned long )0, (long long )1UL);
    __CrestApply2(46856, 0, (long long )(totalnl + 1UL));
    __CrestStore(46859, (unsigned long )(& totalnl));
# 398 "grep.c"
    totalnl ++;
    __CrestLoad(46860, (unsigned long )(& totalnl), (long long )totalnl);
    __CrestLoad(46861, (unsigned long )(& sep), (long long )sep);
# 398 "grep.c"
    printf((char const * __restrict )"%u%c", (unsigned int )totalnl, sep);
    __CrestClearStack(46862);
# 399 "grep.c"
    lastnl = lim;
  } else {
    __CrestBranch(46854, 14028, 0);

  }
  }
  {
  __CrestLoad(46865, (unsigned long )(& out_byte), (long long )out_byte);
  __CrestLoad(46864, (unsigned long )0, (long long )0);
  __CrestApply2(46863, 13, (long long )(out_byte != 0));
# 401 "grep.c"
  if (out_byte != 0) {
    __CrestBranch(46866, 14030, 1);
    __CrestLoad(46872, (unsigned long )(& totalcc), (long long )totalcc);
    __CrestLoad(46871, (unsigned long )(& beg), (long long )((unsigned long )beg));
    __CrestLoad(46870, (unsigned long )(& bufbeg), (long long )((unsigned long )bufbeg));
    __CrestApply2(46869, 18, (long long )(beg - bufbeg));
    __CrestApply2(46868, 0, (long long )(totalcc + (size_t )(beg - bufbeg)));
    __CrestLoad(46873, (unsigned long )(& sep), (long long )sep);
# 406 "grep.c"
    printf((char const * __restrict )"%lu%c", totalcc + (size_t )(beg - bufbeg),
           sep);
    __CrestClearStack(46874);
  } else {
    __CrestBranch(46867, 14031, 0);

  }
  }
  __CrestLoad(46875, (unsigned long )0, (long long )((size_t )1));
  __CrestLoad(46878, (unsigned long )(& lim), (long long )((unsigned long )lim));
  __CrestLoad(46877, (unsigned long )(& beg), (long long )((unsigned long )beg));
  __CrestApply2(46876, 18, (long long )(lim - beg));
# 408 "grep.c"
  fwrite((void const * __restrict )beg, (size_t )1, (size_t )(lim - beg), (FILE * __restrict )stdout);
  __CrestClearStack(46879);
# 409 "grep.c"
  tmp___1 = ferror(stdout);
  __CrestHandleReturn(46881, (long long )tmp___1);
  __CrestStore(46880, (unsigned long )(& tmp___1));
  {
  __CrestLoad(46884, (unsigned long )(& tmp___1), (long long )tmp___1);
  __CrestLoad(46883, (unsigned long )0, (long long )0);
  __CrestApply2(46882, 13, (long long )(tmp___1 != 0));
# 409 "grep.c"
  if (tmp___1 != 0) {
    __CrestBranch(46885, 14034, 1);
# 410 "grep.c"
    tmp = __errno_location();
    __CrestClearStack(46887);
    __CrestLoad(46888, (unsigned long )0, (long long )5);
# 410 "grep.c"
    tmp___0 = dcgettext((char const *)((void *)0), "writing output", 5);
    __CrestClearStack(46889);
    __CrestLoad(46890, (unsigned long )tmp, (long long )*tmp);
# 410 "grep.c"
    error((char const *)tmp___0, *tmp);
    __CrestClearStack(46891);
  } else {
    __CrestBranch(46886, 14035, 0);

  }
  }
# 411 "grep.c"
  lastout = lim;

  {
  __CrestReturn(46892);
# 387 "grep.c"
  return;
  }
}
}
# 415 "grep.c"
static void prpending(char *lim )
{
  char *nl ;
  void *tmp ;

  {
  __CrestCall(46893, 362);

  {
  __CrestLoad(46896, (unsigned long )(& lastout), (long long )((unsigned long )lastout));
  __CrestLoad(46895, (unsigned long )0, (long long )0);
  __CrestApply2(46894, 12, (long long )(lastout == 0));
# 421 "grep.c"
  if (lastout == 0) {
    __CrestBranch(46897, 14040, 1);
# 422 "grep.c"
    lastout = bufbeg;
  } else {
    __CrestBranch(46898, 14041, 0);

  }
  }
  {
# 423 "grep.c"
  while (1) {
    while_continue: ;
    {
    __CrestLoad(46901, (unsigned long )(& pending), (long long )pending);
    __CrestLoad(46900, (unsigned long )0, (long long )0);
    __CrestApply2(46899, 14, (long long )(pending > 0));
# 423 "grep.c"
    if (pending > 0) {
      __CrestBranch(46902, 14046, 1);
      {
      __CrestLoad(46906, (unsigned long )(& lastout), (long long )((unsigned long )lastout));
      __CrestLoad(46905, (unsigned long )(& lim), (long long )((unsigned long )lim));
      __CrestApply2(46904, 16, (long long )((unsigned long )lastout < (unsigned long )lim));
# 423 "grep.c"
      if ((unsigned long )lastout < (unsigned long )lim) {
        __CrestBranch(46907, 14047, 1);

      } else {
        __CrestBranch(46908, 14048, 0);
# 423 "grep.c"
        goto while_break;
      }
      }
    } else {
      __CrestBranch(46903, 14049, 0);
# 423 "grep.c"
      goto while_break;
    }
    }
    __CrestLoad(46911, (unsigned long )(& pending), (long long )pending);
    __CrestLoad(46910, (unsigned long )0, (long long )1);
    __CrestApply2(46909, 1, (long long )(pending - 1));
    __CrestStore(46912, (unsigned long )(& pending));
# 425 "grep.c"
    pending --;
    __CrestLoad(46913, (unsigned long )0, (long long )'\n');
    __CrestLoad(46916, (unsigned long )(& lim), (long long )((unsigned long )lim));
    __CrestLoad(46915, (unsigned long )(& lastout), (long long )((unsigned long )lastout));
    __CrestApply2(46914, 18, (long long )(lim - lastout));
# 426 "grep.c"
    tmp = memchr((void const *)lastout, '\n', (size_t )(lim - lastout));
    __CrestClearStack(46917);
# 426 "grep.c"
    nl = (char *)tmp;
    {
    __CrestLoad(46920, (unsigned long )(& nl), (long long )((unsigned long )nl));
    __CrestLoad(46919, (unsigned long )0, (long long )((unsigned long )((char *)0)));
    __CrestApply2(46918, 13, (long long )((unsigned long )nl != (unsigned long )((char *)0)));
# 426 "grep.c"
    if ((unsigned long )nl != (unsigned long )((char *)0)) {
      __CrestBranch(46921, 14052, 1);
# 427 "grep.c"
      nl ++;
    } else {
      __CrestBranch(46922, 14053, 0);
# 429 "grep.c"
      nl = lim;
    }
    }
    __CrestLoad(46923, (unsigned long )0, (long long )'-');
# 430 "grep.c"
    prline(lastout, nl, '-');
    __CrestClearStack(46924);
  }
  while_break: ;
  }

  {
  __CrestReturn(46925);
# 415 "grep.c"
  return;
  }
}
}
# 442 "grep.c"
static int used ;
# 436 "grep.c"
static void prtext(char *beg , char *lim , int *nlinesp )
{
  char *bp ;
  char *p ;
  char *nl ;
  int i ;
  int n ;
  void *tmp ;
  void *tmp___0 ;
  char *mem_11 ;

  {
  __CrestCall(46926, 363);

  {
  __CrestLoad(46929, (unsigned long )(& out_quiet), (long long )out_quiet);
  __CrestLoad(46928, (unsigned long )0, (long long )0);
  __CrestApply2(46927, 12, (long long )(out_quiet == 0));
# 446 "grep.c"
  if (out_quiet == 0) {
    __CrestBranch(46930, 14059, 1);
    {
    __CrestLoad(46934, (unsigned long )(& pending), (long long )pending);
    __CrestLoad(46933, (unsigned long )0, (long long )0);
    __CrestApply2(46932, 14, (long long )(pending > 0));
# 446 "grep.c"
    if (pending > 0) {
      __CrestBranch(46935, 14060, 1);
# 447 "grep.c"
      prpending(beg);
      __CrestClearStack(46937);
    } else {
      __CrestBranch(46936, 14061, 0);

    }
    }
  } else {
    __CrestBranch(46931, 14062, 0);

  }
  }
# 449 "grep.c"
  p = beg;
  {
  __CrestLoad(46940, (unsigned long )(& out_quiet), (long long )out_quiet);
  __CrestLoad(46939, (unsigned long )0, (long long )0);
  __CrestApply2(46938, 12, (long long )(out_quiet == 0));
# 451 "grep.c"
  if (out_quiet == 0) {
    __CrestBranch(46941, 14065, 1);
    {
    __CrestLoad(46945, (unsigned long )(& lastout), (long long )((unsigned long )lastout));
    __CrestLoad(46944, (unsigned long )0, (long long )0);
    __CrestApply2(46943, 13, (long long )(lastout != 0));
# 455 "grep.c"
    if (lastout != 0) {
      __CrestBranch(46946, 14066, 1);
# 455 "grep.c"
      bp = lastout;
    } else {
      __CrestBranch(46947, 14067, 0);
# 455 "grep.c"
      bp = bufbeg;
    }
    }
    __CrestLoad(46948, (unsigned long )0, (long long )0);
    __CrestStore(46949, (unsigned long )(& i));
# 456 "grep.c"
    i = 0;
    {
# 456 "grep.c"
    while (1) {
      while_continue: ;
      {
      __CrestLoad(46952, (unsigned long )(& i), (long long )i);
      __CrestLoad(46951, (unsigned long )(& out_before), (long long )out_before);
      __CrestApply2(46950, 16, (long long )(i < out_before));
# 456 "grep.c"
      if (i < out_before) {
        __CrestBranch(46953, 14073, 1);

      } else {
        __CrestBranch(46954, 14074, 0);
# 456 "grep.c"
        goto while_break;
      }
      }
      {
      __CrestLoad(46957, (unsigned long )(& p), (long long )((unsigned long )p));
      __CrestLoad(46956, (unsigned long )(& bp), (long long )((unsigned long )bp));
      __CrestApply2(46955, 14, (long long )((unsigned long )p > (unsigned long )bp));
# 457 "grep.c"
      if ((unsigned long )p > (unsigned long )bp) {
        __CrestBranch(46958, 14076, 1);
        {
# 458 "grep.c"
        while (1) {
          while_continue___0: ;
# 459 "grep.c"
          p --;
          {
          __CrestLoad(46962, (unsigned long )(& p), (long long )((unsigned long )p));
          __CrestLoad(46961, (unsigned long )(& bp), (long long )((unsigned long )bp));
          __CrestApply2(46960, 14, (long long )((unsigned long )p > (unsigned long )bp));
# 458 "grep.c"
          if ((unsigned long )p > (unsigned long )bp) {
            __CrestBranch(46963, 14081, 1);
            {
# 458 "grep.c"
            mem_11 = p + -1;
            {
            __CrestLoad(46967, (unsigned long )mem_11, (long long )*mem_11);
            __CrestLoad(46966, (unsigned long )0, (long long )10);
            __CrestApply2(46965, 13, (long long )((int )*mem_11 != 10));
# 458 "grep.c"
            if ((int )*mem_11 != 10) {
              __CrestBranch(46968, 14084, 1);

            } else {
              __CrestBranch(46969, 14085, 0);
# 458 "grep.c"
              goto while_break___0;
            }
            }
            }
          } else {
            __CrestBranch(46964, 14086, 0);
# 458 "grep.c"
            goto while_break___0;
          }
          }
        }
        while_break___0: ;
        }
      } else {
        __CrestBranch(46959, 14088, 0);

      }
      }
      __CrestLoad(46972, (unsigned long )(& i), (long long )i);
      __CrestLoad(46971, (unsigned long )0, (long long )1);
      __CrestApply2(46970, 0, (long long )(i + 1));
      __CrestStore(46973, (unsigned long )(& i));
# 456 "grep.c"
      i ++;
    }
    while_break: ;
    }
    {
    __CrestLoad(46976, (unsigned long )(& out_before), (long long )out_before);
    __CrestLoad(46975, (unsigned long )0, (long long )0);
    __CrestApply2(46974, 13, (long long )(out_before != 0));
# 464 "grep.c"
    if (out_before != 0) {
      __CrestBranch(46977, 14092, 1);
# 464 "grep.c"
      goto _L;
    } else {
      __CrestBranch(46978, 14093, 0);
      {
      __CrestLoad(46981, (unsigned long )(& out_after), (long long )out_after);
      __CrestLoad(46980, (unsigned long )0, (long long )0);
      __CrestApply2(46979, 13, (long long )(out_after != 0));
# 464 "grep.c"
      if (out_after != 0) {
        __CrestBranch(46982, 14094, 1);
        _L:
        {
        __CrestLoad(46986, (unsigned long )(& used), (long long )used);
        __CrestLoad(46985, (unsigned long )0, (long long )0);
        __CrestApply2(46984, 13, (long long )(used != 0));
# 464 "grep.c"
        if (used != 0) {
          __CrestBranch(46987, 14095, 1);
          {
          __CrestLoad(46991, (unsigned long )(& p), (long long )((unsigned long )p));
          __CrestLoad(46990, (unsigned long )(& lastout), (long long )((unsigned long )lastout));
          __CrestApply2(46989, 13, (long long )((unsigned long )p != (unsigned long )lastout));
# 464 "grep.c"
          if ((unsigned long )p != (unsigned long )lastout) {
            __CrestBranch(46992, 14096, 1);
# 465 "grep.c"
            puts("--");
            __CrestClearStack(46994);
          } else {
            __CrestBranch(46993, 14097, 0);

          }
          }
        } else {
          __CrestBranch(46988, 14098, 0);

        }
        }
      } else {
        __CrestBranch(46983, 14099, 0);

      }
      }
    }
    }
    {
# 467 "grep.c"
    while (1) {
      while_continue___1: ;
      {
      __CrestLoad(46997, (unsigned long )(& p), (long long )((unsigned long )p));
      __CrestLoad(46996, (unsigned long )(& beg), (long long )((unsigned long )beg));
      __CrestApply2(46995, 16, (long long )((unsigned long )p < (unsigned long )beg));
# 467 "grep.c"
      if ((unsigned long )p < (unsigned long )beg) {
        __CrestBranch(46998, 14104, 1);

      } else {
        __CrestBranch(46999, 14105, 0);
# 467 "grep.c"
        goto while_break___1;
      }
      }
      __CrestLoad(47000, (unsigned long )0, (long long )'\n');
      __CrestLoad(47003, (unsigned long )(& beg), (long long )((unsigned long )beg));
      __CrestLoad(47002, (unsigned long )(& p), (long long )((unsigned long )p));
      __CrestApply2(47001, 18, (long long )(beg - p));
# 469 "grep.c"
      tmp = memchr((void const *)p, '\n', (size_t )(beg - p));
      __CrestClearStack(47004);
# 469 "grep.c"
      nl = (char *)tmp;
      __CrestLoad(47005, (unsigned long )0, (long long )'-');
# 470 "grep.c"
      prline(p, nl + 1, '-');
      __CrestClearStack(47006);
# 471 "grep.c"
      p = nl + 1;
    }
    while_break___1: ;
    }
  } else {
    __CrestBranch(46942, 14108, 0);

  }
  }
  {
  __CrestLoad(47009, (unsigned long )(& nlinesp), (long long )((unsigned long )nlinesp));
  __CrestLoad(47008, (unsigned long )0, (long long )0);
  __CrestApply2(47007, 13, (long long )(nlinesp != 0));
# 475 "grep.c"
  if (nlinesp != 0) {
    __CrestBranch(47010, 14110, 1);
    __CrestLoad(47012, (unsigned long )0, (long long )0);
    __CrestStore(47013, (unsigned long )(& n));
# 478 "grep.c"
    n = 0;
    {
# 478 "grep.c"
    while (1) {
      while_continue___2: ;
      {
      __CrestLoad(47016, (unsigned long )(& p), (long long )((unsigned long )p));
      __CrestLoad(47015, (unsigned long )(& lim), (long long )((unsigned long )lim));
      __CrestApply2(47014, 16, (long long )((unsigned long )p < (unsigned long )lim));
# 478 "grep.c"
      if ((unsigned long )p < (unsigned long )lim) {
        __CrestBranch(47017, 14115, 1);

      } else {
        __CrestBranch(47018, 14116, 0);
# 478 "grep.c"
        goto while_break___2;
      }
      }
      __CrestLoad(47019, (unsigned long )0, (long long )'\n');
      __CrestLoad(47022, (unsigned long )(& lim), (long long )((unsigned long )lim));
      __CrestLoad(47021, (unsigned long )(& p), (long long )((unsigned long )p));
      __CrestApply2(47020, 18, (long long )(lim - p));
# 480 "grep.c"
      tmp___0 = memchr((void const *)p, '\n', (size_t )(lim - p));
      __CrestClearStack(47023);
# 480 "grep.c"
      nl = (char *)tmp___0;
      {
      __CrestLoad(47026, (unsigned long )(& nl), (long long )((unsigned long )nl));
      __CrestLoad(47025, (unsigned long )0, (long long )((unsigned long )((char *)0)));
      __CrestApply2(47024, 13, (long long )((unsigned long )nl != (unsigned long )((char *)0)));
# 480 "grep.c"
      if ((unsigned long )nl != (unsigned long )((char *)0)) {
        __CrestBranch(47027, 14119, 1);
# 481 "grep.c"
        nl ++;
      } else {
        __CrestBranch(47028, 14120, 0);
# 483 "grep.c"
        nl = lim;
      }
      }
      {
      __CrestLoad(47031, (unsigned long )(& out_quiet), (long long )out_quiet);
      __CrestLoad(47030, (unsigned long )0, (long long )0);
      __CrestApply2(47029, 12, (long long )(out_quiet == 0));
# 484 "grep.c"
      if (out_quiet == 0) {
        __CrestBranch(47032, 14122, 1);
        __CrestLoad(47034, (unsigned long )0, (long long )':');
# 485 "grep.c"
        prline(p, nl, ':');
        __CrestClearStack(47035);
      } else {
        __CrestBranch(47033, 14123, 0);

      }
      }
# 486 "grep.c"
      p = nl;
      __CrestLoad(47038, (unsigned long )(& n), (long long )n);
      __CrestLoad(47037, (unsigned long )0, (long long )1);
      __CrestApply2(47036, 0, (long long )(n + 1));
      __CrestStore(47039, (unsigned long )(& n));
# 478 "grep.c"
      n ++;
    }
    while_break___2: ;
    }
    __CrestLoad(47040, (unsigned long )(& n), (long long )n);
    __CrestStore(47041, (unsigned long )nlinesp);
# 488 "grep.c"
    *nlinesp = n;
  } else {
    __CrestBranch(47011, 14127, 0);
    {
    __CrestLoad(47044, (unsigned long )(& out_quiet), (long long )out_quiet);
    __CrestLoad(47043, (unsigned long )0, (long long )0);
    __CrestApply2(47042, 12, (long long )(out_quiet == 0));
# 491 "grep.c"
    if (out_quiet == 0) {
      __CrestBranch(47045, 14128, 1);
      __CrestLoad(47047, (unsigned long )0, (long long )':');
# 492 "grep.c"
      prline(beg, lim, ':');
      __CrestClearStack(47048);
    } else {
      __CrestBranch(47046, 14129, 0);

    }
    }
  }
  }
  __CrestLoad(47049, (unsigned long )(& out_after), (long long )out_after);
  __CrestStore(47050, (unsigned long )(& pending));
# 494 "grep.c"
  pending = out_after;
  __CrestLoad(47051, (unsigned long )0, (long long )1);
  __CrestStore(47052, (unsigned long )(& used));
# 495 "grep.c"
  used = 1;

  {
  __CrestReturn(47053);
# 436 "grep.c"
  return;
  }
}
}
# 501 "grep.c"
static int grepbuf(char *beg , char *lim )
{
  int nlines ;
  int n ;
  char *p ;
  char *b ;
  char *endp ;
  char *mem_8 ;
  int __retres9 ;

  {
  __CrestCall(47054, 364);

  __CrestLoad(47055, (unsigned long )0, (long long )0);
  __CrestStore(47056, (unsigned long )(& nlines));
# 510 "grep.c"
  nlines = 0;
# 511 "grep.c"
  p = beg;
  {
# 512 "grep.c"
  while (1) {
    while_continue: ;
    __CrestLoad(47059, (unsigned long )(& lim), (long long )((unsigned long )lim));
    __CrestLoad(47058, (unsigned long )(& p), (long long )((unsigned long )p));
    __CrestApply2(47057, 18, (long long )(lim - p));
# 512 "grep.c"
    b = (*execute)(p, (size_t )(lim - p), & endp);
    __CrestClearStack(47060);
    {
    __CrestLoad(47063, (unsigned long )(& b), (long long )((unsigned long )b));
    __CrestLoad(47062, (unsigned long )0, (long long )((unsigned long )((char *)0)));
    __CrestApply2(47061, 13, (long long )((unsigned long )b != (unsigned long )((char *)0)));
# 512 "grep.c"
    if ((unsigned long )b != (unsigned long )((char *)0)) {
      __CrestBranch(47064, 14139, 1);

    } else {
      __CrestBranch(47065, 14140, 0);
# 512 "grep.c"
      goto while_break;
    }
    }
    {
    __CrestLoad(47068, (unsigned long )(& b), (long long )((unsigned long )b));
    __CrestLoad(47067, (unsigned long )(& lim), (long long )((unsigned long )lim));
    __CrestApply2(47066, 12, (long long )((unsigned long )b == (unsigned long )lim));
# 515 "grep.c"
    if ((unsigned long )b == (unsigned long )lim) {
      __CrestBranch(47069, 14142, 1);
      {
      __CrestLoad(47073, (unsigned long )(& b), (long long )((unsigned long )b));
      __CrestLoad(47072, (unsigned long )(& beg), (long long )((unsigned long )beg));
      __CrestApply2(47071, 14, (long long )((unsigned long )b > (unsigned long )beg));
# 515 "grep.c"
      if ((unsigned long )b > (unsigned long )beg) {
        __CrestBranch(47074, 14143, 1);
        {
# 515 "grep.c"
        mem_8 = b + -1;
        {
        __CrestLoad(47078, (unsigned long )mem_8, (long long )*mem_8);
        __CrestLoad(47077, (unsigned long )0, (long long )10);
        __CrestApply2(47076, 12, (long long )((int )*mem_8 == 10));
# 515 "grep.c"
        if ((int )*mem_8 == 10) {
          __CrestBranch(47079, 14146, 1);
# 516 "grep.c"
          goto while_break;
        } else {
          __CrestBranch(47080, 14147, 0);
# 515 "grep.c"
          goto _L;
        }
        }
        }
      } else {
        __CrestBranch(47075, 14148, 0);
        _L:
        {
        __CrestLoad(47083, (unsigned long )(& b), (long long )((unsigned long )b));
        __CrestLoad(47082, (unsigned long )(& beg), (long long )((unsigned long )beg));
        __CrestApply2(47081, 12, (long long )((unsigned long )b == (unsigned long )beg));
# 515 "grep.c"
        if ((unsigned long )b == (unsigned long )beg) {
          __CrestBranch(47084, 14149, 1);
# 516 "grep.c"
          goto while_break;
        } else {
          __CrestBranch(47085, 14150, 0);

        }
        }
      }
      }
    } else {
      __CrestBranch(47070, 14151, 0);

    }
    }
    {
    __CrestLoad(47088, (unsigned long )(& out_invert), (long long )out_invert);
    __CrestLoad(47087, (unsigned long )0, (long long )0);
    __CrestApply2(47086, 12, (long long )(out_invert == 0));
# 517 "grep.c"
    if (out_invert == 0) {
      __CrestBranch(47089, 14153, 1);
# 519 "grep.c"
      prtext(b, endp, (int *)0);
      __CrestClearStack(47091);
      __CrestLoad(47094, (unsigned long )(& nlines), (long long )nlines);
      __CrestLoad(47093, (unsigned long )0, (long long )1);
      __CrestApply2(47092, 0, (long long )(nlines + 1));
      __CrestStore(47095, (unsigned long )(& nlines));
# 520 "grep.c"
      nlines ++;
      {
      __CrestLoad(47098, (unsigned long )(& done_on_match), (long long )done_on_match);
      __CrestLoad(47097, (unsigned long )0, (long long )0);
      __CrestApply2(47096, 13, (long long )(done_on_match != 0));
# 521 "grep.c"
      if (done_on_match != 0) {
        __CrestBranch(47099, 14155, 1);
        __CrestLoad(47101, (unsigned long )(& nlines), (long long )nlines);
        __CrestStore(47102, (unsigned long )(& __retres9));
# 522 "grep.c"
        __retres9 = nlines;
# 522 "grep.c"
        goto return_label;
      } else {
        __CrestBranch(47100, 14157, 0);

      }
      }
    } else {
      __CrestBranch(47090, 14158, 0);
      {
      __CrestLoad(47105, (unsigned long )(& p), (long long )((unsigned long )p));
      __CrestLoad(47104, (unsigned long )(& b), (long long )((unsigned long )b));
      __CrestApply2(47103, 16, (long long )((unsigned long )p < (unsigned long )b));
# 524 "grep.c"
      if ((unsigned long )p < (unsigned long )b) {
        __CrestBranch(47106, 14159, 1);
# 526 "grep.c"
        prtext(p, b, & n);
        __CrestClearStack(47108);
        __CrestLoad(47111, (unsigned long )(& nlines), (long long )nlines);
        __CrestLoad(47110, (unsigned long )(& n), (long long )n);
        __CrestApply2(47109, 0, (long long )(nlines + n));
        __CrestStore(47112, (unsigned long )(& nlines));
# 527 "grep.c"
        nlines += n;
      } else {
        __CrestBranch(47107, 14160, 0);

      }
      }
    }
    }
# 529 "grep.c"
    p = endp;
  }
  while_break: ;
  }
  {
  __CrestLoad(47115, (unsigned long )(& out_invert), (long long )out_invert);
  __CrestLoad(47114, (unsigned long )0, (long long )0);
  __CrestApply2(47113, 13, (long long )(out_invert != 0));
# 531 "grep.c"
  if (out_invert != 0) {
    __CrestBranch(47116, 14164, 1);
    {
    __CrestLoad(47120, (unsigned long )(& p), (long long )((unsigned long )p));
    __CrestLoad(47119, (unsigned long )(& lim), (long long )((unsigned long )lim));
    __CrestApply2(47118, 16, (long long )((unsigned long )p < (unsigned long )lim));
# 531 "grep.c"
    if ((unsigned long )p < (unsigned long )lim) {
      __CrestBranch(47121, 14165, 1);
# 533 "grep.c"
      prtext(p, lim, & n);
      __CrestClearStack(47123);
      __CrestLoad(47126, (unsigned long )(& nlines), (long long )nlines);
      __CrestLoad(47125, (unsigned long )(& n), (long long )n);
      __CrestApply2(47124, 0, (long long )(nlines + n));
      __CrestStore(47127, (unsigned long )(& nlines));
# 534 "grep.c"
      nlines += n;
    } else {
      __CrestBranch(47122, 14166, 0);

    }
    }
  } else {
    __CrestBranch(47117, 14167, 0);

  }
  }
  __CrestLoad(47128, (unsigned long )(& nlines), (long long )nlines);
  __CrestStore(47129, (unsigned long )(& __retres9));
# 536 "grep.c"
  __retres9 = nlines;
  return_label:
  {
  __CrestLoad(47130, (unsigned long )(& __retres9), (long long )__retres9);
  __CrestReturn(47131);
# 501 "grep.c"
  return (__retres9);
  }
}
}
# 540 "grep.c"
static int grep(int fd )
{
  int nlines ;
  int i ;
  size_t residue ;
  size_t save ;
  char *beg ;
  char *lim ;
  int *tmp ;
  int tmp___0 ;
  int tmp___1 ;
  int tmp___2 ;
  char *mem_12 ;
  char *mem_13 ;
  int __retres14 ;

  {
  __CrestCall(47133, 365);
  __CrestStore(47132, (unsigned long )(& fd));
  __CrestLoad(47134, (unsigned long )(& fd), (long long )fd);
# 548 "grep.c"
  reset(fd);
  __CrestClearStack(47135);
  __CrestLoad(47136, (unsigned long )0, (long long )((size_t )0));
  __CrestStore(47137, (unsigned long )(& totalcc));
# 550 "grep.c"
  totalcc = (size_t )0;
# 551 "grep.c"
  lastout = (char *)0;
  __CrestLoad(47138, (unsigned long )0, (long long )((size_t )0));
  __CrestStore(47139, (unsigned long )(& totalnl));
# 552 "grep.c"
  totalnl = (size_t )0;
  __CrestLoad(47140, (unsigned long )0, (long long )0);
  __CrestStore(47141, (unsigned long )(& pending));
# 553 "grep.c"
  pending = 0;
  __CrestLoad(47142, (unsigned long )0, (long long )0);
  __CrestStore(47143, (unsigned long )(& nlines));
# 555 "grep.c"
  nlines = 0;
  __CrestLoad(47144, (unsigned long )0, (long long )((size_t )0));
  __CrestStore(47145, (unsigned long )(& residue));
# 556 "grep.c"
  residue = (size_t )0;
  __CrestLoad(47146, (unsigned long )0, (long long )((size_t )0));
  __CrestStore(47147, (unsigned long )(& save));
# 557 "grep.c"
  save = (size_t )0;
  {
# 559 "grep.c"
  while (1) {
    while_continue: ;
    __CrestLoad(47148, (unsigned long )(& save), (long long )save);
# 561 "grep.c"
    tmp___0 = fillbuf(save);
    __CrestHandleReturn(47150, (long long )tmp___0);
    __CrestStore(47149, (unsigned long )(& tmp___0));
    {
    __CrestLoad(47153, (unsigned long )(& tmp___0), (long long )tmp___0);
    __CrestLoad(47152, (unsigned long )0, (long long )0);
    __CrestApply2(47151, 16, (long long )(tmp___0 < 0));
# 561 "grep.c"
    if (tmp___0 < 0) {
      __CrestBranch(47154, 14176, 1);
# 563 "grep.c"
      tmp = __errno_location();
      __CrestClearStack(47156);
      __CrestLoad(47157, (unsigned long )tmp, (long long )*tmp);
# 563 "grep.c"
      error((char const *)filename, *tmp);
      __CrestClearStack(47158);
      __CrestLoad(47159, (unsigned long )(& nlines), (long long )nlines);
      __CrestStore(47160, (unsigned long )(& __retres14));
# 564 "grep.c"
      __retres14 = nlines;
# 564 "grep.c"
      goto return_label;
    } else {
      __CrestBranch(47155, 14179, 0);

    }
    }
# 566 "grep.c"
    lastnl = bufbeg;
    {
    __CrestLoad(47163, (unsigned long )(& lastout), (long long )((unsigned long )lastout));
    __CrestLoad(47162, (unsigned long )0, (long long )0);
    __CrestApply2(47161, 13, (long long )(lastout != 0));
# 567 "grep.c"
    if (lastout != 0) {
      __CrestBranch(47164, 14182, 1);
# 568 "grep.c"
      lastout = bufbeg;
    } else {
      __CrestBranch(47165, 14183, 0);

    }
    }
    {
    __CrestLoad(47170, (unsigned long )(& buflim), (long long )((unsigned long )buflim));
    __CrestLoad(47169, (unsigned long )(& bufbeg), (long long )((unsigned long )bufbeg));
    __CrestApply2(47168, 18, (long long )(buflim - bufbeg));
    __CrestLoad(47167, (unsigned long )(& save), (long long )save);
    __CrestApply2(47166, 12, (long long )((size_t )(buflim - bufbeg) == save));
# 569 "grep.c"
    if ((size_t )(buflim - bufbeg) == save) {
      __CrestBranch(47171, 14185, 1);
# 570 "grep.c"
      goto while_break;
    } else {
      __CrestBranch(47172, 14186, 0);

    }
    }
# 571 "grep.c"
    beg = (bufbeg + save) - residue;
# 572 "grep.c"
    lim = buflim;
    {
# 572 "grep.c"
    while (1) {
      while_continue___0: ;
      {
      __CrestLoad(47175, (unsigned long )(& lim), (long long )((unsigned long )lim));
      __CrestLoad(47174, (unsigned long )(& beg), (long long )((unsigned long )beg));
      __CrestApply2(47173, 14, (long long )((unsigned long )lim > (unsigned long )beg));
# 572 "grep.c"
      if ((unsigned long )lim > (unsigned long )beg) {
        __CrestBranch(47176, 14192, 1);
        {
# 572 "grep.c"
        mem_12 = lim + -1;
        {
        __CrestLoad(47180, (unsigned long )mem_12, (long long )*mem_12);
        __CrestLoad(47179, (unsigned long )0, (long long )10);
        __CrestApply2(47178, 13, (long long )((int )*mem_12 != 10));
# 572 "grep.c"
        if ((int )*mem_12 != 10) {
          __CrestBranch(47181, 14195, 1);

        } else {
          __CrestBranch(47182, 14196, 0);
# 572 "grep.c"
          goto while_break___0;
        }
        }
        }
      } else {
        __CrestBranch(47177, 14197, 0);
# 572 "grep.c"
        goto while_break___0;
      }
      }
# 572 "grep.c"
      lim --;
    }
    while_break___0: ;
    }
    __CrestLoad(47185, (unsigned long )(& buflim), (long long )((unsigned long )buflim));
    __CrestLoad(47184, (unsigned long )(& lim), (long long )((unsigned long )lim));
    __CrestApply2(47183, 18, (long long )(buflim - lim));
    __CrestStore(47186, (unsigned long )(& residue));
# 574 "grep.c"
    residue = (size_t )(buflim - lim);
    {
    __CrestLoad(47189, (unsigned long )(& beg), (long long )((unsigned long )beg));
    __CrestLoad(47188, (unsigned long )(& lim), (long long )((unsigned long )lim));
    __CrestApply2(47187, 16, (long long )((unsigned long )beg < (unsigned long )lim));
# 575 "grep.c"
    if ((unsigned long )beg < (unsigned long )lim) {
      __CrestBranch(47190, 14202, 1);
# 577 "grep.c"
      tmp___1 = grepbuf(beg, lim);
      __CrestHandleReturn(47193, (long long )tmp___1);
      __CrestStore(47192, (unsigned long )(& tmp___1));
      __CrestLoad(47196, (unsigned long )(& nlines), (long long )nlines);
      __CrestLoad(47195, (unsigned long )(& tmp___1), (long long )tmp___1);
      __CrestApply2(47194, 0, (long long )(nlines + tmp___1));
      __CrestStore(47197, (unsigned long )(& nlines));
# 577 "grep.c"
      nlines += tmp___1;
      {
      __CrestLoad(47200, (unsigned long )(& pending), (long long )pending);
      __CrestLoad(47199, (unsigned long )0, (long long )0);
      __CrestApply2(47198, 13, (long long )(pending != 0));
# 578 "grep.c"
      if (pending != 0) {
        __CrestBranch(47201, 14204, 1);
# 579 "grep.c"
        prpending(lim);
        __CrestClearStack(47203);
      } else {
        __CrestBranch(47202, 14205, 0);

      }
      }
      {
      __CrestLoad(47206, (unsigned long )(& nlines), (long long )nlines);
      __CrestLoad(47205, (unsigned long )0, (long long )0);
      __CrestApply2(47204, 13, (long long )(nlines != 0));
# 580 "grep.c"
      if (nlines != 0) {
        __CrestBranch(47207, 14207, 1);
        {
        __CrestLoad(47211, (unsigned long )(& done_on_match), (long long )done_on_match);
        __CrestLoad(47210, (unsigned long )0, (long long )0);
        __CrestApply2(47209, 13, (long long )(done_on_match != 0));
# 580 "grep.c"
        if (done_on_match != 0) {
          __CrestBranch(47212, 14208, 1);
          {
          __CrestLoad(47216, (unsigned long )(& out_invert), (long long )out_invert);
          __CrestLoad(47215, (unsigned long )0, (long long )0);
          __CrestApply2(47214, 12, (long long )(out_invert == 0));
# 580 "grep.c"
          if (out_invert == 0) {
            __CrestBranch(47217, 14209, 1);
            __CrestLoad(47219, (unsigned long )(& nlines), (long long )nlines);
            __CrestStore(47220, (unsigned long )(& __retres14));
# 581 "grep.c"
            __retres14 = nlines;
# 581 "grep.c"
            goto return_label;
          } else {
            __CrestBranch(47218, 14211, 0);

          }
          }
        } else {
          __CrestBranch(47213, 14212, 0);

        }
        }
      } else {
        __CrestBranch(47208, 14213, 0);

      }
      }
    } else {
      __CrestBranch(47191, 14214, 0);

    }
    }
    __CrestLoad(47221, (unsigned long )0, (long long )0);
    __CrestStore(47222, (unsigned long )(& i));
# 583 "grep.c"
    i = 0;
# 584 "grep.c"
    beg = lim;
    {
# 585 "grep.c"
    while (1) {
      while_continue___1: ;
      {
      __CrestLoad(47225, (unsigned long )(& i), (long long )i);
      __CrestLoad(47224, (unsigned long )(& out_before), (long long )out_before);
      __CrestApply2(47223, 16, (long long )(i < out_before));
# 585 "grep.c"
      if (i < out_before) {
        __CrestBranch(47226, 14220, 1);
        {
        __CrestLoad(47230, (unsigned long )(& beg), (long long )((unsigned long )beg));
        __CrestLoad(47229, (unsigned long )(& bufbeg), (long long )((unsigned long )bufbeg));
        __CrestApply2(47228, 14, (long long )((unsigned long )beg > (unsigned long )bufbeg));
# 585 "grep.c"
        if ((unsigned long )beg > (unsigned long )bufbeg) {
          __CrestBranch(47231, 14221, 1);
          {
          __CrestLoad(47235, (unsigned long )(& beg), (long long )((unsigned long )beg));
          __CrestLoad(47234, (unsigned long )(& lastout), (long long )((unsigned long )lastout));
          __CrestApply2(47233, 13, (long long )((unsigned long )beg != (unsigned long )lastout));
# 585 "grep.c"
          if ((unsigned long )beg != (unsigned long )lastout) {
            __CrestBranch(47236, 14222, 1);

          } else {
            __CrestBranch(47237, 14223, 0);
# 585 "grep.c"
            goto while_break___1;
          }
          }
        } else {
          __CrestBranch(47232, 14224, 0);
# 585 "grep.c"
          goto while_break___1;
        }
        }
      } else {
        __CrestBranch(47227, 14225, 0);
# 585 "grep.c"
        goto while_break___1;
      }
      }
      __CrestLoad(47240, (unsigned long )(& i), (long long )i);
      __CrestLoad(47239, (unsigned long )0, (long long )1);
      __CrestApply2(47238, 0, (long long )(i + 1));
      __CrestStore(47241, (unsigned long )(& i));
# 587 "grep.c"
      i ++;
      {
# 588 "grep.c"
      while (1) {
        while_continue___2: ;
# 589 "grep.c"
        beg --;
        {
        __CrestLoad(47244, (unsigned long )(& beg), (long long )((unsigned long )beg));
        __CrestLoad(47243, (unsigned long )(& bufbeg), (long long )((unsigned long )bufbeg));
        __CrestApply2(47242, 14, (long long )((unsigned long )beg > (unsigned long )bufbeg));
# 588 "grep.c"
        if ((unsigned long )beg > (unsigned long )bufbeg) {
          __CrestBranch(47245, 14232, 1);
          {
# 588 "grep.c"
          mem_13 = beg + -1;
          {
          __CrestLoad(47249, (unsigned long )mem_13, (long long )*mem_13);
          __CrestLoad(47248, (unsigned long )0, (long long )10);
          __CrestApply2(47247, 13, (long long )((int )*mem_13 != 10));
# 588 "grep.c"
          if ((int )*mem_13 != 10) {
            __CrestBranch(47250, 14235, 1);

          } else {
            __CrestBranch(47251, 14236, 0);
# 588 "grep.c"
            goto while_break___2;
          }
          }
          }
        } else {
          __CrestBranch(47246, 14237, 0);
# 588 "grep.c"
          goto while_break___2;
        }
        }
      }
      while_break___2: ;
      }
    }
    while_break___1: ;
    }
    {
    __CrestLoad(47254, (unsigned long )(& beg), (long long )((unsigned long )beg));
    __CrestLoad(47253, (unsigned long )(& lastout), (long long )((unsigned long )lastout));
    __CrestApply2(47252, 13, (long long )((unsigned long )beg != (unsigned long )lastout));
# 592 "grep.c"
    if ((unsigned long )beg != (unsigned long )lastout) {
      __CrestBranch(47255, 14241, 1);
# 593 "grep.c"
      lastout = (char *)0;
    } else {
      __CrestBranch(47256, 14242, 0);

    }
    }
    __CrestLoad(47261, (unsigned long )(& lim), (long long )((unsigned long )lim));
    __CrestLoad(47260, (unsigned long )(& residue), (long long )residue);
    __CrestApply2(47259, 18, (long long )((unsigned long )(lim + residue)));
    __CrestLoad(47258, (unsigned long )(& beg), (long long )((unsigned long )beg));
    __CrestApply2(47257, 18, (long long )((lim + residue) - beg));
    __CrestStore(47262, (unsigned long )(& save));
# 594 "grep.c"
    save = (size_t )((lim + residue) - beg);
    __CrestLoad(47269, (unsigned long )(& totalcc), (long long )totalcc);
    __CrestLoad(47268, (unsigned long )(& buflim), (long long )((unsigned long )buflim));
    __CrestLoad(47267, (unsigned long )(& bufbeg), (long long )((unsigned long )bufbeg));
    __CrestApply2(47266, 18, (long long )(buflim - bufbeg));
    __CrestLoad(47265, (unsigned long )(& save), (long long )save);
    __CrestApply2(47264, 1, (long long )((size_t )(buflim - bufbeg) - save));
    __CrestApply2(47263, 0, (long long )(totalcc + ((size_t )(buflim - bufbeg) - save)));
    __CrestStore(47270, (unsigned long )(& totalcc));
# 595 "grep.c"
    totalcc += (size_t )(buflim - bufbeg) - save;
    {
    __CrestLoad(47273, (unsigned long )(& out_line), (long long )out_line);
    __CrestLoad(47272, (unsigned long )0, (long long )0);
    __CrestApply2(47271, 13, (long long )(out_line != 0));
# 596 "grep.c"
    if (out_line != 0) {
      __CrestBranch(47274, 14245, 1);
# 597 "grep.c"
      nlscan(beg);
      __CrestClearStack(47276);
    } else {
      __CrestBranch(47275, 14246, 0);

    }
    }
  }
  while_break: ;
  }
  {
  __CrestLoad(47279, (unsigned long )(& residue), (long long )residue);
  __CrestLoad(47278, (unsigned long )0, (long long )0);
  __CrestApply2(47277, 13, (long long )(residue != 0));
# 599 "grep.c"
  if (residue != 0) {
    __CrestBranch(47280, 14249, 1);
# 601 "grep.c"
    tmp___2 = grepbuf((bufbeg + save) - residue, buflim);
    __CrestHandleReturn(47283, (long long )tmp___2);
    __CrestStore(47282, (unsigned long )(& tmp___2));
    __CrestLoad(47286, (unsigned long )(& nlines), (long long )nlines);
    __CrestLoad(47285, (unsigned long )(& tmp___2), (long long )tmp___2);
    __CrestApply2(47284, 0, (long long )(nlines + tmp___2));
    __CrestStore(47287, (unsigned long )(& nlines));
# 601 "grep.c"
    nlines += tmp___2;
    {
    __CrestLoad(47290, (unsigned long )(& pending), (long long )pending);
    __CrestLoad(47289, (unsigned long )0, (long long )0);
    __CrestApply2(47288, 13, (long long )(pending != 0));
# 602 "grep.c"
    if (pending != 0) {
      __CrestBranch(47291, 14251, 1);
# 603 "grep.c"
      prpending(buflim);
      __CrestClearStack(47293);
    } else {
      __CrestBranch(47292, 14252, 0);

    }
    }
  } else {
    __CrestBranch(47281, 14253, 0);

  }
  }
  __CrestLoad(47294, (unsigned long )(& nlines), (long long )nlines);
  __CrestStore(47295, (unsigned long )(& __retres14));
# 605 "grep.c"
  __retres14 = nlines;
  return_label:
  {
  __CrestLoad(47296, (unsigned long )(& __retres14), (long long )__retres14);
  __CrestReturn(47297);
# 540 "grep.c"
  return (__retres14);
  }
}
}
# 609 "grep.c"
static void usage(int status )
{
  char *tmp ;
  char *tmp___0 ;
  char *tmp___1 ;
  char *tmp___2 ;
  char *tmp___3 ;
  char *tmp___4 ;
  char *tmp___5 ;
  char *tmp___6 ;

  {
  __CrestCall(47299, 366);
  __CrestStore(47298, (unsigned long )(& status));
  {
  __CrestLoad(47302, (unsigned long )(& status), (long long )status);
  __CrestLoad(47301, (unsigned long )0, (long long )0);
  __CrestApply2(47300, 13, (long long )(status != 0));
# 613 "grep.c"
  if (status != 0) {
    __CrestBranch(47303, 14257, 1);
    __CrestLoad(47305, (unsigned long )0, (long long )5);
# 615 "grep.c"
    tmp = dcgettext((char const *)((void *)0), "Usage: %s [OPTION]... PATTERN [FILE]...\n",
                    5);
    __CrestClearStack(47306);
# 615 "grep.c"
    fprintf((FILE * __restrict )stderr, (char const * __restrict )tmp, prog);
    __CrestClearStack(47307);
    __CrestLoad(47308, (unsigned long )0, (long long )5);
# 616 "grep.c"
    tmp___0 = dcgettext((char const *)((void *)0), "Try `%s --help\' for more information.\n",
                        5);
    __CrestClearStack(47309);
# 616 "grep.c"
    fprintf((FILE * __restrict )stderr, (char const * __restrict )tmp___0, prog);
    __CrestClearStack(47310);
  } else {
    __CrestBranch(47304, 14258, 0);
    __CrestLoad(47311, (unsigned long )0, (long long )5);
# 620 "grep.c"
    tmp___1 = dcgettext((char const *)((void *)0), "Usage: %s [OPTION]... PATTERN [FILE] ...\n",
                        5);
    __CrestClearStack(47312);
# 620 "grep.c"
    printf((char const * __restrict )tmp___1, prog);
    __CrestClearStack(47313);
    __CrestLoad(47314, (unsigned long )0, (long long )5);
# 621 "grep.c"
    tmp___2 = dcgettext((char const *)((void *)0), "Search for PATTERN in each FILE or standard input.\n\nRegexp selection and interpretation:\n  -E, --extended-regexp     PATTERN is an extended regular expression\n  -F, --fixed-regexp        PATTERN is a fixed string separated by newlines\n  -G, --basic-regexp        PATTERN is a basic regular expression\n  -e, --regexp=PATTERN      use PATTERN as a regular expression\n  -f, --file=FILE           obtain PATTERN from FILE\n  -i, --ignore-case         ignore case distinctions\n  -w, --word-regexp         force PATTERN to match only whole words\n  -x, --line-regexp         force PATTERN to match only whole lines\n",
                        5);
    __CrestClearStack(47315);
# 621 "grep.c"
    printf((char const * __restrict )tmp___2);
    __CrestClearStack(47316);
    __CrestLoad(47317, (unsigned long )0, (long long )5);
# 633 "grep.c"
    tmp___3 = dcgettext((char const *)((void *)0), "\nMiscellaneous:\n  -s, --no-messages         suppress error messages\n  -v, --revert-match        select non-matching lines\n  -V, --version             print version information and exit\n      --help                display this help and exit\n",
                        5);
    __CrestClearStack(47318);
# 633 "grep.c"
    printf((char const * __restrict )tmp___3);
    __CrestClearStack(47319);
    __CrestLoad(47320, (unsigned long )0, (long long )5);
# 640 "grep.c"
    tmp___4 = dcgettext((char const *)((void *)0), "\nOutput control:\n  -b, --byte-offset         print the byte offset with output lines\n  -n, --line-number         print line number with output lines\n  -H, --with-filename       print the filename for each match\n  -h, --no-filename         suppress the prefixing filename on output\n  -q, --quiet, --silent     suppress all normal output\n  -L, --files-without-match only print FILE names containing no match\n  -l, --files-with-matches  only print FILE names containing matches\n  -c, --count               only print a count of matching lines per FILE\n",
                        5);
    __CrestClearStack(47321);
# 640 "grep.c"
    printf((char const * __restrict )tmp___4);
    __CrestClearStack(47322);
    __CrestLoad(47323, (unsigned long )0, (long long )5);
# 651 "grep.c"
    tmp___5 = dcgettext((char const *)((void *)0), "\nContext control:\n  -B, --before-context=NUM  print NUM lines of leading context\n  -A, --after-context=NUM   print NUM lines of trailing context\n  -NUM                      same as both -B NUM and -A NUM\n  -C, --context             same as -2\n  -U, --binary              do not strip CR characters at EOL (MSDOS)\n  -u, --unix-byte-offsets   report offsets as if CRs were not there (MSDOS)\n\nIf no -[GEF], then `egrep\' assumes -E, `fgrep\' -F, else -G.\nWith no FILE, or when FILE is -, read standard input. If less than\ntwo FILEs given, assume -h. Exit with 0 if matches, with 1 if none.\nExit with 2 if syntax errors or system errors.\n",
                        5);
    __CrestClearStack(47324);
# 651 "grep.c"
    printf((char const * __restrict )tmp___5);
    __CrestClearStack(47325);
    __CrestLoad(47326, (unsigned long )0, (long long )5);
# 665 "grep.c"
    tmp___6 = dcgettext((char const *)((void *)0), "\nReport bugs to <bug-gnu-utils@gnu.org>.\n",
                        5);
    __CrestClearStack(47327);
# 665 "grep.c"
    printf((char const * __restrict )tmp___6);
    __CrestClearStack(47328);
  }
  }
  __CrestLoad(47329, (unsigned long )(& status), (long long )status);
# 667 "grep.c"
  exit(status);
  __CrestClearStack(47330);
  {
  __CrestReturn(47331);
# 609 "grep.c"
  return;
  }
}
}
# 672 "grep.c"
static int setmatcher(char *name )
{
  int i ;
  int tmp ;
  int __retres4 ;

  {
  __CrestCall(47332, 367);

  __CrestLoad(47333, (unsigned long )0, (long long )0);
  __CrestStore(47334, (unsigned long )(& i));
# 681 "grep.c"
  i = 0;
  {
# 681 "grep.c"
  while (1) {
    while_continue: ;
    {
    __CrestLoad(47337, (unsigned long )(& matchers[i].name), (long long )((unsigned long )matchers[i].name));
    __CrestLoad(47336, (unsigned long )0, (long long )0);
    __CrestApply2(47335, 13, (long long )(matchers[i].name != 0));
# 681 "grep.c"
    if (matchers[i].name != 0) {
      __CrestBranch(47338, 14266, 1);

    } else {
      __CrestBranch(47339, 14267, 0);
# 681 "grep.c"
      goto while_break;
    }
    }
# 682 "grep.c"
    tmp = strcmp((char const *)name, (char const *)matchers[i].name);
    __CrestHandleReturn(47341, (long long )tmp);
    __CrestStore(47340, (unsigned long )(& tmp));
    {
    __CrestLoad(47344, (unsigned long )(& tmp), (long long )tmp);
    __CrestLoad(47343, (unsigned long )0, (long long )0);
    __CrestApply2(47342, 12, (long long )(tmp == 0));
# 682 "grep.c"
    if (tmp == 0) {
      __CrestBranch(47345, 14270, 1);
# 684 "grep.c"
      compile = matchers[i].compile;
# 685 "grep.c"
      execute = matchers[i].execute;
      __CrestLoad(47347, (unsigned long )0, (long long )1);
      __CrestStore(47348, (unsigned long )(& __retres4));
# 711 "grep.c"
      __retres4 = 1;
# 711 "grep.c"
      goto return_label;
    } else {
      __CrestBranch(47346, 14273, 0);

    }
    }
    __CrestLoad(47351, (unsigned long )(& i), (long long )i);
    __CrestLoad(47350, (unsigned long )0, (long long )1);
    __CrestApply2(47349, 0, (long long )(i + 1));
    __CrestStore(47352, (unsigned long )(& i));
# 681 "grep.c"
    i ++;
  }
  while_break: ;
  }
  __CrestLoad(47353, (unsigned long )0, (long long )0);
  __CrestStore(47354, (unsigned long )(& __retres4));
# 713 "grep.c"
  __retres4 = 0;
  return_label:
  {
  __CrestLoad(47355, (unsigned long )(& __retres4), (long long )__retres4);
  __CrestReturn(47356);
# 672 "grep.c"
  return (__retres4);
  }
}
}
# 716 "grep.c"
int main(int argc , char **argv )
{
  char *keys ;
  size_t keycc ;
  size_t oldcc ;
  size_t keyalloc ;
  int count_matches ;
  int no_filenames ;
  int list_files ;
  int suppress_errors ;
  int with_filenames ;
  int opt ;
  int cc ;
  int desc ;
  int count ;
  int status ;
  FILE *fp ;
  char *tmp ;
  char *tmp___0 ;
  char *tmp___1 ;
  int tmp___2 ;
  char *tmp___3 ;
  int tmp___4 ;
  char *tmp___5 ;
  int tmp___6 ;
  char *tmp___7 ;
  int tmp___8 ;
  char *tmp___9 ;
  int tmp___10 ;
  char *tmp___11 ;
  size_t tmp___12 ;
  size_t tmp___13 ;
  FILE *tmp___15 ;
  int tmp___16 ;
  int *tmp___17 ;
  int tmp___18 ;
  size_t tmp___19 ;
  size_t tmp___20 ;
  char *tmp___21 ;
  char *tmp___22 ;
  char *tmp___23 ;
  int tmp___24 ;
  int tmp___25 ;
  int tmp___26 ;
  int tmp___27 ;
  int *tmp___28 ;
  char *tmp___29 ;
  char *tmp___30 ;
  int *tmp___31 ;
  char *tmp___32 ;
  int tmp___33 ;
  int tmp___34 ;
  char **mem_56 ;
  char *mem_57 ;
  char *mem_58 ;
  char *mem_59 ;
  char **mem_60 ;
  char **mem_61 ;
  char **mem_62 ;
  char **mem_63 ;
  char **mem_64 ;
  int __retres65 ;

  {
  __globinit_grep();
  __CrestCall(47358, 368);
  __CrestStore(47357, (unsigned long )(& argc));
# 731 "grep.c"
  mem_56 = argv + 0;
# 731 "grep.c"
  prog = *mem_56;
  __CrestLoad(47361, (unsigned long )(& prog), (long long )((unsigned long )prog));
  __CrestLoad(47360, (unsigned long )0, (long long )0);
  __CrestApply2(47359, 13, (long long )(prog != 0));
# 732 "grep.c"
  if (prog != 0) {
    __CrestBranch(47362, 14280, 1);
    __CrestLoad(47364, (unsigned long )0, (long long )'/');
# 732 "grep.c"
    tmp___0 = strrchr((char const *)prog, '/');
    __CrestClearStack(47365);
    {
    __CrestLoad(47368, (unsigned long )(& tmp___0), (long long )((unsigned long )tmp___0));
    __CrestLoad(47367, (unsigned long )0, (long long )0);
    __CrestApply2(47366, 13, (long long )(tmp___0 != 0));
# 732 "grep.c"
    if (tmp___0 != 0) {
      __CrestBranch(47369, 14282, 1);
      __CrestLoad(47371, (unsigned long )0, (long long )'/');
# 733 "grep.c"
      tmp = strrchr((char const *)prog, '/');
      __CrestClearStack(47372);
# 733 "grep.c"
      prog = tmp + 1;
    } else {
      __CrestBranch(47370, 14283, 0);

    }
    }
  } else {
    __CrestBranch(47363, 14284, 0);

  }
# 760 "grep.c"
  keys = (char *)((void *)0);
  __CrestLoad(47373, (unsigned long )0, (long long )((size_t )0));
  __CrestStore(47374, (unsigned long )(& keycc));
# 761 "grep.c"
  keycc = (size_t )0;
  __CrestLoad(47375, (unsigned long )0, (long long )0);
  __CrestStore(47376, (unsigned long )(& count_matches));
# 762 "grep.c"
  count_matches = 0;
  __CrestLoad(47377, (unsigned long )0, (long long )0);
  __CrestStore(47378, (unsigned long )(& no_filenames));
# 763 "grep.c"
  no_filenames = 0;
  __CrestLoad(47379, (unsigned long )0, (long long )0);
  __CrestStore(47380, (unsigned long )(& with_filenames));
# 764 "grep.c"
  with_filenames = 0;
  __CrestLoad(47381, (unsigned long )0, (long long )0);
  __CrestStore(47382, (unsigned long )(& list_files));
# 765 "grep.c"
  list_files = 0;
  __CrestLoad(47383, (unsigned long )0, (long long )0);
  __CrestStore(47384, (unsigned long )(& suppress_errors));
# 766 "grep.c"
  suppress_errors = 0;
# 767 "grep.c"
  matcher = (char *)((void *)0);
  __CrestLoad(47385, (unsigned long )0, (long long )6);
# 771 "grep.c"
  setlocale(6, "");
  __CrestClearStack(47386);
# 774 "grep.c"
  bindtextdomain("grep", "/usr/local/share/locale");
  __CrestClearStack(47387);
# 775 "grep.c"
  textdomain("grep");
  __CrestClearStack(47388);
# 778 "grep.c"
  while (1) {
    while_continue: ;
    __CrestLoad(47389, (unsigned long )(& argc), (long long )argc);
# 778 "grep.c"
    opt = getopt_long(argc, (char * const *)argv, "0123456789A:B:CEFGHVX:bce:f:hiLlnqsvwxy",
                      (struct option const *)(long_options), (int *)((void *)0));
    __CrestHandleReturn(47391, (long long )opt);
    __CrestStore(47390, (unsigned long )(& opt));
    {
    __CrestLoad(47394, (unsigned long )(& opt), (long long )opt);
    __CrestLoad(47393, (unsigned long )0, (long long )-1);
    __CrestApply2(47392, 13, (long long )(opt != -1));
# 778 "grep.c"
    if (opt != -1) {
      __CrestBranch(47395, 14291, 1);

    } else {
      __CrestBranch(47396, 14292, 0);
# 778 "grep.c"
      goto while_break;
    }
    }
    {
    {
    __CrestLoad(47399, (unsigned long )(& opt), (long long )opt);
    __CrestLoad(47398, (unsigned long )0, (long long )57);
    __CrestApply2(47397, 12, (long long )(opt == 57));
# 796 "grep.c"
    if (opt == 57) {
      __CrestBranch(47400, 14295, 1);
# 796 "grep.c"
      goto case_57;
    } else {
      __CrestBranch(47401, 14296, 0);

    }
    }
    {
    __CrestLoad(47404, (unsigned long )(& opt), (long long )opt);
    __CrestLoad(47403, (unsigned long )0, (long long )56);
    __CrestApply2(47402, 12, (long long )(opt == 56));
# 796 "grep.c"
    if (opt == 56) {
      __CrestBranch(47405, 14298, 1);
# 796 "grep.c"
      goto case_57;
    } else {
      __CrestBranch(47406, 14299, 0);

    }
    }
    {
    __CrestLoad(47409, (unsigned long )(& opt), (long long )opt);
    __CrestLoad(47408, (unsigned long )0, (long long )55);
    __CrestApply2(47407, 12, (long long )(opt == 55));
# 796 "grep.c"
    if (opt == 55) {
      __CrestBranch(47410, 14301, 1);
# 796 "grep.c"
      goto case_57;
    } else {
      __CrestBranch(47411, 14302, 0);

    }
    }
    {
    __CrestLoad(47414, (unsigned long )(& opt), (long long )opt);
    __CrestLoad(47413, (unsigned long )0, (long long )54);
    __CrestApply2(47412, 12, (long long )(opt == 54));
# 796 "grep.c"
    if (opt == 54) {
      __CrestBranch(47415, 14304, 1);
# 796 "grep.c"
      goto case_57;
    } else {
      __CrestBranch(47416, 14305, 0);

    }
    }
    {
    __CrestLoad(47419, (unsigned long )(& opt), (long long )opt);
    __CrestLoad(47418, (unsigned long )0, (long long )53);
    __CrestApply2(47417, 12, (long long )(opt == 53));
# 796 "grep.c"
    if (opt == 53) {
      __CrestBranch(47420, 14307, 1);
# 796 "grep.c"
      goto case_57;
    } else {
      __CrestBranch(47421, 14308, 0);

    }
    }
    {
    __CrestLoad(47424, (unsigned long )(& opt), (long long )opt);
    __CrestLoad(47423, (unsigned long )0, (long long )52);
    __CrestApply2(47422, 12, (long long )(opt == 52));
# 796 "grep.c"
    if (opt == 52) {
      __CrestBranch(47425, 14310, 1);
# 796 "grep.c"
      goto case_57;
    } else {
      __CrestBranch(47426, 14311, 0);

    }
    }
    {
    __CrestLoad(47429, (unsigned long )(& opt), (long long )opt);
    __CrestLoad(47428, (unsigned long )0, (long long )51);
    __CrestApply2(47427, 12, (long long )(opt == 51));
# 796 "grep.c"
    if (opt == 51) {
      __CrestBranch(47430, 14313, 1);
# 796 "grep.c"
      goto case_57;
    } else {
      __CrestBranch(47431, 14314, 0);

    }
    }
    {
    __CrestLoad(47434, (unsigned long )(& opt), (long long )opt);
    __CrestLoad(47433, (unsigned long )0, (long long )50);
    __CrestApply2(47432, 12, (long long )(opt == 50));
# 796 "grep.c"
    if (opt == 50) {
      __CrestBranch(47435, 14316, 1);
# 796 "grep.c"
      goto case_57;
    } else {
      __CrestBranch(47436, 14317, 0);

    }
    }
    {
    __CrestLoad(47439, (unsigned long )(& opt), (long long )opt);
    __CrestLoad(47438, (unsigned long )0, (long long )49);
    __CrestApply2(47437, 12, (long long )(opt == 49));
# 796 "grep.c"
    if (opt == 49) {
      __CrestBranch(47440, 14319, 1);
# 796 "grep.c"
      goto case_57;
    } else {
      __CrestBranch(47441, 14320, 0);

    }
    }
    {
    __CrestLoad(47444, (unsigned long )(& opt), (long long )opt);
    __CrestLoad(47443, (unsigned long )0, (long long )48);
    __CrestApply2(47442, 12, (long long )(opt == 48));
# 796 "grep.c"
    if (opt == 48) {
      __CrestBranch(47445, 14322, 1);
# 796 "grep.c"
      goto case_57;
    } else {
      __CrestBranch(47446, 14323, 0);

    }
    }
    {
    __CrestLoad(47449, (unsigned long )(& opt), (long long )opt);
    __CrestLoad(47448, (unsigned long )0, (long long )65);
    __CrestApply2(47447, 12, (long long )(opt == 65));
# 800 "grep.c"
    if (opt == 65) {
      __CrestBranch(47450, 14325, 1);
# 800 "grep.c"
      goto case_65;
    } else {
      __CrestBranch(47451, 14326, 0);

    }
    }
    {
    __CrestLoad(47454, (unsigned long )(& opt), (long long )opt);
    __CrestLoad(47453, (unsigned long )0, (long long )66);
    __CrestApply2(47452, 12, (long long )(opt == 66));
# 807 "grep.c"
    if (opt == 66) {
      __CrestBranch(47455, 14328, 1);
# 807 "grep.c"
      goto case_66;
    } else {
      __CrestBranch(47456, 14329, 0);

    }
    }
    {
    __CrestLoad(47459, (unsigned long )(& opt), (long long )opt);
    __CrestLoad(47458, (unsigned long )0, (long long )67);
    __CrestApply2(47457, 12, (long long )(opt == 67));
# 814 "grep.c"
    if (opt == 67) {
      __CrestBranch(47460, 14331, 1);
# 814 "grep.c"
      goto case_67;
    } else {
      __CrestBranch(47461, 14332, 0);

    }
    }
    {
    __CrestLoad(47464, (unsigned long )(& opt), (long long )opt);
    __CrestLoad(47463, (unsigned long )0, (long long )69);
    __CrestApply2(47462, 12, (long long )(opt == 69));
# 817 "grep.c"
    if (opt == 69) {
      __CrestBranch(47465, 14334, 1);
# 817 "grep.c"
      goto case_69;
    } else {
      __CrestBranch(47466, 14335, 0);

    }
    }
    {
    __CrestLoad(47469, (unsigned long )(& opt), (long long )opt);
    __CrestLoad(47468, (unsigned long )0, (long long )70);
    __CrestApply2(47467, 12, (long long )(opt == 70));
# 822 "grep.c"
    if (opt == 70) {
      __CrestBranch(47470, 14337, 1);
# 822 "grep.c"
      goto case_70;
    } else {
      __CrestBranch(47471, 14338, 0);

    }
    }
    {
    __CrestLoad(47474, (unsigned long )(& opt), (long long )opt);
    __CrestLoad(47473, (unsigned long )0, (long long )71);
    __CrestApply2(47472, 12, (long long )(opt == 71));
# 827 "grep.c"
    if (opt == 71) {
      __CrestBranch(47475, 14340, 1);
# 827 "grep.c"
      goto case_71;
    } else {
      __CrestBranch(47476, 14341, 0);

    }
    }
    {
    __CrestLoad(47479, (unsigned long )(& opt), (long long )opt);
    __CrestLoad(47478, (unsigned long )0, (long long )72);
    __CrestApply2(47477, 12, (long long )(opt == 72));
# 832 "grep.c"
    if (opt == 72) {
      __CrestBranch(47480, 14343, 1);
# 832 "grep.c"
      goto case_72;
    } else {
      __CrestBranch(47481, 14344, 0);

    }
    }
    {
    __CrestLoad(47484, (unsigned long )(& opt), (long long )opt);
    __CrestLoad(47483, (unsigned long )0, (long long )86);
    __CrestApply2(47482, 12, (long long )(opt == 86));
# 843 "grep.c"
    if (opt == 86) {
      __CrestBranch(47485, 14346, 1);
# 843 "grep.c"
      goto case_86;
    } else {
      __CrestBranch(47486, 14347, 0);

    }
    }
    {
    __CrestLoad(47489, (unsigned long )(& opt), (long long )opt);
    __CrestLoad(47488, (unsigned long )0, (long long )88);
    __CrestApply2(47487, 12, (long long )(opt == 88));
# 846 "grep.c"
    if (opt == 88) {
      __CrestBranch(47490, 14349, 1);
# 846 "grep.c"
      goto case_88;
    } else {
      __CrestBranch(47491, 14350, 0);

    }
    }
    {
    __CrestLoad(47494, (unsigned long )(& opt), (long long )opt);
    __CrestLoad(47493, (unsigned long )0, (long long )98);
    __CrestApply2(47492, 12, (long long )(opt == 98));
# 851 "grep.c"
    if (opt == 98) {
      __CrestBranch(47495, 14352, 1);
# 851 "grep.c"
      goto case_98;
    } else {
      __CrestBranch(47496, 14353, 0);

    }
    }
    {
    __CrestLoad(47499, (unsigned long )(& opt), (long long )opt);
    __CrestLoad(47498, (unsigned long )0, (long long )99);
    __CrestApply2(47497, 12, (long long )(opt == 99));
# 854 "grep.c"
    if (opt == 99) {
      __CrestBranch(47500, 14355, 1);
# 854 "grep.c"
      goto case_99;
    } else {
      __CrestBranch(47501, 14356, 0);

    }
    }
    {
    __CrestLoad(47504, (unsigned long )(& opt), (long long )opt);
    __CrestLoad(47503, (unsigned long )0, (long long )101);
    __CrestApply2(47502, 12, (long long )(opt == 101));
# 858 "grep.c"
    if (opt == 101) {
      __CrestBranch(47505, 14358, 1);
# 858 "grep.c"
      goto case_101;
    } else {
      __CrestBranch(47506, 14359, 0);

    }
    }
    {
    __CrestLoad(47509, (unsigned long )(& opt), (long long )opt);
    __CrestLoad(47508, (unsigned long )0, (long long )102);
    __CrestApply2(47507, 12, (long long )(opt == 102));
# 865 "grep.c"
    if (opt == 102) {
      __CrestBranch(47510, 14361, 1);
# 865 "grep.c"
      goto case_102;
    } else {
      __CrestBranch(47511, 14362, 0);

    }
    }
    {
    __CrestLoad(47514, (unsigned long )(& opt), (long long )opt);
    __CrestLoad(47513, (unsigned long )0, (long long )104);
    __CrestApply2(47512, 12, (long long )(opt == 104));
# 886 "grep.c"
    if (opt == 104) {
      __CrestBranch(47515, 14364, 1);
# 886 "grep.c"
      goto case_104;
    } else {
      __CrestBranch(47516, 14365, 0);

    }
    }
    {
    __CrestLoad(47519, (unsigned long )(& opt), (long long )opt);
    __CrestLoad(47518, (unsigned long )0, (long long )121);
    __CrestApply2(47517, 12, (long long )(opt == 121));
# 890 "grep.c"
    if (opt == 121) {
      __CrestBranch(47520, 14367, 1);
# 890 "grep.c"
      goto case_121;
    } else {
      __CrestBranch(47521, 14368, 0);

    }
    }
    {
    __CrestLoad(47524, (unsigned long )(& opt), (long long )opt);
    __CrestLoad(47523, (unsigned long )0, (long long )105);
    __CrestApply2(47522, 12, (long long )(opt == 105));
# 890 "grep.c"
    if (opt == 105) {
      __CrestBranch(47525, 14370, 1);
# 890 "grep.c"
      goto case_121;
    } else {
      __CrestBranch(47526, 14371, 0);

    }
    }
    {
    __CrestLoad(47529, (unsigned long )(& opt), (long long )opt);
    __CrestLoad(47528, (unsigned long )0, (long long )76);
    __CrestApply2(47527, 12, (long long )(opt == 76));
# 893 "grep.c"
    if (opt == 76) {
      __CrestBranch(47530, 14373, 1);
# 893 "grep.c"
      goto case_76;
    } else {
      __CrestBranch(47531, 14374, 0);

    }
    }
    {
    __CrestLoad(47534, (unsigned long )(& opt), (long long )opt);
    __CrestLoad(47533, (unsigned long )0, (long long )108);
    __CrestApply2(47532, 12, (long long )(opt == 108));
# 900 "grep.c"
    if (opt == 108) {
      __CrestBranch(47535, 14376, 1);
# 900 "grep.c"
      goto case_108;
    } else {
      __CrestBranch(47536, 14377, 0);

    }
    }
    {
    __CrestLoad(47539, (unsigned long )(& opt), (long long )opt);
    __CrestLoad(47538, (unsigned long )0, (long long )110);
    __CrestApply2(47537, 12, (long long )(opt == 110));
# 905 "grep.c"
    if (opt == 110) {
      __CrestBranch(47540, 14379, 1);
# 905 "grep.c"
      goto case_110;
    } else {
      __CrestBranch(47541, 14380, 0);

    }
    }
    {
    __CrestLoad(47544, (unsigned long )(& opt), (long long )opt);
    __CrestLoad(47543, (unsigned long )0, (long long )113);
    __CrestApply2(47542, 12, (long long )(opt == 113));
# 908 "grep.c"
    if (opt == 113) {
      __CrestBranch(47545, 14382, 1);
# 908 "grep.c"
      goto case_113;
    } else {
      __CrestBranch(47546, 14383, 0);

    }
    }
    {
    __CrestLoad(47549, (unsigned long )(& opt), (long long )opt);
    __CrestLoad(47548, (unsigned long )0, (long long )115);
    __CrestApply2(47547, 12, (long long )(opt == 115));
# 912 "grep.c"
    if (opt == 115) {
      __CrestBranch(47550, 14385, 1);
# 912 "grep.c"
      goto case_115;
    } else {
      __CrestBranch(47551, 14386, 0);

    }
    }
    {
    __CrestLoad(47554, (unsigned long )(& opt), (long long )opt);
    __CrestLoad(47553, (unsigned long )0, (long long )118);
    __CrestApply2(47552, 12, (long long )(opt == 118));
# 915 "grep.c"
    if (opt == 118) {
      __CrestBranch(47555, 14388, 1);
# 915 "grep.c"
      goto case_118;
    } else {
      __CrestBranch(47556, 14389, 0);

    }
    }
    {
    __CrestLoad(47559, (unsigned long )(& opt), (long long )opt);
    __CrestLoad(47558, (unsigned long )0, (long long )119);
    __CrestApply2(47557, 12, (long long )(opt == 119));
# 918 "grep.c"
    if (opt == 119) {
      __CrestBranch(47560, 14391, 1);
# 918 "grep.c"
      goto case_119;
    } else {
      __CrestBranch(47561, 14392, 0);

    }
    }
    {
    __CrestLoad(47564, (unsigned long )(& opt), (long long )opt);
    __CrestLoad(47563, (unsigned long )0, (long long )120);
    __CrestApply2(47562, 12, (long long )(opt == 120));
# 921 "grep.c"
    if (opt == 120) {
      __CrestBranch(47565, 14394, 1);
# 921 "grep.c"
      goto case_120;
    } else {
      __CrestBranch(47566, 14395, 0);

    }
    }
    {
    __CrestLoad(47569, (unsigned long )(& opt), (long long )opt);
    __CrestLoad(47568, (unsigned long )0, (long long )0);
    __CrestApply2(47567, 12, (long long )(opt == 0));
# 924 "grep.c"
    if (opt == 0) {
      __CrestBranch(47570, 14397, 1);
# 924 "grep.c"
      goto case_0;
    } else {
      __CrestBranch(47571, 14398, 0);

    }
    }
# 927 "grep.c"
    goto switch_default;
    case_57:
    case_56:
    case_55:
    case_54:
    case_53:
    case_52:
    case_51:
    case_50:
    case_49:
    case_48:
    __CrestLoad(47578, (unsigned long )0, (long long )10);
    __CrestLoad(47577, (unsigned long )(& out_before), (long long )out_before);
    __CrestApply2(47576, 2, (long long )(10 * out_before));
    __CrestLoad(47575, (unsigned long )(& opt), (long long )opt);
    __CrestApply2(47574, 0, (long long )(10 * out_before + opt));
    __CrestLoad(47573, (unsigned long )0, (long long )48);
    __CrestApply2(47572, 1, (long long )((10 * out_before + opt) - 48));
    __CrestStore(47579, (unsigned long )(& out_before));
# 797 "grep.c"
    out_before = (10 * out_before + opt) - 48;
    __CrestLoad(47586, (unsigned long )0, (long long )10);
    __CrestLoad(47585, (unsigned long )(& out_after), (long long )out_after);
    __CrestApply2(47584, 2, (long long )(10 * out_after));
    __CrestLoad(47583, (unsigned long )(& opt), (long long )opt);
    __CrestApply2(47582, 0, (long long )(10 * out_after + opt));
    __CrestLoad(47581, (unsigned long )0, (long long )48);
    __CrestApply2(47580, 1, (long long )((10 * out_after + opt) - 48));
    __CrestStore(47587, (unsigned long )(& out_after));
# 798 "grep.c"
    out_after = (10 * out_after + opt) - 48;
# 799 "grep.c"
    goto switch_break;
    case_65:
    {
    __CrestLoad(47590, (unsigned long )(& optarg), (long long )((unsigned long )optarg));
    __CrestLoad(47589, (unsigned long )0, (long long )0);
    __CrestApply2(47588, 13, (long long )(optarg != 0));
# 801 "grep.c"
    if (optarg != 0) {
      __CrestBranch(47591, 14403, 1);
# 803 "grep.c"
      tmp___2 = ck_atoi((char const *)optarg, & out_after);
      __CrestHandleReturn(47594, (long long )tmp___2);
      __CrestStore(47593, (unsigned long )(& tmp___2));
      {
      __CrestLoad(47597, (unsigned long )(& tmp___2), (long long )tmp___2);
      __CrestLoad(47596, (unsigned long )0, (long long )0);
      __CrestApply2(47595, 13, (long long )(tmp___2 != 0));
# 803 "grep.c"
      if (tmp___2 != 0) {
        __CrestBranch(47598, 14405, 1);
        __CrestLoad(47600, (unsigned long )0, (long long )5);
# 804 "grep.c"
        tmp___1 = dcgettext((char const *)((void *)0), "invalid context length argument",
                            5);
        __CrestClearStack(47601);
        __CrestLoad(47602, (unsigned long )0, (long long )0);
# 804 "grep.c"
        fatal((char const *)tmp___1, 0);
        __CrestClearStack(47603);
      } else {
        __CrestBranch(47599, 14406, 0);

      }
      }
    } else {
      __CrestBranch(47592, 14407, 0);

    }
    }
# 806 "grep.c"
    goto switch_break;
    case_66:
    {
    __CrestLoad(47606, (unsigned long )(& optarg), (long long )((unsigned long )optarg));
    __CrestLoad(47605, (unsigned long )0, (long long )0);
    __CrestApply2(47604, 13, (long long )(optarg != 0));
# 808 "grep.c"
    if (optarg != 0) {
      __CrestBranch(47607, 14410, 1);
# 810 "grep.c"
      tmp___4 = ck_atoi((char const *)optarg, & out_before);
      __CrestHandleReturn(47610, (long long )tmp___4);
      __CrestStore(47609, (unsigned long )(& tmp___4));
      {
      __CrestLoad(47613, (unsigned long )(& tmp___4), (long long )tmp___4);
      __CrestLoad(47612, (unsigned long )0, (long long )0);
      __CrestApply2(47611, 13, (long long )(tmp___4 != 0));
# 810 "grep.c"
      if (tmp___4 != 0) {
        __CrestBranch(47614, 14412, 1);
        __CrestLoad(47616, (unsigned long )0, (long long )5);
# 811 "grep.c"
        tmp___3 = dcgettext((char const *)((void *)0), "invalid context length argument",
                            5);
        __CrestClearStack(47617);
        __CrestLoad(47618, (unsigned long )0, (long long )0);
# 811 "grep.c"
        fatal((char const *)tmp___3, 0);
        __CrestClearStack(47619);
      } else {
        __CrestBranch(47615, 14413, 0);

      }
      }
    } else {
      __CrestBranch(47608, 14414, 0);

    }
    }
# 813 "grep.c"
    goto switch_break;
    case_67:
    __CrestLoad(47620, (unsigned long )0, (long long )2);
    __CrestStore(47621, (unsigned long )(& out_after));
# 815 "grep.c"
    out_after = 2;
    __CrestLoad(47622, (unsigned long )(& out_after), (long long )out_after);
    __CrestStore(47623, (unsigned long )(& out_before));
# 815 "grep.c"
    out_before = out_after;
# 816 "grep.c"
    goto switch_break;
    case_69:
    {
    __CrestLoad(47626, (unsigned long )(& matcher), (long long )((unsigned long )matcher));
    __CrestLoad(47625, (unsigned long )0, (long long )0);
    __CrestApply2(47624, 13, (long long )(matcher != 0));
# 818 "grep.c"
    if (matcher != 0) {
      __CrestBranch(47627, 14419, 1);
# 818 "grep.c"
      tmp___6 = strcmp((char const *)matcher, "posix-egrep");
      __CrestHandleReturn(47630, (long long )tmp___6);
      __CrestStore(47629, (unsigned long )(& tmp___6));
      {
      __CrestLoad(47633, (unsigned long )(& tmp___6), (long long )tmp___6);
      __CrestLoad(47632, (unsigned long )0, (long long )0);
      __CrestApply2(47631, 13, (long long )(tmp___6 != 0));
# 818 "grep.c"
      if (tmp___6 != 0) {
        __CrestBranch(47634, 14421, 1);
        __CrestLoad(47636, (unsigned long )0, (long long )5);
# 819 "grep.c"
        tmp___5 = dcgettext((char const *)((void *)0), "you may specify only one of -E, -F, or -G",
                            5);
        __CrestClearStack(47637);
        __CrestLoad(47638, (unsigned long )0, (long long )0);
# 819 "grep.c"
        fatal((char const *)tmp___5, 0);
        __CrestClearStack(47639);
      } else {
        __CrestBranch(47635, 14422, 0);

      }
      }
    } else {
      __CrestBranch(47628, 14423, 0);

    }
    }
# 820 "grep.c"
    matcher = (char *)"posix-egrep";
# 821 "grep.c"
    goto switch_break;
    case_70:
    {
    __CrestLoad(47642, (unsigned long )(& matcher), (long long )((unsigned long )matcher));
    __CrestLoad(47641, (unsigned long )0, (long long )0);
    __CrestApply2(47640, 13, (long long )(matcher != 0));
# 823 "grep.c"
    if (matcher != 0) {
      __CrestBranch(47643, 14427, 1);
# 823 "grep.c"
      tmp___8 = strcmp((char const *)matcher, "fgrep");
      __CrestHandleReturn(47646, (long long )tmp___8);
      __CrestStore(47645, (unsigned long )(& tmp___8));
      {
      __CrestLoad(47649, (unsigned long )(& tmp___8), (long long )tmp___8);
      __CrestLoad(47648, (unsigned long )0, (long long )0);
      __CrestApply2(47647, 13, (long long )(tmp___8 != 0));
# 823 "grep.c"
      if (tmp___8 != 0) {
        __CrestBranch(47650, 14429, 1);
        __CrestLoad(47652, (unsigned long )0, (long long )5);
# 824 "grep.c"
        tmp___7 = dcgettext((char const *)((void *)0), "you may specify only one of -E, -F, or -G",
                            5);
        __CrestClearStack(47653);
        __CrestLoad(47654, (unsigned long )0, (long long )0);
# 824 "grep.c"
        fatal((char const *)tmp___7, 0);
        __CrestClearStack(47655);
      } else {
        __CrestBranch(47651, 14430, 0);

      }
      }
    } else {
      __CrestBranch(47644, 14431, 0);

    }
    }
# 825 "grep.c"
    matcher = (char *)"fgrep";
# 826 "grep.c"
    goto switch_break;
    case_71:
    {
    __CrestLoad(47658, (unsigned long )(& matcher), (long long )((unsigned long )matcher));
    __CrestLoad(47657, (unsigned long )0, (long long )0);
    __CrestApply2(47656, 13, (long long )(matcher != 0));
# 828 "grep.c"
    if (matcher != 0) {
      __CrestBranch(47659, 14435, 1);
# 828 "grep.c"
      tmp___10 = strcmp((char const *)matcher, "grep");
      __CrestHandleReturn(47662, (long long )tmp___10);
      __CrestStore(47661, (unsigned long )(& tmp___10));
      {
      __CrestLoad(47665, (unsigned long )(& tmp___10), (long long )tmp___10);
      __CrestLoad(47664, (unsigned long )0, (long long )0);
      __CrestApply2(47663, 13, (long long )(tmp___10 != 0));
# 828 "grep.c"
      if (tmp___10 != 0) {
        __CrestBranch(47666, 14437, 1);
        __CrestLoad(47668, (unsigned long )0, (long long )5);
# 829 "grep.c"
        tmp___9 = dcgettext((char const *)((void *)0), "you may specify only one of -E, -F, or -G",
                            5);
        __CrestClearStack(47669);
        __CrestLoad(47670, (unsigned long )0, (long long )0);
# 829 "grep.c"
        fatal((char const *)tmp___9, 0);
        __CrestClearStack(47671);
      } else {
        __CrestBranch(47667, 14438, 0);

      }
      }
    } else {
      __CrestBranch(47660, 14439, 0);

    }
    }
# 830 "grep.c"
    matcher = (char *)"grep";
# 831 "grep.c"
    goto switch_break;
    case_72:
    __CrestLoad(47672, (unsigned long )0, (long long )1);
    __CrestStore(47673, (unsigned long )(& with_filenames));
# 833 "grep.c"
    with_filenames = 1;
# 834 "grep.c"
    goto switch_break;
    case_86:
    __CrestLoad(47674, (unsigned long )0, (long long )1);
    __CrestStore(47675, (unsigned long )(& show_version));
# 844 "grep.c"
    show_version = 1;
# 845 "grep.c"
    goto switch_break;
    case_88:
    {
    __CrestLoad(47678, (unsigned long )(& matcher), (long long )((unsigned long )matcher));
    __CrestLoad(47677, (unsigned long )0, (long long )0);
    __CrestApply2(47676, 13, (long long )(matcher != 0));
# 847 "grep.c"
    if (matcher != 0) {
      __CrestBranch(47679, 14447, 1);
      __CrestLoad(47681, (unsigned long )0, (long long )5);
# 848 "grep.c"
      tmp___11 = dcgettext((char const *)((void *)0), "matcher already specified",
                           5);
      __CrestClearStack(47682);
      __CrestLoad(47683, (unsigned long )0, (long long )0);
# 848 "grep.c"
      fatal((char const *)tmp___11, 0);
      __CrestClearStack(47684);
    } else {
      __CrestBranch(47680, 14448, 0);

    }
    }
# 849 "grep.c"
    matcher = optarg;
# 850 "grep.c"
    goto switch_break;
    case_98:
    __CrestLoad(47685, (unsigned long )0, (long long )1);
    __CrestStore(47686, (unsigned long )(& out_byte));
# 852 "grep.c"
    out_byte = 1;
# 853 "grep.c"
    goto switch_break;
    case_99:
    __CrestLoad(47687, (unsigned long )0, (long long )1);
    __CrestStore(47688, (unsigned long )(& out_quiet));
# 855 "grep.c"
    out_quiet = 1;
    __CrestLoad(47689, (unsigned long )0, (long long )1);
    __CrestStore(47690, (unsigned long )(& count_matches));
# 856 "grep.c"
    count_matches = 1;
# 857 "grep.c"
    goto switch_break;
    case_101:
# 859 "grep.c"
    tmp___12 = strlen((char const *)optarg);
    __CrestHandleReturn(47692, (long long )tmp___12);
    __CrestStore(47691, (unsigned long )(& tmp___12));
    __CrestLoad(47693, (unsigned long )(& tmp___12), (long long )tmp___12);
    __CrestStore(47694, (unsigned long )(& cc));
# 859 "grep.c"
    cc = (int )tmp___12;
    __CrestLoad(47699, (unsigned long )(& keycc), (long long )keycc);
    __CrestLoad(47698, (unsigned long )(& cc), (long long )cc);
    __CrestApply2(47697, 0, (long long )(keycc + (size_t )cc));
    __CrestLoad(47696, (unsigned long )0, (long long )1UL);
    __CrestApply2(47695, 0, (long long )((keycc + (size_t )cc) + 1UL));
# 860 "grep.c"
    keys = xrealloc(keys, (keycc + (size_t )cc) + 1UL);
    __CrestClearStack(47700);
# 861 "grep.c"
    strcpy((char * __restrict )(keys + keycc), (char const * __restrict )optarg);
    __CrestClearStack(47701);
    __CrestLoad(47704, (unsigned long )(& keycc), (long long )keycc);
    __CrestLoad(47703, (unsigned long )(& cc), (long long )cc);
    __CrestApply2(47702, 0, (long long )(keycc + (size_t )cc));
    __CrestStore(47705, (unsigned long )(& keycc));
# 862 "grep.c"
    keycc += (size_t )cc;
    __CrestLoad(47706, (unsigned long )(& keycc), (long long )keycc);
    __CrestStore(47707, (unsigned long )(& tmp___13));
# 863 "grep.c"
    tmp___13 = keycc;
    __CrestLoad(47710, (unsigned long )(& keycc), (long long )keycc);
    __CrestLoad(47709, (unsigned long )0, (long long )1UL);
    __CrestApply2(47708, 0, (long long )(keycc + 1UL));
    __CrestStore(47711, (unsigned long )(& keycc));
# 863 "grep.c"
    keycc ++;
# 863 "grep.c"
    mem_57 = keys + tmp___13;
    __CrestLoad(47712, (unsigned long )0, (long long )((char )'\n'));
    __CrestStore(47713, (unsigned long )mem_57);
# 863 "grep.c"
    *mem_57 = (char )'\n';
# 864 "grep.c"
    goto switch_break;
    case_102:
# 866 "grep.c"
    tmp___16 = strcmp((char const *)optarg, "-");
    __CrestHandleReturn(47715, (long long )tmp___16);
    __CrestStore(47714, (unsigned long )(& tmp___16));
    {
    __CrestLoad(47718, (unsigned long )(& tmp___16), (long long )tmp___16);
    __CrestLoad(47717, (unsigned long )0, (long long )0);
    __CrestApply2(47716, 13, (long long )(tmp___16 != 0));
# 866 "grep.c"
    if (tmp___16 != 0) {
      __CrestBranch(47719, 14459, 1);
# 866 "grep.c"
      tmp___15 = fopen((char const * __restrict )optarg, (char const * __restrict )"r");
      __CrestClearStack(47721);
# 866 "grep.c"
      fp = tmp___15;
    } else {
      __CrestBranch(47720, 14460, 0);
# 866 "grep.c"
      fp = stdin;
    }
    }
    {
    __CrestLoad(47724, (unsigned long )(& fp), (long long )((unsigned long )fp));
    __CrestLoad(47723, (unsigned long )0, (long long )0);
    __CrestApply2(47722, 12, (long long )(fp == 0));
# 867 "grep.c"
    if (fp == 0) {
      __CrestBranch(47725, 14462, 1);
# 868 "grep.c"
      tmp___17 = __errno_location();
      __CrestClearStack(47727);
      __CrestLoad(47728, (unsigned long )tmp___17, (long long )*tmp___17);
# 868 "grep.c"
      fatal((char const *)optarg, *tmp___17);
      __CrestClearStack(47729);
    } else {
      __CrestBranch(47726, 14463, 0);

    }
    }
    __CrestLoad(47730, (unsigned long )0, (long long )((size_t )1));
    __CrestStore(47731, (unsigned long )(& keyalloc));
# 869 "grep.c"
    keyalloc = (size_t )1;
    {
# 869 "grep.c"
    while (1) {
      while_continue___0: ;
      {
      __CrestLoad(47736, (unsigned long )(& keyalloc), (long long )keyalloc);
      __CrestLoad(47735, (unsigned long )(& keycc), (long long )keycc);
      __CrestLoad(47734, (unsigned long )0, (long long )1UL);
      __CrestApply2(47733, 0, (long long )(keycc + 1UL));
      __CrestApply2(47732, 15, (long long )(keyalloc <= keycc + 1UL));
# 869 "grep.c"
      if (keyalloc <= keycc + 1UL) {
        __CrestBranch(47737, 14469, 1);

      } else {
        __CrestBranch(47738, 14470, 0);
# 869 "grep.c"
        goto while_break___0;
      }
      }
      __CrestLoad(47741, (unsigned long )(& keyalloc), (long long )keyalloc);
      __CrestLoad(47740, (unsigned long )0, (long long )2UL);
      __CrestApply2(47739, 2, (long long )(keyalloc * 2UL));
      __CrestStore(47742, (unsigned long )(& keyalloc));
# 869 "grep.c"
      keyalloc *= 2UL;
    }
    while_break___0: ;
    }
    __CrestLoad(47743, (unsigned long )(& keyalloc), (long long )keyalloc);
# 871 "grep.c"
    keys = xrealloc(keys, keyalloc);
    __CrestClearStack(47744);
    __CrestLoad(47745, (unsigned long )(& keycc), (long long )keycc);
    __CrestStore(47746, (unsigned long )(& oldcc));
# 872 "grep.c"
    oldcc = keycc;
    {
# 873 "grep.c"
    while (1) {
      while_continue___1: ;
# 873 "grep.c"
      tmp___18 = feof(fp);
      __CrestHandleReturn(47748, (long long )tmp___18);
      __CrestStore(47747, (unsigned long )(& tmp___18));
      {
      __CrestLoad(47751, (unsigned long )(& tmp___18), (long long )tmp___18);
      __CrestLoad(47750, (unsigned long )0, (long long )0);
      __CrestApply2(47749, 13, (long long )(tmp___18 != 0));
# 873 "grep.c"
      if (tmp___18 != 0) {
        __CrestBranch(47752, 14479, 1);
# 873 "grep.c"
        goto while_break___1;
      } else {
        __CrestBranch(47753, 14480, 0);
        __CrestLoad(47754, (unsigned long )0, (long long )((size_t )1));
        __CrestLoad(47759, (unsigned long )(& keyalloc), (long long )keyalloc);
        __CrestLoad(47758, (unsigned long )0, (long long )1UL);
        __CrestApply2(47757, 1, (long long )(keyalloc - 1UL));
        __CrestLoad(47756, (unsigned long )(& keycc), (long long )keycc);
        __CrestApply2(47755, 1, (long long )((keyalloc - 1UL) - keycc));
# 873 "grep.c"
        tmp___19 = fread((void * __restrict )(keys + keycc), (size_t )1, (keyalloc - 1UL) - keycc,
                         (FILE * __restrict )fp);
        __CrestHandleReturn(47761, (long long )tmp___19);
        __CrestStore(47760, (unsigned long )(& tmp___19));
        __CrestLoad(47762, (unsigned long )(& tmp___19), (long long )tmp___19);
        __CrestStore(47763, (unsigned long )(& cc));
# 873 "grep.c"
        cc = (int )tmp___19;
        {
        __CrestLoad(47766, (unsigned long )(& cc), (long long )cc);
        __CrestLoad(47765, (unsigned long )0, (long long )0);
        __CrestApply2(47764, 14, (long long )(cc > 0));
# 873 "grep.c"
        if (cc > 0) {
          __CrestBranch(47767, 14482, 1);

        } else {
          __CrestBranch(47768, 14483, 0);
# 873 "grep.c"
          goto while_break___1;
        }
        }
      }
      }
      __CrestLoad(47771, (unsigned long )(& keycc), (long long )keycc);
      __CrestLoad(47770, (unsigned long )(& cc), (long long )cc);
      __CrestApply2(47769, 0, (long long )(keycc + (size_t )cc));
      __CrestStore(47772, (unsigned long )(& keycc));
# 876 "grep.c"
      keycc += (size_t )cc;
      {
      __CrestLoad(47777, (unsigned long )(& keycc), (long long )keycc);
      __CrestLoad(47776, (unsigned long )(& keyalloc), (long long )keyalloc);
      __CrestLoad(47775, (unsigned long )0, (long long )1UL);
      __CrestApply2(47774, 1, (long long )(keyalloc - 1UL));
      __CrestApply2(47773, 12, (long long )(keycc == keyalloc - 1UL));
# 877 "grep.c"
      if (keycc == keyalloc - 1UL) {
        __CrestBranch(47778, 14486, 1);
        __CrestLoad(47782, (unsigned long )(& keyalloc), (long long )keyalloc);
        __CrestLoad(47781, (unsigned long )0, (long long )2UL);
        __CrestApply2(47780, 2, (long long )(keyalloc * 2UL));
        __CrestStore(47783, (unsigned long )(& keyalloc));
# 878 "grep.c"
        keyalloc *= 2UL;
        __CrestLoad(47784, (unsigned long )(& keyalloc), (long long )keyalloc);
# 878 "grep.c"
        keys = xrealloc(keys, keyalloc);
        __CrestClearStack(47785);
      } else {
        __CrestBranch(47779, 14487, 0);

      }
      }
    }
    while_break___1: ;
    }
    {
    __CrestLoad(47788, (unsigned long )(& fp), (long long )((unsigned long )fp));
    __CrestLoad(47787, (unsigned long )(& stdin), (long long )((unsigned long )stdin));
    __CrestApply2(47786, 13, (long long )((unsigned long )fp != (unsigned long )stdin));
# 880 "grep.c"
    if ((unsigned long )fp != (unsigned long )stdin) {
      __CrestBranch(47789, 14490, 1);
# 881 "grep.c"
      fclose(fp);
      __CrestClearStack(47791);
    } else {
      __CrestBranch(47790, 14491, 0);

    }
    }
    {
    __CrestLoad(47794, (unsigned long )(& oldcc), (long long )oldcc);
    __CrestLoad(47793, (unsigned long )(& keycc), (long long )keycc);
    __CrestApply2(47792, 13, (long long )(oldcc != keycc));
# 883 "grep.c"
    if (oldcc != keycc) {
      __CrestBranch(47795, 14493, 1);
      {
# 883 "grep.c"
      mem_58 = keys + (keycc - 1UL);
      {
      __CrestLoad(47799, (unsigned long )mem_58, (long long )*mem_58);
      __CrestLoad(47798, (unsigned long )0, (long long )10);
      __CrestApply2(47797, 13, (long long )((int )*mem_58 != 10));
# 883 "grep.c"
      if ((int )*mem_58 != 10) {
        __CrestBranch(47800, 14496, 1);
        __CrestLoad(47802, (unsigned long )(& keycc), (long long )keycc);
        __CrestStore(47803, (unsigned long )(& tmp___20));
# 884 "grep.c"
        tmp___20 = keycc;
        __CrestLoad(47806, (unsigned long )(& keycc), (long long )keycc);
        __CrestLoad(47805, (unsigned long )0, (long long )1UL);
        __CrestApply2(47804, 0, (long long )(keycc + 1UL));
        __CrestStore(47807, (unsigned long )(& keycc));
# 884 "grep.c"
        keycc ++;
# 884 "grep.c"
        mem_59 = keys + tmp___20;
        __CrestLoad(47808, (unsigned long )0, (long long )((char )'\n'));
        __CrestStore(47809, (unsigned long )mem_59);
# 884 "grep.c"
        *mem_59 = (char )'\n';
      } else {
        __CrestBranch(47801, 14497, 0);

      }
      }
      }
    } else {
      __CrestBranch(47796, 14498, 0);

    }
    }
# 885 "grep.c"
    goto switch_break;
    case_104:
    __CrestLoad(47810, (unsigned long )0, (long long )1);
    __CrestStore(47811, (unsigned long )(& no_filenames));
# 887 "grep.c"
    no_filenames = 1;
# 888 "grep.c"
    goto switch_break;
    case_121:
    case_105:
    __CrestLoad(47812, (unsigned long )0, (long long )1);
    __CrestStore(47813, (unsigned long )(& match_icase));
# 891 "grep.c"
    match_icase = 1;
# 892 "grep.c"
    goto switch_break;
    case_76:
    __CrestLoad(47814, (unsigned long )0, (long long )1);
    __CrestStore(47815, (unsigned long )(& out_quiet));
# 896 "grep.c"
    out_quiet = 1;
    __CrestLoad(47816, (unsigned long )0, (long long )-1);
    __CrestStore(47817, (unsigned long )(& list_files));
# 897 "grep.c"
    list_files = -1;
    __CrestLoad(47818, (unsigned long )0, (long long )1);
    __CrestStore(47819, (unsigned long )(& done_on_match));
# 898 "grep.c"
    done_on_match = 1;
# 899 "grep.c"
    goto switch_break;
    case_108:
    __CrestLoad(47820, (unsigned long )0, (long long )1);
    __CrestStore(47821, (unsigned long )(& out_quiet));
# 901 "grep.c"
    out_quiet = 1;
    __CrestLoad(47822, (unsigned long )0, (long long )1);
    __CrestStore(47823, (unsigned long )(& list_files));
# 902 "grep.c"
    list_files = 1;
    __CrestLoad(47824, (unsigned long )0, (long long )1);
    __CrestStore(47825, (unsigned long )(& done_on_match));
# 903 "grep.c"
    done_on_match = 1;
# 904 "grep.c"
    goto switch_break;
    case_110:
    __CrestLoad(47826, (unsigned long )0, (long long )1);
    __CrestStore(47827, (unsigned long )(& out_line));
# 906 "grep.c"
    out_line = 1;
# 907 "grep.c"
    goto switch_break;
    case_113:
    __CrestLoad(47828, (unsigned long )0, (long long )1);
    __CrestStore(47829, (unsigned long )(& done_on_match));
# 909 "grep.c"
    done_on_match = 1;
    __CrestLoad(47830, (unsigned long )0, (long long )1);
    __CrestStore(47831, (unsigned long )(& out_quiet));
# 910 "grep.c"
    out_quiet = 1;
# 911 "grep.c"
    goto switch_break;
    case_115:
    __CrestLoad(47832, (unsigned long )0, (long long )1);
    __CrestStore(47833, (unsigned long )(& suppress_errors));
# 913 "grep.c"
    suppress_errors = 1;
# 914 "grep.c"
    goto switch_break;
    case_118:
    __CrestLoad(47834, (unsigned long )0, (long long )1);
    __CrestStore(47835, (unsigned long )(& out_invert));
# 916 "grep.c"
    out_invert = 1;
# 917 "grep.c"
    goto switch_break;
    case_119:
    __CrestLoad(47836, (unsigned long )0, (long long )1);
    __CrestStore(47837, (unsigned long )(& match_words));
# 919 "grep.c"
    match_words = 1;
# 920 "grep.c"
    goto switch_break;
    case_120:
    __CrestLoad(47838, (unsigned long )0, (long long )1);
    __CrestStore(47839, (unsigned long )(& match_lines));
# 922 "grep.c"
    match_lines = 1;
# 923 "grep.c"
    goto switch_break;
    case_0:
# 926 "grep.c"
    goto switch_break;
    switch_default:
    __CrestLoad(47840, (unsigned long )0, (long long )2);
# 928 "grep.c"
    usage(2);
    __CrestClearStack(47841);
# 929 "grep.c"
    goto switch_break;
    switch_break: ;
    }
  }
  while_break:
  __CrestLoad(47844, (unsigned long )(& show_version), (long long )show_version);
  __CrestLoad(47843, (unsigned long )0, (long long )0);
  __CrestApply2(47842, 13, (long long )(show_version != 0));
# 932 "grep.c"
  if (show_version != 0) {
    __CrestBranch(47845, 14526, 1);
    __CrestLoad(47847, (unsigned long )0, (long long )5);
# 934 "grep.c"
    tmp___21 = dcgettext((char const *)((void *)0), "grep (GNU grep) %s\n", 5);
    __CrestClearStack(47848);
# 934 "grep.c"
    printf((char const * __restrict )tmp___21, "2.2");
    __CrestClearStack(47849);
# 935 "grep.c"
    printf((char const * __restrict )"\n");
    __CrestClearStack(47850);
    __CrestLoad(47851, (unsigned long )0, (long long )5);
# 936 "grep.c"
    tmp___22 = dcgettext((char const *)((void *)0), "Copyright (C) 1988, 92, 93, 94, 95, 96, 97 Free Software Foundation, Inc.\n",
                         5);
    __CrestClearStack(47852);
# 936 "grep.c"
    printf((char const * __restrict )tmp___22);
    __CrestClearStack(47853);
    __CrestLoad(47854, (unsigned long )0, (long long )5);
# 938 "grep.c"
    tmp___23 = dcgettext((char const *)((void *)0), "This is free software; see the source for copying conditions. There is NO\nwarranty; not even for MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.\n",
                         5);
    __CrestClearStack(47855);
# 938 "grep.c"
    printf((char const * __restrict )tmp___23);
    __CrestClearStack(47856);
# 941 "grep.c"
    printf((char const * __restrict )"\n");
    __CrestClearStack(47857);
    __CrestLoad(47858, (unsigned long )0, (long long )0);
# 942 "grep.c"
    exit(0);
    __CrestClearStack(47859);
  } else {
    __CrestBranch(47846, 14527, 0);

  }
  __CrestLoad(47862, (unsigned long )(& show_help), (long long )show_help);
  __CrestLoad(47861, (unsigned long )0, (long long )0);
  __CrestApply2(47860, 13, (long long )(show_help != 0));
# 945 "grep.c"
  if (show_help != 0) {
    __CrestBranch(47863, 14529, 1);
    __CrestLoad(47865, (unsigned long )0, (long long )0);
# 946 "grep.c"
    usage(0);
    __CrestClearStack(47866);
  } else {
    __CrestBranch(47864, 14530, 0);

  }
  __CrestLoad(47869, (unsigned long )(& keys), (long long )((unsigned long )keys));
  __CrestLoad(47868, (unsigned long )0, (long long )0);
  __CrestApply2(47867, 13, (long long )(keys != 0));
# 948 "grep.c"
  if (keys != 0) {
    __CrestBranch(47870, 14532, 1);
    {
    __CrestLoad(47874, (unsigned long )(& keycc), (long long )keycc);
    __CrestLoad(47873, (unsigned long )0, (long long )0UL);
    __CrestApply2(47872, 12, (long long )(keycc == 0UL));
# 951 "grep.c"
    if (keycc == 0UL) {
      __CrestBranch(47875, 14533, 1);
      __CrestLoad(47879, (unsigned long )(& out_invert), (long long )out_invert);
      __CrestLoad(47878, (unsigned long )0, (long long )1);
      __CrestApply2(47877, 7, (long long )(out_invert ^ 1));
      __CrestStore(47880, (unsigned long )(& out_invert));
# 952 "grep.c"
      out_invert ^= 1;
    } else {
      __CrestBranch(47876, 14534, 0);
      __CrestLoad(47883, (unsigned long )(& keycc), (long long )keycc);
      __CrestLoad(47882, (unsigned long )0, (long long )1UL);
      __CrestApply2(47881, 1, (long long )(keycc - 1UL));
      __CrestStore(47884, (unsigned long )(& keycc));
# 954 "grep.c"
      keycc --;
    }
    }
  } else {
    __CrestBranch(47871, 14535, 0);
    {
    __CrestLoad(47887, (unsigned long )(& optind), (long long )optind);
    __CrestLoad(47886, (unsigned long )(& argc), (long long )argc);
    __CrestApply2(47885, 16, (long long )(optind < argc));
# 957 "grep.c"
    if (optind < argc) {
      __CrestBranch(47888, 14536, 1);
      __CrestLoad(47890, (unsigned long )(& optind), (long long )optind);
      __CrestStore(47891, (unsigned long )(& tmp___24));
# 959 "grep.c"
      tmp___24 = optind;
      __CrestLoad(47894, (unsigned long )(& optind), (long long )optind);
      __CrestLoad(47893, (unsigned long )0, (long long )1);
      __CrestApply2(47892, 0, (long long )(optind + 1));
      __CrestStore(47895, (unsigned long )(& optind));
# 959 "grep.c"
      optind ++;
# 959 "grep.c"
      mem_60 = argv + tmp___24;
# 959 "grep.c"
      keys = *mem_60;
# 960 "grep.c"
      keycc = strlen((char const *)keys);
      __CrestHandleReturn(47897, (long long )keycc);
      __CrestStore(47896, (unsigned long )(& keycc));
# 970 "grep.c"
      __CrestChar(keys + 0);
# 971 "grep.c"
      __CrestChar(keys + 1);
# 972 "grep.c"
      __CrestChar(keys + 2);
# 973 "grep.c"
      __CrestChar(keys + 3);
# 974 "grep.c"
      __CrestChar(keys + 4);
# 975 "grep.c"
      __CrestChar(keys + 5);
# 976 "grep.c"
      __CrestChar(keys + 6);
# 977 "grep.c"
      __CrestChar(keys + 7);
# 978 "grep.c"
      __CrestChar(keys + 8);
# 979 "grep.c"
      __CrestChar(keys + 9);
# 981 "grep.c"
      printf((char const * __restrict )"%s\n", keys);
      __CrestClearStack(47898);
    } else {
      __CrestBranch(47889, 14537, 0);
      __CrestLoad(47899, (unsigned long )0, (long long )2);
# 984 "grep.c"
      usage(2);
      __CrestClearStack(47900);
    }
    }
  }
  __CrestLoad(47903, (unsigned long )(& matcher), (long long )((unsigned long )matcher));
  __CrestLoad(47902, (unsigned long )0, (long long )0);
  __CrestApply2(47901, 12, (long long )(matcher == 0));
# 986 "grep.c"
  if (matcher == 0) {
    __CrestBranch(47904, 14539, 1);
# 987 "grep.c"
    matcher = prog;
  } else {
    __CrestBranch(47905, 14540, 0);

  }
# 989 "grep.c"
  tmp___25 = setmatcher(matcher);
  __CrestHandleReturn(47907, (long long )tmp___25);
  __CrestStore(47906, (unsigned long )(& tmp___25));
  __CrestLoad(47910, (unsigned long )(& tmp___25), (long long )tmp___25);
  __CrestLoad(47909, (unsigned long )0, (long long )0);
  __CrestApply2(47908, 13, (long long )(tmp___25 != 0));
# 989 "grep.c"
  if (tmp___25 != 0) {
    __CrestBranch(47911, 14543, 1);

  } else {
    __CrestBranch(47912, 14544, 0);
# 989 "grep.c"
    tmp___26 = setmatcher((char *)"default");
    __CrestHandleReturn(47914, (long long )tmp___26);
    __CrestStore(47913, (unsigned long )(& tmp___26));
    {
    __CrestLoad(47917, (unsigned long )(& tmp___26), (long long )tmp___26);
    __CrestLoad(47916, (unsigned long )0, (long long )0);
    __CrestApply2(47915, 13, (long long )(tmp___26 != 0));
# 989 "grep.c"
    if (tmp___26 != 0) {
      __CrestBranch(47918, 14546, 1);

    } else {
      __CrestBranch(47919, 14547, 0);
# 990 "grep.c"
      abort();
      __CrestClearStack(47920);
    }
    }
  }
  __CrestLoad(47921, (unsigned long )(& keycc), (long long )keycc);
# 992 "grep.c"
  (*compile)(keys, keycc);
  __CrestClearStack(47922);
  __CrestLoad(47927, (unsigned long )(& argc), (long long )argc);
  __CrestLoad(47926, (unsigned long )(& optind), (long long )optind);
  __CrestApply2(47925, 1, (long long )(argc - optind));
  __CrestLoad(47924, (unsigned long )0, (long long )1);
  __CrestApply2(47923, 14, (long long )(argc - optind > 1));
# 994 "grep.c"
  if (argc - optind > 1) {
    __CrestBranch(47928, 14550, 1);
    {
    __CrestLoad(47932, (unsigned long )(& no_filenames), (long long )no_filenames);
    __CrestLoad(47931, (unsigned long )0, (long long )0);
    __CrestApply2(47930, 12, (long long )(no_filenames == 0));
# 994 "grep.c"
    if (no_filenames == 0) {
      __CrestBranch(47933, 14551, 1);
      __CrestLoad(47935, (unsigned long )0, (long long )1);
      __CrestStore(47936, (unsigned long )(& out_file));
# 995 "grep.c"
      out_file = 1;
    } else {
      __CrestBranch(47934, 14552, 0);
# 994 "grep.c"
      goto _L;
    }
    }
  } else {
    __CrestBranch(47929, 14553, 0);
    _L:
    {
    __CrestLoad(47939, (unsigned long )(& with_filenames), (long long )with_filenames);
    __CrestLoad(47938, (unsigned long )0, (long long )0);
    __CrestApply2(47937, 13, (long long )(with_filenames != 0));
# 994 "grep.c"
    if (with_filenames != 0) {
      __CrestBranch(47940, 14554, 1);
      __CrestLoad(47942, (unsigned long )0, (long long )1);
      __CrestStore(47943, (unsigned long )(& out_file));
# 995 "grep.c"
      out_file = 1;
    } else {
      __CrestBranch(47941, 14555, 0);

    }
    }
  }
  __CrestLoad(47944, (unsigned long )0, (long long )1);
  __CrestStore(47945, (unsigned long )(& status));
# 997 "grep.c"
  status = 1;
  __CrestLoad(47948, (unsigned long )(& optind), (long long )optind);
  __CrestLoad(47947, (unsigned long )(& argc), (long long )argc);
  __CrestApply2(47946, 16, (long long )(optind < argc));
# 999 "grep.c"
  if (optind < argc) {
    __CrestBranch(47949, 14558, 1);
    {
# 1000 "grep.c"
    while (1) {
      while_continue___2: ;
      {
      __CrestLoad(47953, (unsigned long )(& optind), (long long )optind);
      __CrestLoad(47952, (unsigned long )(& argc), (long long )argc);
      __CrestApply2(47951, 16, (long long )(optind < argc));
# 1000 "grep.c"
      if (optind < argc) {
        __CrestBranch(47954, 14562, 1);

      } else {
        __CrestBranch(47955, 14563, 0);
# 1000 "grep.c"
        goto while_break___2;
      }
      }
# 1002 "grep.c"
      mem_61 = argv + optind;
# 1002 "grep.c"
      tmp___27 = strcmp((char const *)*mem_61, "-");
      __CrestHandleReturn(47957, (long long )tmp___27);
      __CrestStore(47956, (unsigned long )(& tmp___27));
      {
      __CrestLoad(47960, (unsigned long )(& tmp___27), (long long )tmp___27);
      __CrestLoad(47959, (unsigned long )0, (long long )0);
      __CrestApply2(47958, 12, (long long )(tmp___27 == 0));
# 1002 "grep.c"
      if (tmp___27 == 0) {
        __CrestBranch(47961, 14566, 1);
        __CrestLoad(47963, (unsigned long )0, (long long )5);
# 1004 "grep.c"
        filename = dcgettext((char const *)((void *)0), "(standard input)", 5);
        __CrestClearStack(47964);
        __CrestLoad(47965, (unsigned long )0, (long long )0);
        __CrestStore(47966, (unsigned long )(& desc));
# 1005 "grep.c"
        desc = 0;
      } else {
        __CrestBranch(47962, 14567, 0);
# 1017 "grep.c"
        mem_62 = argv + optind;
# 1017 "grep.c"
        filename = *mem_62;
# 1018 "grep.c"
        mem_63 = argv + optind;
        __CrestLoad(47967, (unsigned long )0, (long long )0);
# 1018 "grep.c"
        desc = open((char const *)*mem_63, 0);
        __CrestHandleReturn(47969, (long long )desc);
        __CrestStore(47968, (unsigned long )(& desc));
      }
      }
      {
      __CrestLoad(47972, (unsigned long )(& desc), (long long )desc);
      __CrestLoad(47971, (unsigned long )0, (long long )0);
      __CrestApply2(47970, 16, (long long )(desc < 0));
# 1020 "grep.c"
      if (desc < 0) {
        __CrestBranch(47973, 14569, 1);
        {
        __CrestLoad(47977, (unsigned long )(& suppress_errors), (long long )suppress_errors);
        __CrestLoad(47976, (unsigned long )0, (long long )0);
        __CrestApply2(47975, 12, (long long )(suppress_errors == 0));
# 1022 "grep.c"
        if (suppress_errors == 0) {
          __CrestBranch(47978, 14570, 1);
# 1023 "grep.c"
          tmp___28 = __errno_location();
          __CrestClearStack(47980);
# 1023 "grep.c"
          mem_64 = argv + optind;
          __CrestLoad(47981, (unsigned long )tmp___28, (long long )*tmp___28);
# 1023 "grep.c"
          error((char const *)*mem_64, *tmp___28);
          __CrestClearStack(47982);
        } else {
          __CrestBranch(47979, 14571, 0);

        }
        }
      } else {
        __CrestBranch(47974, 14572, 0);
        __CrestLoad(47983, (unsigned long )(& desc), (long long )desc);
# 1033 "grep.c"
        count = grep(desc);
        __CrestHandleReturn(47985, (long long )count);
        __CrestStore(47984, (unsigned long )(& count));
        {
        __CrestLoad(47988, (unsigned long )(& count_matches), (long long )count_matches);
        __CrestLoad(47987, (unsigned long )0, (long long )0);
        __CrestApply2(47986, 13, (long long )(count_matches != 0));
# 1034 "grep.c"
        if (count_matches != 0) {
          __CrestBranch(47989, 14574, 1);
          {
          __CrestLoad(47993, (unsigned long )(& out_file), (long long )out_file);
          __CrestLoad(47992, (unsigned long )0, (long long )0);
          __CrestApply2(47991, 13, (long long )(out_file != 0));
# 1036 "grep.c"
          if (out_file != 0) {
            __CrestBranch(47994, 14575, 1);
# 1037 "grep.c"
            printf((char const * __restrict )"%s:", filename);
            __CrestClearStack(47996);
          } else {
            __CrestBranch(47995, 14576, 0);

          }
          }
          __CrestLoad(47997, (unsigned long )(& count), (long long )count);
# 1038 "grep.c"
          printf((char const * __restrict )"%d\n", count);
          __CrestClearStack(47998);
        } else {
          __CrestBranch(47990, 14578, 0);

        }
        }
        {
        __CrestLoad(48001, (unsigned long )(& count), (long long )count);
        __CrestLoad(48000, (unsigned long )0, (long long )0);
        __CrestApply2(47999, 13, (long long )(count != 0));
# 1040 "grep.c"
        if (count != 0) {
          __CrestBranch(48002, 14580, 1);
          __CrestLoad(48004, (unsigned long )0, (long long )0);
          __CrestStore(48005, (unsigned long )(& status));
# 1042 "grep.c"
          status = 0;
          {
          __CrestLoad(48008, (unsigned long )(& list_files), (long long )list_files);
          __CrestLoad(48007, (unsigned long )0, (long long )1);
          __CrestApply2(48006, 12, (long long )(list_files == 1));
# 1043 "grep.c"
          if (list_files == 1) {
            __CrestBranch(48009, 14582, 1);
# 1044 "grep.c"
            printf((char const * __restrict )"%s\n", filename);
            __CrestClearStack(48011);
          } else {
            __CrestBranch(48010, 14583, 0);

          }
          }
        } else {
          __CrestBranch(48003, 14584, 0);
          {
          __CrestLoad(48014, (unsigned long )(& list_files), (long long )list_files);
          __CrestLoad(48013, (unsigned long )0, (long long )-1);
          __CrestApply2(48012, 12, (long long )(list_files == -1));
# 1046 "grep.c"
          if (list_files == -1) {
            __CrestBranch(48015, 14585, 1);
# 1047 "grep.c"
            printf((char const * __restrict )"%s\n", filename);
            __CrestClearStack(48017);
          } else {
            __CrestBranch(48016, 14586, 0);

          }
          }
        }
        }
        {
        __CrestLoad(48020, (unsigned long )(& desc), (long long )desc);
        __CrestLoad(48019, (unsigned long )0, (long long )0);
        __CrestApply2(48018, 13, (long long )(desc != 0));
# 1048 "grep.c"
        if (desc != 0) {
          __CrestBranch(48021, 14588, 1);
          __CrestLoad(48023, (unsigned long )(& desc), (long long )desc);
# 1049 "grep.c"
          close(desc);
          __CrestClearStack(48024);
        } else {
          __CrestBranch(48022, 14589, 0);

        }
        }
      }
      }
      __CrestLoad(48027, (unsigned long )(& optind), (long long )optind);
      __CrestLoad(48026, (unsigned long )0, (long long )1);
      __CrestApply2(48025, 0, (long long )(optind + 1));
      __CrestStore(48028, (unsigned long )(& optind));
# 1051 "grep.c"
      optind ++;
    }
    while_break___2: ;
    }
  } else {
    __CrestBranch(47950, 14592, 0);
    __CrestLoad(48029, (unsigned long )0, (long long )5);
# 1055 "grep.c"
    filename = dcgettext((char const *)((void *)0), "(standard input)", 5);
    __CrestClearStack(48030);
    __CrestLoad(48031, (unsigned long )0, (long long )0);
# 1060 "grep.c"
    count = grep(0);
    __CrestHandleReturn(48033, (long long )count);
    __CrestStore(48032, (unsigned long )(& count));
    {
    __CrestLoad(48036, (unsigned long )(& count_matches), (long long )count_matches);
    __CrestLoad(48035, (unsigned long )0, (long long )0);
    __CrestApply2(48034, 13, (long long )(count_matches != 0));
# 1061 "grep.c"
    if (count_matches != 0) {
      __CrestBranch(48037, 14594, 1);
      __CrestLoad(48039, (unsigned long )(& count), (long long )count);
# 1062 "grep.c"
      printf((char const * __restrict )"%d\n", count);
      __CrestClearStack(48040);
    } else {
      __CrestBranch(48038, 14595, 0);

    }
    }
    {
    __CrestLoad(48043, (unsigned long )(& count), (long long )count);
    __CrestLoad(48042, (unsigned long )0, (long long )0);
    __CrestApply2(48041, 13, (long long )(count != 0));
# 1063 "grep.c"
    if (count != 0) {
      __CrestBranch(48044, 14597, 1);
      __CrestLoad(48046, (unsigned long )0, (long long )0);
      __CrestStore(48047, (unsigned long )(& status));
# 1065 "grep.c"
      status = 0;
      {
      __CrestLoad(48050, (unsigned long )(& list_files), (long long )list_files);
      __CrestLoad(48049, (unsigned long )0, (long long )1);
      __CrestApply2(48048, 12, (long long )(list_files == 1));
# 1066 "grep.c"
      if (list_files == 1) {
        __CrestBranch(48051, 14599, 1);
        __CrestLoad(48053, (unsigned long )0, (long long )5);
# 1067 "grep.c"
        tmp___29 = dcgettext((char const *)((void *)0), "(standard input)\n", 5);
        __CrestClearStack(48054);
# 1067 "grep.c"
        printf((char const * __restrict )tmp___29);
        __CrestClearStack(48055);
      } else {
        __CrestBranch(48052, 14600, 0);

      }
      }
    } else {
      __CrestBranch(48045, 14601, 0);
      {
      __CrestLoad(48058, (unsigned long )(& list_files), (long long )list_files);
      __CrestLoad(48057, (unsigned long )0, (long long )-1);
      __CrestApply2(48056, 12, (long long )(list_files == -1));
# 1069 "grep.c"
      if (list_files == -1) {
        __CrestBranch(48059, 14602, 1);
        __CrestLoad(48061, (unsigned long )0, (long long )5);
# 1070 "grep.c"
        tmp___30 = dcgettext((char const *)((void *)0), "(standard input)\n", 5);
        __CrestClearStack(48062);
# 1070 "grep.c"
        printf((char const * __restrict )tmp___30);
        __CrestClearStack(48063);
      } else {
        __CrestBranch(48060, 14603, 0);

      }
      }
    }
    }
  }
# 1073 "grep.c"
  tmp___33 = fclose(stdout);
  __CrestHandleReturn(48065, (long long )tmp___33);
  __CrestStore(48064, (unsigned long )(& tmp___33));
  __CrestLoad(48068, (unsigned long )(& tmp___33), (long long )tmp___33);
  __CrestLoad(48067, (unsigned long )0, (long long )-1);
  __CrestApply2(48066, 12, (long long )(tmp___33 == -1));
# 1073 "grep.c"
  if (tmp___33 == -1) {
    __CrestBranch(48069, 14606, 1);
# 1074 "grep.c"
    tmp___31 = __errno_location();
    __CrestClearStack(48071);
    __CrestLoad(48072, (unsigned long )0, (long long )5);
# 1074 "grep.c"
    tmp___32 = dcgettext((char const *)((void *)0), "writing output", 5);
    __CrestClearStack(48073);
    __CrestLoad(48074, (unsigned long )tmp___31, (long long )*tmp___31);
# 1074 "grep.c"
    error((char const *)tmp___32, *tmp___31);
    __CrestClearStack(48075);
  } else {
    __CrestBranch(48070, 14607, 0);

  }
  __CrestLoad(48078, (unsigned long )(& errseen), (long long )errseen);
  __CrestLoad(48077, (unsigned long )0, (long long )0);
  __CrestApply2(48076, 13, (long long )(errseen != 0));
# 1076 "grep.c"
  if (errseen != 0) {
    __CrestBranch(48079, 14609, 1);
    __CrestLoad(48081, (unsigned long )0, (long long )2);
    __CrestStore(48082, (unsigned long )(& tmp___34));
# 1076 "grep.c"
    tmp___34 = 2;
  } else {
    __CrestBranch(48080, 14610, 0);
    __CrestLoad(48083, (unsigned long )(& status), (long long )status);
    __CrestStore(48084, (unsigned long )(& tmp___34));
# 1076 "grep.c"
    tmp___34 = status;
  }
  __CrestLoad(48085, (unsigned long )(& tmp___34), (long long )tmp___34);
# 1076 "grep.c"
  exit(tmp___34);
  __CrestClearStack(48086);
  __CrestLoad(48087, (unsigned long )(& __retres65), (long long )__retres65);
  __CrestReturn(48088);
# 716 "grep.c"
  return (__retres65);
}
}
void __globinit_grep(void)
{


  {
  __CrestInit();
}
}
