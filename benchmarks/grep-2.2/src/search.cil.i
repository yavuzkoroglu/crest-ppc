# 1 "./search.cil.c"
# 1 "/home/yavuz/crest/benchmarks/grep-2.2/src//"
# 1 "<command-line>"
# 1 "/usr/include/stdc-predef.h" 1 3 4
# 1 "<command-line>" 2
# 1 "./search.cil.c"



extern void __CrestInit(void) __attribute__((__crest_skip__)) ;
extern void __CrestHandleReturn(int id , long long val ) __attribute__((__crest_skip__)) ;
extern void __CrestReturn(int id ) __attribute__((__crest_skip__)) ;
extern void __CrestCall(int id , unsigned int fid ) __attribute__((__crest_skip__)) ;
extern void __CrestBranch(int id , int bid , unsigned char b ) __attribute__((__crest_skip__)) ;
extern void __CrestApply2(int id , int op , long long val ) __attribute__((__crest_skip__)) ;
extern void __CrestApply1(int id , int op , long long val ) __attribute__((__crest_skip__)) ;
extern void __CrestClearStack(int id ) __attribute__((__crest_skip__)) ;
extern void __CrestStore(int id , unsigned long addr ) __attribute__((__crest_skip__)) ;
extern void __CrestLoad(int id , unsigned long addr , long long val ) __attribute__((__crest_skip__)) ;
# 212 "/usr/lib/gcc/x86_64-linux-gnu/4.8/include/stddef.h"
typedef unsigned long size_t;
# 23 "system.h"
typedef void *ptr_t;
# 27 "grep.h"
struct matcher {
   char *name ;
   void (*compile)(char * , size_t ) ;
   char *(*execute)(char * , size_t , char ** ) ;
};
# 53 "regex.h"
typedef unsigned long reg_syntax_t;
# 320 "regex.h"
struct re_pattern_buffer {
   unsigned char *buffer ;
   unsigned long allocated ;
   unsigned long used ;
   reg_syntax_t syntax ;
   char *fastmap ;
   char *translate ;
   size_t re_nsub ;
   unsigned int can_be_null : 1 ;
   unsigned int regs_allocated : 2 ;
   unsigned int fastmap_accurate : 1 ;
   unsigned int no_sub : 1 ;
   unsigned int not_bol : 1 ;
   unsigned int not_eol : 1 ;
   unsigned int newline_anchor : 1 ;
};
# 391 "regex.h"
typedef int regoff_t;
# 396 "regex.h"
struct re_registers {
   unsigned int num_regs ;
   regoff_t *start ;
   regoff_t *end ;
};
# 57 "dfa.h"
typedef int charclass[(((unsigned long )(1 << 8) + 8UL * sizeof(int )) - 1UL) / (8UL * sizeof(int ))];
# 63 "dfa.h"
enum __anonenum_token_27 {
    END = -1,
    EMPTY = 256,
    BACKREF = 257,
    BEGLINE = 258,
    ENDLINE = 259,
    BEGWORD = 260,
    ENDWORD = 261,
    LIMWORD = 262,
    NOTLIMWORD = 263,
    QMARK = 264,
    STAR = 265,
    PLUS = 266,
    REPMN = 267,
    CAT = 268,
    OR = 269,
    ORTOP = 270,
    LPAREN = 271,
    RPAREN = 272,
    CSET = 273
} ;
# 63 "dfa.h"
typedef enum __anonenum_token_27 token;
# 201 "dfa.h"
struct __anonstruct_position_28 {
   unsigned int index ;
   unsigned int constraint ;
};
# 201 "dfa.h"
typedef struct __anonstruct_position_28 position;
# 208 "dfa.h"
struct __anonstruct_position_set_29 {
   position *elems ;
   int nelem ;
};
# 208 "dfa.h"
typedef struct __anonstruct_position_set_29 position_set;
# 217 "dfa.h"
struct __anonstruct_dfa_state_30 {
   int hash ;
   position_set elems ;
   char newline ;
   char letter ;
   char backref ;
   unsigned char constraint ;
   int first_end ;
};
# 217 "dfa.h"
typedef struct __anonstruct_dfa_state_30 dfa_state;
# 230 "dfa.h"
struct dfamust {
   int exact ;
   char *must ;
   struct dfamust *next ;
};
# 238 "dfa.h"
struct dfa {
   charclass *charclasses ;
   int cindex ;
   int calloc ;
   token *tokens ;
   int tindex ;
   int talloc ;
   int depth ;
   int nleaves ;
   int nregexps ;
   dfa_state *states ;
   int sindex ;
   int salloc ;
   position_set *follows ;
   int searchflag ;
   int tralloc ;
   int trcount ;
   int **trans ;
   int **realtrans ;
   int **fails ;
   int *success ;
   int *newlines ;
   struct dfamust *musts ;
};
# 23 "kwset.h"
struct kwsmatch {
   int index ;
   char *beg[1] ;
   size_t size[1] ;
};
# 30 "kwset.h"
typedef ptr_t kwset_t;
# 466 "/usr/include/stdlib.h"
extern __attribute__((__nothrow__)) void *( __attribute__((__leaf__)) malloc)(size_t __size ) __attribute__((__malloc__)) ;
# 46 "/usr/include/string.h"
extern __attribute__((__nothrow__)) void *( __attribute__((__nonnull__(1,2), __leaf__)) memcpy)(void * __restrict __dest ,
                                                                                                 void const * __restrict __src ,
                                                                                                 size_t __n ) ;
# 96 "/usr/include/string.h"
extern __attribute__((__nothrow__)) void *( __attribute__((__nonnull__(1), __leaf__)) memchr)(void const *__s ,
                                                                                               int __c ,
                                                                                               size_t __n ) __attribute__((__pure__)) ;
# 129 "/usr/include/string.h"
extern __attribute__((__nothrow__)) char *( __attribute__((__nonnull__(1,2), __leaf__)) strcpy)(char * __restrict __dest ,
                                                                                                 char const * __restrict __src ) ;
# 144 "/usr/include/string.h"
extern __attribute__((__nothrow__)) int ( __attribute__((__nonnull__(1,2), __leaf__)) strcmp)(char const *__s1 ,
                                                                                               char const *__s2 ) __attribute__((__pure__)) ;
# 399 "/usr/include/string.h"
extern __attribute__((__nothrow__)) size_t ( __attribute__((__nonnull__(1), __leaf__)) strlen)(char const *__s ) __attribute__((__pure__)) ;
# 79 "/usr/include/ctype.h"
extern __attribute__((__nothrow__)) unsigned short const **( __attribute__((__leaf__)) __ctype_b_loc)(void) __attribute__((__const__)) ;
# 124 "/usr/include/ctype.h"
extern __attribute__((__nothrow__)) int ( __attribute__((__leaf__)) tolower)(int __c ) ;
# 19 "grep.h"
extern void fatal(char const * , int ) ;
# 27 "grep.h"
struct matcher matchers[10] ;
# 35 "grep.h"
extern char *matcher ;
# 39 "grep.h"
extern int match_icase ;
# 40 "grep.h"
extern int match_words ;
# 41 "grep.h"
extern int match_lines ;
# 441 "regex.h"
extern reg_syntax_t re_set_syntax(reg_syntax_t syntax ) ;
# 446 "regex.h"
extern char const *re_compile_pattern(char const *pattern , size_t length , struct re_pattern_buffer *buffer ) ;
# 462 "regex.h"
extern int re_search(struct re_pattern_buffer *buffer , char const *string , int length ,
                     int start , int range , struct re_registers *regs ) ;
# 477 "regex.h"
extern int re_match(struct re_pattern_buffer *buffer , char const *string , int length ,
                    int start , struct re_registers *regs ) ;
# 325 "dfa.h"
extern void dfasyntax(reg_syntax_t , int ) ;
# 330 "dfa.h"
extern void dfacomp(char * , size_t , struct dfa * , int ) ;
# 344 "dfa.h"
extern char *dfaexec(struct dfa * , char * , char * , int , int * , int * ) ;
# 352 "dfa.h"
extern void dfainit(struct dfa * ) ;
# 371 "dfa.h"
void dfaerror(char const *mesg ) ;
# 36 "kwset.h"
extern kwset_t kwsalloc(char * ) ;
# 41 "kwset.h"
extern char *kwsincr(kwset_t , char * , size_t ) ;
# 45 "kwset.h"
extern char *kwsprep(kwset_t ) ;
# 53 "kwset.h"
extern char *kwsexec(kwset_t , char * , size_t , struct kwsmatch * ) ;
# 33 "search.c"
static void Gcompile(char *pattern , size_t size ) ;
# 34 "search.c"
static void Ecompile(char *pattern , size_t size ) ;
# 35 "search.c"
static char *EGexecute(char *buf , size_t size , char **endp ) ;
# 36 "search.c"
static void Fcompile(char *pattern , size_t size ) ;
# 37 "search.c"
static char *Fexecute(char *buf , size_t size , char **endp ) ;
# 38 "search.c"
static void kwsinit(void) ;
# 41 "search.c"
struct matcher matchers[10] =
# 41 "search.c"
  { {(char *)"default", & Gcompile, & EGexecute},
        {(char *)"grep", & Gcompile, & EGexecute},
        {(char *)"ggrep", & Gcompile, & EGexecute},
        {(char *)"egrep", & Ecompile, & EGexecute},
        {(char *)"posix-egrep", & Ecompile, & EGexecute},
        {(char *)"awk", & Ecompile, & EGexecute},
        {(char *)"gegrep", & Ecompile, & EGexecute},
        {(char *)"fgrep", & Fcompile, & Fexecute},
        {(char *)"gfgrep", & Fcompile, & Fexecute},
        {(char *)0, (void (*)(char * , size_t ))0, (char *(*)(char * , size_t , char ** ))0}};
# 58 "search.c"
static struct dfa dfa ;
# 61 "search.c"
static struct re_pattern_buffer regex ;
# 66 "search.c"
static kwset_t kwset ;
# 71 "search.c"
static int lastexact ;
# 73 "search.c"
void dfaerror(char const *mesg )
{


  {
  __CrestCall(34232, 283);

  __CrestLoad(34233, (unsigned long )0, (long long )0);
# 77 "search.c"
  fatal(mesg, 0);
  __CrestClearStack(34234);

  {
  __CrestReturn(34235);
# 73 "search.c"
  return;
  }
}
}
# 83 "search.c"
static char trans[256] ;
# 80 "search.c"
static void kwsinit(void)
{
  int i ;
  int tmp___0 ;
  unsigned short const **tmp___1 ;
  char *tmp___2 ;
  unsigned short const *mem_6 ;

  {
  __CrestCall(34236, 284);

  {
  __CrestLoad(34239, (unsigned long )(& match_icase), (long long )match_icase);
  __CrestLoad(34238, (unsigned long )0, (long long )0);
  __CrestApply2(34237, 13, (long long )(match_icase != 0));
# 86 "search.c"
  if (match_icase != 0) {
    __CrestBranch(34240, 7341, 1);
    __CrestLoad(34242, (unsigned long )0, (long long )0);
    __CrestStore(34243, (unsigned long )(& i));
# 87 "search.c"
    i = 0;
    {
# 87 "search.c"
    while (1) {
      while_continue: ;
      {
      __CrestLoad(34246, (unsigned long )(& i), (long long )i);
      __CrestLoad(34245, (unsigned long )0, (long long )256);
      __CrestApply2(34244, 16, (long long )(i < 256));
# 87 "search.c"
      if (i < 256) {
        __CrestBranch(34247, 7346, 1);

      } else {
        __CrestBranch(34248, 7347, 0);
# 87 "search.c"
        goto while_break;
      }
      }
      {
      __CrestLoad(34253, (unsigned long )(& i), (long long )i);
      __CrestLoad(34252, (unsigned long )0, (long long )-128);
      __CrestApply2(34251, 5, (long long )(i & -128));
      __CrestLoad(34250, (unsigned long )0, (long long )0);
      __CrestApply2(34249, 12, (long long )((i & -128) == 0));
# 88 "search.c"
      if ((i & -128) == 0) {
        __CrestBranch(34254, 7349, 1);
# 88 "search.c"
        tmp___1 = __ctype_b_loc();
        __CrestClearStack(34256);
        {
# 88 "search.c"
        mem_6 = *tmp___1 + i;
        {
        __CrestLoad(34261, (unsigned long )mem_6, (long long )*mem_6);
        __CrestLoad(34260, (unsigned long )0, (long long )256);
        __CrestApply2(34259, 5, (long long )((int const )*mem_6 & 256));
        __CrestLoad(34258, (unsigned long )0, (long long )0);
        __CrestApply2(34257, 13, (long long )(((int const )*mem_6 & 256) != 0));
# 88 "search.c"
        if (((int const )*mem_6 & 256) != 0) {
          __CrestBranch(34262, 7353, 1);
          __CrestLoad(34264, (unsigned long )(& i), (long long )i);
# 88 "search.c"
          tmp___0 = tolower(i);
          __CrestHandleReturn(34266, (long long )tmp___0);
          __CrestStore(34265, (unsigned long )(& tmp___0));
          __CrestLoad(34267, (unsigned long )(& tmp___0), (long long )tmp___0);
          __CrestStore(34268, (unsigned long )(& trans[i]));
# 88 "search.c"
          trans[i] = (char )tmp___0;
        } else {
          __CrestBranch(34263, 7354, 0);
          __CrestLoad(34269, (unsigned long )(& i), (long long )i);
          __CrestStore(34270, (unsigned long )(& trans[i]));
# 88 "search.c"
          trans[i] = (char )i;
        }
        }
        }
      } else {
        __CrestBranch(34255, 7355, 0);
        __CrestLoad(34271, (unsigned long )(& i), (long long )i);
        __CrestStore(34272, (unsigned long )(& trans[i]));
# 88 "search.c"
        trans[i] = (char )i;
      }
      }
      __CrestLoad(34275, (unsigned long )(& i), (long long )i);
      __CrestLoad(34274, (unsigned long )0, (long long )1);
      __CrestApply2(34273, 0, (long long )(i + 1));
      __CrestStore(34276, (unsigned long )(& i));
# 87 "search.c"
      i ++;
    }
    while_break: ;
    }
  } else {
    __CrestBranch(34241, 7358, 0);

  }
  }
  {
  __CrestLoad(34279, (unsigned long )(& match_icase), (long long )match_icase);
  __CrestLoad(34278, (unsigned long )0, (long long )0);
  __CrestApply2(34277, 13, (long long )(match_icase != 0));
# 90 "search.c"
  if (match_icase != 0) {
    __CrestBranch(34280, 7360, 1);
# 90 "search.c"
    tmp___2 = trans;
  } else {
    __CrestBranch(34281, 7361, 0);
# 90 "search.c"
    tmp___2 = (char *)0;
  }
  }
# 90 "search.c"
  kwset = kwsalloc(tmp___2);
  __CrestClearStack(34282);
  {
  __CrestLoad(34285, (unsigned long )(& kwset), (long long )((unsigned long )kwset));
  __CrestLoad(34284, (unsigned long )0, (long long )0);
  __CrestApply2(34283, 13, (long long )(kwset != 0));
# 90 "search.c"
  if (kwset != 0) {
    __CrestBranch(34286, 7364, 1);

  } else {
    __CrestBranch(34287, 7365, 0);
    __CrestLoad(34288, (unsigned long )0, (long long )0);
# 91 "search.c"
    fatal("memory exhausted", 0);
    __CrestClearStack(34289);
  }
  }

  {
  __CrestReturn(34290);
# 80 "search.c"
  return;
  }
}
}
# 98 "search.c"
static void kwsmusts(void)
{
  struct dfamust *dm ;
  char *err ;
  size_t tmp ;
  size_t tmp___0 ;

  {
  __CrestCall(34291, 285);

  {
  __CrestLoad(34294, (unsigned long )(& dfa.musts), (long long )((unsigned long )dfa.musts));
  __CrestLoad(34293, (unsigned long )0, (long long )0);
  __CrestApply2(34292, 13, (long long )(dfa.musts != 0));
# 104 "search.c"
  if (dfa.musts != 0) {
    __CrestBranch(34295, 7369, 1);
# 106 "search.c"
    kwsinit();
    __CrestClearStack(34297);
# 110 "search.c"
    dm = dfa.musts;
    {
# 110 "search.c"
    while (1) {
      while_continue: ;
      {
      __CrestLoad(34300, (unsigned long )(& dm), (long long )((unsigned long )dm));
      __CrestLoad(34299, (unsigned long )0, (long long )0);
      __CrestApply2(34298, 13, (long long )(dm != 0));
# 110 "search.c"
      if (dm != 0) {
        __CrestBranch(34301, 7374, 1);

      } else {
        __CrestBranch(34302, 7375, 0);
# 110 "search.c"
        goto while_break;
      }
      }
      {
      __CrestLoad(34305, (unsigned long )(& dm->exact), (long long )dm->exact);
      __CrestLoad(34304, (unsigned long )0, (long long )0);
      __CrestApply2(34303, 12, (long long )(dm->exact == 0));
# 112 "search.c"
      if (dm->exact == 0) {
        __CrestBranch(34306, 7377, 1);
# 113 "search.c"
        goto __Cont;
      } else {
        __CrestBranch(34307, 7378, 0);

      }
      }
      __CrestLoad(34310, (unsigned long )(& lastexact), (long long )lastexact);
      __CrestLoad(34309, (unsigned long )0, (long long )1);
      __CrestApply2(34308, 0, (long long )(lastexact + 1));
      __CrestStore(34311, (unsigned long )(& lastexact));
# 114 "search.c"
      lastexact ++;
# 115 "search.c"
      tmp = strlen((char const *)dm->must);
      __CrestHandleReturn(34313, (long long )tmp);
      __CrestStore(34312, (unsigned long )(& tmp));
      __CrestLoad(34314, (unsigned long )(& tmp), (long long )tmp);
# 115 "search.c"
      err = kwsincr(kwset, dm->must, tmp);
      __CrestClearStack(34315);
      {
      __CrestLoad(34318, (unsigned long )(& err), (long long )((unsigned long )err));
      __CrestLoad(34317, (unsigned long )0, (long long )((unsigned long )((char *)0)));
      __CrestApply2(34316, 13, (long long )((unsigned long )err != (unsigned long )((char *)0)));
# 115 "search.c"
      if ((unsigned long )err != (unsigned long )((char *)0)) {
        __CrestBranch(34319, 7381, 1);
        __CrestLoad(34321, (unsigned long )0, (long long )0);
# 116 "search.c"
        fatal((char const *)err, 0);
        __CrestClearStack(34322);
      } else {
        __CrestBranch(34320, 7382, 0);

      }
      }
      __Cont:
# 110 "search.c"
      dm = dm->next;
    }
    while_break: ;
    }
# 120 "search.c"
    dm = dfa.musts;
    {
# 120 "search.c"
    while (1) {
      while_continue___0: ;
      {
      __CrestLoad(34325, (unsigned long )(& dm), (long long )((unsigned long )dm));
      __CrestLoad(34324, (unsigned long )0, (long long )0);
      __CrestApply2(34323, 13, (long long )(dm != 0));
# 120 "search.c"
      if (dm != 0) {
        __CrestBranch(34326, 7390, 1);

      } else {
        __CrestBranch(34327, 7391, 0);
# 120 "search.c"
        goto while_break___0;
      }
      }
      {
      __CrestLoad(34330, (unsigned long )(& dm->exact), (long long )dm->exact);
      __CrestLoad(34329, (unsigned long )0, (long long )0);
      __CrestApply2(34328, 13, (long long )(dm->exact != 0));
# 122 "search.c"
      if (dm->exact != 0) {
        __CrestBranch(34331, 7393, 1);
# 123 "search.c"
        goto __Cont___0;
      } else {
        __CrestBranch(34332, 7394, 0);

      }
      }
# 124 "search.c"
      tmp___0 = strlen((char const *)dm->must);
      __CrestHandleReturn(34334, (long long )tmp___0);
      __CrestStore(34333, (unsigned long )(& tmp___0));
      __CrestLoad(34335, (unsigned long )(& tmp___0), (long long )tmp___0);
# 124 "search.c"
      err = kwsincr(kwset, dm->must, tmp___0);
      __CrestClearStack(34336);
      {
      __CrestLoad(34339, (unsigned long )(& err), (long long )((unsigned long )err));
      __CrestLoad(34338, (unsigned long )0, (long long )((unsigned long )((char *)0)));
      __CrestApply2(34337, 13, (long long )((unsigned long )err != (unsigned long )((char *)0)));
# 124 "search.c"
      if ((unsigned long )err != (unsigned long )((char *)0)) {
        __CrestBranch(34340, 7397, 1);
        __CrestLoad(34342, (unsigned long )0, (long long )0);
# 125 "search.c"
        fatal((char const *)err, 0);
        __CrestClearStack(34343);
      } else {
        __CrestBranch(34341, 7398, 0);

      }
      }
      __Cont___0:
# 120 "search.c"
      dm = dm->next;
    }
    while_break___0: ;
    }
# 127 "search.c"
    err = kwsprep(kwset);
    __CrestClearStack(34344);
    {
    __CrestLoad(34347, (unsigned long )(& err), (long long )((unsigned long )err));
    __CrestLoad(34346, (unsigned long )0, (long long )((unsigned long )((char *)0)));
    __CrestApply2(34345, 13, (long long )((unsigned long )err != (unsigned long )((char *)0)));
# 127 "search.c"
    if ((unsigned long )err != (unsigned long )((char *)0)) {
      __CrestBranch(34348, 7403, 1);
      __CrestLoad(34350, (unsigned long )0, (long long )0);
# 128 "search.c"
      fatal((char const *)err, 0);
      __CrestClearStack(34351);
    } else {
      __CrestBranch(34349, 7404, 0);

    }
    }
  } else {
    __CrestBranch(34296, 7405, 0);

  }
  }

  {
  __CrestReturn(34352);
# 98 "search.c"
  return;
  }
}
}
# 132 "search.c"
static void Gcompile(char *pattern , size_t size )
{
  char const *err ;
  char *n ;
  void *tmp ;
  int i ;
  size_t tmp___0 ;
  size_t tmp___1 ;

  {
  __CrestCall(34354, 286);
  __CrestStore(34353, (unsigned long )(& size));
  __CrestLoad(34355, (unsigned long )0, (long long )((((((1UL << 1) | ((1UL << 1) << 1)) | ((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1)) | (((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1)) | (((((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1)) | ((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1)));
# 139 "search.c"
  re_set_syntax((((((1UL << 1) | ((1UL << 1) << 1)) | ((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1)) | (((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1)) | (((((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1)) | ((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1));
  __CrestClearStack(34356);
  __CrestLoad(34357, (unsigned long )0, (long long )((((((1UL << 1) | ((1UL << 1) << 1)) | ((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1)) | (((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1)) | (((((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1)) | ((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1)));
  __CrestLoad(34358, (unsigned long )(& match_icase), (long long )match_icase);
# 140 "search.c"
  dfasyntax((((((1UL << 1) | ((1UL << 1) << 1)) | ((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1)) | (((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1)) | (((((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1)) | ((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1),
            match_icase);
  __CrestClearStack(34359);
  __CrestLoad(34360, (unsigned long )(& size), (long long )size);
# 142 "search.c"
  err = re_compile_pattern((char const *)pattern, size, & regex);
  __CrestClearStack(34361);
  {
  __CrestLoad(34364, (unsigned long )(& err), (long long )((unsigned long )err));
  __CrestLoad(34363, (unsigned long )0, (long long )((unsigned long )((char const *)0)));
  __CrestApply2(34362, 13, (long long )((unsigned long )err != (unsigned long )((char const *)0)));
# 142 "search.c"
  if ((unsigned long )err != (unsigned long )((char const *)0)) {
    __CrestBranch(34365, 7410, 1);
    __CrestLoad(34367, (unsigned long )0, (long long )0);
# 143 "search.c"
    fatal(err, 0);
    __CrestClearStack(34368);
  } else {
    __CrestBranch(34366, 7411, 0);

  }
  }
# 145 "search.c"
  dfainit(& dfa);
  __CrestClearStack(34369);
  {
  __CrestLoad(34372, (unsigned long )(& match_words), (long long )match_words);
  __CrestLoad(34371, (unsigned long )0, (long long )0);
  __CrestApply2(34370, 13, (long long )(match_words != 0));
# 151 "search.c"
  if (match_words != 0) {
    __CrestBranch(34373, 7414, 1);
# 151 "search.c"
    goto _L;
  } else {
    __CrestBranch(34374, 7415, 0);
    {
    __CrestLoad(34377, (unsigned long )(& match_lines), (long long )match_lines);
    __CrestLoad(34376, (unsigned long )0, (long long )0);
    __CrestApply2(34375, 13, (long long )(match_lines != 0));
# 151 "search.c"
    if (match_lines != 0) {
      __CrestBranch(34378, 7416, 1);
      _L:
      __CrestLoad(34382, (unsigned long )(& size), (long long )size);
      __CrestLoad(34381, (unsigned long )0, (long long )50UL);
      __CrestApply2(34380, 0, (long long )(size + 50UL));
# 159 "search.c"
      tmp = malloc(size + 50UL);
      __CrestClearStack(34383);
# 159 "search.c"
      n = (char *)tmp;
      __CrestLoad(34384, (unsigned long )0, (long long )0);
      __CrestStore(34385, (unsigned long )(& i));
# 160 "search.c"
      i = 0;
# 162 "search.c"
      strcpy((char * __restrict )n, (char const * __restrict )"");
      __CrestClearStack(34386);
      {
      __CrestLoad(34389, (unsigned long )(& match_lines), (long long )match_lines);
      __CrestLoad(34388, (unsigned long )0, (long long )0);
      __CrestApply2(34387, 13, (long long )(match_lines != 0));
# 164 "search.c"
      if (match_lines != 0) {
        __CrestBranch(34390, 7418, 1);
# 165 "search.c"
        strcpy((char * __restrict )n, (char const * __restrict )"^\\(");
        __CrestClearStack(34392);
      } else {
        __CrestBranch(34391, 7419, 0);

      }
      }
      {
      __CrestLoad(34395, (unsigned long )(& match_words), (long long )match_words);
      __CrestLoad(34394, (unsigned long )0, (long long )0);
      __CrestApply2(34393, 13, (long long )(match_words != 0));
# 166 "search.c"
      if (match_words != 0) {
        __CrestBranch(34396, 7421, 1);
# 167 "search.c"
        strcpy((char * __restrict )n, (char const * __restrict )"\\(^\\|[^0-9A-Za-z_]\\)\\(");
        __CrestClearStack(34398);
      } else {
        __CrestBranch(34397, 7422, 0);

      }
      }
# 169 "search.c"
      tmp___0 = strlen((char const *)n);
      __CrestHandleReturn(34400, (long long )tmp___0);
      __CrestStore(34399, (unsigned long )(& tmp___0));
      __CrestLoad(34401, (unsigned long )(& tmp___0), (long long )tmp___0);
      __CrestStore(34402, (unsigned long )(& i));
# 169 "search.c"
      i = (int )tmp___0;
      __CrestLoad(34403, (unsigned long )(& size), (long long )size);
# 170 "search.c"
      memcpy((void * __restrict )(n + i), (void const * __restrict )pattern, size);
      __CrestClearStack(34404);
      __CrestLoad(34407, (unsigned long )(& i), (long long )i);
      __CrestLoad(34406, (unsigned long )(& size), (long long )size);
      __CrestApply2(34405, 0, (long long )((size_t )i + size));
      __CrestStore(34408, (unsigned long )(& i));
# 171 "search.c"
      i = (int )((size_t )i + size);
      {
      __CrestLoad(34411, (unsigned long )(& match_words), (long long )match_words);
      __CrestLoad(34410, (unsigned long )0, (long long )0);
      __CrestApply2(34409, 13, (long long )(match_words != 0));
# 173 "search.c"
      if (match_words != 0) {
        __CrestBranch(34412, 7425, 1);
# 174 "search.c"
        strcpy((char * __restrict )(n + i), (char const * __restrict )"\\)\\([^0-9A-Za-z_]\\|$\\)");
        __CrestClearStack(34414);
      } else {
        __CrestBranch(34413, 7426, 0);

      }
      }
      {
      __CrestLoad(34417, (unsigned long )(& match_lines), (long long )match_lines);
      __CrestLoad(34416, (unsigned long )0, (long long )0);
      __CrestApply2(34415, 13, (long long )(match_lines != 0));
# 175 "search.c"
      if (match_lines != 0) {
        __CrestBranch(34418, 7428, 1);
# 176 "search.c"
        strcpy((char * __restrict )(n + i), (char const * __restrict )"\\)$");
        __CrestClearStack(34420);
      } else {
        __CrestBranch(34419, 7429, 0);

      }
      }
# 178 "search.c"
      tmp___1 = strlen((char const *)(n + i));
      __CrestHandleReturn(34422, (long long )tmp___1);
      __CrestStore(34421, (unsigned long )(& tmp___1));
      __CrestLoad(34425, (unsigned long )(& i), (long long )i);
      __CrestLoad(34424, (unsigned long )(& tmp___1), (long long )tmp___1);
      __CrestApply2(34423, 0, (long long )((size_t )i + tmp___1));
      __CrestStore(34426, (unsigned long )(& i));
# 178 "search.c"
      i = (int )((size_t )i + tmp___1);
      __CrestLoad(34427, (unsigned long )(& i), (long long )i);
      __CrestLoad(34428, (unsigned long )0, (long long )1);
# 179 "search.c"
      dfacomp(n, (size_t )i, & dfa, 1);
      __CrestClearStack(34429);
    } else {
      __CrestBranch(34379, 7431, 0);
      __CrestLoad(34430, (unsigned long )(& size), (long long )size);
      __CrestLoad(34431, (unsigned long )0, (long long )1);
# 182 "search.c"
      dfacomp(pattern, size, & dfa, 1);
      __CrestClearStack(34432);
    }
    }
  }
  }
# 184 "search.c"
  kwsmusts();
  __CrestClearStack(34433);

  {
  __CrestReturn(34434);
# 132 "search.c"
  return;
  }
}
}
# 187 "search.c"
static void Ecompile(char *pattern , size_t size )
{
  char const *err ;
  int tmp ;
  int tmp___0 ;
  char *n ;
  void *tmp___1 ;
  int i ;
  size_t tmp___2 ;
  size_t tmp___3 ;

  {
  __CrestCall(34436, 287);
  __CrestStore(34435, (unsigned long )(& size));
# 194 "search.c"
  tmp___0 = strcmp((char const *)matcher, "posix-egrep");
  __CrestHandleReturn(34438, (long long )tmp___0);
  __CrestStore(34437, (unsigned long )(& tmp___0));
  {
  __CrestLoad(34441, (unsigned long )(& tmp___0), (long long )tmp___0);
  __CrestLoad(34440, (unsigned long )0, (long long )0);
  __CrestApply2(34439, 12, (long long )(tmp___0 == 0));
# 194 "search.c"
  if (tmp___0 == 0) {
    __CrestBranch(34442, 7437, 1);
    __CrestLoad(34444, (unsigned long )0, (long long )((((((((((1UL << 1) << 1) | (((1UL << 1) << 1) << 1)) | ((((1UL << 1) << 1) << 1) << 1)) | ((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1)) | (((((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1)) | (((((((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1)) | (((((((((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1)) | (((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1)) | ((((((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1)));
# 196 "search.c"
    re_set_syntax((((((((((1UL << 1) << 1) | (((1UL << 1) << 1) << 1)) | ((((1UL << 1) << 1) << 1) << 1)) | ((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1)) | (((((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1)) | (((((((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1)) | (((((((((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1)) | (((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1)) | ((((((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1));
    __CrestClearStack(34445);
    __CrestLoad(34446, (unsigned long )0, (long long )((((((((((1UL << 1) << 1) | (((1UL << 1) << 1) << 1)) | ((((1UL << 1) << 1) << 1) << 1)) | ((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1)) | (((((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1)) | (((((((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1)) | (((((((((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1)) | (((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1)) | ((((((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1)));
    __CrestLoad(34447, (unsigned long )(& match_icase), (long long )match_icase);
# 197 "search.c"
    dfasyntax((((((((((1UL << 1) << 1) | (((1UL << 1) << 1) << 1)) | ((((1UL << 1) << 1) << 1) << 1)) | ((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1)) | (((((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1)) | (((((((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1)) | (((((((((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1)) | (((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1)) | ((((((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1),
              match_icase);
    __CrestClearStack(34448);
  } else {
    __CrestBranch(34443, 7438, 0);
# 199 "search.c"
    tmp = strcmp((char const *)matcher, "awk");
    __CrestHandleReturn(34450, (long long )tmp);
    __CrestStore(34449, (unsigned long )(& tmp));
    {
    __CrestLoad(34453, (unsigned long )(& tmp), (long long )tmp);
    __CrestLoad(34452, (unsigned long )0, (long long )0);
    __CrestApply2(34451, 12, (long long )(tmp == 0));
# 199 "search.c"
    if (tmp == 0) {
      __CrestBranch(34454, 7440, 1);
      __CrestLoad(34456, (unsigned long )0, (long long )(((((((((1UL | (((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1)) | (((((((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1)) | ((((((((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1)) | (((((((((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1)) | ((((((((((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1)) | ((((((1UL << 1) << 1) << 1) << 1) << 1) << 1)) | (((1UL << 1) << 1) << 1)) | (((((((((((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1)) | (((((((((((((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1)));
# 201 "search.c"
      re_set_syntax(((((((((1UL | (((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1)) | (((((((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1)) | ((((((((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1)) | (((((((((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1)) | ((((((((((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1)) | ((((((1UL << 1) << 1) << 1) << 1) << 1) << 1)) | (((1UL << 1) << 1) << 1)) | (((((((((((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1)) | (((((((((((((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1));
      __CrestClearStack(34457);
      __CrestLoad(34458, (unsigned long )0, (long long )(((((((((1UL | (((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1)) | (((((((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1)) | ((((((((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1)) | (((((((((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1)) | ((((((((((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1)) | ((((((1UL << 1) << 1) << 1) << 1) << 1) << 1)) | (((1UL << 1) << 1) << 1)) | (((((((((((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1)) | (((((((((((((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1)));
      __CrestLoad(34459, (unsigned long )(& match_icase), (long long )match_icase);
# 202 "search.c"
      dfasyntax(((((((((1UL | (((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1)) | (((((((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1)) | ((((((((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1)) | (((((((((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1)) | ((((((((((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1)) | ((((((1UL << 1) << 1) << 1) << 1) << 1) << 1)) | (((1UL << 1) << 1) << 1)) | (((((((((((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1)) | (((((((((((((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1),
                match_icase);
      __CrestClearStack(34460);
    } else {
      __CrestBranch(34455, 7441, 0);
      __CrestLoad(34461, (unsigned long )0, (long long )((((((((1UL << 1) << 1) | (((1UL << 1) << 1) << 1)) | ((((1UL << 1) << 1) << 1) << 1)) | ((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1)) | (((((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1)) | (((((((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1)) | (((((((((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1)));
# 206 "search.c"
      re_set_syntax((((((((1UL << 1) << 1) | (((1UL << 1) << 1) << 1)) | ((((1UL << 1) << 1) << 1) << 1)) | ((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1)) | (((((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1)) | (((((((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1)) | (((((((((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1));
      __CrestClearStack(34462);
      __CrestLoad(34463, (unsigned long )0, (long long )((((((((1UL << 1) << 1) | (((1UL << 1) << 1) << 1)) | ((((1UL << 1) << 1) << 1) << 1)) | ((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1)) | (((((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1)) | (((((((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1)) | (((((((((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1)));
      __CrestLoad(34464, (unsigned long )(& match_icase), (long long )match_icase);
# 207 "search.c"
      dfasyntax((((((((1UL << 1) << 1) | (((1UL << 1) << 1) << 1)) | ((((1UL << 1) << 1) << 1) << 1)) | ((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1)) | (((((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1)) | (((((((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1)) | (((((((((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1),
                match_icase);
      __CrestClearStack(34465);
    }
    }
  }
  }
  __CrestLoad(34466, (unsigned long )(& size), (long long )size);
# 210 "search.c"
  err = re_compile_pattern((char const *)pattern, size, & regex);
  __CrestClearStack(34467);
  {
  __CrestLoad(34470, (unsigned long )(& err), (long long )((unsigned long )err));
  __CrestLoad(34469, (unsigned long )0, (long long )((unsigned long )((char const *)0)));
  __CrestApply2(34468, 13, (long long )((unsigned long )err != (unsigned long )((char const *)0)));
# 210 "search.c"
  if ((unsigned long )err != (unsigned long )((char const *)0)) {
    __CrestBranch(34471, 7444, 1);
    __CrestLoad(34473, (unsigned long )0, (long long )0);
# 211 "search.c"
    fatal(err, 0);
    __CrestClearStack(34474);
  } else {
    __CrestBranch(34472, 7445, 0);

  }
  }
# 213 "search.c"
  dfainit(& dfa);
  __CrestClearStack(34475);
  {
  __CrestLoad(34478, (unsigned long )(& match_words), (long long )match_words);
  __CrestLoad(34477, (unsigned long )0, (long long )0);
  __CrestApply2(34476, 13, (long long )(match_words != 0));
# 219 "search.c"
  if (match_words != 0) {
    __CrestBranch(34479, 7448, 1);
# 219 "search.c"
    goto _L;
  } else {
    __CrestBranch(34480, 7449, 0);
    {
    __CrestLoad(34483, (unsigned long )(& match_lines), (long long )match_lines);
    __CrestLoad(34482, (unsigned long )0, (long long )0);
    __CrestApply2(34481, 13, (long long )(match_lines != 0));
# 219 "search.c"
    if (match_lines != 0) {
      __CrestBranch(34484, 7450, 1);
      _L:
      __CrestLoad(34488, (unsigned long )(& size), (long long )size);
      __CrestLoad(34487, (unsigned long )0, (long long )50UL);
      __CrestApply2(34486, 0, (long long )(size + 50UL));
# 227 "search.c"
      tmp___1 = malloc(size + 50UL);
      __CrestClearStack(34489);
# 227 "search.c"
      n = (char *)tmp___1;
      __CrestLoad(34490, (unsigned long )0, (long long )0);
      __CrestStore(34491, (unsigned long )(& i));
# 228 "search.c"
      i = 0;
# 230 "search.c"
      strcpy((char * __restrict )n, (char const * __restrict )"");
      __CrestClearStack(34492);
      {
      __CrestLoad(34495, (unsigned long )(& match_lines), (long long )match_lines);
      __CrestLoad(34494, (unsigned long )0, (long long )0);
      __CrestApply2(34493, 13, (long long )(match_lines != 0));
# 232 "search.c"
      if (match_lines != 0) {
        __CrestBranch(34496, 7452, 1);
# 233 "search.c"
        strcpy((char * __restrict )n, (char const * __restrict )"^(");
        __CrestClearStack(34498);
      } else {
        __CrestBranch(34497, 7453, 0);

      }
      }
      {
      __CrestLoad(34501, (unsigned long )(& match_words), (long long )match_words);
      __CrestLoad(34500, (unsigned long )0, (long long )0);
      __CrestApply2(34499, 13, (long long )(match_words != 0));
# 234 "search.c"
      if (match_words != 0) {
        __CrestBranch(34502, 7455, 1);
# 235 "search.c"
        strcpy((char * __restrict )n, (char const * __restrict )"(^|[^0-9A-Za-z_])(");
        __CrestClearStack(34504);
      } else {
        __CrestBranch(34503, 7456, 0);

      }
      }
# 237 "search.c"
      tmp___2 = strlen((char const *)n);
      __CrestHandleReturn(34506, (long long )tmp___2);
      __CrestStore(34505, (unsigned long )(& tmp___2));
      __CrestLoad(34507, (unsigned long )(& tmp___2), (long long )tmp___2);
      __CrestStore(34508, (unsigned long )(& i));
# 237 "search.c"
      i = (int )tmp___2;
      __CrestLoad(34509, (unsigned long )(& size), (long long )size);
# 238 "search.c"
      memcpy((void * __restrict )(n + i), (void const * __restrict )pattern, size);
      __CrestClearStack(34510);
      __CrestLoad(34513, (unsigned long )(& i), (long long )i);
      __CrestLoad(34512, (unsigned long )(& size), (long long )size);
      __CrestApply2(34511, 0, (long long )((size_t )i + size));
      __CrestStore(34514, (unsigned long )(& i));
# 239 "search.c"
      i = (int )((size_t )i + size);
      {
      __CrestLoad(34517, (unsigned long )(& match_words), (long long )match_words);
      __CrestLoad(34516, (unsigned long )0, (long long )0);
      __CrestApply2(34515, 13, (long long )(match_words != 0));
# 241 "search.c"
      if (match_words != 0) {
        __CrestBranch(34518, 7459, 1);
# 242 "search.c"
        strcpy((char * __restrict )(n + i), (char const * __restrict )")([^0-9A-Za-z_]|$)");
        __CrestClearStack(34520);
      } else {
        __CrestBranch(34519, 7460, 0);

      }
      }
      {
      __CrestLoad(34523, (unsigned long )(& match_lines), (long long )match_lines);
      __CrestLoad(34522, (unsigned long )0, (long long )0);
      __CrestApply2(34521, 13, (long long )(match_lines != 0));
# 243 "search.c"
      if (match_lines != 0) {
        __CrestBranch(34524, 7462, 1);
# 244 "search.c"
        strcpy((char * __restrict )(n + i), (char const * __restrict )")$");
        __CrestClearStack(34526);
      } else {
        __CrestBranch(34525, 7463, 0);

      }
      }
# 246 "search.c"
      tmp___3 = strlen((char const *)(n + i));
      __CrestHandleReturn(34528, (long long )tmp___3);
      __CrestStore(34527, (unsigned long )(& tmp___3));
      __CrestLoad(34531, (unsigned long )(& i), (long long )i);
      __CrestLoad(34530, (unsigned long )(& tmp___3), (long long )tmp___3);
      __CrestApply2(34529, 0, (long long )((size_t )i + tmp___3));
      __CrestStore(34532, (unsigned long )(& i));
# 246 "search.c"
      i = (int )((size_t )i + tmp___3);
      __CrestLoad(34533, (unsigned long )(& i), (long long )i);
      __CrestLoad(34534, (unsigned long )0, (long long )1);
# 247 "search.c"
      dfacomp(n, (size_t )i, & dfa, 1);
      __CrestClearStack(34535);
    } else {
      __CrestBranch(34485, 7465, 0);
      __CrestLoad(34536, (unsigned long )(& size), (long long )size);
      __CrestLoad(34537, (unsigned long )0, (long long )1);
# 250 "search.c"
      dfacomp(pattern, size, & dfa, 1);
      __CrestClearStack(34538);
    }
    }
  }
  }
# 252 "search.c"
  kwsmusts();
  __CrestClearStack(34539);

  {
  __CrestReturn(34540);
# 187 "search.c"
  return;
  }
}
}
# 264 "search.c"
static struct re_registers regs ;
# 255 "search.c"
static char *EGexecute(char *buf , size_t size , char **endp )
{
  char *buflim ;
  char *beg ;
  char *end ;
  char save ;
  int backref ;
  int start ;
  int len ;
  struct kwsmatch kwsm ;
  void *tmp ;
  char *tmp___0 ;
  void *tmp___1 ;
  unsigned short const **tmp___2 ;
  unsigned short const **tmp___3 ;
  char *mem_17 ;
  char *mem_18 ;
  regoff_t *mem_19 ;
  char *mem_20 ;
  char *mem_21 ;
  unsigned short const *mem_22 ;
  char *mem_23 ;
  char *mem_24 ;
  char *mem_25 ;
  unsigned short const *mem_26 ;
  char *mem_27 ;
  regoff_t *mem_28 ;
  char *__retres29 ;

  {
  __CrestCall(34542, 288);
  __CrestStore(34541, (unsigned long )(& size));
# 267 "search.c"
  buflim = buf + size;
# 269 "search.c"
  end = buf;
# 269 "search.c"
  beg = end;
  {
# 269 "search.c"
  while (1) {
    while_continue: ;
    {
    __CrestLoad(34545, (unsigned long )(& end), (long long )((unsigned long )end));
    __CrestLoad(34544, (unsigned long )(& buflim), (long long )((unsigned long )buflim));
    __CrestApply2(34543, 16, (long long )((unsigned long )end < (unsigned long )buflim));
# 269 "search.c"
    if ((unsigned long )end < (unsigned long )buflim) {
      __CrestBranch(34546, 7474, 1);

    } else {
      __CrestBranch(34547, 7475, 0);
# 269 "search.c"
      goto while_break;
    }
    }
    {
    __CrestLoad(34550, (unsigned long )(& kwset), (long long )((unsigned long )kwset));
    __CrestLoad(34549, (unsigned long )0, (long long )0);
    __CrestApply2(34548, 13, (long long )(kwset != 0));
# 271 "search.c"
    if (kwset != 0) {
      __CrestBranch(34551, 7477, 1);
      __CrestLoad(34555, (unsigned long )(& buflim), (long long )((unsigned long )buflim));
      __CrestLoad(34554, (unsigned long )(& beg), (long long )((unsigned long )beg));
      __CrestApply2(34553, 18, (long long )(buflim - beg));
# 274 "search.c"
      beg = kwsexec(kwset, beg, (size_t )(buflim - beg), & kwsm);
      __CrestClearStack(34556);
      {
      __CrestLoad(34559, (unsigned long )(& beg), (long long )((unsigned long )beg));
      __CrestLoad(34558, (unsigned long )0, (long long )0);
      __CrestApply2(34557, 12, (long long )(beg == 0));
# 275 "search.c"
      if (beg == 0) {
        __CrestBranch(34560, 7479, 1);
# 276 "search.c"
        goto failure;
      } else {
        __CrestBranch(34561, 7480, 0);

      }
      }
      __CrestLoad(34562, (unsigned long )0, (long long )'\n');
      __CrestLoad(34565, (unsigned long )(& buflim), (long long )((unsigned long )buflim));
      __CrestLoad(34564, (unsigned long )(& beg), (long long )((unsigned long )beg));
      __CrestApply2(34563, 18, (long long )(buflim - beg));
# 279 "search.c"
      tmp = memchr((void const *)beg, '\n', (size_t )(buflim - beg));
      __CrestClearStack(34566);
# 279 "search.c"
      end = (char *)tmp;
      {
      __CrestLoad(34569, (unsigned long )(& end), (long long )((unsigned long )end));
      __CrestLoad(34568, (unsigned long )0, (long long )0);
      __CrestApply2(34567, 12, (long long )(end == 0));
# 280 "search.c"
      if (end == 0) {
        __CrestBranch(34570, 7483, 1);
# 281 "search.c"
        end = buflim;
      } else {
        __CrestBranch(34571, 7484, 0);

      }
      }
      {
# 282 "search.c"
      while (1) {
        while_continue___0: ;
        {
        __CrestLoad(34574, (unsigned long )(& beg), (long long )((unsigned long )beg));
        __CrestLoad(34573, (unsigned long )(& buf), (long long )((unsigned long )buf));
        __CrestApply2(34572, 14, (long long )((unsigned long )beg > (unsigned long )buf));
# 282 "search.c"
        if ((unsigned long )beg > (unsigned long )buf) {
          __CrestBranch(34575, 7489, 1);
          {
# 282 "search.c"
          mem_17 = beg + -1;
          {
          __CrestLoad(34579, (unsigned long )mem_17, (long long )*mem_17);
          __CrestLoad(34578, (unsigned long )0, (long long )10);
          __CrestApply2(34577, 13, (long long )((int )*mem_17 != 10));
# 282 "search.c"
          if ((int )*mem_17 != 10) {
            __CrestBranch(34580, 7492, 1);

          } else {
            __CrestBranch(34581, 7493, 0);
# 282 "search.c"
            goto while_break___0;
          }
          }
          }
        } else {
          __CrestBranch(34576, 7494, 0);
# 282 "search.c"
          goto while_break___0;
        }
        }
# 283 "search.c"
        beg --;
      }
      while_break___0: ;
      }
      __CrestLoad(34582, (unsigned long )end, (long long )*end);
      __CrestStore(34583, (unsigned long )(& save));
# 284 "search.c"
      save = *end;
      {
      __CrestLoad(34586, (unsigned long )(& kwsm.index), (long long )kwsm.index);
      __CrestLoad(34585, (unsigned long )(& lastexact), (long long )lastexact);
      __CrestApply2(34584, 16, (long long )(kwsm.index < lastexact));
# 285 "search.c"
      if (kwsm.index < lastexact) {
        __CrestBranch(34587, 7499, 1);
# 286 "search.c"
        goto success;
      } else {
        __CrestBranch(34588, 7500, 0);

      }
      }
      __CrestLoad(34589, (unsigned long )0, (long long )0);
# 287 "search.c"
      tmp___0 = dfaexec(& dfa, beg, end, 0, (int *)0, & backref);
      __CrestClearStack(34590);
      {
      __CrestLoad(34593, (unsigned long )(& tmp___0), (long long )((unsigned long )tmp___0));
      __CrestLoad(34592, (unsigned long )0, (long long )0);
      __CrestApply2(34591, 13, (long long )(tmp___0 != 0));
# 287 "search.c"
      if (tmp___0 != 0) {
        __CrestBranch(34594, 7503, 1);

      } else {
        __CrestBranch(34595, 7504, 0);
        __CrestLoad(34596, (unsigned long )(& save), (long long )save);
        __CrestStore(34597, (unsigned long )end);
# 289 "search.c"
        *end = save;
# 290 "search.c"
        goto __Cont;
      }
      }
      __CrestLoad(34598, (unsigned long )(& save), (long long )save);
      __CrestStore(34599, (unsigned long )end);
# 292 "search.c"
      *end = save;
      {
      __CrestLoad(34602, (unsigned long )(& backref), (long long )backref);
      __CrestLoad(34601, (unsigned long )0, (long long )0);
      __CrestApply2(34600, 12, (long long )(backref == 0));
# 294 "search.c"
      if (backref == 0) {
        __CrestBranch(34603, 7508, 1);
# 295 "search.c"
        goto success;
      } else {
        __CrestBranch(34604, 7509, 0);

      }
      }
    } else {
      __CrestBranch(34552, 7510, 0);
      __CrestLoad(34605, (unsigned long )buflim, (long long )*buflim);
      __CrestStore(34606, (unsigned long )(& save));
# 300 "search.c"
      save = *buflim;
      __CrestLoad(34607, (unsigned long )0, (long long )0);
# 301 "search.c"
      beg = dfaexec(& dfa, beg, buflim, 0, (int *)0, & backref);
      __CrestClearStack(34608);
      __CrestLoad(34609, (unsigned long )(& save), (long long )save);
      __CrestStore(34610, (unsigned long )buflim);
# 302 "search.c"
      *buflim = save;
      {
      __CrestLoad(34613, (unsigned long )(& beg), (long long )((unsigned long )beg));
      __CrestLoad(34612, (unsigned long )0, (long long )0);
      __CrestApply2(34611, 12, (long long )(beg == 0));
# 303 "search.c"
      if (beg == 0) {
        __CrestBranch(34614, 7512, 1);
# 304 "search.c"
        goto failure;
      } else {
        __CrestBranch(34615, 7513, 0);

      }
      }
      __CrestLoad(34616, (unsigned long )0, (long long )'\n');
      __CrestLoad(34619, (unsigned long )(& buflim), (long long )((unsigned long )buflim));
      __CrestLoad(34618, (unsigned long )(& beg), (long long )((unsigned long )beg));
      __CrestApply2(34617, 18, (long long )(buflim - beg));
# 306 "search.c"
      tmp___1 = memchr((void const *)beg, '\n', (size_t )(buflim - beg));
      __CrestClearStack(34620);
# 306 "search.c"
      end = (char *)tmp___1;
      {
      __CrestLoad(34623, (unsigned long )(& end), (long long )((unsigned long )end));
      __CrestLoad(34622, (unsigned long )0, (long long )0);
      __CrestApply2(34621, 12, (long long )(end == 0));
# 307 "search.c"
      if (end == 0) {
        __CrestBranch(34624, 7516, 1);
# 308 "search.c"
        end = buflim;
      } else {
        __CrestBranch(34625, 7517, 0);

      }
      }
      {
# 309 "search.c"
      while (1) {
        while_continue___1: ;
        {
        __CrestLoad(34628, (unsigned long )(& beg), (long long )((unsigned long )beg));
        __CrestLoad(34627, (unsigned long )(& buf), (long long )((unsigned long )buf));
        __CrestApply2(34626, 14, (long long )((unsigned long )beg > (unsigned long )buf));
# 309 "search.c"
        if ((unsigned long )beg > (unsigned long )buf) {
          __CrestBranch(34629, 7522, 1);
          {
# 309 "search.c"
          mem_18 = beg + -1;
          {
          __CrestLoad(34633, (unsigned long )mem_18, (long long )*mem_18);
          __CrestLoad(34632, (unsigned long )0, (long long )10);
          __CrestApply2(34631, 13, (long long )((int )*mem_18 != 10));
# 309 "search.c"
          if ((int )*mem_18 != 10) {
            __CrestBranch(34634, 7525, 1);

          } else {
            __CrestBranch(34635, 7526, 0);
# 309 "search.c"
            goto while_break___1;
          }
          }
          }
        } else {
          __CrestBranch(34630, 7527, 0);
# 309 "search.c"
          goto while_break___1;
        }
        }
# 310 "search.c"
        beg --;
      }
      while_break___1: ;
      }
      {
      __CrestLoad(34638, (unsigned long )(& backref), (long long )backref);
      __CrestLoad(34637, (unsigned long )0, (long long )0);
      __CrestApply2(34636, 12, (long long )(backref == 0));
# 312 "search.c"
      if (backref == 0) {
        __CrestBranch(34639, 7531, 1);
# 313 "search.c"
        goto success;
      } else {
        __CrestBranch(34640, 7532, 0);

      }
      }
    }
    }
# 317 "search.c"
    regex.not_eol = 0U;
    __CrestLoad(34643, (unsigned long )(& end), (long long )((unsigned long )end));
    __CrestLoad(34642, (unsigned long )(& beg), (long long )((unsigned long )beg));
    __CrestApply2(34641, 18, (long long )(end - beg));
    __CrestLoad(34644, (unsigned long )0, (long long )0);
    __CrestLoad(34647, (unsigned long )(& end), (long long )((unsigned long )end));
    __CrestLoad(34646, (unsigned long )(& beg), (long long )((unsigned long )beg));
    __CrestApply2(34645, 18, (long long )(end - beg));
# 318 "search.c"
    start = re_search(& regex, (char const *)beg, (int )(end - beg), 0, (int )(end - beg),
                      & regs);
    __CrestHandleReturn(34649, (long long )start);
    __CrestStore(34648, (unsigned long )(& start));
    {
    __CrestLoad(34652, (unsigned long )(& start), (long long )start);
    __CrestLoad(34651, (unsigned long )0, (long long )0);
    __CrestApply2(34650, 17, (long long )(start >= 0));
# 318 "search.c"
    if (start >= 0) {
      __CrestBranch(34653, 7535, 1);
# 320 "search.c"
      mem_19 = regs.end + 0;
      __CrestLoad(34657, (unsigned long )mem_19, (long long )*mem_19);
      __CrestLoad(34656, (unsigned long )(& start), (long long )start);
      __CrestApply2(34655, 1, (long long )(*mem_19 - start));
      __CrestStore(34658, (unsigned long )(& len));
# 320 "search.c"
      len = *mem_19 - start;
      {
      __CrestLoad(34661, (unsigned long )(& match_lines), (long long )match_lines);
      __CrestLoad(34660, (unsigned long )0, (long long )0);
      __CrestApply2(34659, 12, (long long )(match_lines == 0));
# 321 "search.c"
      if (match_lines == 0) {
        __CrestBranch(34662, 7537, 1);
        {
        __CrestLoad(34666, (unsigned long )(& match_words), (long long )match_words);
        __CrestLoad(34665, (unsigned long )0, (long long )0);
        __CrestApply2(34664, 12, (long long )(match_words == 0));
# 321 "search.c"
        if (match_words == 0) {
          __CrestBranch(34667, 7538, 1);
# 323 "search.c"
          goto success;
        } else {
          __CrestBranch(34668, 7539, 0);
# 321 "search.c"
          goto _L;
        }
        }
      } else {
        __CrestBranch(34663, 7540, 0);
        _L:
        {
        __CrestLoad(34671, (unsigned long )(& match_lines), (long long )match_lines);
        __CrestLoad(34670, (unsigned long )0, (long long )0);
        __CrestApply2(34669, 13, (long long )(match_lines != 0));
# 321 "search.c"
        if (match_lines != 0) {
          __CrestBranch(34672, 7541, 1);
          {
          __CrestLoad(34678, (unsigned long )(& len), (long long )len);
          __CrestLoad(34677, (unsigned long )(& end), (long long )((unsigned long )end));
          __CrestLoad(34676, (unsigned long )(& beg), (long long )((unsigned long )beg));
          __CrestApply2(34675, 18, (long long )(end - beg));
          __CrestApply2(34674, 12, (long long )((long )len == end - beg));
# 321 "search.c"
          if ((long )len == end - beg) {
            __CrestBranch(34679, 7542, 1);
# 323 "search.c"
            goto success;
          } else {
            __CrestBranch(34680, 7543, 0);

          }
          }
        } else {
          __CrestBranch(34673, 7544, 0);

        }
        }
      }
      }
      {
      __CrestLoad(34683, (unsigned long )(& match_words), (long long )match_words);
      __CrestLoad(34682, (unsigned long )0, (long long )0);
      __CrestApply2(34681, 13, (long long )(match_words != 0));
# 330 "search.c"
      if (match_words != 0) {
        __CrestBranch(34684, 7546, 1);
        {
# 331 "search.c"
        while (1) {
          while_continue___2: ;
          {
          __CrestLoad(34688, (unsigned long )(& start), (long long )start);
          __CrestLoad(34687, (unsigned long )0, (long long )0);
          __CrestApply2(34686, 17, (long long )(start >= 0));
# 331 "search.c"
          if (start >= 0) {
            __CrestBranch(34689, 7550, 1);

          } else {
            __CrestBranch(34690, 7551, 0);
# 331 "search.c"
            goto while_break___2;
          }
          }
          {
          __CrestLoad(34693, (unsigned long )(& start), (long long )start);
          __CrestLoad(34692, (unsigned long )0, (long long )0);
          __CrestApply2(34691, 12, (long long )(start == 0));
# 333 "search.c"
          if (start == 0) {
            __CrestBranch(34694, 7553, 1);
# 333 "search.c"
            goto _L___1;
          } else {
            __CrestBranch(34695, 7554, 0);
            {
# 333 "search.c"
            mem_20 = beg + (start - 1);
            {
            __CrestLoad(34700, (unsigned long )mem_20, (long long )*mem_20);
            __CrestLoad(34699, (unsigned long )0, (long long )-128);
            __CrestApply2(34698, 5, (long long )((int )*mem_20 & -128));
            __CrestLoad(34697, (unsigned long )0, (long long )0);
            __CrestApply2(34696, 12, (long long )(((int )*mem_20 & -128) == 0));
# 333 "search.c"
            if (((int )*mem_20 & -128) == 0) {
              __CrestBranch(34701, 7557, 1);
# 333 "search.c"
              tmp___2 = __ctype_b_loc();
              __CrestClearStack(34703);
              {
# 333 "search.c"
              mem_21 = beg + (start - 1);
# 333 "search.c"
              mem_22 = *tmp___2 + (int )*mem_21;
              {
              __CrestLoad(34708, (unsigned long )mem_22, (long long )*mem_22);
              __CrestLoad(34707, (unsigned long )0, (long long )8);
              __CrestApply2(34706, 5, (long long )((int const )*mem_22 & 8));
              __CrestLoad(34705, (unsigned long )0, (long long )0);
              __CrestApply2(34704, 13, (long long )(((int const )*mem_22 & 8) != 0));
# 333 "search.c"
              if (((int const )*mem_22 & 8) != 0) {
                __CrestBranch(34709, 7561, 1);

              } else {
                __CrestBranch(34710, 7562, 0);
# 333 "search.c"
                goto _L___2;
              }
              }
              }
            } else {
              __CrestBranch(34702, 7563, 0);
              _L___2:
              {
# 333 "search.c"
              mem_23 = beg + (start - 1);
              {
              __CrestLoad(34713, (unsigned long )mem_23, (long long )*mem_23);
              __CrestLoad(34712, (unsigned long )0, (long long )95);
              __CrestApply2(34711, 12, (long long )((int )*mem_23 == 95));
# 333 "search.c"
              if ((int )*mem_23 == 95) {
                __CrestBranch(34714, 7566, 1);

              } else {
                __CrestBranch(34715, 7567, 0);
                _L___1:
                {
                __CrestLoad(34720, (unsigned long )(& len), (long long )len);
                __CrestLoad(34719, (unsigned long )(& end), (long long )((unsigned long )end));
                __CrestLoad(34718, (unsigned long )(& beg), (long long )((unsigned long )beg));
                __CrestApply2(34717, 18, (long long )(end - beg));
                __CrestApply2(34716, 12, (long long )((long )len == end - beg));
# 333 "search.c"
                if ((long )len == end - beg) {
                  __CrestBranch(34721, 7568, 1);
# 335 "search.c"
                  goto success;
                } else {
                  __CrestBranch(34722, 7569, 0);
                  {
# 333 "search.c"
                  mem_24 = beg + (start + len);
                  {
                  __CrestLoad(34727, (unsigned long )mem_24, (long long )*mem_24);
                  __CrestLoad(34726, (unsigned long )0, (long long )-128);
                  __CrestApply2(34725, 5, (long long )((int )*mem_24 & -128));
                  __CrestLoad(34724, (unsigned long )0, (long long )0);
                  __CrestApply2(34723, 12, (long long )(((int )*mem_24 & -128) == 0));
# 333 "search.c"
                  if (((int )*mem_24 & -128) == 0) {
                    __CrestBranch(34728, 7572, 1);
# 333 "search.c"
                    tmp___3 = __ctype_b_loc();
                    __CrestClearStack(34730);
                    {
# 333 "search.c"
                    mem_25 = beg + (start + len);
# 333 "search.c"
                    mem_26 = *tmp___3 + (int )*mem_25;
                    {
                    __CrestLoad(34735, (unsigned long )mem_26, (long long )*mem_26);
                    __CrestLoad(34734, (unsigned long )0, (long long )8);
                    __CrestApply2(34733, 5, (long long )((int const )*mem_26 & 8));
                    __CrestLoad(34732, (unsigned long )0, (long long )0);
                    __CrestApply2(34731, 13, (long long )(((int const )*mem_26 & 8) != 0));
# 333 "search.c"
                    if (((int const )*mem_26 & 8) != 0) {
                      __CrestBranch(34736, 7576, 1);

                    } else {
                      __CrestBranch(34737, 7577, 0);
# 333 "search.c"
                      goto _L___0;
                    }
                    }
                    }
                  } else {
                    __CrestBranch(34729, 7578, 0);
                    _L___0:
                    {
# 333 "search.c"
                    mem_27 = beg + (start + len);
                    {
                    __CrestLoad(34740, (unsigned long )mem_27, (long long )*mem_27);
                    __CrestLoad(34739, (unsigned long )0, (long long )95);
                    __CrestApply2(34738, 12, (long long )((int )*mem_27 == 95));
# 333 "search.c"
                    if ((int )*mem_27 == 95) {
                      __CrestBranch(34741, 7581, 1);

                    } else {
                      __CrestBranch(34742, 7582, 0);
# 335 "search.c"
                      goto success;
                    }
                    }
                    }
                  }
                  }
                  }
                }
                }
              }
              }
              }
            }
            }
            }
          }
          }
          {
          __CrestLoad(34745, (unsigned long )(& len), (long long )len);
          __CrestLoad(34744, (unsigned long )0, (long long )0);
          __CrestApply2(34743, 14, (long long )(len > 0));
# 336 "search.c"
          if (len > 0) {
            __CrestBranch(34746, 7584, 1);
            __CrestLoad(34750, (unsigned long )(& len), (long long )len);
            __CrestLoad(34749, (unsigned long )0, (long long )1);
            __CrestApply2(34748, 1, (long long )(len - 1));
            __CrestStore(34751, (unsigned long )(& len));
# 339 "search.c"
            len --;
# 340 "search.c"
            regex.not_eol = 1U;
            __CrestLoad(34754, (unsigned long )(& start), (long long )start);
            __CrestLoad(34753, (unsigned long )(& len), (long long )len);
            __CrestApply2(34752, 0, (long long )(start + len));
            __CrestLoad(34755, (unsigned long )(& start), (long long )start);
# 341 "search.c"
            len = re_match(& regex, (char const *)beg, start + len, start, & regs);
            __CrestHandleReturn(34757, (long long )len);
            __CrestStore(34756, (unsigned long )(& len));
          } else {
            __CrestBranch(34747, 7585, 0);

          }
          }
          {
          __CrestLoad(34760, (unsigned long )(& len), (long long )len);
          __CrestLoad(34759, (unsigned long )0, (long long )0);
          __CrestApply2(34758, 15, (long long )(len <= 0));
# 343 "search.c"
          if (len <= 0) {
            __CrestBranch(34761, 7587, 1);
            {
            __CrestLoad(34767, (unsigned long )(& start), (long long )start);
            __CrestLoad(34766, (unsigned long )(& end), (long long )((unsigned long )end));
            __CrestLoad(34765, (unsigned long )(& beg), (long long )((unsigned long )beg));
            __CrestApply2(34764, 18, (long long )(end - beg));
            __CrestApply2(34763, 12, (long long )((long )start == end - beg));
# 346 "search.c"
            if ((long )start == end - beg) {
              __CrestBranch(34768, 7588, 1);
# 347 "search.c"
              goto while_break___2;
            } else {
              __CrestBranch(34769, 7589, 0);

            }
            }
            __CrestLoad(34772, (unsigned long )(& start), (long long )start);
            __CrestLoad(34771, (unsigned long )0, (long long )1);
            __CrestApply2(34770, 0, (long long )(start + 1));
            __CrestStore(34773, (unsigned long )(& start));
# 348 "search.c"
            start ++;
# 349 "search.c"
            regex.not_eol = 0U;
            __CrestLoad(34776, (unsigned long )(& end), (long long )((unsigned long )end));
            __CrestLoad(34775, (unsigned long )(& beg), (long long )((unsigned long )beg));
            __CrestApply2(34774, 18, (long long )(end - beg));
            __CrestLoad(34777, (unsigned long )(& start), (long long )start);
            __CrestLoad(34782, (unsigned long )(& end), (long long )((unsigned long )end));
            __CrestLoad(34781, (unsigned long )(& beg), (long long )((unsigned long )beg));
            __CrestApply2(34780, 18, (long long )(end - beg));
            __CrestLoad(34779, (unsigned long )(& start), (long long )start);
            __CrestApply2(34778, 1, (long long )((end - beg) - (long )start));
# 350 "search.c"
            start = re_search(& regex, (char const *)beg, (int )(end - beg), start,
                              (int )((end - beg) - (long )start), & regs);
            __CrestHandleReturn(34784, (long long )start);
            __CrestStore(34783, (unsigned long )(& start));
# 352 "search.c"
            mem_28 = regs.end + 0;
            __CrestLoad(34787, (unsigned long )mem_28, (long long )*mem_28);
            __CrestLoad(34786, (unsigned long )(& start), (long long )start);
            __CrestApply2(34785, 1, (long long )(*mem_28 - start));
            __CrestStore(34788, (unsigned long )(& len));
# 352 "search.c"
            len = *mem_28 - start;
          } else {
            __CrestBranch(34762, 7591, 0);

          }
          }
        }
        while_break___2: ;
        }
      } else {
        __CrestBranch(34685, 7593, 0);

      }
      }
    } else {
      __CrestBranch(34654, 7594, 0);

    }
    }
    __Cont:
# 269 "search.c"
    beg = end + 1;
  }
  while_break: ;
  }
  failure:
# 359 "search.c"
  __retres29 = (char *)0;
# 359 "search.c"
  goto return_label;
  success:
  {
  __CrestLoad(34791, (unsigned long )(& end), (long long )((unsigned long )end));
  __CrestLoad(34790, (unsigned long )(& buflim), (long long )((unsigned long )buflim));
  __CrestApply2(34789, 16, (long long )((unsigned long )end < (unsigned long )buflim));
# 362 "search.c"
  if ((unsigned long )end < (unsigned long )buflim) {
    __CrestBranch(34792, 7600, 1);
# 362 "search.c"
    *endp = end + 1;
  } else {
    __CrestBranch(34793, 7601, 0);
# 362 "search.c"
    *endp = end;
  }
  }
# 363 "search.c"
  __retres29 = beg;
  return_label:
  {
  __CrestReturn(34794);
# 255 "search.c"
  return (__retres29);
  }
}
}
# 366 "search.c"
static void Fcompile(char *pattern , size_t size )
{
  char *beg ;
  char *lim ;
  char *err ;

  {
  __CrestCall(34796, 289);
  __CrestStore(34795, (unsigned long )(& size));
# 373 "search.c"
  kwsinit();
  __CrestClearStack(34797);
# 374 "search.c"
  beg = pattern;
  {
# 375 "search.c"
  while (1) {
    while_continue: ;
# 377 "search.c"
    lim = beg;
    {
# 377 "search.c"
    while (1) {
      while_continue___0: ;
      {
      __CrestLoad(34802, (unsigned long )(& lim), (long long )((unsigned long )lim));
      __CrestLoad(34801, (unsigned long )(& pattern), (long long )((unsigned long )pattern));
      __CrestLoad(34800, (unsigned long )(& size), (long long )size);
      __CrestApply2(34799, 18, (long long )((unsigned long )(pattern + size)));
      __CrestApply2(34798, 16, (long long )((unsigned long )lim < (unsigned long )(pattern + size)));
# 377 "search.c"
      if ((unsigned long )lim < (unsigned long )(pattern + size)) {
        __CrestBranch(34803, 7613, 1);
        {
        __CrestLoad(34807, (unsigned long )lim, (long long )*lim);
        __CrestLoad(34806, (unsigned long )0, (long long )10);
        __CrestApply2(34805, 13, (long long )((int )*lim != 10));
# 377 "search.c"
        if ((int )*lim != 10) {
          __CrestBranch(34808, 7614, 1);

        } else {
          __CrestBranch(34809, 7615, 0);
# 377 "search.c"
          goto while_break___0;
        }
        }
      } else {
        __CrestBranch(34804, 7616, 0);
# 377 "search.c"
        goto while_break___0;
      }
      }
# 377 "search.c"
      lim ++;
    }
    while_break___0: ;
    }
    __CrestLoad(34812, (unsigned long )(& lim), (long long )((unsigned long )lim));
    __CrestLoad(34811, (unsigned long )(& beg), (long long )((unsigned long )beg));
    __CrestApply2(34810, 18, (long long )(lim - beg));
# 379 "search.c"
    err = kwsincr(kwset, beg, (size_t )(lim - beg));
    __CrestClearStack(34813);
    {
    __CrestLoad(34816, (unsigned long )(& err), (long long )((unsigned long )err));
    __CrestLoad(34815, (unsigned long )0, (long long )((unsigned long )((char *)0)));
    __CrestApply2(34814, 13, (long long )((unsigned long )err != (unsigned long )((char *)0)));
# 379 "search.c"
    if ((unsigned long )err != (unsigned long )((char *)0)) {
      __CrestBranch(34817, 7621, 1);
      __CrestLoad(34819, (unsigned long )0, (long long )0);
# 380 "search.c"
      fatal((char const *)err, 0);
      __CrestClearStack(34820);
    } else {
      __CrestBranch(34818, 7622, 0);

    }
    }
    {
    __CrestLoad(34825, (unsigned long )(& lim), (long long )((unsigned long )lim));
    __CrestLoad(34824, (unsigned long )(& pattern), (long long )((unsigned long )pattern));
    __CrestLoad(34823, (unsigned long )(& size), (long long )size);
    __CrestApply2(34822, 18, (long long )((unsigned long )(pattern + size)));
    __CrestApply2(34821, 16, (long long )((unsigned long )lim < (unsigned long )(pattern + size)));
# 381 "search.c"
    if ((unsigned long )lim < (unsigned long )(pattern + size)) {
      __CrestBranch(34826, 7624, 1);
# 382 "search.c"
      lim ++;
    } else {
      __CrestBranch(34827, 7625, 0);

    }
    }
# 383 "search.c"
    beg = lim;
    {
    __CrestLoad(34832, (unsigned long )(& beg), (long long )((unsigned long )beg));
    __CrestLoad(34831, (unsigned long )(& pattern), (long long )((unsigned long )pattern));
    __CrestLoad(34830, (unsigned long )(& size), (long long )size);
    __CrestApply2(34829, 18, (long long )((unsigned long )(pattern + size)));
    __CrestApply2(34828, 16, (long long )((unsigned long )beg < (unsigned long )(pattern + size)));
# 375 "search.c"
    if ((unsigned long )beg < (unsigned long )(pattern + size)) {
      __CrestBranch(34833, 7628, 1);

    } else {
      __CrestBranch(34834, 7629, 0);
# 375 "search.c"
      goto while_break;
    }
    }
  }
  while_break: ;
  }
# 387 "search.c"
  err = kwsprep(kwset);
  __CrestClearStack(34835);
  {
  __CrestLoad(34838, (unsigned long )(& err), (long long )((unsigned long )err));
  __CrestLoad(34837, (unsigned long )0, (long long )((unsigned long )((char *)0)));
  __CrestApply2(34836, 13, (long long )((unsigned long )err != (unsigned long )((char *)0)));
# 387 "search.c"
  if ((unsigned long )err != (unsigned long )((char *)0)) {
    __CrestBranch(34839, 7633, 1);
    __CrestLoad(34841, (unsigned long )0, (long long )0);
# 388 "search.c"
    fatal((char const *)err, 0);
    __CrestClearStack(34842);
  } else {
    __CrestBranch(34840, 7634, 0);

  }
  }

  {
  __CrestReturn(34843);
# 366 "search.c"
  return;
  }
}
}
# 391 "search.c"
static char *Fexecute(char *buf , size_t size , char **endp )
{
  char *beg ;
  char *try ;
  char *end ;
  size_t len ;
  struct kwsmatch kwsmatch ;
  unsigned short const **tmp ;
  unsigned short const **tmp___0 ;
  void *tmp___1 ;
  char *mem_12 ;
  char *mem_13 ;
  char *mem_14 ;
  char *mem_15 ;
  unsigned short const *mem_16 ;
  char *mem_17 ;
  char *mem_18 ;
  char *mem_19 ;
  unsigned short const *mem_20 ;
  char *mem_21 ;
  char *mem_22 ;
  char *__retres23 ;

  {
  __CrestCall(34845, 290);
  __CrestStore(34844, (unsigned long )(& size));
# 401 "search.c"
  beg = buf;
  {
# 401 "search.c"
  while (1) {
    while_continue: ;
    {
    __CrestLoad(34850, (unsigned long )(& beg), (long long )((unsigned long )beg));
    __CrestLoad(34849, (unsigned long )(& buf), (long long )((unsigned long )buf));
    __CrestLoad(34848, (unsigned long )(& size), (long long )size);
    __CrestApply2(34847, 18, (long long )((unsigned long )(buf + size)));
    __CrestApply2(34846, 15, (long long )((unsigned long )beg <= (unsigned long )(buf + size)));
# 401 "search.c"
    if ((unsigned long )beg <= (unsigned long )(buf + size)) {
      __CrestBranch(34851, 7642, 1);

    } else {
      __CrestBranch(34852, 7643, 0);
# 401 "search.c"
      goto while_break;
    }
    }
    __CrestLoad(34857, (unsigned long )(& buf), (long long )((unsigned long )buf));
    __CrestLoad(34856, (unsigned long )(& size), (long long )size);
    __CrestApply2(34855, 18, (long long )((unsigned long )(buf + size)));
    __CrestLoad(34854, (unsigned long )(& beg), (long long )((unsigned long )beg));
    __CrestApply2(34853, 18, (long long )((buf + size) - beg));
# 403 "search.c"
    beg = kwsexec(kwset, beg, (size_t )((buf + size) - beg), & kwsmatch);
    __CrestClearStack(34858);
    {
    __CrestLoad(34861, (unsigned long )(& beg), (long long )((unsigned long )beg));
    __CrestLoad(34860, (unsigned long )0, (long long )0);
    __CrestApply2(34859, 13, (long long )(beg != 0));
# 403 "search.c"
    if (beg != 0) {
      __CrestBranch(34862, 7646, 1);

    } else {
      __CrestBranch(34863, 7647, 0);
# 404 "search.c"
      __retres23 = (char *)0;
# 404 "search.c"
      goto return_label;
    }
    }
    __CrestLoad(34864, (unsigned long )(& kwsmatch.size[0]), (long long )kwsmatch.size[0]);
    __CrestStore(34865, (unsigned long )(& len));
# 405 "search.c"
    len = kwsmatch.size[0];
    {
    __CrestLoad(34868, (unsigned long )(& match_lines), (long long )match_lines);
    __CrestLoad(34867, (unsigned long )0, (long long )0);
    __CrestApply2(34866, 13, (long long )(match_lines != 0));
# 406 "search.c"
    if (match_lines != 0) {
      __CrestBranch(34869, 7651, 1);
      {
      __CrestLoad(34873, (unsigned long )(& beg), (long long )((unsigned long )beg));
      __CrestLoad(34872, (unsigned long )(& buf), (long long )((unsigned long )buf));
      __CrestApply2(34871, 14, (long long )((unsigned long )beg > (unsigned long )buf));
# 408 "search.c"
      if ((unsigned long )beg > (unsigned long )buf) {
        __CrestBranch(34874, 7652, 1);
        {
# 408 "search.c"
        mem_12 = beg + -1;
        {
        __CrestLoad(34878, (unsigned long )mem_12, (long long )*mem_12);
        __CrestLoad(34877, (unsigned long )0, (long long )10);
        __CrestApply2(34876, 13, (long long )((int )*mem_12 != 10));
# 408 "search.c"
        if ((int )*mem_12 != 10) {
          __CrestBranch(34879, 7655, 1);
# 409 "search.c"
          goto __Cont;
        } else {
          __CrestBranch(34880, 7656, 0);

        }
        }
        }
      } else {
        __CrestBranch(34875, 7657, 0);

      }
      }
      {
      __CrestLoad(34887, (unsigned long )(& beg), (long long )((unsigned long )beg));
      __CrestLoad(34886, (unsigned long )(& len), (long long )len);
      __CrestApply2(34885, 18, (long long )((unsigned long )(beg + len)));
      __CrestLoad(34884, (unsigned long )(& buf), (long long )((unsigned long )buf));
      __CrestLoad(34883, (unsigned long )(& size), (long long )size);
      __CrestApply2(34882, 18, (long long )((unsigned long )(buf + size)));
      __CrestApply2(34881, 16, (long long )((unsigned long )(beg + len) < (unsigned long )(buf + size)));
# 410 "search.c"
      if ((unsigned long )(beg + len) < (unsigned long )(buf + size)) {
        __CrestBranch(34888, 7659, 1);
        {
# 410 "search.c"
        mem_13 = beg + len;
        {
        __CrestLoad(34892, (unsigned long )mem_13, (long long )*mem_13);
        __CrestLoad(34891, (unsigned long )0, (long long )10);
        __CrestApply2(34890, 13, (long long )((int )*mem_13 != 10));
# 410 "search.c"
        if ((int )*mem_13 != 10) {
          __CrestBranch(34893, 7662, 1);
# 411 "search.c"
          goto __Cont;
        } else {
          __CrestBranch(34894, 7663, 0);

        }
        }
        }
      } else {
        __CrestBranch(34889, 7664, 0);

      }
      }
# 412 "search.c"
      goto success;
    } else {
      __CrestBranch(34870, 7666, 0);
      {
      __CrestLoad(34897, (unsigned long )(& match_words), (long long )match_words);
      __CrestLoad(34896, (unsigned long )0, (long long )0);
      __CrestApply2(34895, 13, (long long )(match_words != 0));
# 414 "search.c"
      if (match_words != 0) {
        __CrestBranch(34898, 7667, 1);
# 415 "search.c"
        try = beg;
        {
# 415 "search.c"
        while (1) {
          while_continue___0: ;
          {
          __CrestLoad(34902, (unsigned long )(& len), (long long )len);
          __CrestLoad(34901, (unsigned long )0, (long long )0);
          __CrestApply2(34900, 13, (long long )(len != 0));
# 415 "search.c"
          if (len != 0) {
            __CrestBranch(34903, 7672, 1);
            {
            __CrestLoad(34907, (unsigned long )(& try), (long long )((unsigned long )try));
            __CrestLoad(34906, (unsigned long )0, (long long )0);
            __CrestApply2(34905, 13, (long long )(try != 0));
# 415 "search.c"
            if (try != 0) {
              __CrestBranch(34908, 7673, 1);

            } else {
              __CrestBranch(34909, 7674, 0);
# 415 "search.c"
              goto while_break___0;
            }
            }
          } else {
            __CrestBranch(34904, 7675, 0);
# 415 "search.c"
            goto while_break___0;
          }
          }
          {
          __CrestLoad(34912, (unsigned long )(& try), (long long )((unsigned long )try));
          __CrestLoad(34911, (unsigned long )(& buf), (long long )((unsigned long )buf));
          __CrestApply2(34910, 14, (long long )((unsigned long )try > (unsigned long )buf));
# 417 "search.c"
          if ((unsigned long )try > (unsigned long )buf) {
            __CrestBranch(34913, 7677, 1);
            {
# 417 "search.c"
            mem_14 = try + -1;
            {
            __CrestLoad(34919, (unsigned long )mem_14, (long long )*mem_14);
            __CrestLoad(34918, (unsigned long )0, (long long )-128);
            __CrestApply2(34917, 5, (long long )((int )((unsigned char )*mem_14) & -128));
            __CrestLoad(34916, (unsigned long )0, (long long )0);
            __CrestApply2(34915, 12, (long long )(((int )((unsigned char )*mem_14) & -128) == 0));
# 417 "search.c"
            if (((int )((unsigned char )*mem_14) & -128) == 0) {
              __CrestBranch(34920, 7680, 1);
# 417 "search.c"
              tmp = __ctype_b_loc();
              __CrestClearStack(34922);
              {
# 417 "search.c"
              mem_15 = try + -1;
# 417 "search.c"
              mem_16 = *tmp + (int )((unsigned char )*mem_15);
              {
              __CrestLoad(34927, (unsigned long )mem_16, (long long )*mem_16);
              __CrestLoad(34926, (unsigned long )0, (long long )8);
              __CrestApply2(34925, 5, (long long )((int const )*mem_16 & 8));
              __CrestLoad(34924, (unsigned long )0, (long long )0);
              __CrestApply2(34923, 13, (long long )(((int const )*mem_16 & 8) != 0));
# 417 "search.c"
              if (((int const )*mem_16 & 8) != 0) {
                __CrestBranch(34928, 7684, 1);
# 418 "search.c"
                goto while_break___0;
              } else {
                __CrestBranch(34929, 7685, 0);
# 417 "search.c"
                goto _L;
              }
              }
              }
            } else {
              __CrestBranch(34921, 7686, 0);
              _L:
              {
# 417 "search.c"
              mem_17 = try + -1;
              {
              __CrestLoad(34932, (unsigned long )mem_17, (long long )*mem_17);
              __CrestLoad(34931, (unsigned long )0, (long long )95);
              __CrestApply2(34930, 12, (long long )((int )((unsigned char )*mem_17) == 95));
# 417 "search.c"
              if ((int )((unsigned char )*mem_17) == 95) {
                __CrestBranch(34933, 7689, 1);
# 418 "search.c"
                goto while_break___0;
              } else {
                __CrestBranch(34934, 7690, 0);

              }
              }
              }
            }
            }
            }
          } else {
            __CrestBranch(34914, 7691, 0);

          }
          }
          {
          __CrestLoad(34941, (unsigned long )(& try), (long long )((unsigned long )try));
          __CrestLoad(34940, (unsigned long )(& len), (long long )len);
          __CrestApply2(34939, 18, (long long )((unsigned long )(try + len)));
          __CrestLoad(34938, (unsigned long )(& buf), (long long )((unsigned long )buf));
          __CrestLoad(34937, (unsigned long )(& size), (long long )size);
          __CrestApply2(34936, 18, (long long )((unsigned long )(buf + size)));
          __CrestApply2(34935, 16, (long long )((unsigned long )(try + len) < (unsigned long )(buf + size)));
# 419 "search.c"
          if ((unsigned long )(try + len) < (unsigned long )(buf + size)) {
            __CrestBranch(34942, 7693, 1);
            {
# 419 "search.c"
            mem_18 = try + len;
            {
            __CrestLoad(34948, (unsigned long )mem_18, (long long )*mem_18);
            __CrestLoad(34947, (unsigned long )0, (long long )-128);
            __CrestApply2(34946, 5, (long long )((int )((unsigned char )*mem_18) & -128));
            __CrestLoad(34945, (unsigned long )0, (long long )0);
            __CrestApply2(34944, 12, (long long )(((int )((unsigned char )*mem_18) & -128) == 0));
# 419 "search.c"
            if (((int )((unsigned char )*mem_18) & -128) == 0) {
              __CrestBranch(34949, 7696, 1);
# 419 "search.c"
              tmp___0 = __ctype_b_loc();
              __CrestClearStack(34951);
              {
# 419 "search.c"
              mem_19 = try + len;
# 419 "search.c"
              mem_20 = *tmp___0 + (int )((unsigned char )*mem_19);
              {
              __CrestLoad(34956, (unsigned long )mem_20, (long long )*mem_20);
              __CrestLoad(34955, (unsigned long )0, (long long )8);
              __CrestApply2(34954, 5, (long long )((int const )*mem_20 & 8));
              __CrestLoad(34953, (unsigned long )0, (long long )0);
              __CrestApply2(34952, 13, (long long )(((int const )*mem_20 & 8) != 0));
# 419 "search.c"
              if (((int const )*mem_20 & 8) != 0) {
                __CrestBranch(34957, 7700, 1);
                __CrestLoad(34961, (unsigned long )(& len), (long long )len);
                __CrestLoad(34960, (unsigned long )0, (long long )1UL);
                __CrestApply2(34959, 1, (long long )(len - 1UL));
                __CrestStore(34962, (unsigned long )(& len));
# 421 "search.c"
                len --;
                __CrestLoad(34963, (unsigned long )(& len), (long long )len);
# 421 "search.c"
                try = kwsexec(kwset, beg, len, & kwsmatch);
                __CrestClearStack(34964);
                __CrestLoad(34965, (unsigned long )(& kwsmatch.size[0]), (long long )kwsmatch.size[0]);
                __CrestStore(34966, (unsigned long )(& len));
# 422 "search.c"
                len = kwsmatch.size[0];
              } else {
                __CrestBranch(34958, 7701, 0);
# 419 "search.c"
                goto _L___0;
              }
              }
              }
            } else {
              __CrestBranch(34950, 7702, 0);
              _L___0:
              {
# 419 "search.c"
              mem_21 = try + len;
              {
              __CrestLoad(34969, (unsigned long )mem_21, (long long )*mem_21);
              __CrestLoad(34968, (unsigned long )0, (long long )95);
              __CrestApply2(34967, 12, (long long )((int )((unsigned char )*mem_21) == 95));
# 419 "search.c"
              if ((int )((unsigned char )*mem_21) == 95) {
                __CrestBranch(34970, 7705, 1);
                __CrestLoad(34974, (unsigned long )(& len), (long long )len);
                __CrestLoad(34973, (unsigned long )0, (long long )1UL);
                __CrestApply2(34972, 1, (long long )(len - 1UL));
                __CrestStore(34975, (unsigned long )(& len));
# 421 "search.c"
                len --;
                __CrestLoad(34976, (unsigned long )(& len), (long long )len);
# 421 "search.c"
                try = kwsexec(kwset, beg, len, & kwsmatch);
                __CrestClearStack(34977);
                __CrestLoad(34978, (unsigned long )(& kwsmatch.size[0]), (long long )kwsmatch.size[0]);
                __CrestStore(34979, (unsigned long )(& len));
# 422 "search.c"
                len = kwsmatch.size[0];
              } else {
                __CrestBranch(34971, 7706, 0);
# 425 "search.c"
                goto success;
              }
              }
              }
            }
            }
            }
          } else {
            __CrestBranch(34943, 7707, 0);
# 425 "search.c"
            goto success;
          }
          }
        }
        while_break___0: ;
        }
      } else {
        __CrestBranch(34899, 7709, 0);
# 428 "search.c"
        goto success;
      }
      }
    }
    }
    __Cont:
# 401 "search.c"
    beg ++;
  }
  while_break: ;
  }
# 431 "search.c"
  __retres23 = (char *)0;
# 431 "search.c"
  goto return_label;
  success:
  __CrestLoad(34980, (unsigned long )0, (long long )'\n');
  __CrestLoad(34987, (unsigned long )(& buf), (long long )((unsigned long )buf));
  __CrestLoad(34986, (unsigned long )(& size), (long long )size);
  __CrestApply2(34985, 18, (long long )((unsigned long )(buf + size)));
  __CrestLoad(34984, (unsigned long )(& beg), (long long )((unsigned long )beg));
  __CrestLoad(34983, (unsigned long )(& len), (long long )len);
  __CrestApply2(34982, 18, (long long )((unsigned long )(beg + len)));
  __CrestApply2(34981, 18, (long long )((buf + size) - (beg + len)));
# 434 "search.c"
  tmp___1 = memchr((void const *)(beg + len), '\n', (size_t )((buf + size) - (beg + len)));
  __CrestClearStack(34988);
# 434 "search.c"
  end = (char *)tmp___1;
  {
  __CrestLoad(34991, (unsigned long )(& end), (long long )((unsigned long )end));
  __CrestLoad(34990, (unsigned long )0, (long long )((unsigned long )((char *)0)));
  __CrestApply2(34989, 13, (long long )((unsigned long )end != (unsigned long )((char *)0)));
# 434 "search.c"
  if ((unsigned long )end != (unsigned long )((char *)0)) {
    __CrestBranch(34992, 7716, 1);
# 435 "search.c"
    end ++;
  } else {
    __CrestBranch(34993, 7717, 0);
# 437 "search.c"
    end = buf + size;
  }
  }
# 438 "search.c"
  *endp = end;
  {
# 439 "search.c"
  while (1) {
    while_continue___1: ;
    {
    __CrestLoad(34996, (unsigned long )(& beg), (long long )((unsigned long )beg));
    __CrestLoad(34995, (unsigned long )(& buf), (long long )((unsigned long )buf));
    __CrestApply2(34994, 14, (long long )((unsigned long )beg > (unsigned long )buf));
# 439 "search.c"
    if ((unsigned long )beg > (unsigned long )buf) {
      __CrestBranch(34997, 7723, 1);
      {
# 439 "search.c"
      mem_22 = beg + -1;
      {
      __CrestLoad(35001, (unsigned long )mem_22, (long long )*mem_22);
      __CrestLoad(35000, (unsigned long )0, (long long )10);
      __CrestApply2(34999, 13, (long long )((int )*mem_22 != 10));
# 439 "search.c"
      if ((int )*mem_22 != 10) {
        __CrestBranch(35002, 7726, 1);

      } else {
        __CrestBranch(35003, 7727, 0);
# 439 "search.c"
        goto while_break___1;
      }
      }
      }
    } else {
      __CrestBranch(34998, 7728, 0);
# 439 "search.c"
      goto while_break___1;
    }
    }
# 440 "search.c"
    beg --;
  }
  while_break___1: ;
  }
# 441 "search.c"
  __retres23 = beg;
  return_label:
  {
  __CrestReturn(35004);
# 391 "search.c"
  return (__retres23);
  }
}
}
void __globinit_search(void)
{


  {
  __CrestInit();
}
}
