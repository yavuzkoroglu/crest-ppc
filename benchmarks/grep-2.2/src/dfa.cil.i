# 1 "./dfa.cil.c"
# 1 "/home/yavuz/crest/benchmarks/grep-2.2/src//"
# 1 "<command-line>"
# 1 "/usr/include/stdc-predef.h" 1 3 4
# 1 "<command-line>" 2
# 1 "./dfa.cil.c"



extern void __CrestInit(void) __attribute__((__crest_skip__)) ;
extern void __CrestHandleReturn(int id , long long val ) __attribute__((__crest_skip__)) ;
extern void __CrestReturn(int id ) __attribute__((__crest_skip__)) ;
extern void __CrestCall(int id , unsigned int fid ) __attribute__((__crest_skip__)) ;
extern void __CrestBranch(int id , int bid , unsigned char b ) __attribute__((__crest_skip__)) ;
extern void __CrestApply2(int id , int op , long long val ) __attribute__((__crest_skip__)) ;
extern void __CrestApply1(int id , int op , long long val ) __attribute__((__crest_skip__)) ;
extern void __CrestClearStack(int id ) __attribute__((__crest_skip__)) ;
extern void __CrestStore(int id , unsigned long addr ) __attribute__((__crest_skip__)) ;
extern void __CrestLoad(int id , unsigned long addr , long long val ) __attribute__((__crest_skip__)) ;
# 212 "/usr/lib/gcc/x86_64-linux-gnu/4.8/include/stddef.h"
typedef unsigned long size_t;
# 53 "regex.h"
typedef unsigned long reg_syntax_t;
# 29 "dfa.h"
typedef void *ptr_t;
# 57 "dfa.h"
typedef int charclass[(((unsigned long )(1 << 8) + 8UL * sizeof(int )) - 1UL) / (8UL * sizeof(int ))];
# 63 "dfa.h"
enum __anonenum_token_28 {
    END = -1,
    EMPTY = 256,
    BACKREF = 257,
    BEGLINE = 258,
    ENDLINE = 259,
    BEGWORD = 260,
    ENDWORD = 261,
    LIMWORD = 262,
    NOTLIMWORD = 263,
    QMARK = 264,
    STAR = 265,
    PLUS = 266,
    REPMN = 267,
    CAT = 268,
    OR = 269,
    ORTOP = 270,
    LPAREN = 271,
    RPAREN = 272,
    CSET = 273
} ;
# 63 "dfa.h"
typedef enum __anonenum_token_28 token;
# 201 "dfa.h"
struct __anonstruct_position_29 {
   unsigned int strchr ;
   unsigned int constraint ;
};
# 201 "dfa.h"
typedef struct __anonstruct_position_29 position;
# 208 "dfa.h"
struct __anonstruct_position_set_30 {
   position *elems ;
   int nelem ;
};
# 208 "dfa.h"
typedef struct __anonstruct_position_set_30 position_set;
# 217 "dfa.h"
struct __anonstruct_dfa_state_31 {
   int hash ;
   position_set elems ;
   char newline ;
   char letter ;
   char backref ;
   unsigned char constraint ;
   int first_end ;
};
# 217 "dfa.h"
typedef struct __anonstruct_dfa_state_31 dfa_state;
# 230 "dfa.h"
struct dfamust {
   int exact ;
   char *must ;
   struct dfamust *next ;
};
# 238 "dfa.h"
struct dfa {
   charclass *charclasses ;
   int cindex ;
   int calloc ;
   token *tokens ;
   int tindex ;
   int talloc ;
   int depth ;
   int nleaves ;
   int nregexps ;
   dfa_state *states ;
   int sindex ;
   int salloc ;
   position_set *follows ;
   int searchflag ;
   int tralloc ;
   int trcount ;
   int **trans ;
   int **realtrans ;
   int **fails ;
   int *success ;
   int *newlines ;
   struct dfamust *musts ;
};
# 387 "dfa.c"
struct __anonstruct_prednames_32 {
   char const *name ;
   int (*pred)(int ) ;
};
# 2339 "dfa.c"
struct __anonstruct_must_33 {
   char **in ;
   char *left ;
   char *right ;
   char *is ;
};
# 2339 "dfa.c"
typedef struct __anonstruct_must_33 must;
# 79 "/usr/include/ctype.h"
extern __attribute__((__nothrow__)) unsigned short const **( __attribute__((__leaf__)) __ctype_b_loc)(void) __attribute__((__const__)) ;
# 124 "/usr/include/ctype.h"
extern __attribute__((__nothrow__)) int ( __attribute__((__leaf__)) tolower)(int __c ) ;
# 127 "/usr/include/ctype.h"
extern __attribute__((__nothrow__)) int ( __attribute__((__leaf__)) toupper)(int __c ) ;
# 466 "/usr/include/stdlib.h"
extern __attribute__((__nothrow__)) void *( __attribute__((__leaf__)) malloc)(size_t __size ) __attribute__((__malloc__)) ;
# 468 "/usr/include/stdlib.h"
extern __attribute__((__nothrow__)) void *( __attribute__((__leaf__)) calloc)(size_t __nmemb ,
                                                                               size_t __size ) __attribute__((__malloc__)) ;
# 480 "/usr/include/stdlib.h"
extern __attribute__((__nothrow__)) void *( __attribute__((__warn_unused_result__,
__leaf__)) realloc)(void *__ptr , size_t __size ) ;
# 483 "/usr/include/stdlib.h"
extern __attribute__((__nothrow__)) void ( __attribute__((__leaf__)) free)(void *__ptr ) ;
# 515 "/usr/include/stdlib.h"
extern __attribute__((__nothrow__, __noreturn__)) void ( __attribute__((__leaf__)) abort)(void) ;
# 129 "/usr/include/string.h"
extern __attribute__((__nothrow__)) char *( __attribute__((__nonnull__(1,2), __leaf__)) strcpy)(char * __restrict __dest ,
                                                                                                 char const * __restrict __src ) ;
# 144 "/usr/include/string.h"
extern __attribute__((__nothrow__)) int ( __attribute__((__nonnull__(1,2), __leaf__)) strcmp)(char const *__s1 ,
                                                                                               char const *__s2 ) __attribute__((__pure__)) ;
# 147 "/usr/include/string.h"
extern __attribute__((__nothrow__)) int ( __attribute__((__nonnull__(1,2), __leaf__)) strncmp)(char const *__s1 ,
                                                                                                char const *__s2 ,
                                                                                                size_t __n ) __attribute__((__pure__)) ;
# 236 "/usr/include/string.h"
extern __attribute__((__nothrow__)) char *( __attribute__((__nonnull__(1), __leaf__)) strchr)(char const *__s ,
                                                                                               int __c ) __attribute__((__pure__)) ;
# 399 "/usr/include/string.h"
extern __attribute__((__nothrow__)) size_t ( __attribute__((__nonnull__(1), __leaf__)) strlen)(char const *__s ) __attribute__((__pure__)) ;
# 105 "../intl/libintl.h"
extern char *dcgettext(char const *__domainname , char const *__msgid , int __category ) ;
# 325 "dfa.h"
void dfasyntax(reg_syntax_t bits , int fold ) ;
# 330 "dfa.h"
void dfacomp(char *s , size_t len , struct dfa *d , int searchflag ) ;
# 344 "dfa.h"
char *dfaexec(struct dfa *d , char *begin , char *end , int newline___0 , int *count ,
              int *backref ) ;
# 347 "dfa.h"
void dfafree(struct dfa *d ) ;
# 352 "dfa.h"
void dfainit(struct dfa *d ) ;
# 355 "dfa.h"
void dfaparse(char *s , size_t len , struct dfa *d ) ;
# 359 "dfa.h"
void dfaanalyze(struct dfa *d , int searchflag ) ;
# 363 "dfa.h"
void dfastate(int s , struct dfa *d , int *trans ) ;
# 371 "dfa.h"
extern void dfaerror(char const * ) ;
# 96 "dfa.c"
static void dfamust(struct dfa *dfa___0 ) ;
# 98 "dfa.c"
static ptr_t xcalloc(size_t n , size_t s ) ;
# 99 "dfa.c"
static ptr_t xmalloc(size_t n ) ;
# 100 "dfa.c"
static ptr_t xrealloc(ptr_t p , size_t n ) ;
# 104 "dfa.c"
static int tstbit(int b , int *c ) ;
# 105 "dfa.c"
static void setbit(int b , int *c ) ;
# 106 "dfa.c"
static void clrbit(int b , int *c ) ;
# 107 "dfa.c"
static void copyset(int *src , int *dst ) ;
# 108 "dfa.c"
static void zeroset(int *s ) ;
# 109 "dfa.c"
static void notset(int *s ) ;
# 110 "dfa.c"
static int equal(int *s1 , int *s2 ) ;
# 111 "dfa.c"
static int charclass_index(int *s ) ;
# 112 "dfa.c"
static int looking_at(char const *s ) ;
# 113 "dfa.c"
static token lex(void) ;
# 114 "dfa.c"
static void addtok(token t ) ;
# 115 "dfa.c"
static void atom(void) ;
# 116 "dfa.c"
static int nsubtoks(int tindex ) ;
# 117 "dfa.c"
static void copytoks(int tindex , int ntokens ) ;
# 118 "dfa.c"
static void closure(void) ;
# 119 "dfa.c"
static void branch(void) ;
# 120 "dfa.c"
static void regexp(int toplevel ) ;
# 121 "dfa.c"
static void copy(position_set *src , position_set *dst ) ;
# 122 "dfa.c"
static void insert(position p , position_set *s ) ;
# 123 "dfa.c"
static void merge(position_set *s1 , position_set *s2 , position_set *m ) ;
# 124 "dfa.c"
static void delete(position p , position_set *s ) ;
# 125 "dfa.c"
static int state_index(struct dfa *d , position_set *s , int newline , int letter ) ;
# 127 "dfa.c"
static void build_state(int s , struct dfa *d ) ;
# 128 "dfa.c"
static void build_state_zero(struct dfa *d ) ;
# 129 "dfa.c"
static char *icatalloc(char *old , char *new ) ;
# 130 "dfa.c"
static char *icpyalloc(char *string ) ;
# 131 "dfa.c"
static char *istrstr(char *lookin , char *lookfor ) ;
# 132 "dfa.c"
static void ifree(char *cp ) ;
# 133 "dfa.c"
static void freelist(char **cpp ) ;
# 134 "dfa.c"
static char **enlist(char **cpp , char *new , size_t len ) ;
# 135 "dfa.c"
static char **comsubs(char *left , char *right ) ;
# 136 "dfa.c"
static char **addlists(char **old , char **new ) ;
# 137 "dfa.c"
static char **inboth(char **left , char **right ) ;
# 139 "dfa.c"
static ptr_t xcalloc(size_t n , size_t s )
{
  ptr_t r ;
  void *tmp ;
  char *tmp___0 ;

  {
  __CrestCall(27531, 212);
  __CrestStore(27530, (unsigned long )(& s));
  __CrestStore(27529, (unsigned long )(& n));
  __CrestLoad(27532, (unsigned long )(& n), (long long )n);
  __CrestLoad(27533, (unsigned long )(& s), (long long )s);
# 144 "dfa.c"
  tmp = calloc(n, s);
  __CrestClearStack(27534);
# 144 "dfa.c"
  r = tmp;
  {
  __CrestLoad(27537, (unsigned long )(& r), (long long )((unsigned long )r));
  __CrestLoad(27536, (unsigned long )0, (long long )0);
  __CrestApply2(27535, 12, (long long )(r == 0));
# 146 "dfa.c"
  if (r == 0) {
    __CrestBranch(27538, 3818, 1);
    __CrestLoad(27540, (unsigned long )0, (long long )5);
# 147 "dfa.c"
    tmp___0 = dcgettext((char const *)((void *)0), "Memory exhausted", 5);
    __CrestClearStack(27541);
# 147 "dfa.c"
    dfaerror((char const *)tmp___0);
    __CrestClearStack(27542);
  } else {
    __CrestBranch(27539, 3819, 0);

  }
  }
  {
  __CrestReturn(27543);
# 148 "dfa.c"
  return (r);
  }
}
}
# 151 "dfa.c"
static ptr_t xmalloc(size_t n )
{
  ptr_t r ;
  void *tmp ;
  char *tmp___0 ;

  {
  __CrestCall(27545, 213);
  __CrestStore(27544, (unsigned long )(& n));
  __CrestLoad(27546, (unsigned long )(& n), (long long )n);
# 155 "dfa.c"
  tmp = malloc(n);
  __CrestClearStack(27547);
# 155 "dfa.c"
  r = tmp;
  {
  __CrestLoad(27550, (unsigned long )(& r), (long long )((unsigned long )r));
  __CrestLoad(27549, (unsigned long )0, (long long )0);
  __CrestApply2(27548, 12, (long long )(r == 0));
# 158 "dfa.c"
  if (r == 0) {
    __CrestBranch(27551, 3823, 1);
    __CrestLoad(27553, (unsigned long )0, (long long )5);
# 159 "dfa.c"
    tmp___0 = dcgettext((char const *)((void *)0), "Memory exhausted", 5);
    __CrestClearStack(27554);
# 159 "dfa.c"
    dfaerror((char const *)tmp___0);
    __CrestClearStack(27555);
  } else {
    __CrestBranch(27552, 3824, 0);

  }
  }
  {
  __CrestReturn(27556);
# 160 "dfa.c"
  return (r);
  }
}
}
# 163 "dfa.c"
static ptr_t xrealloc(ptr_t p , size_t n )
{
  ptr_t r ;
  void *tmp ;
  char *tmp___0 ;

  {
  __CrestCall(27558, 214);
  __CrestStore(27557, (unsigned long )(& n));
  __CrestLoad(27559, (unsigned long )(& n), (long long )n);
# 168 "dfa.c"
  tmp = realloc(p, n);
  __CrestClearStack(27560);
# 168 "dfa.c"
  r = tmp;
  {
  __CrestLoad(27563, (unsigned long )(& r), (long long )((unsigned long )r));
  __CrestLoad(27562, (unsigned long )0, (long long )0);
  __CrestApply2(27561, 12, (long long )(r == 0));
# 171 "dfa.c"
  if (r == 0) {
    __CrestBranch(27564, 3828, 1);
    __CrestLoad(27566, (unsigned long )0, (long long )5);
# 172 "dfa.c"
    tmp___0 = dcgettext((char const *)((void *)0), "Memory exhausted", 5);
    __CrestClearStack(27567);
# 172 "dfa.c"
    dfaerror((char const *)tmp___0);
    __CrestClearStack(27568);
  } else {
    __CrestBranch(27565, 3829, 0);

  }
  }
  {
  __CrestReturn(27569);
# 173 "dfa.c"
  return (r);
  }
}
}
# 230 "dfa.c"
static int tstbit(int b , int *c )
{
  int *mem_3 ;
  int __retres4 ;

  {
  __CrestCall(27571, 215);
  __CrestStore(27570, (unsigned long )(& b));
  {
# 235 "dfa.c"
  mem_3 = c + (unsigned long )b / (8UL * sizeof(int ));
  __CrestLoad(27578, (unsigned long )mem_3, (long long )*mem_3);
  __CrestLoad(27577, (unsigned long )0, (long long )1);
  __CrestLoad(27576, (unsigned long )(& b), (long long )b);
  __CrestLoad(27575, (unsigned long )0, (long long )(8UL * sizeof(int )));
  __CrestApply2(27574, 4, (long long )((unsigned long )b % (8UL * sizeof(int ))));
  __CrestApply2(27573, 8, (long long )(1 << (unsigned long )b % (8UL * sizeof(int ))));
  __CrestApply2(27572, 5, (long long )(*mem_3 & (1 << (unsigned long )b % (8UL * sizeof(int )))));
  __CrestStore(27579, (unsigned long )(& __retres4));
# 235 "dfa.c"
  __retres4 = *mem_3 & (1 << (unsigned long )b % (8UL * sizeof(int )));
# 235 "dfa.c"
  goto return_label;
  }
  return_label:
  {
  __CrestLoad(27580, (unsigned long )(& __retres4), (long long )__retres4);
  __CrestReturn(27581);
# 230 "dfa.c"
  return (__retres4);
  }
}
}
# 238 "dfa.c"
static void setbit(int b , int *c )
{
  int *mem_3 ;
  int *mem_4 ;

  {
  __CrestCall(27583, 216);
  __CrestStore(27582, (unsigned long )(& b));
# 243 "dfa.c"
  mem_3 = c + (unsigned long )b / (8UL * sizeof(int ));
# 243 "dfa.c"
  mem_4 = c + (unsigned long )b / (8UL * sizeof(int ));
  __CrestLoad(27590, (unsigned long )mem_4, (long long )*mem_4);
  __CrestLoad(27589, (unsigned long )0, (long long )1);
  __CrestLoad(27588, (unsigned long )(& b), (long long )b);
  __CrestLoad(27587, (unsigned long )0, (long long )(8UL * sizeof(int )));
  __CrestApply2(27586, 4, (long long )((unsigned long )b % (8UL * sizeof(int ))));
  __CrestApply2(27585, 8, (long long )(1 << (unsigned long )b % (8UL * sizeof(int ))));
  __CrestApply2(27584, 6, (long long )(*mem_4 | (1 << (unsigned long )b % (8UL * sizeof(int )))));
  __CrestStore(27591, (unsigned long )mem_3);
# 243 "dfa.c"
  *mem_3 = *mem_4 | (1 << (unsigned long )b % (8UL * sizeof(int )));

  {
  __CrestReturn(27592);
# 238 "dfa.c"
  return;
  }
}
}
# 246 "dfa.c"
static void clrbit(int b , int *c )
{
  int *mem_3 ;
  int *mem_4 ;

  {
  __CrestCall(27594, 217);
  __CrestStore(27593, (unsigned long )(& b));
# 251 "dfa.c"
  mem_3 = c + (unsigned long )b / (8UL * sizeof(int ));
# 251 "dfa.c"
  mem_4 = c + (unsigned long )b / (8UL * sizeof(int ));
  __CrestLoad(27602, (unsigned long )mem_4, (long long )*mem_4);
  __CrestLoad(27601, (unsigned long )0, (long long )1);
  __CrestLoad(27600, (unsigned long )(& b), (long long )b);
  __CrestLoad(27599, (unsigned long )0, (long long )(8UL * sizeof(int )));
  __CrestApply2(27598, 4, (long long )((unsigned long )b % (8UL * sizeof(int ))));
  __CrestApply2(27597, 8, (long long )(1 << (unsigned long )b % (8UL * sizeof(int ))));
  __CrestApply1(27596, 20, (long long )(~ (1 << (unsigned long )b % (8UL * sizeof(int )))));
  __CrestApply2(27595, 5, (long long )(*mem_4 & ~ (1 << (unsigned long )b % (8UL * sizeof(int )))));
  __CrestStore(27603, (unsigned long )mem_3);
# 251 "dfa.c"
  *mem_3 = *mem_4 & ~ (1 << (unsigned long )b % (8UL * sizeof(int )));

  {
  __CrestReturn(27604);
# 246 "dfa.c"
  return;
  }
}
}
# 254 "dfa.c"
static void copyset(int *src , int *dst )
{
  int i ;
  int *mem_4 ;
  int *mem_5 ;

  {
  __CrestCall(27605, 218);

  __CrestLoad(27606, (unsigned long )0, (long long )0);
  __CrestStore(27607, (unsigned long )(& i));
# 261 "dfa.c"
  i = 0;
  {
# 261 "dfa.c"
  while (1) {
    while_continue: ;
    {
    __CrestLoad(27610, (unsigned long )(& i), (long long )i);
    __CrestLoad(27609, (unsigned long )0, (long long )((((unsigned long )(1 << 8) + 8UL * sizeof(int )) - 1UL) / (8UL * sizeof(int ))));
    __CrestApply2(27608, 16, (long long )((unsigned long )i < (((unsigned long )(1 << 8) + 8UL * sizeof(int )) - 1UL) / (8UL * sizeof(int ))));
# 261 "dfa.c"
    if ((unsigned long )i < (((unsigned long )(1 << 8) + 8UL * sizeof(int )) - 1UL) / (8UL * sizeof(int ))) {
      __CrestBranch(27611, 3847, 1);

    } else {
      __CrestBranch(27612, 3848, 0);
# 261 "dfa.c"
      goto while_break;
    }
    }
# 262 "dfa.c"
    mem_4 = dst + i;
# 262 "dfa.c"
    mem_5 = src + i;
    __CrestLoad(27613, (unsigned long )mem_5, (long long )*mem_5);
    __CrestStore(27614, (unsigned long )mem_4);
# 262 "dfa.c"
    *mem_4 = *mem_5;
    __CrestLoad(27617, (unsigned long )(& i), (long long )i);
    __CrestLoad(27616, (unsigned long )0, (long long )1);
    __CrestApply2(27615, 0, (long long )(i + 1));
    __CrestStore(27618, (unsigned long )(& i));
# 261 "dfa.c"
    i ++;
  }
  while_break: ;
  }

  {
  __CrestReturn(27619);
# 254 "dfa.c"
  return;
  }
}
}
# 265 "dfa.c"
static void zeroset(int *s )
{
  int i ;
  int *mem_3 ;

  {
  __CrestCall(27620, 219);

  __CrestLoad(27621, (unsigned long )0, (long long )0);
  __CrestStore(27622, (unsigned long )(& i));
# 271 "dfa.c"
  i = 0;
  {
# 271 "dfa.c"
  while (1) {
    while_continue: ;
    {
    __CrestLoad(27625, (unsigned long )(& i), (long long )i);
    __CrestLoad(27624, (unsigned long )0, (long long )((((unsigned long )(1 << 8) + 8UL * sizeof(int )) - 1UL) / (8UL * sizeof(int ))));
    __CrestApply2(27623, 16, (long long )((unsigned long )i < (((unsigned long )(1 << 8) + 8UL * sizeof(int )) - 1UL) / (8UL * sizeof(int ))));
# 271 "dfa.c"
    if ((unsigned long )i < (((unsigned long )(1 << 8) + 8UL * sizeof(int )) - 1UL) / (8UL * sizeof(int ))) {
      __CrestBranch(27626, 3858, 1);

    } else {
      __CrestBranch(27627, 3859, 0);
# 271 "dfa.c"
      goto while_break;
    }
    }
# 272 "dfa.c"
    mem_3 = s + i;
    __CrestLoad(27628, (unsigned long )0, (long long )0);
    __CrestStore(27629, (unsigned long )mem_3);
# 272 "dfa.c"
    *mem_3 = 0;
    __CrestLoad(27632, (unsigned long )(& i), (long long )i);
    __CrestLoad(27631, (unsigned long )0, (long long )1);
    __CrestApply2(27630, 0, (long long )(i + 1));
    __CrestStore(27633, (unsigned long )(& i));
# 271 "dfa.c"
    i ++;
  }
  while_break: ;
  }

  {
  __CrestReturn(27634);
# 265 "dfa.c"
  return;
  }
}
}
# 275 "dfa.c"
static void notset(int *s )
{
  int i ;
  int *mem_3 ;
  int *mem_4 ;

  {
  __CrestCall(27635, 220);

  __CrestLoad(27636, (unsigned long )0, (long long )0);
  __CrestStore(27637, (unsigned long )(& i));
# 281 "dfa.c"
  i = 0;
  {
# 281 "dfa.c"
  while (1) {
    while_continue: ;
    {
    __CrestLoad(27640, (unsigned long )(& i), (long long )i);
    __CrestLoad(27639, (unsigned long )0, (long long )((((unsigned long )(1 << 8) + 8UL * sizeof(int )) - 1UL) / (8UL * sizeof(int ))));
    __CrestApply2(27638, 16, (long long )((unsigned long )i < (((unsigned long )(1 << 8) + 8UL * sizeof(int )) - 1UL) / (8UL * sizeof(int ))));
# 281 "dfa.c"
    if ((unsigned long )i < (((unsigned long )(1 << 8) + 8UL * sizeof(int )) - 1UL) / (8UL * sizeof(int ))) {
      __CrestBranch(27641, 3869, 1);

    } else {
      __CrestBranch(27642, 3870, 0);
# 281 "dfa.c"
      goto while_break;
    }
    }
# 282 "dfa.c"
    mem_3 = s + i;
# 282 "dfa.c"
    mem_4 = s + i;
    __CrestLoad(27644, (unsigned long )mem_4, (long long )*mem_4);
    __CrestApply1(27643, 20, (long long )(~ *mem_4));
    __CrestStore(27645, (unsigned long )mem_3);
# 282 "dfa.c"
    *mem_3 = ~ *mem_4;
    __CrestLoad(27648, (unsigned long )(& i), (long long )i);
    __CrestLoad(27647, (unsigned long )0, (long long )1);
    __CrestApply2(27646, 0, (long long )(i + 1));
    __CrestStore(27649, (unsigned long )(& i));
# 281 "dfa.c"
    i ++;
  }
  while_break: ;
  }

  {
  __CrestReturn(27650);
# 275 "dfa.c"
  return;
  }
}
}
# 285 "dfa.c"
static int equal(int *s1 , int *s2 )
{
  int i ;
  int *mem_4 ;
  int *mem_5 ;
  int __retres6 ;

  {
  __CrestCall(27651, 221);

  __CrestLoad(27652, (unsigned long )0, (long long )0);
  __CrestStore(27653, (unsigned long )(& i));
# 292 "dfa.c"
  i = 0;
  {
# 292 "dfa.c"
  while (1) {
    while_continue: ;
    {
    __CrestLoad(27656, (unsigned long )(& i), (long long )i);
    __CrestLoad(27655, (unsigned long )0, (long long )((((unsigned long )(1 << 8) + 8UL * sizeof(int )) - 1UL) / (8UL * sizeof(int ))));
    __CrestApply2(27654, 16, (long long )((unsigned long )i < (((unsigned long )(1 << 8) + 8UL * sizeof(int )) - 1UL) / (8UL * sizeof(int ))));
# 292 "dfa.c"
    if ((unsigned long )i < (((unsigned long )(1 << 8) + 8UL * sizeof(int )) - 1UL) / (8UL * sizeof(int ))) {
      __CrestBranch(27657, 3880, 1);

    } else {
      __CrestBranch(27658, 3881, 0);
# 292 "dfa.c"
      goto while_break;
    }
    }
    {
# 293 "dfa.c"
    mem_4 = s1 + i;
# 293 "dfa.c"
    mem_5 = s2 + i;
    {
    __CrestLoad(27661, (unsigned long )mem_4, (long long )*mem_4);
    __CrestLoad(27660, (unsigned long )mem_5, (long long )*mem_5);
    __CrestApply2(27659, 13, (long long )(*mem_4 != *mem_5));
# 293 "dfa.c"
    if (*mem_4 != *mem_5) {
      __CrestBranch(27662, 3885, 1);
      __CrestLoad(27664, (unsigned long )0, (long long )0);
      __CrestStore(27665, (unsigned long )(& __retres6));
# 294 "dfa.c"
      __retres6 = 0;
# 294 "dfa.c"
      goto return_label;
    } else {
      __CrestBranch(27663, 3887, 0);

    }
    }
    }
    __CrestLoad(27668, (unsigned long )(& i), (long long )i);
    __CrestLoad(27667, (unsigned long )0, (long long )1);
    __CrestApply2(27666, 0, (long long )(i + 1));
    __CrestStore(27669, (unsigned long )(& i));
# 292 "dfa.c"
    i ++;
  }
  while_break: ;
  }
  __CrestLoad(27670, (unsigned long )0, (long long )1);
  __CrestStore(27671, (unsigned long )(& __retres6));
# 295 "dfa.c"
  __retres6 = 1;
  return_label:
  {
  __CrestLoad(27672, (unsigned long )(& __retres6), (long long )__retres6);
  __CrestReturn(27673);
# 285 "dfa.c"
  return (__retres6);
  }
}
}
# 299 "dfa.c"
static struct dfa *dfa ;
# 302 "dfa.c"
static int charclass_index(int *s )
{
  int i ;
  int tmp ;
  ptr_t tmp___0 ;
  charclass *mem_5 ;
  charclass *mem_6 ;
  int __retres7 ;

  {
  __CrestCall(27674, 222);

  __CrestLoad(27675, (unsigned long )0, (long long )0);
  __CrestStore(27676, (unsigned long )(& i));
# 308 "dfa.c"
  i = 0;
  {
# 308 "dfa.c"
  while (1) {
    while_continue: ;
    {
    __CrestLoad(27679, (unsigned long )(& i), (long long )i);
    __CrestLoad(27678, (unsigned long )(& dfa->cindex), (long long )dfa->cindex);
    __CrestApply2(27677, 16, (long long )(i < dfa->cindex));
# 308 "dfa.c"
    if (i < dfa->cindex) {
      __CrestBranch(27680, 3897, 1);

    } else {
      __CrestBranch(27681, 3898, 0);
# 308 "dfa.c"
      goto while_break;
    }
    }
# 309 "dfa.c"
    mem_5 = dfa->charclasses + i;
# 309 "dfa.c"
    tmp = equal(s, *mem_5);
    __CrestHandleReturn(27683, (long long )tmp);
    __CrestStore(27682, (unsigned long )(& tmp));
    {
    __CrestLoad(27686, (unsigned long )(& tmp), (long long )tmp);
    __CrestLoad(27685, (unsigned long )0, (long long )0);
    __CrestApply2(27684, 13, (long long )(tmp != 0));
# 309 "dfa.c"
    if (tmp != 0) {
      __CrestBranch(27687, 3901, 1);
      __CrestLoad(27689, (unsigned long )(& i), (long long )i);
      __CrestStore(27690, (unsigned long )(& __retres7));
# 310 "dfa.c"
      __retres7 = i;
# 310 "dfa.c"
      goto return_label;
    } else {
      __CrestBranch(27688, 3903, 0);

    }
    }
    __CrestLoad(27693, (unsigned long )(& i), (long long )i);
    __CrestLoad(27692, (unsigned long )0, (long long )1);
    __CrestApply2(27691, 0, (long long )(i + 1));
    __CrestStore(27694, (unsigned long )(& i));
# 308 "dfa.c"
    i ++;
  }
  while_break: ;
  }
  {
  __CrestLoad(27697, (unsigned long )(& dfa->cindex), (long long )dfa->cindex);
  __CrestLoad(27696, (unsigned long )(& dfa->calloc), (long long )dfa->calloc);
  __CrestApply2(27695, 17, (long long )(dfa->cindex >= dfa->calloc));
# 311 "dfa.c"
  if (dfa->cindex >= dfa->calloc) {
    __CrestBranch(27698, 3907, 1);
    {
# 311 "dfa.c"
    while (1) {
      while_continue___0: ;
      {
      __CrestLoad(27702, (unsigned long )(& dfa->cindex), (long long )dfa->cindex);
      __CrestLoad(27701, (unsigned long )(& dfa->calloc), (long long )dfa->calloc);
      __CrestApply2(27700, 17, (long long )(dfa->cindex >= dfa->calloc));
# 311 "dfa.c"
      if (dfa->cindex >= dfa->calloc) {
        __CrestBranch(27703, 3911, 1);

      } else {
        __CrestBranch(27704, 3912, 0);
# 311 "dfa.c"
        goto while_break___0;
      }
      }
      __CrestLoad(27707, (unsigned long )(& dfa->calloc), (long long )dfa->calloc);
      __CrestLoad(27706, (unsigned long )0, (long long )2);
      __CrestApply2(27705, 2, (long long )(dfa->calloc * 2));
      __CrestStore(27708, (unsigned long )(& dfa->calloc));
# 311 "dfa.c"
      dfa->calloc *= 2;
    }
    while_break___0: ;
    }
    __CrestLoad(27711, (unsigned long )(& dfa->calloc), (long long )dfa->calloc);
    __CrestLoad(27710, (unsigned long )0, (long long )sizeof(charclass ));
    __CrestApply2(27709, 2, (long long )((unsigned long )dfa->calloc * sizeof(charclass )));
# 311 "dfa.c"
    tmp___0 = xrealloc((ptr_t )dfa->charclasses, (unsigned long )dfa->calloc * sizeof(charclass ));
    __CrestClearStack(27712);
# 311 "dfa.c"
    dfa->charclasses = (charclass *)tmp___0;
  } else {
    __CrestBranch(27699, 3916, 0);

  }
  }
  __CrestLoad(27715, (unsigned long )(& dfa->cindex), (long long )dfa->cindex);
  __CrestLoad(27714, (unsigned long )0, (long long )1);
  __CrestApply2(27713, 0, (long long )(dfa->cindex + 1));
  __CrestStore(27716, (unsigned long )(& dfa->cindex));
# 312 "dfa.c"
  (dfa->cindex) ++;
# 313 "dfa.c"
  mem_6 = dfa->charclasses + i;
# 313 "dfa.c"
  copyset(s, *mem_6);
  __CrestClearStack(27717);
  __CrestLoad(27718, (unsigned long )(& i), (long long )i);
  __CrestStore(27719, (unsigned long )(& __retres7));
# 314 "dfa.c"
  __retres7 = i;
  return_label:
  {
  __CrestLoad(27720, (unsigned long )(& __retres7), (long long )__retres7);
  __CrestReturn(27721);
# 302 "dfa.c"
  return (__retres7);
  }
}
}
# 318 "dfa.c"
static reg_syntax_t syntax_bits ;
# 318 "dfa.c"
static reg_syntax_t syntax_bits_set ;
# 321 "dfa.c"
static int case_fold ;
# 324 "dfa.c"
void dfasyntax(reg_syntax_t bits , int fold )
{


  {
  __CrestCall(27724, 223);
  __CrestStore(27723, (unsigned long )(& fold));
  __CrestStore(27722, (unsigned long )(& bits));
  __CrestLoad(27725, (unsigned long )0, (long long )((reg_syntax_t )1));
  __CrestStore(27726, (unsigned long )(& syntax_bits_set));
# 329 "dfa.c"
  syntax_bits_set = (reg_syntax_t )1;
  __CrestLoad(27727, (unsigned long )(& bits), (long long )bits);
  __CrestStore(27728, (unsigned long )(& syntax_bits));
# 330 "dfa.c"
  syntax_bits = bits;
  __CrestLoad(27729, (unsigned long )(& fold), (long long )fold);
  __CrestStore(27730, (unsigned long )(& case_fold));
# 331 "dfa.c"
  case_fold = fold;

  {
  __CrestReturn(27731);
# 324 "dfa.c"
  return;
  }
}
}
# 339 "dfa.c"
static char *lexstart ;
# 340 "dfa.c"
static char *lexptr ;
# 341 "dfa.c"
static int lexleft ;
# 342 "dfa.c"
static token lasttok ;
# 343 "dfa.c"
static int laststart ;
# 345 "dfa.c"
static int parens ;
# 346 "dfa.c"
static int minrep ;
# 346 "dfa.c"
static int maxrep ;
# 366 "dfa.c"
static int is_alpha(int c )
{
  unsigned short const **tmp ;
  unsigned short const *mem_3 ;
  int __retres4 ;

  {
  __CrestCall(27733, 224);
  __CrestStore(27732, (unsigned long )(& c));
# 366 "dfa.c"
  tmp = __ctype_b_loc();
  __CrestClearStack(27734);
  {
# 366 "dfa.c"
  mem_3 = *tmp + c;
  __CrestLoad(27737, (unsigned long )mem_3, (long long )*mem_3);
  __CrestLoad(27736, (unsigned long )0, (long long )1024);
  __CrestApply2(27735, 5, (long long )((int const )*mem_3 & 1024));
  __CrestStore(27738, (unsigned long )(& __retres4));
# 366 "dfa.c"
  __retres4 = (int )((int const )*mem_3 & 1024);
# 366 "dfa.c"
  goto return_label;
  }
  return_label:
  {
  __CrestLoad(27739, (unsigned long )(& __retres4), (long long )__retres4);
  __CrestReturn(27740);
# 366 "dfa.c"
  return (__retres4);
  }
}
}
# 367 "dfa.c"
static int is_upper(int c )
{
  unsigned short const **tmp ;
  unsigned short const *mem_3 ;
  int __retres4 ;

  {
  __CrestCall(27742, 225);
  __CrestStore(27741, (unsigned long )(& c));
# 367 "dfa.c"
  tmp = __ctype_b_loc();
  __CrestClearStack(27743);
  {
# 367 "dfa.c"
  mem_3 = *tmp + c;
  __CrestLoad(27746, (unsigned long )mem_3, (long long )*mem_3);
  __CrestLoad(27745, (unsigned long )0, (long long )256);
  __CrestApply2(27744, 5, (long long )((int const )*mem_3 & 256));
  __CrestStore(27747, (unsigned long )(& __retres4));
# 367 "dfa.c"
  __retres4 = (int )((int const )*mem_3 & 256);
# 367 "dfa.c"
  goto return_label;
  }
  return_label:
  {
  __CrestLoad(27748, (unsigned long )(& __retres4), (long long )__retres4);
  __CrestReturn(27749);
# 367 "dfa.c"
  return (__retres4);
  }
}
}
# 368 "dfa.c"
static int is_lower(int c )
{
  unsigned short const **tmp ;
  unsigned short const *mem_3 ;
  int __retres4 ;

  {
  __CrestCall(27751, 226);
  __CrestStore(27750, (unsigned long )(& c));
# 368 "dfa.c"
  tmp = __ctype_b_loc();
  __CrestClearStack(27752);
  {
# 368 "dfa.c"
  mem_3 = *tmp + c;
  __CrestLoad(27755, (unsigned long )mem_3, (long long )*mem_3);
  __CrestLoad(27754, (unsigned long )0, (long long )512);
  __CrestApply2(27753, 5, (long long )((int const )*mem_3 & 512));
  __CrestStore(27756, (unsigned long )(& __retres4));
# 368 "dfa.c"
  __retres4 = (int )((int const )*mem_3 & 512);
# 368 "dfa.c"
  goto return_label;
  }
  return_label:
  {
  __CrestLoad(27757, (unsigned long )(& __retres4), (long long )__retres4);
  __CrestReturn(27758);
# 368 "dfa.c"
  return (__retres4);
  }
}
}
# 369 "dfa.c"
static int is_digit(int c )
{
  unsigned short const **tmp ;
  unsigned short const *mem_3 ;
  int __retres4 ;

  {
  __CrestCall(27760, 227);
  __CrestStore(27759, (unsigned long )(& c));
# 369 "dfa.c"
  tmp = __ctype_b_loc();
  __CrestClearStack(27761);
  {
# 369 "dfa.c"
  mem_3 = *tmp + c;
  __CrestLoad(27764, (unsigned long )mem_3, (long long )*mem_3);
  __CrestLoad(27763, (unsigned long )0, (long long )2048);
  __CrestApply2(27762, 5, (long long )((int const )*mem_3 & 2048));
  __CrestStore(27765, (unsigned long )(& __retres4));
# 369 "dfa.c"
  __retres4 = (int )((int const )*mem_3 & 2048);
# 369 "dfa.c"
  goto return_label;
  }
  return_label:
  {
  __CrestLoad(27766, (unsigned long )(& __retres4), (long long )__retres4);
  __CrestReturn(27767);
# 369 "dfa.c"
  return (__retres4);
  }
}
}
# 370 "dfa.c"
static int is_xdigit(int c )
{
  unsigned short const **tmp ;
  unsigned short const *mem_3 ;
  int __retres4 ;

  {
  __CrestCall(27769, 228);
  __CrestStore(27768, (unsigned long )(& c));
# 370 "dfa.c"
  tmp = __ctype_b_loc();
  __CrestClearStack(27770);
  {
# 370 "dfa.c"
  mem_3 = *tmp + c;
  __CrestLoad(27773, (unsigned long )mem_3, (long long )*mem_3);
  __CrestLoad(27772, (unsigned long )0, (long long )4096);
  __CrestApply2(27771, 5, (long long )((int const )*mem_3 & 4096));
  __CrestStore(27774, (unsigned long )(& __retres4));
# 370 "dfa.c"
  __retres4 = (int )((int const )*mem_3 & 4096);
# 370 "dfa.c"
  goto return_label;
  }
  return_label:
  {
  __CrestLoad(27775, (unsigned long )(& __retres4), (long long )__retres4);
  __CrestReturn(27776);
# 370 "dfa.c"
  return (__retres4);
  }
}
}
# 371 "dfa.c"
static int is_space(int c )
{
  unsigned short const **tmp ;
  unsigned short const *mem_3 ;
  int __retres4 ;

  {
  __CrestCall(27778, 229);
  __CrestStore(27777, (unsigned long )(& c));
# 371 "dfa.c"
  tmp = __ctype_b_loc();
  __CrestClearStack(27779);
  {
# 371 "dfa.c"
  mem_3 = *tmp + c;
  __CrestLoad(27782, (unsigned long )mem_3, (long long )*mem_3);
  __CrestLoad(27781, (unsigned long )0, (long long )8192);
  __CrestApply2(27780, 5, (long long )((int const )*mem_3 & 8192));
  __CrestStore(27783, (unsigned long )(& __retres4));
# 371 "dfa.c"
  __retres4 = (int )((int const )*mem_3 & 8192);
# 371 "dfa.c"
  goto return_label;
  }
  return_label:
  {
  __CrestLoad(27784, (unsigned long )(& __retres4), (long long )__retres4);
  __CrestReturn(27785);
# 371 "dfa.c"
  return (__retres4);
  }
}
}
# 372 "dfa.c"
static int is_punct(int c )
{
  unsigned short const **tmp ;
  unsigned short const *mem_3 ;
  int __retres4 ;

  {
  __CrestCall(27787, 230);
  __CrestStore(27786, (unsigned long )(& c));
# 372 "dfa.c"
  tmp = __ctype_b_loc();
  __CrestClearStack(27788);
  {
# 372 "dfa.c"
  mem_3 = *tmp + c;
  __CrestLoad(27791, (unsigned long )mem_3, (long long )*mem_3);
  __CrestLoad(27790, (unsigned long )0, (long long )4);
  __CrestApply2(27789, 5, (long long )((int const )*mem_3 & 4));
  __CrestStore(27792, (unsigned long )(& __retres4));
# 372 "dfa.c"
  __retres4 = (int )((int const )*mem_3 & 4);
# 372 "dfa.c"
  goto return_label;
  }
  return_label:
  {
  __CrestLoad(27793, (unsigned long )(& __retres4), (long long )__retres4);
  __CrestReturn(27794);
# 372 "dfa.c"
  return (__retres4);
  }
}
}
# 373 "dfa.c"
static int is_alnum(int c )
{
  unsigned short const **tmp ;
  unsigned short const *mem_3 ;
  int __retres4 ;

  {
  __CrestCall(27796, 231);
  __CrestStore(27795, (unsigned long )(& c));
# 373 "dfa.c"
  tmp = __ctype_b_loc();
  __CrestClearStack(27797);
  {
# 373 "dfa.c"
  mem_3 = *tmp + c;
  __CrestLoad(27800, (unsigned long )mem_3, (long long )*mem_3);
  __CrestLoad(27799, (unsigned long )0, (long long )8);
  __CrestApply2(27798, 5, (long long )((int const )*mem_3 & 8));
  __CrestStore(27801, (unsigned long )(& __retres4));
# 373 "dfa.c"
  __retres4 = (int )((int const )*mem_3 & 8);
# 373 "dfa.c"
  goto return_label;
  }
  return_label:
  {
  __CrestLoad(27802, (unsigned long )(& __retres4), (long long )__retres4);
  __CrestReturn(27803);
# 373 "dfa.c"
  return (__retres4);
  }
}
}
# 374 "dfa.c"
static int is_print(int c )
{
  unsigned short const **tmp ;
  unsigned short const *mem_3 ;
  int __retres4 ;

  {
  __CrestCall(27805, 232);
  __CrestStore(27804, (unsigned long )(& c));
# 374 "dfa.c"
  tmp = __ctype_b_loc();
  __CrestClearStack(27806);
  {
# 374 "dfa.c"
  mem_3 = *tmp + c;
  __CrestLoad(27809, (unsigned long )mem_3, (long long )*mem_3);
  __CrestLoad(27808, (unsigned long )0, (long long )16384);
  __CrestApply2(27807, 5, (long long )((int const )*mem_3 & 16384));
  __CrestStore(27810, (unsigned long )(& __retres4));
# 374 "dfa.c"
  __retres4 = (int )((int const )*mem_3 & 16384);
# 374 "dfa.c"
  goto return_label;
  }
  return_label:
  {
  __CrestLoad(27811, (unsigned long )(& __retres4), (long long )__retres4);
  __CrestReturn(27812);
# 374 "dfa.c"
  return (__retres4);
  }
}
}
# 375 "dfa.c"
static int is_graph(int c )
{
  unsigned short const **tmp ;
  unsigned short const *mem_3 ;
  int __retres4 ;

  {
  __CrestCall(27814, 233);
  __CrestStore(27813, (unsigned long )(& c));
# 375 "dfa.c"
  tmp = __ctype_b_loc();
  __CrestClearStack(27815);
  {
# 375 "dfa.c"
  mem_3 = *tmp + c;
  __CrestLoad(27818, (unsigned long )mem_3, (long long )*mem_3);
  __CrestLoad(27817, (unsigned long )0, (long long )32768);
  __CrestApply2(27816, 5, (long long )((int const )*mem_3 & 32768));
  __CrestStore(27819, (unsigned long )(& __retres4));
# 375 "dfa.c"
  __retres4 = (int )((int const )*mem_3 & 32768);
# 375 "dfa.c"
  goto return_label;
  }
  return_label:
  {
  __CrestLoad(27820, (unsigned long )(& __retres4), (long long )__retres4);
  __CrestReturn(27821);
# 375 "dfa.c"
  return (__retres4);
  }
}
}
# 376 "dfa.c"
static int is_cntrl(int c )
{
  unsigned short const **tmp ;
  unsigned short const *mem_3 ;
  int __retres4 ;

  {
  __CrestCall(27823, 234);
  __CrestStore(27822, (unsigned long )(& c));
# 376 "dfa.c"
  tmp = __ctype_b_loc();
  __CrestClearStack(27824);
  {
# 376 "dfa.c"
  mem_3 = *tmp + c;
  __CrestLoad(27827, (unsigned long )mem_3, (long long )*mem_3);
  __CrestLoad(27826, (unsigned long )0, (long long )2);
  __CrestApply2(27825, 5, (long long )((int const )*mem_3 & 2));
  __CrestStore(27828, (unsigned long )(& __retres4));
# 376 "dfa.c"
  __retres4 = (int )((int const )*mem_3 & 2);
# 376 "dfa.c"
  goto return_label;
  }
  return_label:
  {
  __CrestLoad(27829, (unsigned long )(& __retres4), (long long )__retres4);
  __CrestReturn(27830);
# 376 "dfa.c"
  return (__retres4);
  }
}
}
# 378 "dfa.c"
static int is_blank(int c )
{
  int tmp ;

  {
  __CrestCall(27832, 235);
  __CrestStore(27831, (unsigned long )(& c));
  {
  __CrestLoad(27835, (unsigned long )(& c), (long long )c);
  __CrestLoad(27834, (unsigned long )0, (long long )32);
  __CrestApply2(27833, 12, (long long )(c == 32));
# 381 "dfa.c"
  if (c == 32) {
    __CrestBranch(27836, 3990, 1);
    __CrestLoad(27838, (unsigned long )0, (long long )1);
    __CrestStore(27839, (unsigned long )(& tmp));
# 381 "dfa.c"
    tmp = 1;
  } else {
    __CrestBranch(27837, 3991, 0);
    {
    __CrestLoad(27842, (unsigned long )(& c), (long long )c);
    __CrestLoad(27841, (unsigned long )0, (long long )9);
    __CrestApply2(27840, 12, (long long )(c == 9));
# 381 "dfa.c"
    if (c == 9) {
      __CrestBranch(27843, 3992, 1);
      __CrestLoad(27845, (unsigned long )0, (long long )1);
      __CrestStore(27846, (unsigned long )(& tmp));
# 381 "dfa.c"
      tmp = 1;
    } else {
      __CrestBranch(27844, 3993, 0);
      __CrestLoad(27847, (unsigned long )0, (long long )0);
      __CrestStore(27848, (unsigned long )(& tmp));
# 381 "dfa.c"
      tmp = 0;
    }
    }
  }
  }
  {
  __CrestLoad(27849, (unsigned long )(& tmp), (long long )tmp);
  __CrestReturn(27850);
# 381 "dfa.c"
  return (tmp);
  }
}
}
# 387 "dfa.c"
static struct __anonstruct_prednames_32 prednames[13] =
# 387 "dfa.c"
  { {":alpha:]", & is_alpha},
        {":upper:]", & is_upper},
        {":lower:]", & is_lower},
        {":digit:]", & is_digit},
        {":xdigit:]", & is_xdigit},
        {":space:]", & is_space},
        {":punct:]", & is_punct},
        {":alnum:]", & is_alnum},
        {":print:]", & is_print},
        {":graph:]", & is_graph},
        {":cntrl:]", & is_cntrl},
        {":blank:]", & is_blank},
        {(char const *)0, (int (*)(int ))0}};
# 409 "dfa.c"
static int looking_at(char const *s )
{
  size_t len ;
  int tmp ;
  int __retres4 ;

  {
  __CrestCall(27851, 236);
# 415 "dfa.c"
  len = strlen(s);
  __CrestHandleReturn(27853, (long long )len);
  __CrestStore(27852, (unsigned long )(& len));
  {
  __CrestLoad(27856, (unsigned long )(& lexleft), (long long )lexleft);
  __CrestLoad(27855, (unsigned long )(& len), (long long )len);
  __CrestApply2(27854, 16, (long long )((size_t )lexleft < len));
# 416 "dfa.c"
  if ((size_t )lexleft < len) {
    __CrestBranch(27857, 3997, 1);
    __CrestLoad(27859, (unsigned long )0, (long long )0);
    __CrestStore(27860, (unsigned long )(& __retres4));
# 417 "dfa.c"
    __retres4 = 0;
# 417 "dfa.c"
    goto return_label;
  } else {
    __CrestBranch(27858, 3999, 0);

  }
  }
  __CrestLoad(27861, (unsigned long )(& len), (long long )len);
# 418 "dfa.c"
  tmp = strncmp(s, (char const *)lexptr, len);
  __CrestHandleReturn(27863, (long long )tmp);
  __CrestStore(27862, (unsigned long )(& tmp));
  __CrestLoad(27866, (unsigned long )(& tmp), (long long )tmp);
  __CrestLoad(27865, (unsigned long )0, (long long )0);
  __CrestApply2(27864, 12, (long long )(tmp == 0));
  __CrestStore(27867, (unsigned long )(& __retres4));
# 418 "dfa.c"
  __retres4 = tmp == 0;
  return_label:
  {
  __CrestLoad(27868, (unsigned long )(& __retres4), (long long )__retres4);
  __CrestReturn(27869);
# 409 "dfa.c"
  return (__retres4);
  }
}
}
# 421 "dfa.c"
static token lex(void)
{
  token c ;
  token c1 ;
  token c2 ;
  int backslash ;
  int invert ;
  charclass ccl ;
  int i ;
  char *tmp ;
  char *tmp___0 ;
  int tmp___1 ;
  int tmp___2 ;
  int tmp___3 ;
  int tmp___4 ;
  int tmp___5 ;
  int tmp___6 ;
  char *tmp___7 ;
  char *tmp___8 ;
  char *tmp___9 ;
  char *tmp___10 ;
  char *tmp___11 ;
  char *tmp___12 ;
  unsigned short const **tmp___13 ;
  char *tmp___14 ;
  unsigned short const **tmp___15 ;
  char *tmp___16 ;
  char *tmp___17 ;
  char *tmp___18 ;
  unsigned short const **tmp___19 ;
  char *tmp___20 ;
  char *tmp___21 ;
  char *tmp___22 ;
  char *tmp___23 ;
  char *tmp___24 ;
  int tmp___25 ;
  unsigned short const **tmp___26 ;
  int tmp___27 ;
  char *tmp___28 ;
  char *tmp___29 ;
  char *tmp___30 ;
  char *tmp___31 ;
  char *tmp___32 ;
  char *tmp___33 ;
  int (*pred)() ;
  int tmp___34 ;
  size_t tmp___35 ;
  size_t tmp___36 ;
  char *tmp___37 ;
  char *tmp___38 ;
  char *tmp___39 ;
  int tmp___40 ;
  char *tmp___41 ;
  char *tmp___42 ;
  char *tmp___43 ;
  char *tmp___44 ;
  char *tmp___45 ;
  char *tmp___46 ;
  char *tmp___47 ;
  char *tmp___48 ;
  char *tmp___49 ;
  char *tmp___50 ;
  char *tmp___51 ;
  char *tmp___52 ;
  char *tmp___53 ;
  char *tmp___54 ;
  char *tmp___55 ;
  int tmp___56 ;
  int tmp___57 ;
  unsigned short const **tmp___58 ;
  unsigned short const **tmp___59 ;
  int tmp___60 ;
  int tmp___61 ;
  int tmp___62 ;
  unsigned short const **tmp___63 ;
  int tmp___64 ;
  unsigned short const **tmp___65 ;
  char *mem_76 ;
  char *mem_77 ;
  char *mem_78 ;
  char *mem_79 ;
  unsigned short const *mem_80 ;
  unsigned short const *mem_81 ;
  unsigned short const *mem_82 ;
  unsigned short const *mem_83 ;
  unsigned short const *mem_84 ;
  unsigned short const *mem_85 ;
  unsigned short const *mem_86 ;
  unsigned short const *mem_87 ;
  token __retres88 ;

  {
  __CrestCall(27870, 237);

  __CrestLoad(27871, (unsigned long )0, (long long )0);
  __CrestStore(27872, (unsigned long )(& backslash));
# 425 "dfa.c"
  backslash = 0;
  __CrestLoad(27873, (unsigned long )0, (long long )0);
  __CrestStore(27874, (unsigned long )(& i));
# 435 "dfa.c"
  i = 0;
  {
# 435 "dfa.c"
  while (1) {
    while_continue: ;
    {
    __CrestLoad(27877, (unsigned long )(& i), (long long )i);
    __CrestLoad(27876, (unsigned long )0, (long long )2);
    __CrestApply2(27875, 16, (long long )(i < 2));
# 435 "dfa.c"
    if (i < 2) {
      __CrestBranch(27878, 4008, 1);

    } else {
      __CrestBranch(27879, 4009, 0);
# 435 "dfa.c"
      goto while_break;
    }
    }
    {
    __CrestLoad(27882, (unsigned long )(& lexleft), (long long )lexleft);
    __CrestLoad(27881, (unsigned long )0, (long long )0);
    __CrestApply2(27880, 12, (long long )(lexleft == 0));
# 437 "dfa.c"
    if (lexleft == 0) {
      __CrestBranch(27883, 4011, 1);
      __CrestLoad(27885, (unsigned long )0, (long long )((token )-1));
      __CrestStore(27886, (unsigned long )(& lasttok));
# 437 "dfa.c"
      lasttok = (token )-1;
      __CrestLoad(27887, (unsigned long )(& lasttok), (long long )lasttok);
      __CrestStore(27888, (unsigned long )(& __retres88));
# 437 "dfa.c"
      __retres88 = lasttok;
# 437 "dfa.c"
      goto return_label;
    } else {
      __CrestBranch(27884, 4014, 0);

    }
    }
# 437 "dfa.c"
    tmp = lexptr;
# 437 "dfa.c"
    lexptr ++;
    __CrestLoad(27889, (unsigned long )tmp, (long long )*tmp);
    __CrestStore(27890, (unsigned long )(& c));
# 437 "dfa.c"
    c = (token )((unsigned char )*tmp);
    __CrestLoad(27893, (unsigned long )(& lexleft), (long long )lexleft);
    __CrestLoad(27892, (unsigned long )0, (long long )1);
    __CrestApply2(27891, 1, (long long )(lexleft - 1));
    __CrestStore(27894, (unsigned long )(& lexleft));
# 437 "dfa.c"
    lexleft --;
    {
    {
    __CrestLoad(27897, (unsigned long )(& c), (long long )c);
    __CrestLoad(27896, (unsigned long )0, (long long )92);
    __CrestApply2(27895, 12, (long long )((int )c == 92));
# 440 "dfa.c"
    if ((int )c == 92) {
      __CrestBranch(27898, 4018, 1);
# 440 "dfa.c"
      goto case_92;
    } else {
      __CrestBranch(27899, 4019, 0);

    }
    }
    {
    __CrestLoad(27902, (unsigned long )(& c), (long long )c);
    __CrestLoad(27901, (unsigned long )0, (long long )94);
    __CrestApply2(27900, 12, (long long )((int )c == 94));
# 448 "dfa.c"
    if ((int )c == 94) {
      __CrestBranch(27903, 4021, 1);
# 448 "dfa.c"
      goto case_94;
    } else {
      __CrestBranch(27904, 4022, 0);

    }
    }
    {
    __CrestLoad(27907, (unsigned long )(& c), (long long )c);
    __CrestLoad(27906, (unsigned long )0, (long long )36);
    __CrestApply2(27905, 12, (long long )((int )c == 36));
# 458 "dfa.c"
    if ((int )c == 36) {
      __CrestBranch(27908, 4024, 1);
# 458 "dfa.c"
      goto case_36;
    } else {
      __CrestBranch(27909, 4025, 0);

    }
    }
    {
    __CrestLoad(27912, (unsigned long )(& c), (long long )c);
    __CrestLoad(27911, (unsigned long )0, (long long )57);
    __CrestApply2(27910, 12, (long long )((int )c == 57));
# 482 "dfa.c"
    if ((int )c == 57) {
      __CrestBranch(27913, 4027, 1);
# 482 "dfa.c"
      goto case_57;
    } else {
      __CrestBranch(27914, 4028, 0);

    }
    }
    {
    __CrestLoad(27917, (unsigned long )(& c), (long long )c);
    __CrestLoad(27916, (unsigned long )0, (long long )56);
    __CrestApply2(27915, 12, (long long )((int )c == 56));
# 482 "dfa.c"
    if ((int )c == 56) {
      __CrestBranch(27918, 4030, 1);
# 482 "dfa.c"
      goto case_57;
    } else {
      __CrestBranch(27919, 4031, 0);

    }
    }
    {
    __CrestLoad(27922, (unsigned long )(& c), (long long )c);
    __CrestLoad(27921, (unsigned long )0, (long long )55);
    __CrestApply2(27920, 12, (long long )((int )c == 55));
# 482 "dfa.c"
    if ((int )c == 55) {
      __CrestBranch(27923, 4033, 1);
# 482 "dfa.c"
      goto case_57;
    } else {
      __CrestBranch(27924, 4034, 0);

    }
    }
    {
    __CrestLoad(27927, (unsigned long )(& c), (long long )c);
    __CrestLoad(27926, (unsigned long )0, (long long )54);
    __CrestApply2(27925, 12, (long long )((int )c == 54));
# 482 "dfa.c"
    if ((int )c == 54) {
      __CrestBranch(27928, 4036, 1);
# 482 "dfa.c"
      goto case_57;
    } else {
      __CrestBranch(27929, 4037, 0);

    }
    }
    {
    __CrestLoad(27932, (unsigned long )(& c), (long long )c);
    __CrestLoad(27931, (unsigned long )0, (long long )53);
    __CrestApply2(27930, 12, (long long )((int )c == 53));
# 482 "dfa.c"
    if ((int )c == 53) {
      __CrestBranch(27933, 4039, 1);
# 482 "dfa.c"
      goto case_57;
    } else {
      __CrestBranch(27934, 4040, 0);

    }
    }
    {
    __CrestLoad(27937, (unsigned long )(& c), (long long )c);
    __CrestLoad(27936, (unsigned long )0, (long long )52);
    __CrestApply2(27935, 12, (long long )((int )c == 52));
# 482 "dfa.c"
    if ((int )c == 52) {
      __CrestBranch(27938, 4042, 1);
# 482 "dfa.c"
      goto case_57;
    } else {
      __CrestBranch(27939, 4043, 0);

    }
    }
    {
    __CrestLoad(27942, (unsigned long )(& c), (long long )c);
    __CrestLoad(27941, (unsigned long )0, (long long )51);
    __CrestApply2(27940, 12, (long long )((int )c == 51));
# 482 "dfa.c"
    if ((int )c == 51) {
      __CrestBranch(27943, 4045, 1);
# 482 "dfa.c"
      goto case_57;
    } else {
      __CrestBranch(27944, 4046, 0);

    }
    }
    {
    __CrestLoad(27947, (unsigned long )(& c), (long long )c);
    __CrestLoad(27946, (unsigned long )0, (long long )50);
    __CrestApply2(27945, 12, (long long )((int )c == 50));
# 482 "dfa.c"
    if ((int )c == 50) {
      __CrestBranch(27948, 4048, 1);
# 482 "dfa.c"
      goto case_57;
    } else {
      __CrestBranch(27949, 4049, 0);

    }
    }
    {
    __CrestLoad(27952, (unsigned long )(& c), (long long )c);
    __CrestLoad(27951, (unsigned long )0, (long long )49);
    __CrestApply2(27950, 12, (long long )((int )c == 49));
# 482 "dfa.c"
    if ((int )c == 49) {
      __CrestBranch(27953, 4051, 1);
# 482 "dfa.c"
      goto case_57;
    } else {
      __CrestBranch(27954, 4052, 0);

    }
    }
    {
    __CrestLoad(27957, (unsigned long )(& c), (long long )c);
    __CrestLoad(27956, (unsigned long )0, (long long )96);
    __CrestApply2(27955, 12, (long long )((int )c == 96));
# 490 "dfa.c"
    if ((int )c == 96) {
      __CrestBranch(27958, 4054, 1);
# 490 "dfa.c"
      goto case_96;
    } else {
      __CrestBranch(27959, 4055, 0);

    }
    }
    {
    __CrestLoad(27962, (unsigned long )(& c), (long long )c);
    __CrestLoad(27961, (unsigned long )0, (long long )39);
    __CrestApply2(27960, 12, (long long )((int )c == 39));
# 495 "dfa.c"
    if ((int )c == 39) {
      __CrestBranch(27963, 4057, 1);
# 495 "dfa.c"
      goto case_39;
    } else {
      __CrestBranch(27964, 4058, 0);

    }
    }
    {
    __CrestLoad(27967, (unsigned long )(& c), (long long )c);
    __CrestLoad(27966, (unsigned long )0, (long long )60);
    __CrestApply2(27965, 12, (long long )((int )c == 60));
# 500 "dfa.c"
    if ((int )c == 60) {
      __CrestBranch(27968, 4060, 1);
# 500 "dfa.c"
      goto case_60;
    } else {
      __CrestBranch(27969, 4061, 0);

    }
    }
    {
    __CrestLoad(27972, (unsigned long )(& c), (long long )c);
    __CrestLoad(27971, (unsigned long )0, (long long )62);
    __CrestApply2(27970, 12, (long long )((int )c == 62));
# 505 "dfa.c"
    if ((int )c == 62) {
      __CrestBranch(27973, 4063, 1);
# 505 "dfa.c"
      goto case_62;
    } else {
      __CrestBranch(27974, 4064, 0);

    }
    }
    {
    __CrestLoad(27977, (unsigned long )(& c), (long long )c);
    __CrestLoad(27976, (unsigned long )0, (long long )98);
    __CrestApply2(27975, 12, (long long )((int )c == 98));
# 510 "dfa.c"
    if ((int )c == 98) {
      __CrestBranch(27978, 4066, 1);
# 510 "dfa.c"
      goto case_98;
    } else {
      __CrestBranch(27979, 4067, 0);

    }
    }
    {
    __CrestLoad(27982, (unsigned long )(& c), (long long )c);
    __CrestLoad(27981, (unsigned long )0, (long long )66);
    __CrestApply2(27980, 12, (long long )((int )c == 66));
# 515 "dfa.c"
    if ((int )c == 66) {
      __CrestBranch(27983, 4069, 1);
# 515 "dfa.c"
      goto case_66;
    } else {
      __CrestBranch(27984, 4070, 0);

    }
    }
    {
    __CrestLoad(27987, (unsigned long )(& c), (long long )c);
    __CrestLoad(27986, (unsigned long )0, (long long )63);
    __CrestApply2(27985, 12, (long long )((int )c == 63));
# 520 "dfa.c"
    if ((int )c == 63) {
      __CrestBranch(27988, 4072, 1);
# 520 "dfa.c"
      goto case_63;
    } else {
      __CrestBranch(27989, 4073, 0);

    }
    }
    {
    __CrestLoad(27992, (unsigned long )(& c), (long long )c);
    __CrestLoad(27991, (unsigned long )0, (long long )42);
    __CrestApply2(27990, 12, (long long )((int )c == 42));
# 529 "dfa.c"
    if ((int )c == 42) {
      __CrestBranch(27993, 4075, 1);
# 529 "dfa.c"
      goto case_42;
    } else {
      __CrestBranch(27994, 4076, 0);

    }
    }
    {
    __CrestLoad(27997, (unsigned long )(& c), (long long )c);
    __CrestLoad(27996, (unsigned long )0, (long long )43);
    __CrestApply2(27995, 12, (long long )((int )c == 43));
# 536 "dfa.c"
    if ((int )c == 43) {
      __CrestBranch(27998, 4078, 1);
# 536 "dfa.c"
      goto case_43;
    } else {
      __CrestBranch(27999, 4079, 0);

    }
    }
    {
    __CrestLoad(28002, (unsigned long )(& c), (long long )c);
    __CrestLoad(28001, (unsigned long )0, (long long )123);
    __CrestApply2(28000, 12, (long long )((int )c == 123));
# 545 "dfa.c"
    if ((int )c == 123) {
      __CrestBranch(28003, 4081, 1);
# 545 "dfa.c"
      goto case_123;
    } else {
      __CrestBranch(28004, 4082, 0);

    }
    }
    {
    __CrestLoad(28007, (unsigned long )(& c), (long long )c);
    __CrestLoad(28006, (unsigned long )0, (long long )124);
    __CrestApply2(28005, 12, (long long )((int )c == 124));
# 591 "dfa.c"
    if ((int )c == 124) {
      __CrestBranch(28008, 4084, 1);
# 591 "dfa.c"
      goto case_124;
    } else {
      __CrestBranch(28009, 4085, 0);

    }
    }
    {
    __CrestLoad(28012, (unsigned long )(& c), (long long )c);
    __CrestLoad(28011, (unsigned long )0, (long long )10);
    __CrestApply2(28010, 12, (long long )((int )c == 10));
# 599 "dfa.c"
    if ((int )c == 10) {
      __CrestBranch(28013, 4087, 1);
# 599 "dfa.c"
      goto case_10;
    } else {
      __CrestBranch(28014, 4088, 0);

    }
    }
    {
    __CrestLoad(28017, (unsigned long )(& c), (long long )c);
    __CrestLoad(28016, (unsigned long )0, (long long )40);
    __CrestApply2(28015, 12, (long long )((int )c == 40));
# 607 "dfa.c"
    if ((int )c == 40) {
      __CrestBranch(28018, 4090, 1);
# 607 "dfa.c"
      goto case_40;
    } else {
      __CrestBranch(28019, 4091, 0);

    }
    }
    {
    __CrestLoad(28022, (unsigned long )(& c), (long long )c);
    __CrestLoad(28021, (unsigned long )0, (long long )41);
    __CrestApply2(28020, 12, (long long )((int )c == 41));
# 614 "dfa.c"
    if ((int )c == 41) {
      __CrestBranch(28023, 4093, 1);
# 614 "dfa.c"
      goto case_41;
    } else {
      __CrestBranch(28024, 4094, 0);

    }
    }
    {
    __CrestLoad(28027, (unsigned long )(& c), (long long )c);
    __CrestLoad(28026, (unsigned long )0, (long long )46);
    __CrestApply2(28025, 12, (long long )((int )c == 46));
# 623 "dfa.c"
    if ((int )c == 46) {
      __CrestBranch(28028, 4096, 1);
# 623 "dfa.c"
      goto case_46;
    } else {
      __CrestBranch(28029, 4097, 0);

    }
    }
    {
    __CrestLoad(28032, (unsigned long )(& c), (long long )c);
    __CrestLoad(28031, (unsigned long )0, (long long )87);
    __CrestApply2(28030, 12, (long long )((int )c == 87));
# 636 "dfa.c"
    if ((int )c == 87) {
      __CrestBranch(28033, 4099, 1);
# 636 "dfa.c"
      goto case_87;
    } else {
      __CrestBranch(28034, 4100, 0);

    }
    }
    {
    __CrestLoad(28037, (unsigned long )(& c), (long long )c);
    __CrestLoad(28036, (unsigned long )0, (long long )119);
    __CrestApply2(28035, 12, (long long )((int )c == 119));
# 636 "dfa.c"
    if ((int )c == 119) {
      __CrestBranch(28038, 4102, 1);
# 636 "dfa.c"
      goto case_87;
    } else {
      __CrestBranch(28039, 4103, 0);

    }
    }
    {
    __CrestLoad(28042, (unsigned long )(& c), (long long )c);
    __CrestLoad(28041, (unsigned long )0, (long long )91);
    __CrestApply2(28040, 12, (long long )((int )c == 91));
# 648 "dfa.c"
    if ((int )c == 91) {
      __CrestBranch(28043, 4105, 1);
# 648 "dfa.c"
      goto case_91;
    } else {
      __CrestBranch(28044, 4106, 0);

    }
    }
# 732 "dfa.c"
    goto normal_char;
    case_92:
    {
    __CrestLoad(28047, (unsigned long )(& backslash), (long long )backslash);
    __CrestLoad(28046, (unsigned long )0, (long long )0);
    __CrestApply2(28045, 13, (long long )(backslash != 0));
# 441 "dfa.c"
    if (backslash != 0) {
      __CrestBranch(28048, 4109, 1);
# 442 "dfa.c"
      goto normal_char;
    } else {
      __CrestBranch(28049, 4110, 0);

    }
    }
    {
    __CrestLoad(28052, (unsigned long )(& lexleft), (long long )lexleft);
    __CrestLoad(28051, (unsigned long )0, (long long )0);
    __CrestApply2(28050, 12, (long long )(lexleft == 0));
# 443 "dfa.c"
    if (lexleft == 0) {
      __CrestBranch(28053, 4112, 1);
      __CrestLoad(28055, (unsigned long )0, (long long )5);
# 444 "dfa.c"
      tmp___0 = dcgettext((char const *)((void *)0), "Unfinished \\ escape", 5);
      __CrestClearStack(28056);
# 444 "dfa.c"
      dfaerror((char const *)tmp___0);
      __CrestClearStack(28057);
    } else {
      __CrestBranch(28054, 4113, 0);

    }
    }
    __CrestLoad(28058, (unsigned long )0, (long long )1);
    __CrestStore(28059, (unsigned long )(& backslash));
# 445 "dfa.c"
    backslash = 1;
# 446 "dfa.c"
    goto switch_break;
    case_94:
    {
    __CrestLoad(28062, (unsigned long )(& backslash), (long long )backslash);
    __CrestLoad(28061, (unsigned long )0, (long long )0);
    __CrestApply2(28060, 13, (long long )(backslash != 0));
# 449 "dfa.c"
    if (backslash != 0) {
      __CrestBranch(28063, 4117, 1);
# 450 "dfa.c"
      goto normal_char;
    } else {
      __CrestBranch(28064, 4118, 0);

    }
    }
    {
    __CrestLoad(28069, (unsigned long )(& syntax_bits), (long long )syntax_bits);
    __CrestLoad(28068, (unsigned long )0, (long long )(((1UL << 1) << 1) << 1));
    __CrestApply2(28067, 5, (long long )(syntax_bits & (((1UL << 1) << 1) << 1)));
    __CrestLoad(28066, (unsigned long )0, (long long )0);
    __CrestApply2(28065, 13, (long long )((syntax_bits & (((1UL << 1) << 1) << 1)) != 0));
# 451 "dfa.c"
    if ((syntax_bits & (((1UL << 1) << 1) << 1)) != 0) {
      __CrestBranch(28070, 4120, 1);
      __CrestLoad(28072, (unsigned long )0, (long long )((token )258));
      __CrestStore(28073, (unsigned long )(& lasttok));
# 455 "dfa.c"
      lasttok = (token )258;
      __CrestLoad(28074, (unsigned long )(& lasttok), (long long )lasttok);
      __CrestStore(28075, (unsigned long )(& __retres88));
# 455 "dfa.c"
      __retres88 = lasttok;
# 455 "dfa.c"
      goto return_label;
    } else {
      __CrestBranch(28071, 4123, 0);
      {
      __CrestLoad(28078, (unsigned long )(& lasttok), (long long )lasttok);
      __CrestLoad(28077, (unsigned long )0, (long long )-1);
      __CrestApply2(28076, 12, (long long )((int )lasttok == -1));
# 451 "dfa.c"
      if ((int )lasttok == -1) {
        __CrestBranch(28079, 4124, 1);
        __CrestLoad(28081, (unsigned long )0, (long long )((token )258));
        __CrestStore(28082, (unsigned long )(& lasttok));
# 455 "dfa.c"
        lasttok = (token )258;
        __CrestLoad(28083, (unsigned long )(& lasttok), (long long )lasttok);
        __CrestStore(28084, (unsigned long )(& __retres88));
# 455 "dfa.c"
        __retres88 = lasttok;
# 455 "dfa.c"
        goto return_label;
      } else {
        __CrestBranch(28080, 4127, 0);
        {
        __CrestLoad(28087, (unsigned long )(& lasttok), (long long )lasttok);
        __CrestLoad(28086, (unsigned long )0, (long long )271);
        __CrestApply2(28085, 12, (long long )((int )lasttok == 271));
# 451 "dfa.c"
        if ((int )lasttok == 271) {
          __CrestBranch(28088, 4128, 1);
          __CrestLoad(28090, (unsigned long )0, (long long )((token )258));
          __CrestStore(28091, (unsigned long )(& lasttok));
# 455 "dfa.c"
          lasttok = (token )258;
          __CrestLoad(28092, (unsigned long )(& lasttok), (long long )lasttok);
          __CrestStore(28093, (unsigned long )(& __retres88));
# 455 "dfa.c"
          __retres88 = lasttok;
# 455 "dfa.c"
          goto return_label;
        } else {
          __CrestBranch(28089, 4131, 0);
          {
          __CrestLoad(28096, (unsigned long )(& lasttok), (long long )lasttok);
          __CrestLoad(28095, (unsigned long )0, (long long )269);
          __CrestApply2(28094, 12, (long long )((int )lasttok == 269));
# 451 "dfa.c"
          if ((int )lasttok == 269) {
            __CrestBranch(28097, 4132, 1);
            __CrestLoad(28099, (unsigned long )0, (long long )((token )258));
            __CrestStore(28100, (unsigned long )(& lasttok));
# 455 "dfa.c"
            lasttok = (token )258;
            __CrestLoad(28101, (unsigned long )(& lasttok), (long long )lasttok);
            __CrestStore(28102, (unsigned long )(& __retres88));
# 455 "dfa.c"
            __retres88 = lasttok;
# 455 "dfa.c"
            goto return_label;
          } else {
            __CrestBranch(28098, 4135, 0);

          }
          }
        }
        }
      }
      }
    }
    }
# 456 "dfa.c"
    goto normal_char;
    case_36:
    {
    __CrestLoad(28105, (unsigned long )(& backslash), (long long )backslash);
    __CrestLoad(28104, (unsigned long )0, (long long )0);
    __CrestApply2(28103, 13, (long long )(backslash != 0));
# 459 "dfa.c"
    if (backslash != 0) {
      __CrestBranch(28106, 4138, 1);
# 460 "dfa.c"
      goto normal_char;
    } else {
      __CrestBranch(28107, 4139, 0);

    }
    }
    {
    __CrestLoad(28112, (unsigned long )(& syntax_bits), (long long )syntax_bits);
    __CrestLoad(28111, (unsigned long )0, (long long )(((1UL << 1) << 1) << 1));
    __CrestApply2(28110, 5, (long long )(syntax_bits & (((1UL << 1) << 1) << 1)));
    __CrestLoad(28109, (unsigned long )0, (long long )0);
    __CrestApply2(28108, 13, (long long )((syntax_bits & (((1UL << 1) << 1) << 1)) != 0));
# 461 "dfa.c"
    if ((syntax_bits & (((1UL << 1) << 1) << 1)) != 0) {
      __CrestBranch(28113, 4141, 1);
      __CrestLoad(28115, (unsigned long )0, (long long )((token )259));
      __CrestStore(28116, (unsigned long )(& lasttok));
# 471 "dfa.c"
      lasttok = (token )259;
      __CrestLoad(28117, (unsigned long )(& lasttok), (long long )lasttok);
      __CrestStore(28118, (unsigned long )(& __retres88));
# 471 "dfa.c"
      __retres88 = lasttok;
# 471 "dfa.c"
      goto return_label;
    } else {
      __CrestBranch(28114, 4144, 0);
      {
      __CrestLoad(28121, (unsigned long )(& lexleft), (long long )lexleft);
      __CrestLoad(28120, (unsigned long )0, (long long )0);
      __CrestApply2(28119, 12, (long long )(lexleft == 0));
# 461 "dfa.c"
      if (lexleft == 0) {
        __CrestBranch(28122, 4145, 1);
        __CrestLoad(28124, (unsigned long )0, (long long )((token )259));
        __CrestStore(28125, (unsigned long )(& lasttok));
# 471 "dfa.c"
        lasttok = (token )259;
        __CrestLoad(28126, (unsigned long )(& lasttok), (long long )lasttok);
        __CrestStore(28127, (unsigned long )(& __retres88));
# 471 "dfa.c"
        __retres88 = lasttok;
# 471 "dfa.c"
        goto return_label;
      } else {
        __CrestBranch(28123, 4148, 0);
        {
        __CrestLoad(28132, (unsigned long )(& syntax_bits), (long long )syntax_bits);
        __CrestLoad(28131, (unsigned long )0, (long long )(((((((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1));
        __CrestApply2(28130, 5, (long long )(syntax_bits & (((((((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1)));
        __CrestLoad(28129, (unsigned long )0, (long long )0);
        __CrestApply2(28128, 13, (long long )((syntax_bits & (((((((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1)) != 0));
# 461 "dfa.c"
        if ((syntax_bits & (((((((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1)) != 0) {
          __CrestBranch(28133, 4149, 1);
          {
          __CrestLoad(28137, (unsigned long )(& lexleft), (long long )lexleft);
          __CrestLoad(28136, (unsigned long )0, (long long )0);
          __CrestApply2(28135, 14, (long long )(lexleft > 0));
# 461 "dfa.c"
          if (lexleft > 0) {
            __CrestBranch(28138, 4150, 1);
            {
            __CrestLoad(28142, (unsigned long )lexptr, (long long )*lexptr);
            __CrestLoad(28141, (unsigned long )0, (long long )41);
            __CrestApply2(28140, 12, (long long )((int )*lexptr == 41));
# 461 "dfa.c"
            if ((int )*lexptr == 41) {
              __CrestBranch(28143, 4151, 1);
              __CrestLoad(28145, (unsigned long )0, (long long )1);
              __CrestStore(28146, (unsigned long )(& tmp___1));
# 461 "dfa.c"
              tmp___1 = 1;
            } else {
              __CrestBranch(28144, 4152, 0);
              __CrestLoad(28147, (unsigned long )0, (long long )0);
              __CrestStore(28148, (unsigned long )(& tmp___1));
# 461 "dfa.c"
              tmp___1 = 0;
            }
            }
          } else {
            __CrestBranch(28139, 4153, 0);
            __CrestLoad(28149, (unsigned long )0, (long long )0);
            __CrestStore(28150, (unsigned long )(& tmp___1));
# 461 "dfa.c"
            tmp___1 = 0;
          }
          }
          __CrestLoad(28151, (unsigned long )(& tmp___1), (long long )tmp___1);
          __CrestStore(28152, (unsigned long )(& tmp___3));
# 461 "dfa.c"
          tmp___3 = tmp___1;
        } else {
          __CrestBranch(28134, 4155, 0);
          {
          __CrestLoad(28155, (unsigned long )(& lexleft), (long long )lexleft);
          __CrestLoad(28154, (unsigned long )0, (long long )1);
          __CrestApply2(28153, 14, (long long )(lexleft > 1));
# 461 "dfa.c"
          if (lexleft > 1) {
            __CrestBranch(28156, 4156, 1);
            {
# 461 "dfa.c"
            mem_76 = lexptr + 0;
            {
            __CrestLoad(28160, (unsigned long )mem_76, (long long )*mem_76);
            __CrestLoad(28159, (unsigned long )0, (long long )92);
            __CrestApply2(28158, 12, (long long )((int )*mem_76 == 92));
# 461 "dfa.c"
            if ((int )*mem_76 == 92) {
              __CrestBranch(28161, 4159, 1);
              {
# 461 "dfa.c"
              mem_77 = lexptr + 1;
              {
              __CrestLoad(28165, (unsigned long )mem_77, (long long )*mem_77);
              __CrestLoad(28164, (unsigned long )0, (long long )41);
              __CrestApply2(28163, 12, (long long )((int )*mem_77 == 41));
# 461 "dfa.c"
              if ((int )*mem_77 == 41) {
                __CrestBranch(28166, 4162, 1);
                __CrestLoad(28168, (unsigned long )0, (long long )1);
                __CrestStore(28169, (unsigned long )(& tmp___2));
# 461 "dfa.c"
                tmp___2 = 1;
              } else {
                __CrestBranch(28167, 4163, 0);
                __CrestLoad(28170, (unsigned long )0, (long long )0);
                __CrestStore(28171, (unsigned long )(& tmp___2));
# 461 "dfa.c"
                tmp___2 = 0;
              }
              }
              }
            } else {
              __CrestBranch(28162, 4164, 0);
              __CrestLoad(28172, (unsigned long )0, (long long )0);
              __CrestStore(28173, (unsigned long )(& tmp___2));
# 461 "dfa.c"
              tmp___2 = 0;
            }
            }
            }
          } else {
            __CrestBranch(28157, 4165, 0);
            __CrestLoad(28174, (unsigned long )0, (long long )0);
            __CrestStore(28175, (unsigned long )(& tmp___2));
# 461 "dfa.c"
            tmp___2 = 0;
          }
          }
          __CrestLoad(28176, (unsigned long )(& tmp___2), (long long )tmp___2);
          __CrestStore(28177, (unsigned long )(& tmp___3));
# 461 "dfa.c"
          tmp___3 = tmp___2;
        }
        }
        {
        __CrestLoad(28180, (unsigned long )(& tmp___3), (long long )tmp___3);
        __CrestLoad(28179, (unsigned long )0, (long long )0);
        __CrestApply2(28178, 13, (long long )(tmp___3 != 0));
# 461 "dfa.c"
        if (tmp___3 != 0) {
          __CrestBranch(28181, 4168, 1);
          __CrestLoad(28183, (unsigned long )0, (long long )((token )259));
          __CrestStore(28184, (unsigned long )(& lasttok));
# 471 "dfa.c"
          lasttok = (token )259;
          __CrestLoad(28185, (unsigned long )(& lasttok), (long long )lasttok);
          __CrestStore(28186, (unsigned long )(& __retres88));
# 471 "dfa.c"
          __retres88 = lasttok;
# 471 "dfa.c"
          goto return_label;
        } else {
          __CrestBranch(28182, 4171, 0);
          {
          __CrestLoad(28191, (unsigned long )(& syntax_bits), (long long )syntax_bits);
          __CrestLoad(28190, (unsigned long )0, (long long )(((((((((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1));
          __CrestApply2(28189, 5, (long long )(syntax_bits & (((((((((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1)));
          __CrestLoad(28188, (unsigned long )0, (long long )0);
          __CrestApply2(28187, 13, (long long )((syntax_bits & (((((((((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1)) != 0));
# 461 "dfa.c"
          if ((syntax_bits & (((((((((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1)) != 0) {
            __CrestBranch(28192, 4172, 1);
            {
            __CrestLoad(28196, (unsigned long )(& lexleft), (long long )lexleft);
            __CrestLoad(28195, (unsigned long )0, (long long )0);
            __CrestApply2(28194, 14, (long long )(lexleft > 0));
# 461 "dfa.c"
            if (lexleft > 0) {
              __CrestBranch(28197, 4173, 1);
              {
              __CrestLoad(28201, (unsigned long )lexptr, (long long )*lexptr);
              __CrestLoad(28200, (unsigned long )0, (long long )124);
              __CrestApply2(28199, 12, (long long )((int )*lexptr == 124));
# 461 "dfa.c"
              if ((int )*lexptr == 124) {
                __CrestBranch(28202, 4174, 1);
                __CrestLoad(28204, (unsigned long )0, (long long )1);
                __CrestStore(28205, (unsigned long )(& tmp___4));
# 461 "dfa.c"
                tmp___4 = 1;
              } else {
                __CrestBranch(28203, 4175, 0);
                __CrestLoad(28206, (unsigned long )0, (long long )0);
                __CrestStore(28207, (unsigned long )(& tmp___4));
# 461 "dfa.c"
                tmp___4 = 0;
              }
              }
            } else {
              __CrestBranch(28198, 4176, 0);
              __CrestLoad(28208, (unsigned long )0, (long long )0);
              __CrestStore(28209, (unsigned long )(& tmp___4));
# 461 "dfa.c"
              tmp___4 = 0;
            }
            }
            __CrestLoad(28210, (unsigned long )(& tmp___4), (long long )tmp___4);
            __CrestStore(28211, (unsigned long )(& tmp___6));
# 461 "dfa.c"
            tmp___6 = tmp___4;
          } else {
            __CrestBranch(28193, 4178, 0);
            {
            __CrestLoad(28214, (unsigned long )(& lexleft), (long long )lexleft);
            __CrestLoad(28213, (unsigned long )0, (long long )1);
            __CrestApply2(28212, 14, (long long )(lexleft > 1));
# 461 "dfa.c"
            if (lexleft > 1) {
              __CrestBranch(28215, 4179, 1);
              {
# 461 "dfa.c"
              mem_78 = lexptr + 0;
              {
              __CrestLoad(28219, (unsigned long )mem_78, (long long )*mem_78);
              __CrestLoad(28218, (unsigned long )0, (long long )92);
              __CrestApply2(28217, 12, (long long )((int )*mem_78 == 92));
# 461 "dfa.c"
              if ((int )*mem_78 == 92) {
                __CrestBranch(28220, 4182, 1);
                {
# 461 "dfa.c"
                mem_79 = lexptr + 1;
                {
                __CrestLoad(28224, (unsigned long )mem_79, (long long )*mem_79);
                __CrestLoad(28223, (unsigned long )0, (long long )124);
                __CrestApply2(28222, 12, (long long )((int )*mem_79 == 124));
# 461 "dfa.c"
                if ((int )*mem_79 == 124) {
                  __CrestBranch(28225, 4185, 1);
                  __CrestLoad(28227, (unsigned long )0, (long long )1);
                  __CrestStore(28228, (unsigned long )(& tmp___5));
# 461 "dfa.c"
                  tmp___5 = 1;
                } else {
                  __CrestBranch(28226, 4186, 0);
                  __CrestLoad(28229, (unsigned long )0, (long long )0);
                  __CrestStore(28230, (unsigned long )(& tmp___5));
# 461 "dfa.c"
                  tmp___5 = 0;
                }
                }
                }
              } else {
                __CrestBranch(28221, 4187, 0);
                __CrestLoad(28231, (unsigned long )0, (long long )0);
                __CrestStore(28232, (unsigned long )(& tmp___5));
# 461 "dfa.c"
                tmp___5 = 0;
              }
              }
              }
            } else {
              __CrestBranch(28216, 4188, 0);
              __CrestLoad(28233, (unsigned long )0, (long long )0);
              __CrestStore(28234, (unsigned long )(& tmp___5));
# 461 "dfa.c"
              tmp___5 = 0;
            }
            }
            __CrestLoad(28235, (unsigned long )(& tmp___5), (long long )tmp___5);
            __CrestStore(28236, (unsigned long )(& tmp___6));
# 461 "dfa.c"
            tmp___6 = tmp___5;
          }
          }
          {
          __CrestLoad(28239, (unsigned long )(& tmp___6), (long long )tmp___6);
          __CrestLoad(28238, (unsigned long )0, (long long )0);
          __CrestApply2(28237, 13, (long long )(tmp___6 != 0));
# 461 "dfa.c"
          if (tmp___6 != 0) {
            __CrestBranch(28240, 4191, 1);
            __CrestLoad(28242, (unsigned long )0, (long long )((token )259));
            __CrestStore(28243, (unsigned long )(& lasttok));
# 471 "dfa.c"
            lasttok = (token )259;
            __CrestLoad(28244, (unsigned long )(& lasttok), (long long )lasttok);
            __CrestStore(28245, (unsigned long )(& __retres88));
# 471 "dfa.c"
            __retres88 = lasttok;
# 471 "dfa.c"
            goto return_label;
          } else {
            __CrestBranch(28241, 4194, 0);
            {
            __CrestLoad(28250, (unsigned long )(& syntax_bits), (long long )syntax_bits);
            __CrestLoad(28249, (unsigned long )0, (long long )(((((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1));
            __CrestApply2(28248, 5, (long long )(syntax_bits & (((((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1)));
            __CrestLoad(28247, (unsigned long )0, (long long )0);
            __CrestApply2(28246, 13, (long long )((syntax_bits & (((((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1)) != 0));
# 461 "dfa.c"
            if ((syntax_bits & (((((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1)) != 0) {
              __CrestBranch(28251, 4195, 1);
              {
              __CrestLoad(28255, (unsigned long )(& lexleft), (long long )lexleft);
              __CrestLoad(28254, (unsigned long )0, (long long )0);
              __CrestApply2(28253, 14, (long long )(lexleft > 0));
# 461 "dfa.c"
              if (lexleft > 0) {
                __CrestBranch(28256, 4196, 1);
                {
                __CrestLoad(28260, (unsigned long )lexptr, (long long )*lexptr);
                __CrestLoad(28259, (unsigned long )0, (long long )10);
                __CrestApply2(28258, 12, (long long )((int )*lexptr == 10));
# 461 "dfa.c"
                if ((int )*lexptr == 10) {
                  __CrestBranch(28261, 4197, 1);
                  __CrestLoad(28263, (unsigned long )0, (long long )((token )259));
                  __CrestStore(28264, (unsigned long )(& lasttok));
# 471 "dfa.c"
                  lasttok = (token )259;
                  __CrestLoad(28265, (unsigned long )(& lasttok), (long long )lasttok);
                  __CrestStore(28266, (unsigned long )(& __retres88));
# 471 "dfa.c"
                  __retres88 = lasttok;
# 471 "dfa.c"
                  goto return_label;
                } else {
                  __CrestBranch(28262, 4200, 0);

                }
                }
              } else {
                __CrestBranch(28257, 4201, 0);

              }
              }
            } else {
              __CrestBranch(28252, 4202, 0);

            }
            }
          }
          }
        }
        }
      }
      }
    }
    }
# 472 "dfa.c"
    goto normal_char;
    case_57:
    case_56:
    case_55:
    case_54:
    case_53:
    case_52:
    case_51:
    case_50:
    case_49:
    {
    __CrestLoad(28269, (unsigned long )(& backslash), (long long )backslash);
    __CrestLoad(28268, (unsigned long )0, (long long )0);
    __CrestApply2(28267, 13, (long long )(backslash != 0));
# 483 "dfa.c"
    if (backslash != 0) {
      __CrestBranch(28270, 4205, 1);
      {
      __CrestLoad(28276, (unsigned long )(& syntax_bits), (long long )syntax_bits);
      __CrestLoad(28275, (unsigned long )0, (long long )((((((((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1));
      __CrestApply2(28274, 5, (long long )(syntax_bits & ((((((((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1)));
      __CrestLoad(28273, (unsigned long )0, (long long )0);
      __CrestApply2(28272, 12, (long long )((syntax_bits & ((((((((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1)) == 0));
# 483 "dfa.c"
      if ((syntax_bits & ((((((((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1)) == 0) {
        __CrestBranch(28277, 4206, 1);
        __CrestLoad(28279, (unsigned long )0, (long long )0);
        __CrestStore(28280, (unsigned long )(& laststart));
# 485 "dfa.c"
        laststart = 0;
        __CrestLoad(28281, (unsigned long )0, (long long )((token )257));
        __CrestStore(28282, (unsigned long )(& lasttok));
# 486 "dfa.c"
        lasttok = (token )257;
        __CrestLoad(28283, (unsigned long )(& lasttok), (long long )lasttok);
        __CrestStore(28284, (unsigned long )(& __retres88));
# 486 "dfa.c"
        __retres88 = lasttok;
# 486 "dfa.c"
        goto return_label;
      } else {
        __CrestBranch(28278, 4209, 0);

      }
      }
    } else {
      __CrestBranch(28271, 4210, 0);

    }
    }
# 488 "dfa.c"
    goto normal_char;
    case_96:
    {
    __CrestLoad(28287, (unsigned long )(& backslash), (long long )backslash);
    __CrestLoad(28286, (unsigned long )0, (long long )0);
    __CrestApply2(28285, 13, (long long )(backslash != 0));
# 491 "dfa.c"
    if (backslash != 0) {
      __CrestBranch(28288, 4213, 1);
      {
      __CrestLoad(28294, (unsigned long )(& syntax_bits), (long long )syntax_bits);
      __CrestLoad(28293, (unsigned long )0, (long long )(((((((((((((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1));
      __CrestApply2(28292, 5, (long long )(syntax_bits & (((((((((((((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1)));
      __CrestLoad(28291, (unsigned long )0, (long long )0);
      __CrestApply2(28290, 12, (long long )((syntax_bits & (((((((((((((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1)) == 0));
# 491 "dfa.c"
      if ((syntax_bits & (((((((((((((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1)) == 0) {
        __CrestBranch(28295, 4214, 1);
        __CrestLoad(28297, (unsigned long )0, (long long )((token )258));
        __CrestStore(28298, (unsigned long )(& lasttok));
# 492 "dfa.c"
        lasttok = (token )258;
        __CrestLoad(28299, (unsigned long )(& lasttok), (long long )lasttok);
        __CrestStore(28300, (unsigned long )(& __retres88));
# 492 "dfa.c"
        __retres88 = lasttok;
# 492 "dfa.c"
        goto return_label;
      } else {
        __CrestBranch(28296, 4217, 0);

      }
      }
    } else {
      __CrestBranch(28289, 4218, 0);

    }
    }
# 493 "dfa.c"
    goto normal_char;
    case_39:
    {
    __CrestLoad(28303, (unsigned long )(& backslash), (long long )backslash);
    __CrestLoad(28302, (unsigned long )0, (long long )0);
    __CrestApply2(28301, 13, (long long )(backslash != 0));
# 496 "dfa.c"
    if (backslash != 0) {
      __CrestBranch(28304, 4221, 1);
      {
      __CrestLoad(28310, (unsigned long )(& syntax_bits), (long long )syntax_bits);
      __CrestLoad(28309, (unsigned long )0, (long long )(((((((((((((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1));
      __CrestApply2(28308, 5, (long long )(syntax_bits & (((((((((((((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1)));
      __CrestLoad(28307, (unsigned long )0, (long long )0);
      __CrestApply2(28306, 12, (long long )((syntax_bits & (((((((((((((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1)) == 0));
# 496 "dfa.c"
      if ((syntax_bits & (((((((((((((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1)) == 0) {
        __CrestBranch(28311, 4222, 1);
        __CrestLoad(28313, (unsigned long )0, (long long )((token )259));
        __CrestStore(28314, (unsigned long )(& lasttok));
# 497 "dfa.c"
        lasttok = (token )259;
        __CrestLoad(28315, (unsigned long )(& lasttok), (long long )lasttok);
        __CrestStore(28316, (unsigned long )(& __retres88));
# 497 "dfa.c"
        __retres88 = lasttok;
# 497 "dfa.c"
        goto return_label;
      } else {
        __CrestBranch(28312, 4225, 0);

      }
      }
    } else {
      __CrestBranch(28305, 4226, 0);

    }
    }
# 498 "dfa.c"
    goto normal_char;
    case_60:
    {
    __CrestLoad(28319, (unsigned long )(& backslash), (long long )backslash);
    __CrestLoad(28318, (unsigned long )0, (long long )0);
    __CrestApply2(28317, 13, (long long )(backslash != 0));
# 501 "dfa.c"
    if (backslash != 0) {
      __CrestBranch(28320, 4229, 1);
      {
      __CrestLoad(28326, (unsigned long )(& syntax_bits), (long long )syntax_bits);
      __CrestLoad(28325, (unsigned long )0, (long long )(((((((((((((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1));
      __CrestApply2(28324, 5, (long long )(syntax_bits & (((((((((((((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1)));
      __CrestLoad(28323, (unsigned long )0, (long long )0);
      __CrestApply2(28322, 12, (long long )((syntax_bits & (((((((((((((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1)) == 0));
# 501 "dfa.c"
      if ((syntax_bits & (((((((((((((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1)) == 0) {
        __CrestBranch(28327, 4230, 1);
        __CrestLoad(28329, (unsigned long )0, (long long )((token )260));
        __CrestStore(28330, (unsigned long )(& lasttok));
# 502 "dfa.c"
        lasttok = (token )260;
        __CrestLoad(28331, (unsigned long )(& lasttok), (long long )lasttok);
        __CrestStore(28332, (unsigned long )(& __retres88));
# 502 "dfa.c"
        __retres88 = lasttok;
# 502 "dfa.c"
        goto return_label;
      } else {
        __CrestBranch(28328, 4233, 0);

      }
      }
    } else {
      __CrestBranch(28321, 4234, 0);

    }
    }
# 503 "dfa.c"
    goto normal_char;
    case_62:
    {
    __CrestLoad(28335, (unsigned long )(& backslash), (long long )backslash);
    __CrestLoad(28334, (unsigned long )0, (long long )0);
    __CrestApply2(28333, 13, (long long )(backslash != 0));
# 506 "dfa.c"
    if (backslash != 0) {
      __CrestBranch(28336, 4237, 1);
      {
      __CrestLoad(28342, (unsigned long )(& syntax_bits), (long long )syntax_bits);
      __CrestLoad(28341, (unsigned long )0, (long long )(((((((((((((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1));
      __CrestApply2(28340, 5, (long long )(syntax_bits & (((((((((((((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1)));
      __CrestLoad(28339, (unsigned long )0, (long long )0);
      __CrestApply2(28338, 12, (long long )((syntax_bits & (((((((((((((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1)) == 0));
# 506 "dfa.c"
      if ((syntax_bits & (((((((((((((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1)) == 0) {
        __CrestBranch(28343, 4238, 1);
        __CrestLoad(28345, (unsigned long )0, (long long )((token )261));
        __CrestStore(28346, (unsigned long )(& lasttok));
# 507 "dfa.c"
        lasttok = (token )261;
        __CrestLoad(28347, (unsigned long )(& lasttok), (long long )lasttok);
        __CrestStore(28348, (unsigned long )(& __retres88));
# 507 "dfa.c"
        __retres88 = lasttok;
# 507 "dfa.c"
        goto return_label;
      } else {
        __CrestBranch(28344, 4241, 0);

      }
      }
    } else {
      __CrestBranch(28337, 4242, 0);

    }
    }
# 508 "dfa.c"
    goto normal_char;
    case_98:
    {
    __CrestLoad(28351, (unsigned long )(& backslash), (long long )backslash);
    __CrestLoad(28350, (unsigned long )0, (long long )0);
    __CrestApply2(28349, 13, (long long )(backslash != 0));
# 511 "dfa.c"
    if (backslash != 0) {
      __CrestBranch(28352, 4245, 1);
      {
      __CrestLoad(28358, (unsigned long )(& syntax_bits), (long long )syntax_bits);
      __CrestLoad(28357, (unsigned long )0, (long long )(((((((((((((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1));
      __CrestApply2(28356, 5, (long long )(syntax_bits & (((((((((((((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1)));
      __CrestLoad(28355, (unsigned long )0, (long long )0);
      __CrestApply2(28354, 12, (long long )((syntax_bits & (((((((((((((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1)) == 0));
# 511 "dfa.c"
      if ((syntax_bits & (((((((((((((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1)) == 0) {
        __CrestBranch(28359, 4246, 1);
        __CrestLoad(28361, (unsigned long )0, (long long )((token )262));
        __CrestStore(28362, (unsigned long )(& lasttok));
# 512 "dfa.c"
        lasttok = (token )262;
        __CrestLoad(28363, (unsigned long )(& lasttok), (long long )lasttok);
        __CrestStore(28364, (unsigned long )(& __retres88));
# 512 "dfa.c"
        __retres88 = lasttok;
# 512 "dfa.c"
        goto return_label;
      } else {
        __CrestBranch(28360, 4249, 0);

      }
      }
    } else {
      __CrestBranch(28353, 4250, 0);

    }
    }
# 513 "dfa.c"
    goto normal_char;
    case_66:
    {
    __CrestLoad(28367, (unsigned long )(& backslash), (long long )backslash);
    __CrestLoad(28366, (unsigned long )0, (long long )0);
    __CrestApply2(28365, 13, (long long )(backslash != 0));
# 516 "dfa.c"
    if (backslash != 0) {
      __CrestBranch(28368, 4253, 1);
      {
      __CrestLoad(28374, (unsigned long )(& syntax_bits), (long long )syntax_bits);
      __CrestLoad(28373, (unsigned long )0, (long long )(((((((((((((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1));
      __CrestApply2(28372, 5, (long long )(syntax_bits & (((((((((((((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1)));
      __CrestLoad(28371, (unsigned long )0, (long long )0);
      __CrestApply2(28370, 12, (long long )((syntax_bits & (((((((((((((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1)) == 0));
# 516 "dfa.c"
      if ((syntax_bits & (((((((((((((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1)) == 0) {
        __CrestBranch(28375, 4254, 1);
        __CrestLoad(28377, (unsigned long )0, (long long )((token )263));
        __CrestStore(28378, (unsigned long )(& lasttok));
# 517 "dfa.c"
        lasttok = (token )263;
        __CrestLoad(28379, (unsigned long )(& lasttok), (long long )lasttok);
        __CrestStore(28380, (unsigned long )(& __retres88));
# 517 "dfa.c"
        __retres88 = lasttok;
# 517 "dfa.c"
        goto return_label;
      } else {
        __CrestBranch(28376, 4257, 0);

      }
      }
    } else {
      __CrestBranch(28369, 4258, 0);

    }
    }
# 518 "dfa.c"
    goto normal_char;
    case_63:
    {
    __CrestLoad(28385, (unsigned long )(& syntax_bits), (long long )syntax_bits);
    __CrestLoad(28384, (unsigned long )0, (long long )((((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1));
    __CrestApply2(28383, 5, (long long )(syntax_bits & ((((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1)));
    __CrestLoad(28382, (unsigned long )0, (long long )0);
    __CrestApply2(28381, 13, (long long )((syntax_bits & ((((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1)) != 0));
# 521 "dfa.c"
    if ((syntax_bits & ((((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1)) != 0) {
      __CrestBranch(28386, 4261, 1);
# 522 "dfa.c"
      goto normal_char;
    } else {
      __CrestBranch(28387, 4262, 0);

    }
    }
    {
    __CrestLoad(28394, (unsigned long )(& backslash), (long long )backslash);
    __CrestLoad(28393, (unsigned long )(& syntax_bits), (long long )syntax_bits);
    __CrestLoad(28392, (unsigned long )0, (long long )(1UL << 1));
    __CrestApply2(28391, 5, (long long )(syntax_bits & (1UL << 1)));
    __CrestLoad(28390, (unsigned long )0, (long long )0UL);
    __CrestApply2(28389, 13, (long long )((syntax_bits & (1UL << 1)) != 0UL));
    __CrestApply2(28388, 13, (long long )(backslash != ((syntax_bits & (1UL << 1)) != 0UL)));
# 523 "dfa.c"
    if (backslash != ((syntax_bits & (1UL << 1)) != 0UL)) {
      __CrestBranch(28395, 4264, 1);
# 524 "dfa.c"
      goto normal_char;
    } else {
      __CrestBranch(28396, 4265, 0);

    }
    }
    {
    __CrestLoad(28401, (unsigned long )(& syntax_bits), (long long )syntax_bits);
    __CrestLoad(28400, (unsigned long )0, (long long )((((1UL << 1) << 1) << 1) << 1));
    __CrestApply2(28399, 5, (long long )(syntax_bits & ((((1UL << 1) << 1) << 1) << 1)));
    __CrestLoad(28398, (unsigned long )0, (long long )0);
    __CrestApply2(28397, 12, (long long )((syntax_bits & ((((1UL << 1) << 1) << 1) << 1)) == 0));
# 525 "dfa.c"
    if ((syntax_bits & ((((1UL << 1) << 1) << 1) << 1)) == 0) {
      __CrestBranch(28402, 4267, 1);
      {
      __CrestLoad(28406, (unsigned long )(& laststart), (long long )laststart);
      __CrestLoad(28405, (unsigned long )0, (long long )0);
      __CrestApply2(28404, 13, (long long )(laststart != 0));
# 525 "dfa.c"
      if (laststart != 0) {
        __CrestBranch(28407, 4268, 1);
# 526 "dfa.c"
        goto normal_char;
      } else {
        __CrestBranch(28408, 4269, 0);

      }
      }
    } else {
      __CrestBranch(28403, 4270, 0);

    }
    }
    __CrestLoad(28409, (unsigned long )0, (long long )((token )264));
    __CrestStore(28410, (unsigned long )(& lasttok));
# 527 "dfa.c"
    lasttok = (token )264;
    __CrestLoad(28411, (unsigned long )(& lasttok), (long long )lasttok);
    __CrestStore(28412, (unsigned long )(& __retres88));
# 527 "dfa.c"
    __retres88 = lasttok;
# 527 "dfa.c"
    goto return_label;
    case_42:
    {
    __CrestLoad(28415, (unsigned long )(& backslash), (long long )backslash);
    __CrestLoad(28414, (unsigned long )0, (long long )0);
    __CrestApply2(28413, 13, (long long )(backslash != 0));
# 530 "dfa.c"
    if (backslash != 0) {
      __CrestBranch(28416, 4275, 1);
# 531 "dfa.c"
      goto normal_char;
    } else {
      __CrestBranch(28417, 4276, 0);

    }
    }
    {
    __CrestLoad(28422, (unsigned long )(& syntax_bits), (long long )syntax_bits);
    __CrestLoad(28421, (unsigned long )0, (long long )((((1UL << 1) << 1) << 1) << 1));
    __CrestApply2(28420, 5, (long long )(syntax_bits & ((((1UL << 1) << 1) << 1) << 1)));
    __CrestLoad(28419, (unsigned long )0, (long long )0);
    __CrestApply2(28418, 12, (long long )((syntax_bits & ((((1UL << 1) << 1) << 1) << 1)) == 0));
# 532 "dfa.c"
    if ((syntax_bits & ((((1UL << 1) << 1) << 1) << 1)) == 0) {
      __CrestBranch(28423, 4278, 1);
      {
      __CrestLoad(28427, (unsigned long )(& laststart), (long long )laststart);
      __CrestLoad(28426, (unsigned long )0, (long long )0);
      __CrestApply2(28425, 13, (long long )(laststart != 0));
# 532 "dfa.c"
      if (laststart != 0) {
        __CrestBranch(28428, 4279, 1);
# 533 "dfa.c"
        goto normal_char;
      } else {
        __CrestBranch(28429, 4280, 0);

      }
      }
    } else {
      __CrestBranch(28424, 4281, 0);

    }
    }
    __CrestLoad(28430, (unsigned long )0, (long long )((token )265));
    __CrestStore(28431, (unsigned long )(& lasttok));
# 534 "dfa.c"
    lasttok = (token )265;
    __CrestLoad(28432, (unsigned long )(& lasttok), (long long )lasttok);
    __CrestStore(28433, (unsigned long )(& __retres88));
# 534 "dfa.c"
    __retres88 = lasttok;
# 534 "dfa.c"
    goto return_label;
    case_43:
    {
    __CrestLoad(28438, (unsigned long )(& syntax_bits), (long long )syntax_bits);
    __CrestLoad(28437, (unsigned long )0, (long long )((((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1));
    __CrestApply2(28436, 5, (long long )(syntax_bits & ((((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1)));
    __CrestLoad(28435, (unsigned long )0, (long long )0);
    __CrestApply2(28434, 13, (long long )((syntax_bits & ((((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1)) != 0));
# 537 "dfa.c"
    if ((syntax_bits & ((((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1)) != 0) {
      __CrestBranch(28439, 4286, 1);
# 538 "dfa.c"
      goto normal_char;
    } else {
      __CrestBranch(28440, 4287, 0);

    }
    }
    {
    __CrestLoad(28447, (unsigned long )(& backslash), (long long )backslash);
    __CrestLoad(28446, (unsigned long )(& syntax_bits), (long long )syntax_bits);
    __CrestLoad(28445, (unsigned long )0, (long long )(1UL << 1));
    __CrestApply2(28444, 5, (long long )(syntax_bits & (1UL << 1)));
    __CrestLoad(28443, (unsigned long )0, (long long )0UL);
    __CrestApply2(28442, 13, (long long )((syntax_bits & (1UL << 1)) != 0UL));
    __CrestApply2(28441, 13, (long long )(backslash != ((syntax_bits & (1UL << 1)) != 0UL)));
# 539 "dfa.c"
    if (backslash != ((syntax_bits & (1UL << 1)) != 0UL)) {
      __CrestBranch(28448, 4289, 1);
# 540 "dfa.c"
      goto normal_char;
    } else {
      __CrestBranch(28449, 4290, 0);

    }
    }
    {
    __CrestLoad(28454, (unsigned long )(& syntax_bits), (long long )syntax_bits);
    __CrestLoad(28453, (unsigned long )0, (long long )((((1UL << 1) << 1) << 1) << 1));
    __CrestApply2(28452, 5, (long long )(syntax_bits & ((((1UL << 1) << 1) << 1) << 1)));
    __CrestLoad(28451, (unsigned long )0, (long long )0);
    __CrestApply2(28450, 12, (long long )((syntax_bits & ((((1UL << 1) << 1) << 1) << 1)) == 0));
# 541 "dfa.c"
    if ((syntax_bits & ((((1UL << 1) << 1) << 1) << 1)) == 0) {
      __CrestBranch(28455, 4292, 1);
      {
      __CrestLoad(28459, (unsigned long )(& laststart), (long long )laststart);
      __CrestLoad(28458, (unsigned long )0, (long long )0);
      __CrestApply2(28457, 13, (long long )(laststart != 0));
# 541 "dfa.c"
      if (laststart != 0) {
        __CrestBranch(28460, 4293, 1);
# 542 "dfa.c"
        goto normal_char;
      } else {
        __CrestBranch(28461, 4294, 0);

      }
      }
    } else {
      __CrestBranch(28456, 4295, 0);

    }
    }
    __CrestLoad(28462, (unsigned long )0, (long long )((token )266));
    __CrestStore(28463, (unsigned long )(& lasttok));
# 543 "dfa.c"
    lasttok = (token )266;
    __CrestLoad(28464, (unsigned long )(& lasttok), (long long )lasttok);
    __CrestStore(28465, (unsigned long )(& __retres88));
# 543 "dfa.c"
    __retres88 = lasttok;
# 543 "dfa.c"
    goto return_label;
    case_123:
    {
    __CrestLoad(28470, (unsigned long )(& syntax_bits), (long long )syntax_bits);
    __CrestLoad(28469, (unsigned long )0, (long long )(((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1));
    __CrestApply2(28468, 5, (long long )(syntax_bits & (((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1)));
    __CrestLoad(28467, (unsigned long )0, (long long )0);
    __CrestApply2(28466, 12, (long long )((syntax_bits & (((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1)) == 0));
# 546 "dfa.c"
    if ((syntax_bits & (((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1)) == 0) {
      __CrestBranch(28471, 4300, 1);
# 547 "dfa.c"
      goto normal_char;
    } else {
      __CrestBranch(28472, 4301, 0);

    }
    }
    {
    __CrestLoad(28479, (unsigned long )(& backslash), (long long )backslash);
    __CrestLoad(28478, (unsigned long )(& syntax_bits), (long long )syntax_bits);
    __CrestLoad(28477, (unsigned long )0, (long long )((((((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1));
    __CrestApply2(28476, 5, (long long )(syntax_bits & ((((((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1)));
    __CrestLoad(28475, (unsigned long )0, (long long )0UL);
    __CrestApply2(28474, 12, (long long )((syntax_bits & ((((((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1)) == 0UL));
    __CrestApply2(28473, 13, (long long )(backslash != ((syntax_bits & ((((((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1)) == 0UL)));
# 548 "dfa.c"
    if (backslash != ((syntax_bits & ((((((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1)) == 0UL)) {
      __CrestBranch(28480, 4303, 1);
# 549 "dfa.c"
      goto normal_char;
    } else {
      __CrestBranch(28481, 4304, 0);

    }
    }
    __CrestLoad(28482, (unsigned long )0, (long long )0);
    __CrestStore(28483, (unsigned long )(& maxrep));
# 550 "dfa.c"
    maxrep = 0;
    __CrestLoad(28484, (unsigned long )(& maxrep), (long long )maxrep);
    __CrestStore(28485, (unsigned long )(& minrep));
# 550 "dfa.c"
    minrep = maxrep;
    {
    __CrestLoad(28488, (unsigned long )(& lexleft), (long long )lexleft);
    __CrestLoad(28487, (unsigned long )0, (long long )0);
    __CrestApply2(28486, 12, (long long )(lexleft == 0));
# 556 "dfa.c"
    if (lexleft == 0) {
      __CrestBranch(28489, 4307, 1);
      __CrestLoad(28491, (unsigned long )0, (long long )5);
# 556 "dfa.c"
      tmp___8 = dcgettext((char const *)((void *)0), "unfinished repeat count",
                          5);
      __CrestClearStack(28492);
      {
      __CrestLoad(28495, (unsigned long )(& tmp___8), (long long )((unsigned long )tmp___8));
      __CrestLoad(28494, (unsigned long )0, (long long )((unsigned long )((char *)0)));
      __CrestApply2(28493, 13, (long long )((unsigned long )tmp___8 != (unsigned long )((char *)0)));
# 556 "dfa.c"
      if ((unsigned long )tmp___8 != (unsigned long )((char *)0)) {
        __CrestBranch(28496, 4309, 1);
        __CrestLoad(28498, (unsigned long )0, (long long )5);
# 556 "dfa.c"
        tmp___7 = dcgettext((char const *)((void *)0), "unfinished repeat count",
                            5);
        __CrestClearStack(28499);
# 556 "dfa.c"
        dfaerror((char const *)tmp___7);
        __CrestClearStack(28500);
      } else {
        __CrestBranch(28497, 4310, 0);
        __CrestLoad(28501, (unsigned long )0, (long long )((token )-1));
        __CrestStore(28502, (unsigned long )(& lasttok));
# 556 "dfa.c"
        lasttok = (token )-1;
        __CrestLoad(28503, (unsigned long )(& lasttok), (long long )lasttok);
        __CrestStore(28504, (unsigned long )(& __retres88));
# 556 "dfa.c"
        __retres88 = lasttok;
# 556 "dfa.c"
        goto return_label;
      }
      }
    } else {
      __CrestBranch(28490, 4313, 0);

    }
    }
# 556 "dfa.c"
    tmp___9 = lexptr;
# 556 "dfa.c"
    lexptr ++;
    __CrestLoad(28505, (unsigned long )tmp___9, (long long )*tmp___9);
    __CrestStore(28506, (unsigned long )(& c));
# 556 "dfa.c"
    c = (token )((unsigned char )*tmp___9);
    __CrestLoad(28509, (unsigned long )(& lexleft), (long long )lexleft);
    __CrestLoad(28508, (unsigned long )0, (long long )1);
    __CrestApply2(28507, 1, (long long )(lexleft - 1));
    __CrestStore(28510, (unsigned long )(& lexleft));
# 556 "dfa.c"
    lexleft --;
# 557 "dfa.c"
    tmp___15 = __ctype_b_loc();
    __CrestClearStack(28511);
    {
# 557 "dfa.c"
    mem_80 = *tmp___15 + (int )c;
    {
    __CrestLoad(28516, (unsigned long )mem_80, (long long )*mem_80);
    __CrestLoad(28515, (unsigned long )0, (long long )2048);
    __CrestApply2(28514, 5, (long long )((int const )*mem_80 & 2048));
    __CrestLoad(28513, (unsigned long )0, (long long )0);
    __CrestApply2(28512, 13, (long long )(((int const )*mem_80 & 2048) != 0));
# 557 "dfa.c"
    if (((int const )*mem_80 & 2048) != 0) {
      __CrestBranch(28517, 4318, 1);
      __CrestLoad(28521, (unsigned long )(& c), (long long )c);
      __CrestLoad(28520, (unsigned long )0, (long long )48);
      __CrestApply2(28519, 1, (long long )((int )c - 48));
      __CrestStore(28522, (unsigned long )(& minrep));
# 559 "dfa.c"
      minrep = (int )c - 48;
      {
# 560 "dfa.c"
      while (1) {
        while_continue___0: ;
        {
        __CrestLoad(28525, (unsigned long )(& lexleft), (long long )lexleft);
        __CrestLoad(28524, (unsigned long )0, (long long )0);
        __CrestApply2(28523, 12, (long long )(lexleft == 0));
# 562 "dfa.c"
        if (lexleft == 0) {
          __CrestBranch(28526, 4323, 1);
          __CrestLoad(28528, (unsigned long )0, (long long )5);
# 562 "dfa.c"
          tmp___11 = dcgettext((char const *)((void *)0), "unfinished repeat count",
                               5);
          __CrestClearStack(28529);
          {
          __CrestLoad(28532, (unsigned long )(& tmp___11), (long long )((unsigned long )tmp___11));
          __CrestLoad(28531, (unsigned long )0, (long long )((unsigned long )((char *)0)));
          __CrestApply2(28530, 13, (long long )((unsigned long )tmp___11 != (unsigned long )((char *)0)));
# 562 "dfa.c"
          if ((unsigned long )tmp___11 != (unsigned long )((char *)0)) {
            __CrestBranch(28533, 4325, 1);
            __CrestLoad(28535, (unsigned long )0, (long long )5);
# 562 "dfa.c"
            tmp___10 = dcgettext((char const *)((void *)0), "unfinished repeat count",
                                 5);
            __CrestClearStack(28536);
# 562 "dfa.c"
            dfaerror((char const *)tmp___10);
            __CrestClearStack(28537);
          } else {
            __CrestBranch(28534, 4326, 0);
            __CrestLoad(28538, (unsigned long )0, (long long )((token )-1));
            __CrestStore(28539, (unsigned long )(& lasttok));
# 562 "dfa.c"
            lasttok = (token )-1;
            __CrestLoad(28540, (unsigned long )(& lasttok), (long long )lasttok);
            __CrestStore(28541, (unsigned long )(& __retres88));
# 562 "dfa.c"
            __retres88 = lasttok;
# 562 "dfa.c"
            goto return_label;
          }
          }
        } else {
          __CrestBranch(28527, 4329, 0);

        }
        }
# 562 "dfa.c"
        tmp___12 = lexptr;
# 562 "dfa.c"
        lexptr ++;
        __CrestLoad(28542, (unsigned long )tmp___12, (long long )*tmp___12);
        __CrestStore(28543, (unsigned long )(& c));
# 562 "dfa.c"
        c = (token )((unsigned char )*tmp___12);
        __CrestLoad(28546, (unsigned long )(& lexleft), (long long )lexleft);
        __CrestLoad(28545, (unsigned long )0, (long long )1);
        __CrestApply2(28544, 1, (long long )(lexleft - 1));
        __CrestStore(28547, (unsigned long )(& lexleft));
# 562 "dfa.c"
        lexleft --;
# 563 "dfa.c"
        tmp___13 = __ctype_b_loc();
        __CrestClearStack(28548);
        {
# 563 "dfa.c"
        mem_81 = *tmp___13 + (int )c;
        {
        __CrestLoad(28553, (unsigned long )mem_81, (long long )*mem_81);
        __CrestLoad(28552, (unsigned long )0, (long long )2048);
        __CrestApply2(28551, 5, (long long )((int const )*mem_81 & 2048));
        __CrestLoad(28550, (unsigned long )0, (long long )0);
        __CrestApply2(28549, 13, (long long )(((int const )*mem_81 & 2048) != 0));
# 563 "dfa.c"
        if (((int const )*mem_81 & 2048) != 0) {
          __CrestBranch(28554, 4334, 1);

        } else {
          __CrestBranch(28555, 4335, 0);
# 564 "dfa.c"
          goto while_break___0;
        }
        }
        }
        __CrestLoad(28562, (unsigned long )0, (long long )10);
        __CrestLoad(28561, (unsigned long )(& minrep), (long long )minrep);
        __CrestApply2(28560, 2, (long long )(10 * minrep));
        __CrestLoad(28559, (unsigned long )(& c), (long long )c);
        __CrestApply2(28558, 0, (long long )(10 * minrep + (int )c));
        __CrestLoad(28557, (unsigned long )0, (long long )48);
        __CrestApply2(28556, 1, (long long )((10 * minrep + (int )c) - 48));
        __CrestStore(28563, (unsigned long )(& minrep));
# 565 "dfa.c"
        minrep = (10 * minrep + (int )c) - 48;
      }
      while_break___0: ;
      }
    } else {
      __CrestBranch(28518, 4338, 0);
      {
      __CrestLoad(28566, (unsigned long )(& c), (long long )c);
      __CrestLoad(28565, (unsigned long )0, (long long )44);
      __CrestApply2(28564, 13, (long long )((int )c != 44));
# 568 "dfa.c"
      if ((int )c != 44) {
        __CrestBranch(28567, 4339, 1);
        __CrestLoad(28569, (unsigned long )0, (long long )5);
# 569 "dfa.c"
        tmp___14 = dcgettext((char const *)((void *)0), "malformed repeat count",
                             5);
        __CrestClearStack(28570);
# 569 "dfa.c"
        dfaerror((char const *)tmp___14);
        __CrestClearStack(28571);
      } else {
        __CrestBranch(28568, 4340, 0);

      }
      }
    }
    }
    }
    {
    __CrestLoad(28574, (unsigned long )(& c), (long long )c);
    __CrestLoad(28573, (unsigned long )0, (long long )44);
    __CrestApply2(28572, 12, (long long )((int )c == 44));
# 570 "dfa.c"
    if ((int )c == 44) {
      __CrestBranch(28575, 4342, 1);
      {
# 571 "dfa.c"
      while (1) {
        while_continue___1: ;
        {
        __CrestLoad(28579, (unsigned long )(& lexleft), (long long )lexleft);
        __CrestLoad(28578, (unsigned long )0, (long long )0);
        __CrestApply2(28577, 12, (long long )(lexleft == 0));
# 573 "dfa.c"
        if (lexleft == 0) {
          __CrestBranch(28580, 4346, 1);
          __CrestLoad(28582, (unsigned long )0, (long long )5);
# 573 "dfa.c"
          tmp___17 = dcgettext((char const *)((void *)0), "unfinished repeat count",
                               5);
          __CrestClearStack(28583);
          {
          __CrestLoad(28586, (unsigned long )(& tmp___17), (long long )((unsigned long )tmp___17));
          __CrestLoad(28585, (unsigned long )0, (long long )((unsigned long )((char *)0)));
          __CrestApply2(28584, 13, (long long )((unsigned long )tmp___17 != (unsigned long )((char *)0)));
# 573 "dfa.c"
          if ((unsigned long )tmp___17 != (unsigned long )((char *)0)) {
            __CrestBranch(28587, 4348, 1);
            __CrestLoad(28589, (unsigned long )0, (long long )5);
# 573 "dfa.c"
            tmp___16 = dcgettext((char const *)((void *)0), "unfinished repeat count",
                                 5);
            __CrestClearStack(28590);
# 573 "dfa.c"
            dfaerror((char const *)tmp___16);
            __CrestClearStack(28591);
          } else {
            __CrestBranch(28588, 4349, 0);
            __CrestLoad(28592, (unsigned long )0, (long long )((token )-1));
            __CrestStore(28593, (unsigned long )(& lasttok));
# 573 "dfa.c"
            lasttok = (token )-1;
            __CrestLoad(28594, (unsigned long )(& lasttok), (long long )lasttok);
            __CrestStore(28595, (unsigned long )(& __retres88));
# 573 "dfa.c"
            __retres88 = lasttok;
# 573 "dfa.c"
            goto return_label;
          }
          }
        } else {
          __CrestBranch(28581, 4352, 0);

        }
        }
# 573 "dfa.c"
        tmp___18 = lexptr;
# 573 "dfa.c"
        lexptr ++;
        __CrestLoad(28596, (unsigned long )tmp___18, (long long )*tmp___18);
        __CrestStore(28597, (unsigned long )(& c));
# 573 "dfa.c"
        c = (token )((unsigned char )*tmp___18);
        __CrestLoad(28600, (unsigned long )(& lexleft), (long long )lexleft);
        __CrestLoad(28599, (unsigned long )0, (long long )1);
        __CrestApply2(28598, 1, (long long )(lexleft - 1));
        __CrestStore(28601, (unsigned long )(& lexleft));
# 573 "dfa.c"
        lexleft --;
# 574 "dfa.c"
        tmp___19 = __ctype_b_loc();
        __CrestClearStack(28602);
        {
# 574 "dfa.c"
        mem_82 = *tmp___19 + (int )c;
        {
        __CrestLoad(28607, (unsigned long )mem_82, (long long )*mem_82);
        __CrestLoad(28606, (unsigned long )0, (long long )2048);
        __CrestApply2(28605, 5, (long long )((int const )*mem_82 & 2048));
        __CrestLoad(28604, (unsigned long )0, (long long )0);
        __CrestApply2(28603, 13, (long long )(((int const )*mem_82 & 2048) != 0));
# 574 "dfa.c"
        if (((int const )*mem_82 & 2048) != 0) {
          __CrestBranch(28608, 4357, 1);

        } else {
          __CrestBranch(28609, 4358, 0);
# 575 "dfa.c"
          goto while_break___1;
        }
        }
        }
        __CrestLoad(28616, (unsigned long )0, (long long )10);
        __CrestLoad(28615, (unsigned long )(& maxrep), (long long )maxrep);
        __CrestApply2(28614, 2, (long long )(10 * maxrep));
        __CrestLoad(28613, (unsigned long )(& c), (long long )c);
        __CrestApply2(28612, 0, (long long )(10 * maxrep + (int )c));
        __CrestLoad(28611, (unsigned long )0, (long long )48);
        __CrestApply2(28610, 1, (long long )((10 * maxrep + (int )c) - 48));
        __CrestStore(28617, (unsigned long )(& maxrep));
# 576 "dfa.c"
        maxrep = (10 * maxrep + (int )c) - 48;
      }
      while_break___1: ;
      }
    } else {
      __CrestBranch(28576, 4361, 0);
      __CrestLoad(28618, (unsigned long )(& minrep), (long long )minrep);
      __CrestStore(28619, (unsigned long )(& maxrep));
# 579 "dfa.c"
      maxrep = minrep;
    }
    }
    {
    __CrestLoad(28624, (unsigned long )(& syntax_bits), (long long )syntax_bits);
    __CrestLoad(28623, (unsigned long )0, (long long )((((((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1));
    __CrestApply2(28622, 5, (long long )(syntax_bits & ((((((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1)));
    __CrestLoad(28621, (unsigned long )0, (long long )0);
    __CrestApply2(28620, 12, (long long )((syntax_bits & ((((((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1)) == 0));
# 580 "dfa.c"
    if ((syntax_bits & ((((((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1)) == 0) {
      __CrestBranch(28625, 4363, 1);
      {
      __CrestLoad(28629, (unsigned long )(& c), (long long )c);
      __CrestLoad(28628, (unsigned long )0, (long long )92);
      __CrestApply2(28627, 13, (long long )((int )c != 92));
# 582 "dfa.c"
      if ((int )c != 92) {
        __CrestBranch(28630, 4364, 1);
        __CrestLoad(28632, (unsigned long )0, (long long )5);
# 583 "dfa.c"
        tmp___20 = dcgettext((char const *)((void *)0), "malformed repeat count",
                             5);
        __CrestClearStack(28633);
# 583 "dfa.c"
        dfaerror((char const *)tmp___20);
        __CrestClearStack(28634);
      } else {
        __CrestBranch(28631, 4365, 0);

      }
      }
      {
      __CrestLoad(28637, (unsigned long )(& lexleft), (long long )lexleft);
      __CrestLoad(28636, (unsigned long )0, (long long )0);
      __CrestApply2(28635, 12, (long long )(lexleft == 0));
# 584 "dfa.c"
      if (lexleft == 0) {
        __CrestBranch(28638, 4367, 1);
        __CrestLoad(28640, (unsigned long )0, (long long )5);
# 584 "dfa.c"
        tmp___22 = dcgettext((char const *)((void *)0), "unfinished repeat count",
                             5);
        __CrestClearStack(28641);
        {
        __CrestLoad(28644, (unsigned long )(& tmp___22), (long long )((unsigned long )tmp___22));
        __CrestLoad(28643, (unsigned long )0, (long long )((unsigned long )((char *)0)));
        __CrestApply2(28642, 13, (long long )((unsigned long )tmp___22 != (unsigned long )((char *)0)));
# 584 "dfa.c"
        if ((unsigned long )tmp___22 != (unsigned long )((char *)0)) {
          __CrestBranch(28645, 4369, 1);
          __CrestLoad(28647, (unsigned long )0, (long long )5);
# 584 "dfa.c"
          tmp___21 = dcgettext((char const *)((void *)0), "unfinished repeat count",
                               5);
          __CrestClearStack(28648);
# 584 "dfa.c"
          dfaerror((char const *)tmp___21);
          __CrestClearStack(28649);
        } else {
          __CrestBranch(28646, 4370, 0);
          __CrestLoad(28650, (unsigned long )0, (long long )((token )-1));
          __CrestStore(28651, (unsigned long )(& lasttok));
# 584 "dfa.c"
          lasttok = (token )-1;
          __CrestLoad(28652, (unsigned long )(& lasttok), (long long )lasttok);
          __CrestStore(28653, (unsigned long )(& __retres88));
# 584 "dfa.c"
          __retres88 = lasttok;
# 584 "dfa.c"
          goto return_label;
        }
        }
      } else {
        __CrestBranch(28639, 4373, 0);

      }
      }
# 584 "dfa.c"
      tmp___23 = lexptr;
# 584 "dfa.c"
      lexptr ++;
      __CrestLoad(28654, (unsigned long )tmp___23, (long long )*tmp___23);
      __CrestStore(28655, (unsigned long )(& c));
# 584 "dfa.c"
      c = (token )((unsigned char )*tmp___23);
      __CrestLoad(28658, (unsigned long )(& lexleft), (long long )lexleft);
      __CrestLoad(28657, (unsigned long )0, (long long )1);
      __CrestApply2(28656, 1, (long long )(lexleft - 1));
      __CrestStore(28659, (unsigned long )(& lexleft));
# 584 "dfa.c"
      lexleft --;
    } else {
      __CrestBranch(28626, 4375, 0);

    }
    }
    {
    __CrestLoad(28662, (unsigned long )(& c), (long long )c);
    __CrestLoad(28661, (unsigned long )0, (long long )125);
    __CrestApply2(28660, 13, (long long )((int )c != 125));
# 586 "dfa.c"
    if ((int )c != 125) {
      __CrestBranch(28663, 4377, 1);
      __CrestLoad(28665, (unsigned long )0, (long long )5);
# 587 "dfa.c"
      tmp___24 = dcgettext((char const *)((void *)0), "malformed repeat count",
                           5);
      __CrestClearStack(28666);
# 587 "dfa.c"
      dfaerror((char const *)tmp___24);
      __CrestClearStack(28667);
    } else {
      __CrestBranch(28664, 4378, 0);

    }
    }
    __CrestLoad(28668, (unsigned long )0, (long long )0);
    __CrestStore(28669, (unsigned long )(& laststart));
# 588 "dfa.c"
    laststart = 0;
    __CrestLoad(28670, (unsigned long )0, (long long )((token )267));
    __CrestStore(28671, (unsigned long )(& lasttok));
# 589 "dfa.c"
    lasttok = (token )267;
    __CrestLoad(28672, (unsigned long )(& lasttok), (long long )lasttok);
    __CrestStore(28673, (unsigned long )(& __retres88));
# 589 "dfa.c"
    __retres88 = lasttok;
# 589 "dfa.c"
    goto return_label;
    case_124:
    {
    __CrestLoad(28678, (unsigned long )(& syntax_bits), (long long )syntax_bits);
    __CrestLoad(28677, (unsigned long )0, (long long )((((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1));
    __CrestApply2(28676, 5, (long long )(syntax_bits & ((((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1)));
    __CrestLoad(28675, (unsigned long )0, (long long )0);
    __CrestApply2(28674, 13, (long long )((syntax_bits & ((((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1)) != 0));
# 592 "dfa.c"
    if ((syntax_bits & ((((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1)) != 0) {
      __CrestBranch(28679, 4383, 1);
# 593 "dfa.c"
      goto normal_char;
    } else {
      __CrestBranch(28680, 4384, 0);

    }
    }
    {
    __CrestLoad(28687, (unsigned long )(& backslash), (long long )backslash);
    __CrestLoad(28686, (unsigned long )(& syntax_bits), (long long )syntax_bits);
    __CrestLoad(28685, (unsigned long )0, (long long )(((((((((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1));
    __CrestApply2(28684, 5, (long long )(syntax_bits & (((((((((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1)));
    __CrestLoad(28683, (unsigned long )0, (long long )0UL);
    __CrestApply2(28682, 12, (long long )((syntax_bits & (((((((((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1)) == 0UL));
    __CrestApply2(28681, 13, (long long )(backslash != ((syntax_bits & (((((((((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1)) == 0UL)));
# 594 "dfa.c"
    if (backslash != ((syntax_bits & (((((((((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1)) == 0UL)) {
      __CrestBranch(28688, 4386, 1);
# 595 "dfa.c"
      goto normal_char;
    } else {
      __CrestBranch(28689, 4387, 0);

    }
    }
    __CrestLoad(28690, (unsigned long )0, (long long )1);
    __CrestStore(28691, (unsigned long )(& laststart));
# 596 "dfa.c"
    laststart = 1;
    __CrestLoad(28692, (unsigned long )0, (long long )((token )269));
    __CrestStore(28693, (unsigned long )(& lasttok));
# 597 "dfa.c"
    lasttok = (token )269;
    __CrestLoad(28694, (unsigned long )(& lasttok), (long long )lasttok);
    __CrestStore(28695, (unsigned long )(& __retres88));
# 597 "dfa.c"
    __retres88 = lasttok;
# 597 "dfa.c"
    goto return_label;
    case_10:
    {
    __CrestLoad(28700, (unsigned long )(& syntax_bits), (long long )syntax_bits);
    __CrestLoad(28699, (unsigned long )0, (long long )((((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1));
    __CrestApply2(28698, 5, (long long )(syntax_bits & ((((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1)));
    __CrestLoad(28697, (unsigned long )0, (long long )0);
    __CrestApply2(28696, 13, (long long )((syntax_bits & ((((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1)) != 0));
# 600 "dfa.c"
    if ((syntax_bits & ((((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1)) != 0) {
      __CrestBranch(28701, 4392, 1);
# 603 "dfa.c"
      goto normal_char;
    } else {
      __CrestBranch(28702, 4393, 0);
      {
      __CrestLoad(28705, (unsigned long )(& backslash), (long long )backslash);
      __CrestLoad(28704, (unsigned long )0, (long long )0);
      __CrestApply2(28703, 13, (long long )(backslash != 0));
# 600 "dfa.c"
      if (backslash != 0) {
        __CrestBranch(28706, 4394, 1);
# 603 "dfa.c"
        goto normal_char;
      } else {
        __CrestBranch(28707, 4395, 0);
        {
        __CrestLoad(28712, (unsigned long )(& syntax_bits), (long long )syntax_bits);
        __CrestLoad(28711, (unsigned long )0, (long long )(((((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1));
        __CrestApply2(28710, 5, (long long )(syntax_bits & (((((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1)));
        __CrestLoad(28709, (unsigned long )0, (long long )0);
        __CrestApply2(28708, 12, (long long )((syntax_bits & (((((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1)) == 0));
# 600 "dfa.c"
        if ((syntax_bits & (((((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1)) == 0) {
          __CrestBranch(28713, 4396, 1);
# 603 "dfa.c"
          goto normal_char;
        } else {
          __CrestBranch(28714, 4397, 0);

        }
        }
      }
      }
    }
    }
    __CrestLoad(28715, (unsigned long )0, (long long )1);
    __CrestStore(28716, (unsigned long )(& laststart));
# 604 "dfa.c"
    laststart = 1;
    __CrestLoad(28717, (unsigned long )0, (long long )((token )269));
    __CrestStore(28718, (unsigned long )(& lasttok));
# 605 "dfa.c"
    lasttok = (token )269;
    __CrestLoad(28719, (unsigned long )(& lasttok), (long long )lasttok);
    __CrestStore(28720, (unsigned long )(& __retres88));
# 605 "dfa.c"
    __retres88 = lasttok;
# 605 "dfa.c"
    goto return_label;
    case_40:
    {
    __CrestLoad(28727, (unsigned long )(& backslash), (long long )backslash);
    __CrestLoad(28726, (unsigned long )(& syntax_bits), (long long )syntax_bits);
    __CrestLoad(28725, (unsigned long )0, (long long )(((((((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1));
    __CrestApply2(28724, 5, (long long )(syntax_bits & (((((((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1)));
    __CrestLoad(28723, (unsigned long )0, (long long )0UL);
    __CrestApply2(28722, 12, (long long )((syntax_bits & (((((((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1)) == 0UL));
    __CrestApply2(28721, 13, (long long )(backslash != ((syntax_bits & (((((((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1)) == 0UL)));
# 608 "dfa.c"
    if (backslash != ((syntax_bits & (((((((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1)) == 0UL)) {
      __CrestBranch(28728, 4402, 1);
# 609 "dfa.c"
      goto normal_char;
    } else {
      __CrestBranch(28729, 4403, 0);

    }
    }
    __CrestLoad(28732, (unsigned long )(& parens), (long long )parens);
    __CrestLoad(28731, (unsigned long )0, (long long )1);
    __CrestApply2(28730, 0, (long long )(parens + 1));
    __CrestStore(28733, (unsigned long )(& parens));
# 610 "dfa.c"
    parens ++;
    __CrestLoad(28734, (unsigned long )0, (long long )1);
    __CrestStore(28735, (unsigned long )(& laststart));
# 611 "dfa.c"
    laststart = 1;
    __CrestLoad(28736, (unsigned long )0, (long long )((token )271));
    __CrestStore(28737, (unsigned long )(& lasttok));
# 612 "dfa.c"
    lasttok = (token )271;
    __CrestLoad(28738, (unsigned long )(& lasttok), (long long )lasttok);
    __CrestStore(28739, (unsigned long )(& __retres88));
# 612 "dfa.c"
    __retres88 = lasttok;
# 612 "dfa.c"
    goto return_label;
    case_41:
    {
    __CrestLoad(28746, (unsigned long )(& backslash), (long long )backslash);
    __CrestLoad(28745, (unsigned long )(& syntax_bits), (long long )syntax_bits);
    __CrestLoad(28744, (unsigned long )0, (long long )(((((((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1));
    __CrestApply2(28743, 5, (long long )(syntax_bits & (((((((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1)));
    __CrestLoad(28742, (unsigned long )0, (long long )0UL);
    __CrestApply2(28741, 12, (long long )((syntax_bits & (((((((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1)) == 0UL));
    __CrestApply2(28740, 13, (long long )(backslash != ((syntax_bits & (((((((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1)) == 0UL)));
# 615 "dfa.c"
    if (backslash != ((syntax_bits & (((((((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1)) == 0UL)) {
      __CrestBranch(28747, 4408, 1);
# 616 "dfa.c"
      goto normal_char;
    } else {
      __CrestBranch(28748, 4409, 0);

    }
    }
    {
    __CrestLoad(28751, (unsigned long )(& parens), (long long )parens);
    __CrestLoad(28750, (unsigned long )0, (long long )0);
    __CrestApply2(28749, 12, (long long )(parens == 0));
# 617 "dfa.c"
    if (parens == 0) {
      __CrestBranch(28752, 4411, 1);
      {
      __CrestLoad(28758, (unsigned long )(& syntax_bits), (long long )syntax_bits);
      __CrestLoad(28757, (unsigned long )0, (long long )(((((((((((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1));
      __CrestApply2(28756, 5, (long long )(syntax_bits & (((((((((((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1)));
      __CrestLoad(28755, (unsigned long )0, (long long )0);
      __CrestApply2(28754, 13, (long long )((syntax_bits & (((((((((((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1)) != 0));
# 617 "dfa.c"
      if ((syntax_bits & (((((((((((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1)) != 0) {
        __CrestBranch(28759, 4412, 1);
# 618 "dfa.c"
        goto normal_char;
      } else {
        __CrestBranch(28760, 4413, 0);

      }
      }
    } else {
      __CrestBranch(28753, 4414, 0);

    }
    }
    __CrestLoad(28763, (unsigned long )(& parens), (long long )parens);
    __CrestLoad(28762, (unsigned long )0, (long long )1);
    __CrestApply2(28761, 1, (long long )(parens - 1));
    __CrestStore(28764, (unsigned long )(& parens));
# 619 "dfa.c"
    parens --;
    __CrestLoad(28765, (unsigned long )0, (long long )0);
    __CrestStore(28766, (unsigned long )(& laststart));
# 620 "dfa.c"
    laststart = 0;
    __CrestLoad(28767, (unsigned long )0, (long long )((token )272));
    __CrestStore(28768, (unsigned long )(& lasttok));
# 621 "dfa.c"
    lasttok = (token )272;
    __CrestLoad(28769, (unsigned long )(& lasttok), (long long )lasttok);
    __CrestStore(28770, (unsigned long )(& __retres88));
# 621 "dfa.c"
    __retres88 = lasttok;
# 621 "dfa.c"
    goto return_label;
    case_46:
    {
    __CrestLoad(28773, (unsigned long )(& backslash), (long long )backslash);
    __CrestLoad(28772, (unsigned long )0, (long long )0);
    __CrestApply2(28771, 13, (long long )(backslash != 0));
# 624 "dfa.c"
    if (backslash != 0) {
      __CrestBranch(28774, 4419, 1);
# 625 "dfa.c"
      goto normal_char;
    } else {
      __CrestBranch(28775, 4420, 0);

    }
    }
# 626 "dfa.c"
    zeroset(ccl);
    __CrestClearStack(28776);
# 627 "dfa.c"
    notset(ccl);
    __CrestClearStack(28777);
    {
    __CrestLoad(28782, (unsigned long )(& syntax_bits), (long long )syntax_bits);
    __CrestLoad(28781, (unsigned long )0, (long long )((((((1UL << 1) << 1) << 1) << 1) << 1) << 1));
    __CrestApply2(28780, 5, (long long )(syntax_bits & ((((((1UL << 1) << 1) << 1) << 1) << 1) << 1)));
    __CrestLoad(28779, (unsigned long )0, (long long )0);
    __CrestApply2(28778, 12, (long long )((syntax_bits & ((((((1UL << 1) << 1) << 1) << 1) << 1) << 1)) == 0));
# 628 "dfa.c"
    if ((syntax_bits & ((((((1UL << 1) << 1) << 1) << 1) << 1) << 1)) == 0) {
      __CrestBranch(28783, 4423, 1);
      __CrestLoad(28785, (unsigned long )0, (long long )'\n');
# 629 "dfa.c"
      clrbit('\n', ccl);
      __CrestClearStack(28786);
    } else {
      __CrestBranch(28784, 4424, 0);

    }
    }
    {
    __CrestLoad(28791, (unsigned long )(& syntax_bits), (long long )syntax_bits);
    __CrestLoad(28790, (unsigned long )0, (long long )(((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1));
    __CrestApply2(28789, 5, (long long )(syntax_bits & (((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1)));
    __CrestLoad(28788, (unsigned long )0, (long long )0);
    __CrestApply2(28787, 13, (long long )((syntax_bits & (((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1)) != 0));
# 630 "dfa.c"
    if ((syntax_bits & (((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1)) != 0) {
      __CrestBranch(28792, 4426, 1);
      __CrestLoad(28794, (unsigned long )0, (long long )'\000');
# 631 "dfa.c"
      clrbit('\000', ccl);
      __CrestClearStack(28795);
    } else {
      __CrestBranch(28793, 4427, 0);

    }
    }
    __CrestLoad(28796, (unsigned long )0, (long long )0);
    __CrestStore(28797, (unsigned long )(& laststart));
# 632 "dfa.c"
    laststart = 0;
# 633 "dfa.c"
    tmp___25 = charclass_index(ccl);
    __CrestHandleReturn(28799, (long long )tmp___25);
    __CrestStore(28798, (unsigned long )(& tmp___25));
    __CrestLoad(28802, (unsigned long )0, (long long )273);
    __CrestLoad(28801, (unsigned long )(& tmp___25), (long long )tmp___25);
    __CrestApply2(28800, 0, (long long )(273 + tmp___25));
    __CrestStore(28803, (unsigned long )(& lasttok));
# 633 "dfa.c"
    lasttok = (token )(273 + tmp___25);
    __CrestLoad(28804, (unsigned long )(& lasttok), (long long )lasttok);
    __CrestStore(28805, (unsigned long )(& __retres88));
# 633 "dfa.c"
    __retres88 = lasttok;
# 633 "dfa.c"
    goto return_label;
    case_87:
    case_119:
    {
    __CrestLoad(28808, (unsigned long )(& backslash), (long long )backslash);
    __CrestLoad(28807, (unsigned long )0, (long long )0);
    __CrestApply2(28806, 12, (long long )(backslash == 0));
# 637 "dfa.c"
    if (backslash == 0) {
      __CrestBranch(28809, 4432, 1);
# 638 "dfa.c"
      goto normal_char;
    } else {
      __CrestBranch(28810, 4433, 0);
      {
      __CrestLoad(28815, (unsigned long )(& syntax_bits), (long long )syntax_bits);
      __CrestLoad(28814, (unsigned long )0, (long long )(((((((((((((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1));
      __CrestApply2(28813, 5, (long long )(syntax_bits & (((((((((((((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1)));
      __CrestLoad(28812, (unsigned long )0, (long long )0);
      __CrestApply2(28811, 13, (long long )((syntax_bits & (((((((((((((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1)) != 0));
# 637 "dfa.c"
      if ((syntax_bits & (((((((((((((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1)) != 0) {
        __CrestBranch(28816, 4434, 1);
# 638 "dfa.c"
        goto normal_char;
      } else {
        __CrestBranch(28817, 4435, 0);

      }
      }
    }
    }
# 639 "dfa.c"
    zeroset(ccl);
    __CrestClearStack(28818);
    __CrestLoad(28819, (unsigned long )0, (long long )((token )0));
    __CrestStore(28820, (unsigned long )(& c2));
# 640 "dfa.c"
    c2 = (token )0;
    {
# 640 "dfa.c"
    while (1) {
      while_continue___2: ;
      {
      __CrestLoad(28823, (unsigned long )(& c2), (long long )c2);
      __CrestLoad(28822, (unsigned long )0, (long long )(1 << 8));
      __CrestApply2(28821, 16, (long long )((int )c2 < 1 << 8));
# 640 "dfa.c"
      if ((int )c2 < 1 << 8) {
        __CrestBranch(28824, 4441, 1);

      } else {
        __CrestBranch(28825, 4442, 0);
# 640 "dfa.c"
        goto while_break___2;
      }
      }
# 641 "dfa.c"
      tmp___26 = __ctype_b_loc();
      __CrestClearStack(28826);
      {
# 641 "dfa.c"
      mem_83 = *tmp___26 + (int )c2;
      {
      __CrestLoad(28831, (unsigned long )mem_83, (long long )*mem_83);
      __CrestLoad(28830, (unsigned long )0, (long long )8);
      __CrestApply2(28829, 5, (long long )((int const )*mem_83 & 8));
      __CrestLoad(28828, (unsigned long )0, (long long )0);
      __CrestApply2(28827, 13, (long long )(((int const )*mem_83 & 8) != 0));
# 641 "dfa.c"
      if (((int const )*mem_83 & 8) != 0) {
        __CrestBranch(28832, 4447, 1);
        __CrestLoad(28834, (unsigned long )(& c2), (long long )c2);
# 642 "dfa.c"
        setbit((int )c2, ccl);
        __CrestClearStack(28835);
      } else {
        __CrestBranch(28833, 4448, 0);
        {
        __CrestLoad(28838, (unsigned long )(& c2), (long long )c2);
        __CrestLoad(28837, (unsigned long )0, (long long )95);
        __CrestApply2(28836, 12, (long long )((int )c2 == 95));
# 641 "dfa.c"
        if ((int )c2 == 95) {
          __CrestBranch(28839, 4449, 1);
          __CrestLoad(28841, (unsigned long )(& c2), (long long )c2);
# 642 "dfa.c"
          setbit((int )c2, ccl);
          __CrestClearStack(28842);
        } else {
          __CrestBranch(28840, 4450, 0);

        }
        }
      }
      }
      }
      __CrestLoad(28845, (unsigned long )(& c2), (long long )c2);
      __CrestLoad(28844, (unsigned long )0, (long long )1);
      __CrestApply2(28843, 0, (long long )((int )c2 + 1));
      __CrestStore(28846, (unsigned long )(& c2));
# 640 "dfa.c"
      c2 = (token )((int )c2 + 1);
    }
    while_break___2: ;
    }
    {
    __CrestLoad(28849, (unsigned long )(& c), (long long )c);
    __CrestLoad(28848, (unsigned long )0, (long long )87);
    __CrestApply2(28847, 12, (long long )((int )c == 87));
# 643 "dfa.c"
    if ((int )c == 87) {
      __CrestBranch(28850, 4454, 1);
# 644 "dfa.c"
      notset(ccl);
      __CrestClearStack(28852);
    } else {
      __CrestBranch(28851, 4455, 0);

    }
    }
    __CrestLoad(28853, (unsigned long )0, (long long )0);
    __CrestStore(28854, (unsigned long )(& laststart));
# 645 "dfa.c"
    laststart = 0;
# 646 "dfa.c"
    tmp___27 = charclass_index(ccl);
    __CrestHandleReturn(28856, (long long )tmp___27);
    __CrestStore(28855, (unsigned long )(& tmp___27));
    __CrestLoad(28859, (unsigned long )0, (long long )273);
    __CrestLoad(28858, (unsigned long )(& tmp___27), (long long )tmp___27);
    __CrestApply2(28857, 0, (long long )(273 + tmp___27));
    __CrestStore(28860, (unsigned long )(& lasttok));
# 646 "dfa.c"
    lasttok = (token )(273 + tmp___27);
    __CrestLoad(28861, (unsigned long )(& lasttok), (long long )lasttok);
    __CrestStore(28862, (unsigned long )(& __retres88));
# 646 "dfa.c"
    __retres88 = lasttok;
# 646 "dfa.c"
    goto return_label;
    case_91:
    {
    __CrestLoad(28865, (unsigned long )(& backslash), (long long )backslash);
    __CrestLoad(28864, (unsigned long )0, (long long )0);
    __CrestApply2(28863, 13, (long long )(backslash != 0));
# 649 "dfa.c"
    if (backslash != 0) {
      __CrestBranch(28866, 4460, 1);
# 650 "dfa.c"
      goto normal_char;
    } else {
      __CrestBranch(28867, 4461, 0);

    }
    }
# 651 "dfa.c"
    zeroset(ccl);
    __CrestClearStack(28868);
    {
    __CrestLoad(28871, (unsigned long )(& lexleft), (long long )lexleft);
    __CrestLoad(28870, (unsigned long )0, (long long )0);
    __CrestApply2(28869, 12, (long long )(lexleft == 0));
# 652 "dfa.c"
    if (lexleft == 0) {
      __CrestBranch(28872, 4464, 1);
      __CrestLoad(28874, (unsigned long )0, (long long )5);
# 652 "dfa.c"
      tmp___29 = dcgettext((char const *)((void *)0), "Unbalanced [", 5);
      __CrestClearStack(28875);
      {
      __CrestLoad(28878, (unsigned long )(& tmp___29), (long long )((unsigned long )tmp___29));
      __CrestLoad(28877, (unsigned long )0, (long long )((unsigned long )((char *)0)));
      __CrestApply2(28876, 13, (long long )((unsigned long )tmp___29 != (unsigned long )((char *)0)));
# 652 "dfa.c"
      if ((unsigned long )tmp___29 != (unsigned long )((char *)0)) {
        __CrestBranch(28879, 4466, 1);
        __CrestLoad(28881, (unsigned long )0, (long long )5);
# 652 "dfa.c"
        tmp___28 = dcgettext((char const *)((void *)0), "Unbalanced [", 5);
        __CrestClearStack(28882);
# 652 "dfa.c"
        dfaerror((char const *)tmp___28);
        __CrestClearStack(28883);
      } else {
        __CrestBranch(28880, 4467, 0);
        __CrestLoad(28884, (unsigned long )0, (long long )((token )-1));
        __CrestStore(28885, (unsigned long )(& lasttok));
# 652 "dfa.c"
        lasttok = (token )-1;
        __CrestLoad(28886, (unsigned long )(& lasttok), (long long )lasttok);
        __CrestStore(28887, (unsigned long )(& __retres88));
# 652 "dfa.c"
        __retres88 = lasttok;
# 652 "dfa.c"
        goto return_label;
      }
      }
    } else {
      __CrestBranch(28873, 4470, 0);

    }
    }
# 652 "dfa.c"
    tmp___30 = lexptr;
# 652 "dfa.c"
    lexptr ++;
    __CrestLoad(28888, (unsigned long )tmp___30, (long long )*tmp___30);
    __CrestStore(28889, (unsigned long )(& c));
# 652 "dfa.c"
    c = (token )((unsigned char )*tmp___30);
    __CrestLoad(28892, (unsigned long )(& lexleft), (long long )lexleft);
    __CrestLoad(28891, (unsigned long )0, (long long )1);
    __CrestApply2(28890, 1, (long long )(lexleft - 1));
    __CrestStore(28893, (unsigned long )(& lexleft));
# 652 "dfa.c"
    lexleft --;
    {
    __CrestLoad(28896, (unsigned long )(& c), (long long )c);
    __CrestLoad(28895, (unsigned long )0, (long long )94);
    __CrestApply2(28894, 12, (long long )((int )c == 94));
# 653 "dfa.c"
    if ((int )c == 94) {
      __CrestBranch(28897, 4473, 1);
      {
      __CrestLoad(28901, (unsigned long )(& lexleft), (long long )lexleft);
      __CrestLoad(28900, (unsigned long )0, (long long )0);
      __CrestApply2(28899, 12, (long long )(lexleft == 0));
# 655 "dfa.c"
      if (lexleft == 0) {
        __CrestBranch(28902, 4474, 1);
        __CrestLoad(28904, (unsigned long )0, (long long )5);
# 655 "dfa.c"
        tmp___32 = dcgettext((char const *)((void *)0), "Unbalanced [", 5);
        __CrestClearStack(28905);
        {
        __CrestLoad(28908, (unsigned long )(& tmp___32), (long long )((unsigned long )tmp___32));
        __CrestLoad(28907, (unsigned long )0, (long long )((unsigned long )((char *)0)));
        __CrestApply2(28906, 13, (long long )((unsigned long )tmp___32 != (unsigned long )((char *)0)));
# 655 "dfa.c"
        if ((unsigned long )tmp___32 != (unsigned long )((char *)0)) {
          __CrestBranch(28909, 4476, 1);
          __CrestLoad(28911, (unsigned long )0, (long long )5);
# 655 "dfa.c"
          tmp___31 = dcgettext((char const *)((void *)0), "Unbalanced [", 5);
          __CrestClearStack(28912);
# 655 "dfa.c"
          dfaerror((char const *)tmp___31);
          __CrestClearStack(28913);
        } else {
          __CrestBranch(28910, 4477, 0);
          __CrestLoad(28914, (unsigned long )0, (long long )((token )-1));
          __CrestStore(28915, (unsigned long )(& lasttok));
# 655 "dfa.c"
          lasttok = (token )-1;
          __CrestLoad(28916, (unsigned long )(& lasttok), (long long )lasttok);
          __CrestStore(28917, (unsigned long )(& __retres88));
# 655 "dfa.c"
          __retres88 = lasttok;
# 655 "dfa.c"
          goto return_label;
        }
        }
      } else {
        __CrestBranch(28903, 4480, 0);

      }
      }
# 655 "dfa.c"
      tmp___33 = lexptr;
# 655 "dfa.c"
      lexptr ++;
      __CrestLoad(28918, (unsigned long )tmp___33, (long long )*tmp___33);
      __CrestStore(28919, (unsigned long )(& c));
# 655 "dfa.c"
      c = (token )((unsigned char )*tmp___33);
      __CrestLoad(28922, (unsigned long )(& lexleft), (long long )lexleft);
      __CrestLoad(28921, (unsigned long )0, (long long )1);
      __CrestApply2(28920, 1, (long long )(lexleft - 1));
      __CrestStore(28923, (unsigned long )(& lexleft));
# 655 "dfa.c"
      lexleft --;
      __CrestLoad(28924, (unsigned long )0, (long long )1);
      __CrestStore(28925, (unsigned long )(& invert));
# 656 "dfa.c"
      invert = 1;
    } else {
      __CrestBranch(28898, 4482, 0);
      __CrestLoad(28926, (unsigned long )0, (long long )0);
      __CrestStore(28927, (unsigned long )(& invert));
# 659 "dfa.c"
      invert = 0;
    }
    }
    {
# 660 "dfa.c"
    while (1) {
      while_continue___3: ;
      {
      __CrestLoad(28930, (unsigned long )(& c), (long long )c);
      __CrestLoad(28929, (unsigned long )0, (long long )91);
      __CrestApply2(28928, 12, (long long )((int )c == 91));
# 668 "dfa.c"
      if ((int )c == 91) {
        __CrestBranch(28931, 4487, 1);
        {
        __CrestLoad(28937, (unsigned long )(& syntax_bits), (long long )syntax_bits);
        __CrestLoad(28936, (unsigned long )0, (long long )((1UL << 1) << 1));
        __CrestApply2(28935, 5, (long long )(syntax_bits & ((1UL << 1) << 1)));
        __CrestLoad(28934, (unsigned long )0, (long long )0);
        __CrestApply2(28933, 13, (long long )((syntax_bits & ((1UL << 1) << 1)) != 0));
# 668 "dfa.c"
        if ((syntax_bits & ((1UL << 1) << 1)) != 0) {
          __CrestBranch(28938, 4488, 1);
          __CrestLoad(28940, (unsigned long )0, (long long )((token )0));
          __CrestStore(28941, (unsigned long )(& c1));
# 669 "dfa.c"
          c1 = (token )0;
          {
# 669 "dfa.c"
          while (1) {
            while_continue___4: ;
            {
            __CrestLoad(28944, (unsigned long )(& prednames[c1].name), (long long )((unsigned long )prednames[c1].name));
            __CrestLoad(28943, (unsigned long )0, (long long )0);
            __CrestApply2(28942, 13, (long long )(prednames[c1].name != 0));
# 669 "dfa.c"
            if (prednames[c1].name != 0) {
              __CrestBranch(28945, 4493, 1);

            } else {
              __CrestBranch(28946, 4494, 0);
# 669 "dfa.c"
              goto while_break___4;
            }
            }
# 670 "dfa.c"
            tmp___40 = looking_at(prednames[c1].name);
            __CrestHandleReturn(28948, (long long )tmp___40);
            __CrestStore(28947, (unsigned long )(& tmp___40));
            {
            __CrestLoad(28951, (unsigned long )(& tmp___40), (long long )tmp___40);
            __CrestLoad(28950, (unsigned long )0, (long long )0);
            __CrestApply2(28949, 13, (long long )(tmp___40 != 0));
# 670 "dfa.c"
            if (tmp___40 != 0) {
              __CrestBranch(28952, 4497, 1);
# 672 "dfa.c"
              pred = (int (*)())prednames[c1].pred;
              {
              __CrestLoad(28956, (unsigned long )(& case_fold), (long long )case_fold);
              __CrestLoad(28955, (unsigned long )0, (long long )0);
              __CrestApply2(28954, 13, (long long )(case_fold != 0));
# 673 "dfa.c"
              if (case_fold != 0) {
                __CrestBranch(28957, 4499, 1);
                {
                __CrestLoad(28961, (unsigned long )(& pred), (long long )((unsigned long )pred));
                __CrestLoad(28960, (unsigned long )0, (long long )((unsigned long )(& is_upper)));
                __CrestApply2(28959, 12, (long long )((unsigned long )pred == (unsigned long )(& is_upper)));
# 673 "dfa.c"
                if ((unsigned long )pred == (unsigned long )(& is_upper)) {
                  __CrestBranch(28962, 4500, 1);
# 675 "dfa.c"
                  pred = (int (*)())(& is_alpha);
                } else {
                  __CrestBranch(28963, 4501, 0);
                  {
                  __CrestLoad(28966, (unsigned long )(& pred), (long long )((unsigned long )pred));
                  __CrestLoad(28965, (unsigned long )0, (long long )((unsigned long )(& is_lower)));
                  __CrestApply2(28964, 12, (long long )((unsigned long )pred == (unsigned long )(& is_lower)));
# 673 "dfa.c"
                  if ((unsigned long )pred == (unsigned long )(& is_lower)) {
                    __CrestBranch(28967, 4502, 1);
# 675 "dfa.c"
                    pred = (int (*)())(& is_alpha);
                  } else {
                    __CrestBranch(28968, 4503, 0);

                  }
                  }
                }
                }
              } else {
                __CrestBranch(28958, 4504, 0);

              }
              }
              __CrestLoad(28969, (unsigned long )0, (long long )((token )0));
              __CrestStore(28970, (unsigned long )(& c2));
# 677 "dfa.c"
              c2 = (token )0;
              {
# 677 "dfa.c"
              while (1) {
                while_continue___5: ;
                {
                __CrestLoad(28973, (unsigned long )(& c2), (long long )c2);
                __CrestLoad(28972, (unsigned long )0, (long long )(1 << 8));
                __CrestApply2(28971, 16, (long long )((int )c2 < 1 << 8));
# 677 "dfa.c"
                if ((int )c2 < 1 << 8) {
                  __CrestBranch(28974, 4510, 1);

                } else {
                  __CrestBranch(28975, 4511, 0);
# 677 "dfa.c"
                  goto while_break___5;
                }
                }
                __CrestLoad(28976, (unsigned long )(& c2), (long long )c2);
# 678 "dfa.c"
                tmp___34 = (*pred)((int )c2);
                __CrestHandleReturn(28978, (long long )tmp___34);
                __CrestStore(28977, (unsigned long )(& tmp___34));
                {
                __CrestLoad(28981, (unsigned long )(& tmp___34), (long long )tmp___34);
                __CrestLoad(28980, (unsigned long )0, (long long )0);
                __CrestApply2(28979, 13, (long long )(tmp___34 != 0));
# 678 "dfa.c"
                if (tmp___34 != 0) {
                  __CrestBranch(28982, 4514, 1);
                  __CrestLoad(28984, (unsigned long )(& c2), (long long )c2);
# 679 "dfa.c"
                  setbit((int )c2, ccl);
                  __CrestClearStack(28985);
                } else {
                  __CrestBranch(28983, 4515, 0);

                }
                }
                __CrestLoad(28988, (unsigned long )(& c2), (long long )c2);
                __CrestLoad(28987, (unsigned long )0, (long long )1);
                __CrestApply2(28986, 0, (long long )((int )c2 + 1));
                __CrestStore(28989, (unsigned long )(& c2));
# 677 "dfa.c"
                c2 = (token )((int )c2 + 1);
              }
              while_break___5: ;
              }
# 680 "dfa.c"
              tmp___35 = strlen(prednames[c1].name);
              __CrestHandleReturn(28991, (long long )tmp___35);
              __CrestStore(28990, (unsigned long )(& tmp___35));
# 680 "dfa.c"
              lexptr += tmp___35;
# 681 "dfa.c"
              tmp___36 = strlen(prednames[c1].name);
              __CrestHandleReturn(28993, (long long )tmp___36);
              __CrestStore(28992, (unsigned long )(& tmp___36));
              __CrestLoad(28996, (unsigned long )(& lexleft), (long long )lexleft);
              __CrestLoad(28995, (unsigned long )(& tmp___36), (long long )tmp___36);
              __CrestApply2(28994, 1, (long long )((size_t )lexleft - tmp___36));
              __CrestStore(28997, (unsigned long )(& lexleft));
# 681 "dfa.c"
              lexleft = (int )((size_t )lexleft - tmp___36);
              {
              __CrestLoad(29000, (unsigned long )(& lexleft), (long long )lexleft);
              __CrestLoad(28999, (unsigned long )0, (long long )0);
              __CrestApply2(28998, 12, (long long )(lexleft == 0));
# 682 "dfa.c"
              if (lexleft == 0) {
                __CrestBranch(29001, 4520, 1);
                __CrestLoad(29003, (unsigned long )0, (long long )5);
# 682 "dfa.c"
                tmp___38 = dcgettext((char const *)((void *)0), "Unbalanced [",
                                     5);
                __CrestClearStack(29004);
                {
                __CrestLoad(29007, (unsigned long )(& tmp___38), (long long )((unsigned long )tmp___38));
                __CrestLoad(29006, (unsigned long )0, (long long )((unsigned long )((char *)0)));
                __CrestApply2(29005, 13, (long long )((unsigned long )tmp___38 != (unsigned long )((char *)0)));
# 682 "dfa.c"
                if ((unsigned long )tmp___38 != (unsigned long )((char *)0)) {
                  __CrestBranch(29008, 4522, 1);
                  __CrestLoad(29010, (unsigned long )0, (long long )5);
# 682 "dfa.c"
                  tmp___37 = dcgettext((char const *)((void *)0), "Unbalanced [",
                                       5);
                  __CrestClearStack(29011);
# 682 "dfa.c"
                  dfaerror((char const *)tmp___37);
                  __CrestClearStack(29012);
                } else {
                  __CrestBranch(29009, 4523, 0);
                  __CrestLoad(29013, (unsigned long )0, (long long )((token )-1));
                  __CrestStore(29014, (unsigned long )(& lasttok));
# 682 "dfa.c"
                  lasttok = (token )-1;
                  __CrestLoad(29015, (unsigned long )(& lasttok), (long long )lasttok);
                  __CrestStore(29016, (unsigned long )(& __retres88));
# 682 "dfa.c"
                  __retres88 = lasttok;
# 682 "dfa.c"
                  goto return_label;
                }
                }
              } else {
                __CrestBranch(29002, 4526, 0);

              }
              }
# 682 "dfa.c"
              tmp___39 = lexptr;
# 682 "dfa.c"
              lexptr ++;
              __CrestLoad(29017, (unsigned long )tmp___39, (long long )*tmp___39);
              __CrestStore(29018, (unsigned long )(& c1));
# 682 "dfa.c"
              c1 = (token )((unsigned char )*tmp___39);
              __CrestLoad(29021, (unsigned long )(& lexleft), (long long )lexleft);
              __CrestLoad(29020, (unsigned long )0, (long long )1);
              __CrestApply2(29019, 1, (long long )(lexleft - 1));
              __CrestStore(29022, (unsigned long )(& lexleft));
# 682 "dfa.c"
              lexleft --;
# 683 "dfa.c"
              goto skip;
            } else {
              __CrestBranch(28953, 4529, 0);

            }
            }
            __CrestLoad(29025, (unsigned long )(& c1), (long long )c1);
            __CrestLoad(29024, (unsigned long )0, (long long )1);
            __CrestApply2(29023, 0, (long long )((int )c1 + 1));
            __CrestStore(29026, (unsigned long )(& c1));
# 669 "dfa.c"
            c1 = (token )((int )c1 + 1);
          }
          while_break___4: ;
          }
        } else {
          __CrestBranch(28939, 4532, 0);

        }
        }
      } else {
        __CrestBranch(28932, 4533, 0);

      }
      }
      {
      __CrestLoad(29029, (unsigned long )(& c), (long long )c);
      __CrestLoad(29028, (unsigned long )0, (long long )92);
      __CrestApply2(29027, 12, (long long )((int )c == 92));
# 685 "dfa.c"
      if ((int )c == 92) {
        __CrestBranch(29030, 4535, 1);
        {
        __CrestLoad(29036, (unsigned long )(& syntax_bits), (long long )syntax_bits);
        __CrestLoad(29035, (unsigned long )0, (long long )1UL);
        __CrestApply2(29034, 5, (long long )(syntax_bits & 1UL));
        __CrestLoad(29033, (unsigned long )0, (long long )0);
        __CrestApply2(29032, 13, (long long )((syntax_bits & 1UL) != 0));
# 685 "dfa.c"
        if ((syntax_bits & 1UL) != 0) {
          __CrestBranch(29037, 4536, 1);
          {
          __CrestLoad(29041, (unsigned long )(& lexleft), (long long )lexleft);
          __CrestLoad(29040, (unsigned long )0, (long long )0);
          __CrestApply2(29039, 12, (long long )(lexleft == 0));
# 686 "dfa.c"
          if (lexleft == 0) {
            __CrestBranch(29042, 4537, 1);
            __CrestLoad(29044, (unsigned long )0, (long long )5);
# 686 "dfa.c"
            tmp___42 = dcgettext((char const *)((void *)0), "Unbalanced [", 5);
            __CrestClearStack(29045);
            {
            __CrestLoad(29048, (unsigned long )(& tmp___42), (long long )((unsigned long )tmp___42));
            __CrestLoad(29047, (unsigned long )0, (long long )((unsigned long )((char *)0)));
            __CrestApply2(29046, 13, (long long )((unsigned long )tmp___42 != (unsigned long )((char *)0)));
# 686 "dfa.c"
            if ((unsigned long )tmp___42 != (unsigned long )((char *)0)) {
              __CrestBranch(29049, 4539, 1);
              __CrestLoad(29051, (unsigned long )0, (long long )5);
# 686 "dfa.c"
              tmp___41 = dcgettext((char const *)((void *)0), "Unbalanced [", 5);
              __CrestClearStack(29052);
# 686 "dfa.c"
              dfaerror((char const *)tmp___41);
              __CrestClearStack(29053);
            } else {
              __CrestBranch(29050, 4540, 0);
              __CrestLoad(29054, (unsigned long )0, (long long )((token )-1));
              __CrestStore(29055, (unsigned long )(& lasttok));
# 686 "dfa.c"
              lasttok = (token )-1;
              __CrestLoad(29056, (unsigned long )(& lasttok), (long long )lasttok);
              __CrestStore(29057, (unsigned long )(& __retres88));
# 686 "dfa.c"
              __retres88 = lasttok;
# 686 "dfa.c"
              goto return_label;
            }
            }
          } else {
            __CrestBranch(29043, 4543, 0);

          }
          }
# 686 "dfa.c"
          tmp___43 = lexptr;
# 686 "dfa.c"
          lexptr ++;
          __CrestLoad(29058, (unsigned long )tmp___43, (long long )*tmp___43);
          __CrestStore(29059, (unsigned long )(& c));
# 686 "dfa.c"
          c = (token )((unsigned char )*tmp___43);
          __CrestLoad(29062, (unsigned long )(& lexleft), (long long )lexleft);
          __CrestLoad(29061, (unsigned long )0, (long long )1);
          __CrestApply2(29060, 1, (long long )(lexleft - 1));
          __CrestStore(29063, (unsigned long )(& lexleft));
# 686 "dfa.c"
          lexleft --;
        } else {
          __CrestBranch(29038, 4545, 0);

        }
        }
      } else {
        __CrestBranch(29031, 4546, 0);

      }
      }
      {
      __CrestLoad(29066, (unsigned long )(& lexleft), (long long )lexleft);
      __CrestLoad(29065, (unsigned long )0, (long long )0);
      __CrestApply2(29064, 12, (long long )(lexleft == 0));
# 687 "dfa.c"
      if (lexleft == 0) {
        __CrestBranch(29067, 4548, 1);
        __CrestLoad(29069, (unsigned long )0, (long long )5);
# 687 "dfa.c"
        tmp___45 = dcgettext((char const *)((void *)0), "Unbalanced [", 5);
        __CrestClearStack(29070);
        {
        __CrestLoad(29073, (unsigned long )(& tmp___45), (long long )((unsigned long )tmp___45));
        __CrestLoad(29072, (unsigned long )0, (long long )((unsigned long )((char *)0)));
        __CrestApply2(29071, 13, (long long )((unsigned long )tmp___45 != (unsigned long )((char *)0)));
# 687 "dfa.c"
        if ((unsigned long )tmp___45 != (unsigned long )((char *)0)) {
          __CrestBranch(29074, 4550, 1);
          __CrestLoad(29076, (unsigned long )0, (long long )5);
# 687 "dfa.c"
          tmp___44 = dcgettext((char const *)((void *)0), "Unbalanced [", 5);
          __CrestClearStack(29077);
# 687 "dfa.c"
          dfaerror((char const *)tmp___44);
          __CrestClearStack(29078);
        } else {
          __CrestBranch(29075, 4551, 0);
          __CrestLoad(29079, (unsigned long )0, (long long )((token )-1));
          __CrestStore(29080, (unsigned long )(& lasttok));
# 687 "dfa.c"
          lasttok = (token )-1;
          __CrestLoad(29081, (unsigned long )(& lasttok), (long long )lasttok);
          __CrestStore(29082, (unsigned long )(& __retres88));
# 687 "dfa.c"
          __retres88 = lasttok;
# 687 "dfa.c"
          goto return_label;
        }
        }
      } else {
        __CrestBranch(29068, 4554, 0);

      }
      }
# 687 "dfa.c"
      tmp___46 = lexptr;
# 687 "dfa.c"
      lexptr ++;
      __CrestLoad(29083, (unsigned long )tmp___46, (long long )*tmp___46);
      __CrestStore(29084, (unsigned long )(& c1));
# 687 "dfa.c"
      c1 = (token )((unsigned char )*tmp___46);
      __CrestLoad(29087, (unsigned long )(& lexleft), (long long )lexleft);
      __CrestLoad(29086, (unsigned long )0, (long long )1);
      __CrestApply2(29085, 1, (long long )(lexleft - 1));
      __CrestStore(29088, (unsigned long )(& lexleft));
# 687 "dfa.c"
      lexleft --;
      {
      __CrestLoad(29091, (unsigned long )(& c1), (long long )c1);
      __CrestLoad(29090, (unsigned long )0, (long long )45);
      __CrestApply2(29089, 12, (long long )((int )c1 == 45));
# 688 "dfa.c"
      if ((int )c1 == 45) {
        __CrestBranch(29092, 4557, 1);
        {
        __CrestLoad(29096, (unsigned long )(& lexleft), (long long )lexleft);
        __CrestLoad(29095, (unsigned long )0, (long long )0);
        __CrestApply2(29094, 12, (long long )(lexleft == 0));
# 690 "dfa.c"
        if (lexleft == 0) {
          __CrestBranch(29097, 4558, 1);
          __CrestLoad(29099, (unsigned long )0, (long long )5);
# 690 "dfa.c"
          tmp___48 = dcgettext((char const *)((void *)0), "Unbalanced [", 5);
          __CrestClearStack(29100);
          {
          __CrestLoad(29103, (unsigned long )(& tmp___48), (long long )((unsigned long )tmp___48));
          __CrestLoad(29102, (unsigned long )0, (long long )((unsigned long )((char *)0)));
          __CrestApply2(29101, 13, (long long )((unsigned long )tmp___48 != (unsigned long )((char *)0)));
# 690 "dfa.c"
          if ((unsigned long )tmp___48 != (unsigned long )((char *)0)) {
            __CrestBranch(29104, 4560, 1);
            __CrestLoad(29106, (unsigned long )0, (long long )5);
# 690 "dfa.c"
            tmp___47 = dcgettext((char const *)((void *)0), "Unbalanced [", 5);
            __CrestClearStack(29107);
# 690 "dfa.c"
            dfaerror((char const *)tmp___47);
            __CrestClearStack(29108);
          } else {
            __CrestBranch(29105, 4561, 0);
            __CrestLoad(29109, (unsigned long )0, (long long )((token )-1));
            __CrestStore(29110, (unsigned long )(& lasttok));
# 690 "dfa.c"
            lasttok = (token )-1;
            __CrestLoad(29111, (unsigned long )(& lasttok), (long long )lasttok);
            __CrestStore(29112, (unsigned long )(& __retres88));
# 690 "dfa.c"
            __retres88 = lasttok;
# 690 "dfa.c"
            goto return_label;
          }
          }
        } else {
          __CrestBranch(29098, 4564, 0);

        }
        }
# 690 "dfa.c"
        tmp___49 = lexptr;
# 690 "dfa.c"
        lexptr ++;
        __CrestLoad(29113, (unsigned long )tmp___49, (long long )*tmp___49);
        __CrestStore(29114, (unsigned long )(& c2));
# 690 "dfa.c"
        c2 = (token )((unsigned char )*tmp___49);
        __CrestLoad(29117, (unsigned long )(& lexleft), (long long )lexleft);
        __CrestLoad(29116, (unsigned long )0, (long long )1);
        __CrestApply2(29115, 1, (long long )(lexleft - 1));
        __CrestStore(29118, (unsigned long )(& lexleft));
# 690 "dfa.c"
        lexleft --;
        {
        __CrestLoad(29121, (unsigned long )(& c2), (long long )c2);
        __CrestLoad(29120, (unsigned long )0, (long long )93);
        __CrestApply2(29119, 12, (long long )((int )c2 == 93));
# 691 "dfa.c"
        if ((int )c2 == 93) {
          __CrestBranch(29122, 4567, 1);
# 695 "dfa.c"
          lexptr --;
          __CrestLoad(29126, (unsigned long )(& lexleft), (long long )lexleft);
          __CrestLoad(29125, (unsigned long )0, (long long )1);
          __CrestApply2(29124, 0, (long long )(lexleft + 1));
          __CrestStore(29127, (unsigned long )(& lexleft));
# 696 "dfa.c"
          lexleft ++;
          __CrestLoad(29128, (unsigned long )(& c), (long long )c);
          __CrestStore(29129, (unsigned long )(& c2));
# 697 "dfa.c"
          c2 = c;
        } else {
          __CrestBranch(29123, 4568, 0);
          {
          __CrestLoad(29132, (unsigned long )(& c2), (long long )c2);
          __CrestLoad(29131, (unsigned long )0, (long long )92);
          __CrestApply2(29130, 12, (long long )((int )c2 == 92));
# 701 "dfa.c"
          if ((int )c2 == 92) {
            __CrestBranch(29133, 4569, 1);
            {
            __CrestLoad(29139, (unsigned long )(& syntax_bits), (long long )syntax_bits);
            __CrestLoad(29138, (unsigned long )0, (long long )1UL);
            __CrestApply2(29137, 5, (long long )(syntax_bits & 1UL));
            __CrestLoad(29136, (unsigned long )0, (long long )0);
            __CrestApply2(29135, 13, (long long )((syntax_bits & 1UL) != 0));
# 701 "dfa.c"
            if ((syntax_bits & 1UL) != 0) {
              __CrestBranch(29140, 4570, 1);
              {
              __CrestLoad(29144, (unsigned long )(& lexleft), (long long )lexleft);
              __CrestLoad(29143, (unsigned long )0, (long long )0);
              __CrestApply2(29142, 12, (long long )(lexleft == 0));
# 703 "dfa.c"
              if (lexleft == 0) {
                __CrestBranch(29145, 4571, 1);
                __CrestLoad(29147, (unsigned long )0, (long long )5);
# 703 "dfa.c"
                tmp___51 = dcgettext((char const *)((void *)0), "Unbalanced [",
                                     5);
                __CrestClearStack(29148);
                {
                __CrestLoad(29151, (unsigned long )(& tmp___51), (long long )((unsigned long )tmp___51));
                __CrestLoad(29150, (unsigned long )0, (long long )((unsigned long )((char *)0)));
                __CrestApply2(29149, 13, (long long )((unsigned long )tmp___51 != (unsigned long )((char *)0)));
# 703 "dfa.c"
                if ((unsigned long )tmp___51 != (unsigned long )((char *)0)) {
                  __CrestBranch(29152, 4573, 1);
                  __CrestLoad(29154, (unsigned long )0, (long long )5);
# 703 "dfa.c"
                  tmp___50 = dcgettext((char const *)((void *)0), "Unbalanced [",
                                       5);
                  __CrestClearStack(29155);
# 703 "dfa.c"
                  dfaerror((char const *)tmp___50);
                  __CrestClearStack(29156);
                } else {
                  __CrestBranch(29153, 4574, 0);
                  __CrestLoad(29157, (unsigned long )0, (long long )((token )-1));
                  __CrestStore(29158, (unsigned long )(& lasttok));
# 703 "dfa.c"
                  lasttok = (token )-1;
                  __CrestLoad(29159, (unsigned long )(& lasttok), (long long )lasttok);
                  __CrestStore(29160, (unsigned long )(& __retres88));
# 703 "dfa.c"
                  __retres88 = lasttok;
# 703 "dfa.c"
                  goto return_label;
                }
                }
              } else {
                __CrestBranch(29146, 4577, 0);

              }
              }
# 703 "dfa.c"
              tmp___52 = lexptr;
# 703 "dfa.c"
              lexptr ++;
              __CrestLoad(29161, (unsigned long )tmp___52, (long long )*tmp___52);
              __CrestStore(29162, (unsigned long )(& c2));
# 703 "dfa.c"
              c2 = (token )((unsigned char )*tmp___52);
              __CrestLoad(29165, (unsigned long )(& lexleft), (long long )lexleft);
              __CrestLoad(29164, (unsigned long )0, (long long )1);
              __CrestApply2(29163, 1, (long long )(lexleft - 1));
              __CrestStore(29166, (unsigned long )(& lexleft));
# 703 "dfa.c"
              lexleft --;
            } else {
              __CrestBranch(29141, 4579, 0);

            }
            }
          } else {
            __CrestBranch(29134, 4580, 0);

          }
          }
          {
          __CrestLoad(29169, (unsigned long )(& lexleft), (long long )lexleft);
          __CrestLoad(29168, (unsigned long )0, (long long )0);
          __CrestApply2(29167, 12, (long long )(lexleft == 0));
# 704 "dfa.c"
          if (lexleft == 0) {
            __CrestBranch(29170, 4582, 1);
            __CrestLoad(29172, (unsigned long )0, (long long )5);
# 704 "dfa.c"
            tmp___54 = dcgettext((char const *)((void *)0), "Unbalanced [", 5);
            __CrestClearStack(29173);
            {
            __CrestLoad(29176, (unsigned long )(& tmp___54), (long long )((unsigned long )tmp___54));
            __CrestLoad(29175, (unsigned long )0, (long long )((unsigned long )((char *)0)));
            __CrestApply2(29174, 13, (long long )((unsigned long )tmp___54 != (unsigned long )((char *)0)));
# 704 "dfa.c"
            if ((unsigned long )tmp___54 != (unsigned long )((char *)0)) {
              __CrestBranch(29177, 4584, 1);
              __CrestLoad(29179, (unsigned long )0, (long long )5);
# 704 "dfa.c"
              tmp___53 = dcgettext((char const *)((void *)0), "Unbalanced [", 5);
              __CrestClearStack(29180);
# 704 "dfa.c"
              dfaerror((char const *)tmp___53);
              __CrestClearStack(29181);
            } else {
              __CrestBranch(29178, 4585, 0);
              __CrestLoad(29182, (unsigned long )0, (long long )((token )-1));
              __CrestStore(29183, (unsigned long )(& lasttok));
# 704 "dfa.c"
              lasttok = (token )-1;
              __CrestLoad(29184, (unsigned long )(& lasttok), (long long )lasttok);
              __CrestStore(29185, (unsigned long )(& __retres88));
# 704 "dfa.c"
              __retres88 = lasttok;
# 704 "dfa.c"
              goto return_label;
            }
            }
          } else {
            __CrestBranch(29171, 4588, 0);

          }
          }
# 704 "dfa.c"
          tmp___55 = lexptr;
# 704 "dfa.c"
          lexptr ++;
          __CrestLoad(29186, (unsigned long )tmp___55, (long long )*tmp___55);
          __CrestStore(29187, (unsigned long )(& c1));
# 704 "dfa.c"
          c1 = (token )((unsigned char )*tmp___55);
          __CrestLoad(29190, (unsigned long )(& lexleft), (long long )lexleft);
          __CrestLoad(29189, (unsigned long )0, (long long )1);
          __CrestApply2(29188, 1, (long long )(lexleft - 1));
          __CrestStore(29191, (unsigned long )(& lexleft));
# 704 "dfa.c"
          lexleft --;
        }
        }
      } else {
        __CrestBranch(29093, 4590, 0);
        __CrestLoad(29192, (unsigned long )(& c), (long long )c);
        __CrestStore(29193, (unsigned long )(& c2));
# 708 "dfa.c"
        c2 = c;
      }
      }
      {
# 709 "dfa.c"
      while (1) {
        while_continue___6: ;
        {
        __CrestLoad(29196, (unsigned long )(& c), (long long )c);
        __CrestLoad(29195, (unsigned long )(& c2), (long long )c2);
        __CrestApply2(29194, 15, (long long )((int )c <= (int )c2));
# 709 "dfa.c"
        if ((int )c <= (int )c2) {
          __CrestBranch(29197, 4595, 1);

        } else {
          __CrestBranch(29198, 4596, 0);
# 709 "dfa.c"
          goto while_break___6;
        }
        }
        __CrestLoad(29199, (unsigned long )(& c), (long long )c);
# 711 "dfa.c"
        setbit((int )c, ccl);
        __CrestClearStack(29200);
        {
        __CrestLoad(29203, (unsigned long )(& case_fold), (long long )case_fold);
        __CrestLoad(29202, (unsigned long )0, (long long )0);
        __CrestApply2(29201, 13, (long long )(case_fold != 0));
# 712 "dfa.c"
        if (case_fold != 0) {
          __CrestBranch(29204, 4599, 1);
# 713 "dfa.c"
          tmp___59 = __ctype_b_loc();
          __CrestClearStack(29206);
          {
# 713 "dfa.c"
          mem_84 = *tmp___59 + (int )c;
          {
          __CrestLoad(29211, (unsigned long )mem_84, (long long )*mem_84);
          __CrestLoad(29210, (unsigned long )0, (long long )256);
          __CrestApply2(29209, 5, (long long )((int const )*mem_84 & 256));
          __CrestLoad(29208, (unsigned long )0, (long long )0);
          __CrestApply2(29207, 13, (long long )(((int const )*mem_84 & 256) != 0));
# 713 "dfa.c"
          if (((int const )*mem_84 & 256) != 0) {
            __CrestBranch(29212, 4603, 1);
            __CrestLoad(29214, (unsigned long )(& c), (long long )c);
# 714 "dfa.c"
            tmp___56 = tolower((int )c);
            __CrestHandleReturn(29216, (long long )tmp___56);
            __CrestStore(29215, (unsigned long )(& tmp___56));
            __CrestLoad(29217, (unsigned long )(& tmp___56), (long long )tmp___56);
# 714 "dfa.c"
            setbit(tmp___56, ccl);
            __CrestClearStack(29218);
          } else {
            __CrestBranch(29213, 4604, 0);
# 715 "dfa.c"
            tmp___58 = __ctype_b_loc();
            __CrestClearStack(29219);
            {
# 715 "dfa.c"
            mem_85 = *tmp___58 + (int )c;
            {
            __CrestLoad(29224, (unsigned long )mem_85, (long long )*mem_85);
            __CrestLoad(29223, (unsigned long )0, (long long )512);
            __CrestApply2(29222, 5, (long long )((int const )*mem_85 & 512));
            __CrestLoad(29221, (unsigned long )0, (long long )0);
            __CrestApply2(29220, 13, (long long )(((int const )*mem_85 & 512) != 0));
# 715 "dfa.c"
            if (((int const )*mem_85 & 512) != 0) {
              __CrestBranch(29225, 4608, 1);
              __CrestLoad(29227, (unsigned long )(& c), (long long )c);
# 716 "dfa.c"
              tmp___57 = toupper((int )c);
              __CrestHandleReturn(29229, (long long )tmp___57);
              __CrestStore(29228, (unsigned long )(& tmp___57));
              __CrestLoad(29230, (unsigned long )(& tmp___57), (long long )tmp___57);
# 716 "dfa.c"
              setbit(tmp___57, ccl);
              __CrestClearStack(29231);
            } else {
              __CrestBranch(29226, 4609, 0);

            }
            }
            }
          }
          }
          }
        } else {
          __CrestBranch(29205, 4610, 0);

        }
        }
        __CrestLoad(29234, (unsigned long )(& c), (long long )c);
        __CrestLoad(29233, (unsigned long )0, (long long )1);
        __CrestApply2(29232, 0, (long long )((int )c + 1));
        __CrestStore(29235, (unsigned long )(& c));
# 717 "dfa.c"
        c = (token )((int )c + 1);
      }
      while_break___6: ;
      }
      skip:
      __CrestLoad(29236, (unsigned long )(& c1), (long long )c1);
      __CrestStore(29237, (unsigned long )(& c));
# 660 "dfa.c"
      c = c1;
      {
      __CrestLoad(29240, (unsigned long )(& c), (long long )c);
      __CrestLoad(29239, (unsigned long )0, (long long )93);
      __CrestApply2(29238, 13, (long long )((int )c != 93));
# 660 "dfa.c"
      if ((int )c != 93) {
        __CrestBranch(29241, 4615, 1);

      } else {
        __CrestBranch(29242, 4616, 0);
# 660 "dfa.c"
        goto while_break___3;
      }
      }
    }
    while_break___3: ;
    }
    {
    __CrestLoad(29245, (unsigned long )(& invert), (long long )invert);
    __CrestLoad(29244, (unsigned long )0, (long long )0);
    __CrestApply2(29243, 13, (long long )(invert != 0));
# 723 "dfa.c"
    if (invert != 0) {
      __CrestBranch(29246, 4619, 1);
# 725 "dfa.c"
      notset(ccl);
      __CrestClearStack(29248);
      {
      __CrestLoad(29253, (unsigned long )(& syntax_bits), (long long )syntax_bits);
      __CrestLoad(29252, (unsigned long )0, (long long )((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1));
      __CrestApply2(29251, 5, (long long )(syntax_bits & ((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1)));
      __CrestLoad(29250, (unsigned long )0, (long long )0);
      __CrestApply2(29249, 13, (long long )((syntax_bits & ((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1)) != 0));
# 726 "dfa.c"
      if ((syntax_bits & ((((((((1UL << 1) << 1) << 1) << 1) << 1) << 1) << 1) << 1)) != 0) {
        __CrestBranch(29254, 4621, 1);
        __CrestLoad(29256, (unsigned long )0, (long long )'\n');
# 727 "dfa.c"
        clrbit('\n', ccl);
        __CrestClearStack(29257);
      } else {
        __CrestBranch(29255, 4622, 0);

      }
      }
    } else {
      __CrestBranch(29247, 4623, 0);

    }
    }
    __CrestLoad(29258, (unsigned long )0, (long long )0);
    __CrestStore(29259, (unsigned long )(& laststart));
# 729 "dfa.c"
    laststart = 0;
# 730 "dfa.c"
    tmp___60 = charclass_index(ccl);
    __CrestHandleReturn(29261, (long long )tmp___60);
    __CrestStore(29260, (unsigned long )(& tmp___60));
    __CrestLoad(29264, (unsigned long )0, (long long )273);
    __CrestLoad(29263, (unsigned long )(& tmp___60), (long long )tmp___60);
    __CrestApply2(29262, 0, (long long )(273 + tmp___60));
    __CrestStore(29265, (unsigned long )(& lasttok));
# 730 "dfa.c"
    lasttok = (token )(273 + tmp___60);
    __CrestLoad(29266, (unsigned long )(& lasttok), (long long )lasttok);
    __CrestStore(29267, (unsigned long )(& __retres88));
# 730 "dfa.c"
    __retres88 = lasttok;
# 730 "dfa.c"
    goto return_label;
    normal_char:
    switch_default:
    __CrestLoad(29268, (unsigned long )0, (long long )0);
    __CrestStore(29269, (unsigned long )(& laststart));
# 734 "dfa.c"
    laststart = 0;
    {
    __CrestLoad(29272, (unsigned long )(& case_fold), (long long )case_fold);
    __CrestLoad(29271, (unsigned long )0, (long long )0);
    __CrestApply2(29270, 13, (long long )(case_fold != 0));
# 735 "dfa.c"
    if (case_fold != 0) {
      __CrestBranch(29273, 4629, 1);
# 735 "dfa.c"
      tmp___65 = __ctype_b_loc();
      __CrestClearStack(29275);
      {
# 735 "dfa.c"
      mem_86 = *tmp___65 + (int )c;
      {
      __CrestLoad(29280, (unsigned long )mem_86, (long long )*mem_86);
      __CrestLoad(29279, (unsigned long )0, (long long )1024);
      __CrestApply2(29278, 5, (long long )((int const )*mem_86 & 1024));
      __CrestLoad(29277, (unsigned long )0, (long long )0);
      __CrestApply2(29276, 13, (long long )(((int const )*mem_86 & 1024) != 0));
# 735 "dfa.c"
      if (((int const )*mem_86 & 1024) != 0) {
        __CrestBranch(29281, 4633, 1);
# 737 "dfa.c"
        zeroset(ccl);
        __CrestClearStack(29283);
        __CrestLoad(29284, (unsigned long )(& c), (long long )c);
# 738 "dfa.c"
        setbit((int )c, ccl);
        __CrestClearStack(29285);
# 739 "dfa.c"
        tmp___63 = __ctype_b_loc();
        __CrestClearStack(29286);
        {
# 739 "dfa.c"
        mem_87 = *tmp___63 + (int )c;
        {
        __CrestLoad(29291, (unsigned long )mem_87, (long long )*mem_87);
        __CrestLoad(29290, (unsigned long )0, (long long )256);
        __CrestApply2(29289, 5, (long long )((int const )*mem_87 & 256));
        __CrestLoad(29288, (unsigned long )0, (long long )0);
        __CrestApply2(29287, 13, (long long )(((int const )*mem_87 & 256) != 0));
# 739 "dfa.c"
        if (((int const )*mem_87 & 256) != 0) {
          __CrestBranch(29292, 4637, 1);
          __CrestLoad(29294, (unsigned long )(& c), (long long )c);
# 740 "dfa.c"
          tmp___61 = tolower((int )c);
          __CrestHandleReturn(29296, (long long )tmp___61);
          __CrestStore(29295, (unsigned long )(& tmp___61));
          __CrestLoad(29297, (unsigned long )(& tmp___61), (long long )tmp___61);
# 740 "dfa.c"
          setbit(tmp___61, ccl);
          __CrestClearStack(29298);
        } else {
          __CrestBranch(29293, 4638, 0);
          __CrestLoad(29299, (unsigned long )(& c), (long long )c);
# 742 "dfa.c"
          tmp___62 = toupper((int )c);
          __CrestHandleReturn(29301, (long long )tmp___62);
          __CrestStore(29300, (unsigned long )(& tmp___62));
          __CrestLoad(29302, (unsigned long )(& tmp___62), (long long )tmp___62);
# 742 "dfa.c"
          setbit(tmp___62, ccl);
          __CrestClearStack(29303);
        }
        }
        }
# 743 "dfa.c"
        tmp___64 = charclass_index(ccl);
        __CrestHandleReturn(29305, (long long )tmp___64);
        __CrestStore(29304, (unsigned long )(& tmp___64));
        __CrestLoad(29308, (unsigned long )0, (long long )273);
        __CrestLoad(29307, (unsigned long )(& tmp___64), (long long )tmp___64);
        __CrestApply2(29306, 0, (long long )(273 + tmp___64));
        __CrestStore(29309, (unsigned long )(& lasttok));
# 743 "dfa.c"
        lasttok = (token )(273 + tmp___64);
        __CrestLoad(29310, (unsigned long )(& lasttok), (long long )lasttok);
        __CrestStore(29311, (unsigned long )(& __retres88));
# 743 "dfa.c"
        __retres88 = lasttok;
# 743 "dfa.c"
        goto return_label;
      } else {
        __CrestBranch(29282, 4642, 0);

      }
      }
      }
    } else {
      __CrestBranch(29274, 4643, 0);

    }
    }
    __CrestLoad(29312, (unsigned long )(& c), (long long )c);
    __CrestStore(29313, (unsigned long )(& __retres88));
# 745 "dfa.c"
    __retres88 = c;
# 745 "dfa.c"
    goto return_label;
    switch_break: ;
    }
    __CrestLoad(29316, (unsigned long )(& i), (long long )i);
    __CrestLoad(29315, (unsigned long )0, (long long )1);
    __CrestApply2(29314, 0, (long long )(i + 1));
    __CrestStore(29317, (unsigned long )(& i));
# 435 "dfa.c"
    i ++;
  }
  while_break: ;
  }
# 751 "dfa.c"
  abort();
  __CrestClearStack(29318);
  __CrestLoad(29319, (unsigned long )0, (long long )((token )-1));
  __CrestStore(29320, (unsigned long )(& __retres88));
# 752 "dfa.c"
  __retres88 = (token )-1;
  return_label:
  {
  __CrestLoad(29321, (unsigned long )(& __retres88), (long long )__retres88);
  __CrestReturn(29322);
# 421 "dfa.c"
  return (__retres88);
  }
}
}
# 757 "dfa.c"
static token tok ;
# 758 "dfa.c"
static int depth ;
# 766 "dfa.c"
static void addtok(token t )
{
  ptr_t tmp ;
  int tmp___0 ;
  token *mem_4 ;

  {
  __CrestCall(29324, 238);
  __CrestStore(29323, (unsigned long )(& t));
  {
  __CrestLoad(29327, (unsigned long )(& dfa->tindex), (long long )dfa->tindex);
  __CrestLoad(29326, (unsigned long )(& dfa->talloc), (long long )dfa->talloc);
  __CrestApply2(29325, 17, (long long )(dfa->tindex >= dfa->talloc));
# 770 "dfa.c"
  if (dfa->tindex >= dfa->talloc) {
    __CrestBranch(29328, 4653, 1);
    {
# 770 "dfa.c"
    while (1) {
      while_continue: ;
      {
      __CrestLoad(29332, (unsigned long )(& dfa->tindex), (long long )dfa->tindex);
      __CrestLoad(29331, (unsigned long )(& dfa->talloc), (long long )dfa->talloc);
      __CrestApply2(29330, 17, (long long )(dfa->tindex >= dfa->talloc));
# 770 "dfa.c"
      if (dfa->tindex >= dfa->talloc) {
        __CrestBranch(29333, 4657, 1);

      } else {
        __CrestBranch(29334, 4658, 0);
# 770 "dfa.c"
        goto while_break;
      }
      }
      __CrestLoad(29337, (unsigned long )(& dfa->talloc), (long long )dfa->talloc);
      __CrestLoad(29336, (unsigned long )0, (long long )2);
      __CrestApply2(29335, 2, (long long )(dfa->talloc * 2));
      __CrestStore(29338, (unsigned long )(& dfa->talloc));
# 770 "dfa.c"
      dfa->talloc *= 2;
    }
    while_break: ;
    }
    __CrestLoad(29341, (unsigned long )(& dfa->talloc), (long long )dfa->talloc);
    __CrestLoad(29340, (unsigned long )0, (long long )sizeof(token ));
    __CrestApply2(29339, 2, (long long )((unsigned long )dfa->talloc * sizeof(token )));
# 770 "dfa.c"
    tmp = xrealloc((ptr_t )dfa->tokens, (unsigned long )dfa->talloc * sizeof(token ));
    __CrestClearStack(29342);
# 770 "dfa.c"
    dfa->tokens = (token *)tmp;
  } else {
    __CrestBranch(29329, 4662, 0);

  }
  }
  __CrestLoad(29343, (unsigned long )(& dfa->tindex), (long long )dfa->tindex);
  __CrestStore(29344, (unsigned long )(& tmp___0));
# 771 "dfa.c"
  tmp___0 = dfa->tindex;
  __CrestLoad(29347, (unsigned long )(& dfa->tindex), (long long )dfa->tindex);
  __CrestLoad(29346, (unsigned long )0, (long long )1);
  __CrestApply2(29345, 0, (long long )(dfa->tindex + 1));
  __CrestStore(29348, (unsigned long )(& dfa->tindex));
# 771 "dfa.c"
  (dfa->tindex) ++;
# 771 "dfa.c"
  mem_4 = dfa->tokens + tmp___0;
  __CrestLoad(29349, (unsigned long )(& t), (long long )t);
  __CrestStore(29350, (unsigned long )mem_4);
# 771 "dfa.c"
  *mem_4 = t;
  {
  {
  __CrestLoad(29353, (unsigned long )(& t), (long long )t);
  __CrestLoad(29352, (unsigned long )0, (long long )266);
  __CrestApply2(29351, 12, (long long )((int )t == 266));
# 777 "dfa.c"
  if ((int )t == 266) {
    __CrestBranch(29354, 4666, 1);
# 777 "dfa.c"
    goto case_266;
  } else {
    __CrestBranch(29355, 4667, 0);

  }
  }
  {
  __CrestLoad(29358, (unsigned long )(& t), (long long )t);
  __CrestLoad(29357, (unsigned long )0, (long long )265);
  __CrestApply2(29356, 12, (long long )((int )t == 265));
# 777 "dfa.c"
  if ((int )t == 265) {
    __CrestBranch(29359, 4669, 1);
# 777 "dfa.c"
    goto case_266;
  } else {
    __CrestBranch(29360, 4670, 0);

  }
  }
  {
  __CrestLoad(29363, (unsigned long )(& t), (long long )t);
  __CrestLoad(29362, (unsigned long )0, (long long )264);
  __CrestApply2(29361, 12, (long long )((int )t == 264));
# 777 "dfa.c"
  if ((int )t == 264) {
    __CrestBranch(29364, 4672, 1);
# 777 "dfa.c"
    goto case_266;
  } else {
    __CrestBranch(29365, 4673, 0);

  }
  }
  {
  __CrestLoad(29368, (unsigned long )(& t), (long long )t);
  __CrestLoad(29367, (unsigned long )0, (long long )270);
  __CrestApply2(29366, 12, (long long )((int )t == 270));
# 782 "dfa.c"
  if ((int )t == 270) {
    __CrestBranch(29369, 4675, 1);
# 782 "dfa.c"
    goto case_270;
  } else {
    __CrestBranch(29370, 4676, 0);

  }
  }
  {
  __CrestLoad(29373, (unsigned long )(& t), (long long )t);
  __CrestLoad(29372, (unsigned long )0, (long long )269);
  __CrestApply2(29371, 12, (long long )((int )t == 269));
# 782 "dfa.c"
  if ((int )t == 269) {
    __CrestBranch(29374, 4678, 1);
# 782 "dfa.c"
    goto case_270;
  } else {
    __CrestBranch(29375, 4679, 0);

  }
  }
  {
  __CrestLoad(29378, (unsigned long )(& t), (long long )t);
  __CrestLoad(29377, (unsigned long )0, (long long )268);
  __CrestApply2(29376, 12, (long long )((int )t == 268));
# 782 "dfa.c"
  if ((int )t == 268) {
    __CrestBranch(29379, 4681, 1);
# 782 "dfa.c"
    goto case_270;
  } else {
    __CrestBranch(29380, 4682, 0);

  }
  }
  {
  __CrestLoad(29383, (unsigned long )(& t), (long long )t);
  __CrestLoad(29382, (unsigned long )0, (long long )256);
  __CrestApply2(29381, 12, (long long )((int )t == 256));
# 788 "dfa.c"
  if ((int )t == 256) {
    __CrestBranch(29384, 4684, 1);
# 788 "dfa.c"
    goto case_256;
  } else {
    __CrestBranch(29385, 4685, 0);

  }
  }
# 786 "dfa.c"
  goto switch_default;
  case_266:
  case_265:
  case_264:
# 778 "dfa.c"
  goto switch_break;
  case_270:
  case_269:
  case_268:
  __CrestLoad(29388, (unsigned long )(& depth), (long long )depth);
  __CrestLoad(29387, (unsigned long )0, (long long )1);
  __CrestApply2(29386, 1, (long long )(depth - 1));
  __CrestStore(29389, (unsigned long )(& depth));
# 783 "dfa.c"
  depth --;
# 784 "dfa.c"
  goto switch_break;
  switch_default:
  __CrestLoad(29392, (unsigned long )(& dfa->nleaves), (long long )dfa->nleaves);
  __CrestLoad(29391, (unsigned long )0, (long long )1);
  __CrestApply2(29390, 0, (long long )(dfa->nleaves + 1));
  __CrestStore(29393, (unsigned long )(& dfa->nleaves));
# 787 "dfa.c"
  (dfa->nleaves) ++;
  case_256:
  __CrestLoad(29396, (unsigned long )(& depth), (long long )depth);
  __CrestLoad(29395, (unsigned long )0, (long long )1);
  __CrestApply2(29394, 0, (long long )(depth + 1));
  __CrestStore(29397, (unsigned long )(& depth));
# 789 "dfa.c"
  depth ++;
# 790 "dfa.c"
  goto switch_break;
  switch_break: ;
  }
  {
  __CrestLoad(29400, (unsigned long )(& depth), (long long )depth);
  __CrestLoad(29399, (unsigned long )(& dfa->depth), (long long )dfa->depth);
  __CrestApply2(29398, 14, (long long )(depth > dfa->depth));
# 792 "dfa.c"
  if (depth > dfa->depth) {
    __CrestBranch(29401, 4695, 1);
    __CrestLoad(29403, (unsigned long )(& depth), (long long )depth);
    __CrestStore(29404, (unsigned long )(& dfa->depth));
# 793 "dfa.c"
    dfa->depth = depth;
  } else {
    __CrestBranch(29402, 4696, 0);

  }
  }

  {
  __CrestReturn(29405);
# 766 "dfa.c"
  return;
  }
}
}
# 826 "dfa.c"
static void atom(void)
{
  char *tmp ;

  {
  __CrestCall(29406, 239);

  {
  __CrestLoad(29409, (unsigned long )(& tok), (long long )tok);
  __CrestLoad(29408, (unsigned long )0, (long long )0);
  __CrestApply2(29407, 17, (long long )((int )tok >= 0));
# 829 "dfa.c"
  if ((int )tok >= 0) {
    __CrestBranch(29410, 4700, 1);
    {
    __CrestLoad(29414, (unsigned long )(& tok), (long long )tok);
    __CrestLoad(29413, (unsigned long )0, (long long )(1 << 8));
    __CrestApply2(29412, 16, (long long )((int )tok < 1 << 8));
# 829 "dfa.c"
    if ((int )tok < 1 << 8) {
      __CrestBranch(29415, 4701, 1);
      __CrestLoad(29417, (unsigned long )(& tok), (long long )tok);
# 833 "dfa.c"
      addtok(tok);
      __CrestClearStack(29418);
# 834 "dfa.c"
      tok = lex();
      __CrestHandleReturn(29420, (long long )tok);
      __CrestStore(29419, (unsigned long )(& tok));
    } else {
      __CrestBranch(29416, 4702, 0);
# 829 "dfa.c"
      goto _L;
    }
    }
  } else {
    __CrestBranch(29411, 4703, 0);
    _L:
    {
    __CrestLoad(29423, (unsigned long )(& tok), (long long )tok);
    __CrestLoad(29422, (unsigned long )0, (long long )273);
    __CrestApply2(29421, 17, (long long )((int )tok >= 273));
# 829 "dfa.c"
    if ((int )tok >= 273) {
      __CrestBranch(29424, 4704, 1);
      __CrestLoad(29426, (unsigned long )(& tok), (long long )tok);
# 833 "dfa.c"
      addtok(tok);
      __CrestClearStack(29427);
# 834 "dfa.c"
      tok = lex();
      __CrestHandleReturn(29429, (long long )tok);
      __CrestStore(29428, (unsigned long )(& tok));
    } else {
      __CrestBranch(29425, 4705, 0);
      {
      __CrestLoad(29432, (unsigned long )(& tok), (long long )tok);
      __CrestLoad(29431, (unsigned long )0, (long long )257);
      __CrestApply2(29430, 12, (long long )((int )tok == 257));
# 829 "dfa.c"
      if ((int )tok == 257) {
        __CrestBranch(29433, 4706, 1);
        __CrestLoad(29435, (unsigned long )(& tok), (long long )tok);
# 833 "dfa.c"
        addtok(tok);
        __CrestClearStack(29436);
# 834 "dfa.c"
        tok = lex();
        __CrestHandleReturn(29438, (long long )tok);
        __CrestStore(29437, (unsigned long )(& tok));
      } else {
        __CrestBranch(29434, 4707, 0);
        {
        __CrestLoad(29441, (unsigned long )(& tok), (long long )tok);
        __CrestLoad(29440, (unsigned long )0, (long long )258);
        __CrestApply2(29439, 12, (long long )((int )tok == 258));
# 829 "dfa.c"
        if ((int )tok == 258) {
          __CrestBranch(29442, 4708, 1);
          __CrestLoad(29444, (unsigned long )(& tok), (long long )tok);
# 833 "dfa.c"
          addtok(tok);
          __CrestClearStack(29445);
# 834 "dfa.c"
          tok = lex();
          __CrestHandleReturn(29447, (long long )tok);
          __CrestStore(29446, (unsigned long )(& tok));
        } else {
          __CrestBranch(29443, 4709, 0);
          {
          __CrestLoad(29450, (unsigned long )(& tok), (long long )tok);
          __CrestLoad(29449, (unsigned long )0, (long long )259);
          __CrestApply2(29448, 12, (long long )((int )tok == 259));
# 829 "dfa.c"
          if ((int )tok == 259) {
            __CrestBranch(29451, 4710, 1);
            __CrestLoad(29453, (unsigned long )(& tok), (long long )tok);
# 833 "dfa.c"
            addtok(tok);
            __CrestClearStack(29454);
# 834 "dfa.c"
            tok = lex();
            __CrestHandleReturn(29456, (long long )tok);
            __CrestStore(29455, (unsigned long )(& tok));
          } else {
            __CrestBranch(29452, 4711, 0);
            {
            __CrestLoad(29459, (unsigned long )(& tok), (long long )tok);
            __CrestLoad(29458, (unsigned long )0, (long long )260);
            __CrestApply2(29457, 12, (long long )((int )tok == 260));
# 829 "dfa.c"
            if ((int )tok == 260) {
              __CrestBranch(29460, 4712, 1);
              __CrestLoad(29462, (unsigned long )(& tok), (long long )tok);
# 833 "dfa.c"
              addtok(tok);
              __CrestClearStack(29463);
# 834 "dfa.c"
              tok = lex();
              __CrestHandleReturn(29465, (long long )tok);
              __CrestStore(29464, (unsigned long )(& tok));
            } else {
              __CrestBranch(29461, 4713, 0);
              {
              __CrestLoad(29468, (unsigned long )(& tok), (long long )tok);
              __CrestLoad(29467, (unsigned long )0, (long long )261);
              __CrestApply2(29466, 12, (long long )((int )tok == 261));
# 829 "dfa.c"
              if ((int )tok == 261) {
                __CrestBranch(29469, 4714, 1);
                __CrestLoad(29471, (unsigned long )(& tok), (long long )tok);
# 833 "dfa.c"
                addtok(tok);
                __CrestClearStack(29472);
# 834 "dfa.c"
                tok = lex();
                __CrestHandleReturn(29474, (long long )tok);
                __CrestStore(29473, (unsigned long )(& tok));
              } else {
                __CrestBranch(29470, 4715, 0);
                {
                __CrestLoad(29477, (unsigned long )(& tok), (long long )tok);
                __CrestLoad(29476, (unsigned long )0, (long long )262);
                __CrestApply2(29475, 12, (long long )((int )tok == 262));
# 829 "dfa.c"
                if ((int )tok == 262) {
                  __CrestBranch(29478, 4716, 1);
                  __CrestLoad(29480, (unsigned long )(& tok), (long long )tok);
# 833 "dfa.c"
                  addtok(tok);
                  __CrestClearStack(29481);
# 834 "dfa.c"
                  tok = lex();
                  __CrestHandleReturn(29483, (long long )tok);
                  __CrestStore(29482, (unsigned long )(& tok));
                } else {
                  __CrestBranch(29479, 4717, 0);
                  {
                  __CrestLoad(29486, (unsigned long )(& tok), (long long )tok);
                  __CrestLoad(29485, (unsigned long )0, (long long )263);
                  __CrestApply2(29484, 12, (long long )((int )tok == 263));
# 829 "dfa.c"
                  if ((int )tok == 263) {
                    __CrestBranch(29487, 4718, 1);
                    __CrestLoad(29489, (unsigned long )(& tok), (long long )tok);
# 833 "dfa.c"
                    addtok(tok);
                    __CrestClearStack(29490);
# 834 "dfa.c"
                    tok = lex();
                    __CrestHandleReturn(29492, (long long )tok);
                    __CrestStore(29491, (unsigned long )(& tok));
                  } else {
                    __CrestBranch(29488, 4719, 0);
                    {
                    __CrestLoad(29495, (unsigned long )(& tok), (long long )tok);
                    __CrestLoad(29494, (unsigned long )0, (long long )271);
                    __CrestApply2(29493, 12, (long long )((int )tok == 271));
# 836 "dfa.c"
                    if ((int )tok == 271) {
                      __CrestBranch(29496, 4720, 1);
# 838 "dfa.c"
                      tok = lex();
                      __CrestHandleReturn(29499, (long long )tok);
                      __CrestStore(29498, (unsigned long )(& tok));
                      __CrestLoad(29500, (unsigned long )0, (long long )0);
# 839 "dfa.c"
                      regexp(0);
                      __CrestClearStack(29501);
                      {
                      __CrestLoad(29504, (unsigned long )(& tok), (long long )tok);
                      __CrestLoad(29503, (unsigned long )0, (long long )272);
                      __CrestApply2(29502, 13, (long long )((int )tok != 272));
# 840 "dfa.c"
                      if ((int )tok != 272) {
                        __CrestBranch(29505, 4722, 1);
                        __CrestLoad(29507, (unsigned long )0, (long long )5);
# 841 "dfa.c"
                        tmp = dcgettext((char const *)((void *)0), "Unbalanced (",
                                        5);
                        __CrestClearStack(29508);
# 841 "dfa.c"
                        dfaerror((char const *)tmp);
                        __CrestClearStack(29509);
                      } else {
                        __CrestBranch(29506, 4723, 0);

                      }
                      }
# 842 "dfa.c"
                      tok = lex();
                      __CrestHandleReturn(29511, (long long )tok);
                      __CrestStore(29510, (unsigned long )(& tok));
                    } else {
                      __CrestBranch(29497, 4725, 0);
                      __CrestLoad(29512, (unsigned long )0, (long long )((token )256));
# 845 "dfa.c"
                      addtok((token )256);
                      __CrestClearStack(29513);
                    }
                    }
                  }
                  }
                }
                }
              }
              }
            }
            }
          }
          }
        }
        }
      }
      }
    }
    }
  }
  }

  {
  __CrestReturn(29514);
# 826 "dfa.c"
  return;
  }
}
}
# 849 "dfa.c"
static int nsubtoks(int tindex )
{
  int ntoks1 ;
  int tmp ;
  int tmp___0 ;
  token *mem_5 ;
  int __retres6 ;

  {
  __CrestCall(29516, 240);
  __CrestStore(29515, (unsigned long )(& tindex));
  {
# 855 "dfa.c"
  mem_5 = dfa->tokens + (tindex - 1);
  {
  {
  __CrestLoad(29519, (unsigned long )mem_5, (long long )*mem_5);
  __CrestLoad(29518, (unsigned long )0, (long long )266);
  __CrestApply2(29517, 12, (long long )((int )*mem_5 == 266));
# 861 "dfa.c"
  if ((int )*mem_5 == 266) {
    __CrestBranch(29520, 4732, 1);
# 861 "dfa.c"
    goto case_266;
  } else {
    __CrestBranch(29521, 4733, 0);

  }
  }
  {
  __CrestLoad(29524, (unsigned long )mem_5, (long long )*mem_5);
  __CrestLoad(29523, (unsigned long )0, (long long )265);
  __CrestApply2(29522, 12, (long long )((int )*mem_5 == 265));
# 861 "dfa.c"
  if ((int )*mem_5 == 265) {
    __CrestBranch(29525, 4735, 1);
# 861 "dfa.c"
    goto case_266;
  } else {
    __CrestBranch(29526, 4736, 0);

  }
  }
  {
  __CrestLoad(29529, (unsigned long )mem_5, (long long )*mem_5);
  __CrestLoad(29528, (unsigned long )0, (long long )264);
  __CrestApply2(29527, 12, (long long )((int )*mem_5 == 264));
# 861 "dfa.c"
  if ((int )*mem_5 == 264) {
    __CrestBranch(29530, 4738, 1);
# 861 "dfa.c"
    goto case_266;
  } else {
    __CrestBranch(29531, 4739, 0);

  }
  }
  {
  __CrestLoad(29534, (unsigned long )mem_5, (long long )*mem_5);
  __CrestLoad(29533, (unsigned long )0, (long long )270);
  __CrestApply2(29532, 12, (long long )((int )*mem_5 == 270));
# 865 "dfa.c"
  if ((int )*mem_5 == 270) {
    __CrestBranch(29535, 4741, 1);
# 865 "dfa.c"
    goto case_270;
  } else {
    __CrestBranch(29536, 4742, 0);

  }
  }
  {
  __CrestLoad(29539, (unsigned long )mem_5, (long long )*mem_5);
  __CrestLoad(29538, (unsigned long )0, (long long )269);
  __CrestApply2(29537, 12, (long long )((int )*mem_5 == 269));
# 865 "dfa.c"
  if ((int )*mem_5 == 269) {
    __CrestBranch(29540, 4744, 1);
# 865 "dfa.c"
    goto case_270;
  } else {
    __CrestBranch(29541, 4745, 0);

  }
  }
  {
  __CrestLoad(29544, (unsigned long )mem_5, (long long )*mem_5);
  __CrestLoad(29543, (unsigned long )0, (long long )268);
  __CrestApply2(29542, 12, (long long )((int )*mem_5 == 268));
# 865 "dfa.c"
  if ((int )*mem_5 == 268) {
    __CrestBranch(29545, 4747, 1);
# 865 "dfa.c"
    goto case_270;
  } else {
    __CrestBranch(29546, 4748, 0);

  }
  }
# 857 "dfa.c"
  goto switch_default;
  switch_default:
  __CrestLoad(29547, (unsigned long )0, (long long )1);
  __CrestStore(29548, (unsigned long )(& __retres6));
# 858 "dfa.c"
  __retres6 = 1;
# 858 "dfa.c"
  goto return_label;
  case_266:
  case_265:
  case_264:
  __CrestLoad(29551, (unsigned long )(& tindex), (long long )tindex);
  __CrestLoad(29550, (unsigned long )0, (long long )1);
  __CrestApply2(29549, 1, (long long )(tindex - 1));
# 862 "dfa.c"
  tmp = nsubtoks(tindex - 1);
  __CrestHandleReturn(29553, (long long )tmp);
  __CrestStore(29552, (unsigned long )(& tmp));
  __CrestLoad(29556, (unsigned long )0, (long long )1);
  __CrestLoad(29555, (unsigned long )(& tmp), (long long )tmp);
  __CrestApply2(29554, 0, (long long )(1 + tmp));
  __CrestStore(29557, (unsigned long )(& __retres6));
# 862 "dfa.c"
  __retres6 = 1 + tmp;
# 862 "dfa.c"
  goto return_label;
  case_270:
  case_269:
  case_268:
  __CrestLoad(29560, (unsigned long )(& tindex), (long long )tindex);
  __CrestLoad(29559, (unsigned long )0, (long long )1);
  __CrestApply2(29558, 1, (long long )(tindex - 1));
# 866 "dfa.c"
  ntoks1 = nsubtoks(tindex - 1);
  __CrestHandleReturn(29562, (long long )ntoks1);
  __CrestStore(29561, (unsigned long )(& ntoks1));
  __CrestLoad(29567, (unsigned long )(& tindex), (long long )tindex);
  __CrestLoad(29566, (unsigned long )0, (long long )1);
  __CrestApply2(29565, 1, (long long )(tindex - 1));
  __CrestLoad(29564, (unsigned long )(& ntoks1), (long long )ntoks1);
  __CrestApply2(29563, 1, (long long )((tindex - 1) - ntoks1));
# 867 "dfa.c"
  tmp___0 = nsubtoks((tindex - 1) - ntoks1);
  __CrestHandleReturn(29569, (long long )tmp___0);
  __CrestStore(29568, (unsigned long )(& tmp___0));
  __CrestLoad(29574, (unsigned long )0, (long long )1);
  __CrestLoad(29573, (unsigned long )(& ntoks1), (long long )ntoks1);
  __CrestApply2(29572, 0, (long long )(1 + ntoks1));
  __CrestLoad(29571, (unsigned long )(& tmp___0), (long long )tmp___0);
  __CrestApply2(29570, 0, (long long )((1 + ntoks1) + tmp___0));
  __CrestStore(29575, (unsigned long )(& __retres6));
# 867 "dfa.c"
  __retres6 = (1 + ntoks1) + tmp___0;
# 867 "dfa.c"
  goto return_label;
  switch_break: ;
  }
  }
  return_label:
  {
  __CrestLoad(29576, (unsigned long )(& __retres6), (long long )__retres6);
  __CrestReturn(29577);
# 849 "dfa.c"
  return (__retres6);
  }
}
}
# 872 "dfa.c"
static void copytoks(int tindex , int ntokens )
{
  int i ;
  token *mem_4 ;

  {
  __CrestCall(29580, 241);
  __CrestStore(29579, (unsigned long )(& ntokens));
  __CrestStore(29578, (unsigned long )(& tindex));
  __CrestLoad(29581, (unsigned long )0, (long long )0);
  __CrestStore(29582, (unsigned long )(& i));
# 878 "dfa.c"
  i = 0;
  {
# 878 "dfa.c"
  while (1) {
    while_continue: ;
    {
    __CrestLoad(29585, (unsigned long )(& i), (long long )i);
    __CrestLoad(29584, (unsigned long )(& ntokens), (long long )ntokens);
    __CrestApply2(29583, 16, (long long )(i < ntokens));
# 878 "dfa.c"
    if (i < ntokens) {
      __CrestBranch(29586, 4765, 1);

    } else {
      __CrestBranch(29587, 4766, 0);
# 878 "dfa.c"
      goto while_break;
    }
    }
# 879 "dfa.c"
    mem_4 = dfa->tokens + (tindex + i);
    __CrestLoad(29588, (unsigned long )mem_4, (long long )*mem_4);
# 879 "dfa.c"
    addtok(*mem_4);
    __CrestClearStack(29589);
    __CrestLoad(29592, (unsigned long )(& i), (long long )i);
    __CrestLoad(29591, (unsigned long )0, (long long )1);
    __CrestApply2(29590, 0, (long long )(i + 1));
    __CrestStore(29593, (unsigned long )(& i));
# 878 "dfa.c"
    i ++;
  }
  while_break: ;
  }

  {
  __CrestReturn(29594);
# 872 "dfa.c"
  return;
  }
}
}
# 882 "dfa.c"
static void closure(void)
{
  int tindex ;
  int ntokens ;
  int i ;

  {
  __CrestCall(29595, 242);
# 887 "dfa.c"
  atom();
  __CrestClearStack(29596);
  {
# 888 "dfa.c"
  while (1) {
    while_continue: ;
    {
    __CrestLoad(29599, (unsigned long )(& tok), (long long )tok);
    __CrestLoad(29598, (unsigned long )0, (long long )264);
    __CrestApply2(29597, 12, (long long )((int )tok == 264));
# 888 "dfa.c"
    if ((int )tok == 264) {
      __CrestBranch(29600, 4776, 1);

    } else {
      __CrestBranch(29601, 4777, 0);
      {
      __CrestLoad(29604, (unsigned long )(& tok), (long long )tok);
      __CrestLoad(29603, (unsigned long )0, (long long )265);
      __CrestApply2(29602, 12, (long long )((int )tok == 265));
# 888 "dfa.c"
      if ((int )tok == 265) {
        __CrestBranch(29605, 4778, 1);

      } else {
        __CrestBranch(29606, 4779, 0);
        {
        __CrestLoad(29609, (unsigned long )(& tok), (long long )tok);
        __CrestLoad(29608, (unsigned long )0, (long long )266);
        __CrestApply2(29607, 12, (long long )((int )tok == 266));
# 888 "dfa.c"
        if ((int )tok == 266) {
          __CrestBranch(29610, 4780, 1);

        } else {
          __CrestBranch(29611, 4781, 0);
          {
          __CrestLoad(29614, (unsigned long )(& tok), (long long )tok);
          __CrestLoad(29613, (unsigned long )0, (long long )267);
          __CrestApply2(29612, 12, (long long )((int )tok == 267));
# 888 "dfa.c"
          if ((int )tok == 267) {
            __CrestBranch(29615, 4782, 1);

          } else {
            __CrestBranch(29616, 4783, 0);
# 888 "dfa.c"
            goto while_break;
          }
          }
        }
        }
      }
      }
    }
    }
    {
    __CrestLoad(29619, (unsigned long )(& tok), (long long )tok);
    __CrestLoad(29618, (unsigned long )0, (long long )267);
    __CrestApply2(29617, 12, (long long )((int )tok == 267));
# 889 "dfa.c"
    if ((int )tok == 267) {
      __CrestBranch(29620, 4785, 1);
      __CrestLoad(29622, (unsigned long )(& dfa->tindex), (long long )dfa->tindex);
# 891 "dfa.c"
      ntokens = nsubtoks(dfa->tindex);
      __CrestHandleReturn(29624, (long long )ntokens);
      __CrestStore(29623, (unsigned long )(& ntokens));
      __CrestLoad(29627, (unsigned long )(& dfa->tindex), (long long )dfa->tindex);
      __CrestLoad(29626, (unsigned long )(& ntokens), (long long )ntokens);
      __CrestApply2(29625, 1, (long long )(dfa->tindex - ntokens));
      __CrestStore(29628, (unsigned long )(& tindex));
# 892 "dfa.c"
      tindex = dfa->tindex - ntokens;
      {
      __CrestLoad(29631, (unsigned long )(& maxrep), (long long )maxrep);
      __CrestLoad(29630, (unsigned long )0, (long long )0);
      __CrestApply2(29629, 12, (long long )(maxrep == 0));
# 893 "dfa.c"
      if (maxrep == 0) {
        __CrestBranch(29632, 4787, 1);
        __CrestLoad(29634, (unsigned long )0, (long long )((token )266));
# 894 "dfa.c"
        addtok((token )266);
        __CrestClearStack(29635);
      } else {
        __CrestBranch(29633, 4788, 0);

      }
      }
      {
      __CrestLoad(29638, (unsigned long )(& minrep), (long long )minrep);
      __CrestLoad(29637, (unsigned long )0, (long long )0);
      __CrestApply2(29636, 12, (long long )(minrep == 0));
# 895 "dfa.c"
      if (minrep == 0) {
        __CrestBranch(29639, 4790, 1);
        __CrestLoad(29641, (unsigned long )0, (long long )((token )264));
# 896 "dfa.c"
        addtok((token )264);
        __CrestClearStack(29642);
      } else {
        __CrestBranch(29640, 4791, 0);

      }
      }
      __CrestLoad(29643, (unsigned long )0, (long long )1);
      __CrestStore(29644, (unsigned long )(& i));
# 897 "dfa.c"
      i = 1;
      {
# 897 "dfa.c"
      while (1) {
        while_continue___0: ;
        {
        __CrestLoad(29647, (unsigned long )(& i), (long long )i);
        __CrestLoad(29646, (unsigned long )(& minrep), (long long )minrep);
        __CrestApply2(29645, 16, (long long )(i < minrep));
# 897 "dfa.c"
        if (i < minrep) {
          __CrestBranch(29648, 4797, 1);

        } else {
          __CrestBranch(29649, 4798, 0);
# 897 "dfa.c"
          goto while_break___0;
        }
        }
        __CrestLoad(29650, (unsigned long )(& tindex), (long long )tindex);
        __CrestLoad(29651, (unsigned long )(& ntokens), (long long )ntokens);
# 899 "dfa.c"
        copytoks(tindex, ntokens);
        __CrestClearStack(29652);
        __CrestLoad(29653, (unsigned long )0, (long long )((token )268));
# 900 "dfa.c"
        addtok((token )268);
        __CrestClearStack(29654);
        __CrestLoad(29657, (unsigned long )(& i), (long long )i);
        __CrestLoad(29656, (unsigned long )0, (long long )1);
        __CrestApply2(29655, 0, (long long )(i + 1));
        __CrestStore(29658, (unsigned long )(& i));
# 897 "dfa.c"
        i ++;
      }
      while_break___0: ;
      }
      {
# 902 "dfa.c"
      while (1) {
        while_continue___1: ;
        {
        __CrestLoad(29661, (unsigned long )(& i), (long long )i);
        __CrestLoad(29660, (unsigned long )(& maxrep), (long long )maxrep);
        __CrestApply2(29659, 16, (long long )(i < maxrep));
# 902 "dfa.c"
        if (i < maxrep) {
          __CrestBranch(29662, 4805, 1);

        } else {
          __CrestBranch(29663, 4806, 0);
# 902 "dfa.c"
          goto while_break___1;
        }
        }
        __CrestLoad(29664, (unsigned long )(& tindex), (long long )tindex);
        __CrestLoad(29665, (unsigned long )(& ntokens), (long long )ntokens);
# 904 "dfa.c"
        copytoks(tindex, ntokens);
        __CrestClearStack(29666);
        __CrestLoad(29667, (unsigned long )0, (long long )((token )264));
# 905 "dfa.c"
        addtok((token )264);
        __CrestClearStack(29668);
        __CrestLoad(29669, (unsigned long )0, (long long )((token )268));
# 906 "dfa.c"
        addtok((token )268);
        __CrestClearStack(29670);
        __CrestLoad(29673, (unsigned long )(& i), (long long )i);
        __CrestLoad(29672, (unsigned long )0, (long long )1);
        __CrestApply2(29671, 0, (long long )(i + 1));
        __CrestStore(29674, (unsigned long )(& i));
# 902 "dfa.c"
        i ++;
      }
      while_break___1: ;
      }
# 908 "dfa.c"
      tok = lex();
      __CrestHandleReturn(29676, (long long )tok);
      __CrestStore(29675, (unsigned long )(& tok));
    } else {
      __CrestBranch(29621, 4810, 0);
      __CrestLoad(29677, (unsigned long )(& tok), (long long )tok);
# 912 "dfa.c"
      addtok(tok);
      __CrestClearStack(29678);
# 913 "dfa.c"
      tok = lex();
      __CrestHandleReturn(29680, (long long )tok);
      __CrestStore(29679, (unsigned long )(& tok));
    }
    }
  }
  while_break: ;
  }

  {
  __CrestReturn(29681);
# 882 "dfa.c"
  return;
  }
}
}
# 917 "dfa.c"
static void branch(void)
{


  {
  __CrestCall(29682, 243);
# 920 "dfa.c"
  closure();
  __CrestClearStack(29683);
  {
# 921 "dfa.c"
  while (1) {
    while_continue: ;
    {
    __CrestLoad(29686, (unsigned long )(& tok), (long long )tok);
    __CrestLoad(29685, (unsigned long )0, (long long )272);
    __CrestApply2(29684, 13, (long long )((int )tok != 272));
# 921 "dfa.c"
    if ((int )tok != 272) {
      __CrestBranch(29687, 4819, 1);
      {
      __CrestLoad(29691, (unsigned long )(& tok), (long long )tok);
      __CrestLoad(29690, (unsigned long )0, (long long )269);
      __CrestApply2(29689, 13, (long long )((int )tok != 269));
# 921 "dfa.c"
      if ((int )tok != 269) {
        __CrestBranch(29692, 4820, 1);
        {
        __CrestLoad(29696, (unsigned long )(& tok), (long long )tok);
        __CrestLoad(29695, (unsigned long )0, (long long )0);
        __CrestApply2(29694, 17, (long long )((int )tok >= 0));
# 921 "dfa.c"
        if ((int )tok >= 0) {
          __CrestBranch(29697, 4821, 1);

        } else {
          __CrestBranch(29698, 4822, 0);
# 921 "dfa.c"
          goto while_break;
        }
        }
      } else {
        __CrestBranch(29693, 4823, 0);
# 921 "dfa.c"
        goto while_break;
      }
      }
    } else {
      __CrestBranch(29688, 4824, 0);
# 921 "dfa.c"
      goto while_break;
    }
    }
# 923 "dfa.c"
    closure();
    __CrestClearStack(29699);
    __CrestLoad(29700, (unsigned long )0, (long long )((token )268));
# 924 "dfa.c"
    addtok((token )268);
    __CrestClearStack(29701);
  }
  while_break: ;
  }

  {
  __CrestReturn(29702);
# 917 "dfa.c"
  return;
  }
}
}
# 928 "dfa.c"
static void regexp(int toplevel )
{


  {
  __CrestCall(29704, 244);
  __CrestStore(29703, (unsigned long )(& toplevel));
# 932 "dfa.c"
  branch();
  __CrestClearStack(29705);
  {
# 933 "dfa.c"
  while (1) {
    while_continue: ;
    {
    __CrestLoad(29708, (unsigned long )(& tok), (long long )tok);
    __CrestLoad(29707, (unsigned long )0, (long long )269);
    __CrestApply2(29706, 12, (long long )((int )tok == 269));
# 933 "dfa.c"
    if ((int )tok == 269) {
      __CrestBranch(29709, 4834, 1);

    } else {
      __CrestBranch(29710, 4835, 0);
# 933 "dfa.c"
      goto while_break;
    }
    }
# 935 "dfa.c"
    tok = lex();
    __CrestHandleReturn(29712, (long long )tok);
    __CrestStore(29711, (unsigned long )(& tok));
# 936 "dfa.c"
    branch();
    __CrestClearStack(29713);
    {
    __CrestLoad(29716, (unsigned long )(& toplevel), (long long )toplevel);
    __CrestLoad(29715, (unsigned long )0, (long long )0);
    __CrestApply2(29714, 13, (long long )(toplevel != 0));
# 937 "dfa.c"
    if (toplevel != 0) {
      __CrestBranch(29717, 4838, 1);
      __CrestLoad(29719, (unsigned long )0, (long long )((token )270));
# 938 "dfa.c"
      addtok((token )270);
      __CrestClearStack(29720);
    } else {
      __CrestBranch(29718, 4839, 0);
      __CrestLoad(29721, (unsigned long )0, (long long )((token )269));
# 940 "dfa.c"
      addtok((token )269);
      __CrestClearStack(29722);
    }
    }
  }
  while_break: ;
  }

  {
  __CrestReturn(29723);
# 928 "dfa.c"
  return;
  }
}
}
# 947 "dfa.c"
void dfaparse(char *s , size_t len , struct dfa *d )
{
  char *tmp ;
  char *tmp___0 ;

  {
  __CrestCall(29725, 245);
  __CrestStore(29724, (unsigned long )(& len));
# 954 "dfa.c"
  dfa = d;
# 955 "dfa.c"
  lexptr = s;
# 955 "dfa.c"
  lexstart = lexptr;
  __CrestLoad(29726, (unsigned long )(& len), (long long )len);
  __CrestStore(29727, (unsigned long )(& lexleft));
# 956 "dfa.c"
  lexleft = (int )len;
  __CrestLoad(29728, (unsigned long )0, (long long )((token )-1));
  __CrestStore(29729, (unsigned long )(& lasttok));
# 957 "dfa.c"
  lasttok = (token )-1;
  __CrestLoad(29730, (unsigned long )0, (long long )1);
  __CrestStore(29731, (unsigned long )(& laststart));
# 958 "dfa.c"
  laststart = 1;
  __CrestLoad(29732, (unsigned long )0, (long long )0);
  __CrestStore(29733, (unsigned long )(& parens));
# 959 "dfa.c"
  parens = 0;
  {
  __CrestLoad(29736, (unsigned long )(& syntax_bits_set), (long long )syntax_bits_set);
  __CrestLoad(29735, (unsigned long )0, (long long )0);
  __CrestApply2(29734, 12, (long long )(syntax_bits_set == 0));
# 961 "dfa.c"
  if (syntax_bits_set == 0) {
    __CrestBranch(29737, 4845, 1);
    __CrestLoad(29739, (unsigned long )0, (long long )5);
# 962 "dfa.c"
    tmp = dcgettext((char const *)((void *)0), "No syntax specified", 5);
    __CrestClearStack(29740);
# 962 "dfa.c"
    dfaerror((char const *)tmp);
    __CrestClearStack(29741);
  } else {
    __CrestBranch(29738, 4846, 0);

  }
  }
# 964 "dfa.c"
  tok = lex();
  __CrestHandleReturn(29743, (long long )tok);
  __CrestStore(29742, (unsigned long )(& tok));
  __CrestLoad(29744, (unsigned long )(& d->depth), (long long )d->depth);
  __CrestStore(29745, (unsigned long )(& depth));
# 965 "dfa.c"
  depth = d->depth;
  __CrestLoad(29746, (unsigned long )0, (long long )1);
# 967 "dfa.c"
  regexp(1);
  __CrestClearStack(29747);
  {
  __CrestLoad(29750, (unsigned long )(& tok), (long long )tok);
  __CrestLoad(29749, (unsigned long )0, (long long )-1);
  __CrestApply2(29748, 13, (long long )((int )tok != -1));
# 969 "dfa.c"
  if ((int )tok != -1) {
    __CrestBranch(29751, 4849, 1);
    __CrestLoad(29753, (unsigned long )0, (long long )5);
# 970 "dfa.c"
    tmp___0 = dcgettext((char const *)((void *)0), "Unbalanced )", 5);
    __CrestClearStack(29754);
# 970 "dfa.c"
    dfaerror((char const *)tmp___0);
    __CrestClearStack(29755);
  } else {
    __CrestBranch(29752, 4850, 0);

  }
  }
  __CrestLoad(29758, (unsigned long )0, (long long )-1);
  __CrestLoad(29757, (unsigned long )(& d->nregexps), (long long )d->nregexps);
  __CrestApply2(29756, 1, (long long )(-1 - d->nregexps));
# 972 "dfa.c"
  addtok((token )(-1 - d->nregexps));
  __CrestClearStack(29759);
  __CrestLoad(29760, (unsigned long )0, (long long )((token )268));
# 973 "dfa.c"
  addtok((token )268);
  __CrestClearStack(29761);
  {
  __CrestLoad(29764, (unsigned long )(& d->nregexps), (long long )d->nregexps);
  __CrestLoad(29763, (unsigned long )0, (long long )0);
  __CrestApply2(29762, 13, (long long )(d->nregexps != 0));
# 975 "dfa.c"
  if (d->nregexps != 0) {
    __CrestBranch(29765, 4853, 1);
    __CrestLoad(29767, (unsigned long )0, (long long )((token )270));
# 976 "dfa.c"
    addtok((token )270);
    __CrestClearStack(29768);
  } else {
    __CrestBranch(29766, 4854, 0);

  }
  }
  __CrestLoad(29771, (unsigned long )(& d->nregexps), (long long )d->nregexps);
  __CrestLoad(29770, (unsigned long )0, (long long )1);
  __CrestApply2(29769, 0, (long long )(d->nregexps + 1));
  __CrestStore(29772, (unsigned long )(& d->nregexps));
# 978 "dfa.c"
  (d->nregexps) ++;

  {
  __CrestReturn(29773);
# 947 "dfa.c"
  return;
  }
}
}
# 984 "dfa.c"
static void copy(position_set *src , position_set *dst )
{
  int i ;
  position *mem_4 ;
  position *mem_5 ;

  {
  __CrestCall(29774, 246);

  __CrestLoad(29775, (unsigned long )0, (long long )0);
  __CrestStore(29776, (unsigned long )(& i));
# 991 "dfa.c"
  i = 0;
  {
# 991 "dfa.c"
  while (1) {
    while_continue: ;
    {
    __CrestLoad(29779, (unsigned long )(& i), (long long )i);
    __CrestLoad(29778, (unsigned long )(& src->nelem), (long long )src->nelem);
    __CrestApply2(29777, 16, (long long )(i < src->nelem));
# 991 "dfa.c"
    if (i < src->nelem) {
      __CrestBranch(29780, 4863, 1);

    } else {
      __CrestBranch(29781, 4864, 0);
# 991 "dfa.c"
      goto while_break;
    }
    }
# 992 "dfa.c"
    mem_4 = dst->elems + i;
# 992 "dfa.c"
    mem_5 = src->elems + i;
# 992 "dfa.c"
    *mem_4 = *mem_5;
    __CrestLoad(29784, (unsigned long )(& i), (long long )i);
    __CrestLoad(29783, (unsigned long )0, (long long )1);
    __CrestApply2(29782, 0, (long long )(i + 1));
    __CrestStore(29785, (unsigned long )(& i));
# 991 "dfa.c"
    i ++;
  }
  while_break: ;
  }
  __CrestLoad(29786, (unsigned long )(& src->nelem), (long long )src->nelem);
  __CrestStore(29787, (unsigned long )(& dst->nelem));
# 993 "dfa.c"
  dst->nelem = src->nelem;

  {
  __CrestReturn(29788);
# 984 "dfa.c"
  return;
  }
}
}
# 1000 "dfa.c"
static void insert(position p , position_set *s )
{
  int i ;
  position t1 ;
  position t2 ;
  int tmp ;
  position *mem_7 ;
  position *mem_8 ;
  position *mem_9 ;
  position *mem_10 ;
  position *mem_11 ;
  position *mem_12 ;

  {
  __CrestCall(29789, 247);

  __CrestLoad(29790, (unsigned long )0, (long long )0);
  __CrestStore(29791, (unsigned long )(& i));
# 1008 "dfa.c"
  i = 0;
  {
# 1008 "dfa.c"
  while (1) {
    while_continue: ;
    {
    __CrestLoad(29794, (unsigned long )(& i), (long long )i);
    __CrestLoad(29793, (unsigned long )(& s->nelem), (long long )s->nelem);
    __CrestApply2(29792, 16, (long long )(i < s->nelem));
# 1008 "dfa.c"
    if (i < s->nelem) {
      __CrestBranch(29795, 4875, 1);
      {
# 1008 "dfa.c"
      mem_7 = s->elems + i;
      {
      __CrestLoad(29799, (unsigned long )(& p.strchr), (long long )p.strchr);
      __CrestLoad(29798, (unsigned long )(& mem_7->strchr), (long long )mem_7->strchr);
      __CrestApply2(29797, 16, (long long )(p.strchr < mem_7->strchr));
# 1008 "dfa.c"
      if (p.strchr < mem_7->strchr) {
        __CrestBranch(29800, 4878, 1);

      } else {
        __CrestBranch(29801, 4879, 0);
# 1008 "dfa.c"
        goto while_break;
      }
      }
      }
    } else {
      __CrestBranch(29796, 4880, 0);
# 1008 "dfa.c"
      goto while_break;
    }
    }
# 1009 "dfa.c"
    goto __Cont;
    __Cont:
    __CrestLoad(29804, (unsigned long )(& i), (long long )i);
    __CrestLoad(29803, (unsigned long )0, (long long )1);
    __CrestApply2(29802, 0, (long long )(i + 1));
    __CrestStore(29805, (unsigned long )(& i));
# 1008 "dfa.c"
    i ++;
  }
  while_break: ;
  }
  {
  __CrestLoad(29808, (unsigned long )(& i), (long long )i);
  __CrestLoad(29807, (unsigned long )(& s->nelem), (long long )s->nelem);
  __CrestApply2(29806, 16, (long long )(i < s->nelem));
# 1010 "dfa.c"
  if (i < s->nelem) {
    __CrestBranch(29809, 4885, 1);
    {
# 1010 "dfa.c"
    mem_8 = s->elems + i;
    {
    __CrestLoad(29813, (unsigned long )(& p.strchr), (long long )p.strchr);
    __CrestLoad(29812, (unsigned long )(& mem_8->strchr), (long long )mem_8->strchr);
    __CrestApply2(29811, 12, (long long )(p.strchr == mem_8->strchr));
# 1010 "dfa.c"
    if (p.strchr == mem_8->strchr) {
      __CrestBranch(29814, 4888, 1);
# 1011 "dfa.c"
      mem_9 = s->elems + i;
# 1011 "dfa.c"
      mem_10 = s->elems + i;
      __CrestLoad(29818, (unsigned long )(& mem_10->constraint), (long long )mem_10->constraint);
      __CrestLoad(29817, (unsigned long )(& p.constraint), (long long )p.constraint);
      __CrestApply2(29816, 6, (long long )(mem_10->constraint | p.constraint));
      __CrestStore(29819, (unsigned long )(& mem_9->constraint));
# 1011 "dfa.c"
      mem_9->constraint = mem_10->constraint | p.constraint;
    } else {
      __CrestBranch(29815, 4889, 0);
# 1010 "dfa.c"
      goto _L;
    }
    }
    }
  } else {
    __CrestBranch(29810, 4890, 0);
    _L:
# 1014 "dfa.c"
    t1 = p;
    __CrestLoad(29822, (unsigned long )(& s->nelem), (long long )s->nelem);
    __CrestLoad(29821, (unsigned long )0, (long long )1);
    __CrestApply2(29820, 0, (long long )(s->nelem + 1));
    __CrestStore(29823, (unsigned long )(& s->nelem));
# 1015 "dfa.c"
    (s->nelem) ++;
    {
# 1016 "dfa.c"
    while (1) {
      while_continue___0: ;
      {
      __CrestLoad(29826, (unsigned long )(& i), (long long )i);
      __CrestLoad(29825, (unsigned long )(& s->nelem), (long long )s->nelem);
      __CrestApply2(29824, 16, (long long )(i < s->nelem));
# 1016 "dfa.c"
      if (i < s->nelem) {
        __CrestBranch(29827, 4895, 1);

      } else {
        __CrestBranch(29828, 4896, 0);
# 1016 "dfa.c"
        goto while_break___0;
      }
      }
# 1018 "dfa.c"
      mem_11 = s->elems + i;
# 1018 "dfa.c"
      t2 = *mem_11;
      __CrestLoad(29829, (unsigned long )(& i), (long long )i);
      __CrestStore(29830, (unsigned long )(& tmp));
# 1019 "dfa.c"
      tmp = i;
      __CrestLoad(29833, (unsigned long )(& i), (long long )i);
      __CrestLoad(29832, (unsigned long )0, (long long )1);
      __CrestApply2(29831, 0, (long long )(i + 1));
      __CrestStore(29834, (unsigned long )(& i));
# 1019 "dfa.c"
      i ++;
# 1019 "dfa.c"
      mem_12 = s->elems + tmp;
# 1019 "dfa.c"
      *mem_12 = t1;
# 1020 "dfa.c"
      t1 = t2;
    }
    while_break___0: ;
    }
  }
  }

  {
  __CrestReturn(29835);
# 1000 "dfa.c"
  return;
  }
}
}
# 1027 "dfa.c"
static void merge(position_set *s1 , position_set *s2 , position_set *m )
{
  int i ;
  int j ;
  int tmp ;
  int tmp___0 ;
  int tmp___1 ;
  int tmp___2 ;
  int tmp___3 ;
  int tmp___4 ;
  int tmp___5 ;
  int tmp___6 ;
  int tmp___7 ;
  int tmp___8 ;
  int tmp___9 ;
  position *mem_17 ;
  position *mem_18 ;
  position *mem_19 ;
  position *mem_20 ;
  position *mem_21 ;
  position *mem_22 ;
  position *mem_23 ;
  position *mem_24 ;
  position *mem_25 ;
  position *mem_26 ;
  position *mem_27 ;
  position *mem_28 ;
  position *mem_29 ;
  position *mem_30 ;
  position *mem_31 ;
  position *mem_32 ;
  position *mem_33 ;

  {
  __CrestCall(29836, 248);

  __CrestLoad(29837, (unsigned long )0, (long long )0);
  __CrestStore(29838, (unsigned long )(& i));
# 1033 "dfa.c"
  i = 0;
  __CrestLoad(29839, (unsigned long )0, (long long )0);
  __CrestStore(29840, (unsigned long )(& j));
# 1033 "dfa.c"
  j = 0;
  __CrestLoad(29841, (unsigned long )0, (long long )0);
  __CrestStore(29842, (unsigned long )(& m->nelem));
# 1035 "dfa.c"
  m->nelem = 0;
  {
# 1036 "dfa.c"
  while (1) {
    while_continue: ;
    {
    __CrestLoad(29845, (unsigned long )(& i), (long long )i);
    __CrestLoad(29844, (unsigned long )(& s1->nelem), (long long )s1->nelem);
    __CrestApply2(29843, 16, (long long )(i < s1->nelem));
# 1036 "dfa.c"
    if (i < s1->nelem) {
      __CrestBranch(29846, 4906, 1);
      {
      __CrestLoad(29850, (unsigned long )(& j), (long long )j);
      __CrestLoad(29849, (unsigned long )(& s2->nelem), (long long )s2->nelem);
      __CrestApply2(29848, 16, (long long )(j < s2->nelem));
# 1036 "dfa.c"
      if (j < s2->nelem) {
        __CrestBranch(29851, 4907, 1);

      } else {
        __CrestBranch(29852, 4908, 0);
# 1036 "dfa.c"
        goto while_break;
      }
      }
    } else {
      __CrestBranch(29847, 4909, 0);
# 1036 "dfa.c"
      goto while_break;
    }
    }
    {
# 1037 "dfa.c"
    mem_17 = s1->elems + i;
# 1037 "dfa.c"
    mem_18 = s2->elems + j;
    {
    __CrestLoad(29855, (unsigned long )(& mem_17->strchr), (long long )mem_17->strchr);
    __CrestLoad(29854, (unsigned long )(& mem_18->strchr), (long long )mem_18->strchr);
    __CrestApply2(29853, 14, (long long )(mem_17->strchr > mem_18->strchr));
# 1037 "dfa.c"
    if (mem_17->strchr > mem_18->strchr) {
      __CrestBranch(29856, 4913, 1);
      __CrestLoad(29858, (unsigned long )(& m->nelem), (long long )m->nelem);
      __CrestStore(29859, (unsigned long )(& tmp));
# 1038 "dfa.c"
      tmp = m->nelem;
      __CrestLoad(29862, (unsigned long )(& m->nelem), (long long )m->nelem);
      __CrestLoad(29861, (unsigned long )0, (long long )1);
      __CrestApply2(29860, 0, (long long )(m->nelem + 1));
      __CrestStore(29863, (unsigned long )(& m->nelem));
# 1038 "dfa.c"
      (m->nelem) ++;
      __CrestLoad(29864, (unsigned long )(& i), (long long )i);
      __CrestStore(29865, (unsigned long )(& tmp___0));
# 1038 "dfa.c"
      tmp___0 = i;
      __CrestLoad(29868, (unsigned long )(& i), (long long )i);
      __CrestLoad(29867, (unsigned long )0, (long long )1);
      __CrestApply2(29866, 0, (long long )(i + 1));
      __CrestStore(29869, (unsigned long )(& i));
# 1038 "dfa.c"
      i ++;
# 1038 "dfa.c"
      mem_19 = m->elems + tmp;
# 1038 "dfa.c"
      mem_20 = s1->elems + tmp___0;
# 1038 "dfa.c"
      *mem_19 = *mem_20;
    } else {
      __CrestBranch(29857, 4914, 0);
      {
# 1039 "dfa.c"
      mem_21 = s1->elems + i;
# 1039 "dfa.c"
      mem_22 = s2->elems + j;
      {
      __CrestLoad(29872, (unsigned long )(& mem_21->strchr), (long long )mem_21->strchr);
      __CrestLoad(29871, (unsigned long )(& mem_22->strchr), (long long )mem_22->strchr);
      __CrestApply2(29870, 16, (long long )(mem_21->strchr < mem_22->strchr));
# 1039 "dfa.c"
      if (mem_21->strchr < mem_22->strchr) {
        __CrestBranch(29873, 4917, 1);
        __CrestLoad(29875, (unsigned long )(& m->nelem), (long long )m->nelem);
        __CrestStore(29876, (unsigned long )(& tmp___1));
# 1040 "dfa.c"
        tmp___1 = m->nelem;
        __CrestLoad(29879, (unsigned long )(& m->nelem), (long long )m->nelem);
        __CrestLoad(29878, (unsigned long )0, (long long )1);
        __CrestApply2(29877, 0, (long long )(m->nelem + 1));
        __CrestStore(29880, (unsigned long )(& m->nelem));
# 1040 "dfa.c"
        (m->nelem) ++;
        __CrestLoad(29881, (unsigned long )(& j), (long long )j);
        __CrestStore(29882, (unsigned long )(& tmp___2));
# 1040 "dfa.c"
        tmp___2 = j;
        __CrestLoad(29885, (unsigned long )(& j), (long long )j);
        __CrestLoad(29884, (unsigned long )0, (long long )1);
        __CrestApply2(29883, 0, (long long )(j + 1));
        __CrestStore(29886, (unsigned long )(& j));
# 1040 "dfa.c"
        j ++;
# 1040 "dfa.c"
        mem_23 = m->elems + tmp___1;
# 1040 "dfa.c"
        mem_24 = s2->elems + tmp___2;
# 1040 "dfa.c"
        *mem_23 = *mem_24;
      } else {
        __CrestBranch(29874, 4918, 0);
        __CrestLoad(29887, (unsigned long )(& i), (long long )i);
        __CrestStore(29888, (unsigned long )(& tmp___3));
# 1043 "dfa.c"
        tmp___3 = i;
        __CrestLoad(29891, (unsigned long )(& i), (long long )i);
        __CrestLoad(29890, (unsigned long )0, (long long )1);
        __CrestApply2(29889, 0, (long long )(i + 1));
        __CrestStore(29892, (unsigned long )(& i));
# 1043 "dfa.c"
        i ++;
# 1043 "dfa.c"
        mem_25 = m->elems + m->nelem;
# 1043 "dfa.c"
        mem_26 = s1->elems + tmp___3;
# 1043 "dfa.c"
        *mem_25 = *mem_26;
        __CrestLoad(29893, (unsigned long )(& m->nelem), (long long )m->nelem);
        __CrestStore(29894, (unsigned long )(& tmp___4));
# 1044 "dfa.c"
        tmp___4 = m->nelem;
        __CrestLoad(29897, (unsigned long )(& m->nelem), (long long )m->nelem);
        __CrestLoad(29896, (unsigned long )0, (long long )1);
        __CrestApply2(29895, 0, (long long )(m->nelem + 1));
        __CrestStore(29898, (unsigned long )(& m->nelem));
# 1044 "dfa.c"
        (m->nelem) ++;
        __CrestLoad(29899, (unsigned long )(& j), (long long )j);
        __CrestStore(29900, (unsigned long )(& tmp___5));
# 1044 "dfa.c"
        tmp___5 = j;
        __CrestLoad(29903, (unsigned long )(& j), (long long )j);
        __CrestLoad(29902, (unsigned long )0, (long long )1);
        __CrestApply2(29901, 0, (long long )(j + 1));
        __CrestStore(29904, (unsigned long )(& j));
# 1044 "dfa.c"
        j ++;
# 1044 "dfa.c"
        mem_27 = m->elems + tmp___4;
# 1044 "dfa.c"
        mem_28 = m->elems + tmp___4;
# 1044 "dfa.c"
        mem_29 = s2->elems + tmp___5;
        __CrestLoad(29907, (unsigned long )(& mem_28->constraint), (long long )mem_28->constraint);
        __CrestLoad(29906, (unsigned long )(& mem_29->constraint), (long long )mem_29->constraint);
        __CrestApply2(29905, 6, (long long )(mem_28->constraint | mem_29->constraint));
        __CrestStore(29908, (unsigned long )(& mem_27->constraint));
# 1044 "dfa.c"
        mem_27->constraint = mem_28->constraint | mem_29->constraint;
      }
      }
      }
    }
    }
    }
  }
  while_break: ;
  }
  {
# 1046 "dfa.c"
  while (1) {
    while_continue___0: ;
    {
    __CrestLoad(29911, (unsigned long )(& i), (long long )i);
    __CrestLoad(29910, (unsigned long )(& s1->nelem), (long long )s1->nelem);
    __CrestApply2(29909, 16, (long long )(i < s1->nelem));
# 1046 "dfa.c"
    if (i < s1->nelem) {
      __CrestBranch(29912, 4924, 1);

    } else {
      __CrestBranch(29913, 4925, 0);
# 1046 "dfa.c"
      goto while_break___0;
    }
    }
    __CrestLoad(29914, (unsigned long )(& m->nelem), (long long )m->nelem);
    __CrestStore(29915, (unsigned long )(& tmp___6));
# 1047 "dfa.c"
    tmp___6 = m->nelem;
    __CrestLoad(29918, (unsigned long )(& m->nelem), (long long )m->nelem);
    __CrestLoad(29917, (unsigned long )0, (long long )1);
    __CrestApply2(29916, 0, (long long )(m->nelem + 1));
    __CrestStore(29919, (unsigned long )(& m->nelem));
# 1047 "dfa.c"
    (m->nelem) ++;
    __CrestLoad(29920, (unsigned long )(& i), (long long )i);
    __CrestStore(29921, (unsigned long )(& tmp___7));
# 1047 "dfa.c"
    tmp___7 = i;
    __CrestLoad(29924, (unsigned long )(& i), (long long )i);
    __CrestLoad(29923, (unsigned long )0, (long long )1);
    __CrestApply2(29922, 0, (long long )(i + 1));
    __CrestStore(29925, (unsigned long )(& i));
# 1047 "dfa.c"
    i ++;
# 1047 "dfa.c"
    mem_30 = m->elems + tmp___6;
# 1047 "dfa.c"
    mem_31 = s1->elems + tmp___7;
# 1047 "dfa.c"
    *mem_30 = *mem_31;
  }
  while_break___0: ;
  }
  {
# 1048 "dfa.c"
  while (1) {
    while_continue___1: ;
    {
    __CrestLoad(29928, (unsigned long )(& j), (long long )j);
    __CrestLoad(29927, (unsigned long )(& s2->nelem), (long long )s2->nelem);
    __CrestApply2(29926, 16, (long long )(j < s2->nelem));
# 1048 "dfa.c"
    if (j < s2->nelem) {
      __CrestBranch(29929, 4932, 1);

    } else {
      __CrestBranch(29930, 4933, 0);
# 1048 "dfa.c"
      goto while_break___1;
    }
    }
    __CrestLoad(29931, (unsigned long )(& m->nelem), (long long )m->nelem);
    __CrestStore(29932, (unsigned long )(& tmp___8));
# 1049 "dfa.c"
    tmp___8 = m->nelem;
    __CrestLoad(29935, (unsigned long )(& m->nelem), (long long )m->nelem);
    __CrestLoad(29934, (unsigned long )0, (long long )1);
    __CrestApply2(29933, 0, (long long )(m->nelem + 1));
    __CrestStore(29936, (unsigned long )(& m->nelem));
# 1049 "dfa.c"
    (m->nelem) ++;
    __CrestLoad(29937, (unsigned long )(& j), (long long )j);
    __CrestStore(29938, (unsigned long )(& tmp___9));
# 1049 "dfa.c"
    tmp___9 = j;
    __CrestLoad(29941, (unsigned long )(& j), (long long )j);
    __CrestLoad(29940, (unsigned long )0, (long long )1);
    __CrestApply2(29939, 0, (long long )(j + 1));
    __CrestStore(29942, (unsigned long )(& j));
# 1049 "dfa.c"
    j ++;
# 1049 "dfa.c"
    mem_32 = m->elems + tmp___8;
# 1049 "dfa.c"
    mem_33 = s2->elems + tmp___9;
# 1049 "dfa.c"
    *mem_32 = *mem_33;
  }
  while_break___1: ;
  }

  {
  __CrestReturn(29943);
# 1027 "dfa.c"
  return;
  }
}
}
# 1053 "dfa.c"
static void delete(position p , position_set *s )
{
  int i ;
  position *mem_4 ;
  position *mem_5 ;
  position *mem_6 ;

  {
  __CrestCall(29944, 249);

  __CrestLoad(29945, (unsigned long )0, (long long )0);
  __CrestStore(29946, (unsigned long )(& i));
# 1060 "dfa.c"
  i = 0;
  {
# 1060 "dfa.c"
  while (1) {
    while_continue: ;
    {
    __CrestLoad(29949, (unsigned long )(& i), (long long )i);
    __CrestLoad(29948, (unsigned long )(& s->nelem), (long long )s->nelem);
    __CrestApply2(29947, 16, (long long )(i < s->nelem));
# 1060 "dfa.c"
    if (i < s->nelem) {
      __CrestBranch(29950, 4943, 1);

    } else {
      __CrestBranch(29951, 4944, 0);
# 1060 "dfa.c"
      goto while_break;
    }
    }
    {
# 1061 "dfa.c"
    mem_4 = s->elems + i;
    {
    __CrestLoad(29954, (unsigned long )(& p.strchr), (long long )p.strchr);
    __CrestLoad(29953, (unsigned long )(& mem_4->strchr), (long long )mem_4->strchr);
    __CrestApply2(29952, 12, (long long )(p.strchr == mem_4->strchr));
# 1061 "dfa.c"
    if (p.strchr == mem_4->strchr) {
      __CrestBranch(29955, 4948, 1);
# 1062 "dfa.c"
      goto while_break;
    } else {
      __CrestBranch(29956, 4949, 0);

    }
    }
    }
    __CrestLoad(29959, (unsigned long )(& i), (long long )i);
    __CrestLoad(29958, (unsigned long )0, (long long )1);
    __CrestApply2(29957, 0, (long long )(i + 1));
    __CrestStore(29960, (unsigned long )(& i));
# 1060 "dfa.c"
    i ++;
  }
  while_break: ;
  }
  {
  __CrestLoad(29963, (unsigned long )(& i), (long long )i);
  __CrestLoad(29962, (unsigned long )(& s->nelem), (long long )s->nelem);
  __CrestApply2(29961, 16, (long long )(i < s->nelem));
# 1063 "dfa.c"
  if (i < s->nelem) {
    __CrestBranch(29964, 4953, 1);
    __CrestLoad(29968, (unsigned long )(& s->nelem), (long long )s->nelem);
    __CrestLoad(29967, (unsigned long )0, (long long )1);
    __CrestApply2(29966, 1, (long long )(s->nelem - 1));
    __CrestStore(29969, (unsigned long )(& s->nelem));
# 1064 "dfa.c"
    (s->nelem) --;
    {
# 1064 "dfa.c"
    while (1) {
      while_continue___0: ;
      {
      __CrestLoad(29972, (unsigned long )(& i), (long long )i);
      __CrestLoad(29971, (unsigned long )(& s->nelem), (long long )s->nelem);
      __CrestApply2(29970, 16, (long long )(i < s->nelem));
# 1064 "dfa.c"
      if (i < s->nelem) {
        __CrestBranch(29973, 4958, 1);

      } else {
        __CrestBranch(29974, 4959, 0);
# 1064 "dfa.c"
        goto while_break___0;
      }
      }
# 1065 "dfa.c"
      mem_5 = s->elems + i;
# 1065 "dfa.c"
      mem_6 = s->elems + (i + 1);
# 1065 "dfa.c"
      *mem_5 = *mem_6;
      __CrestLoad(29977, (unsigned long )(& i), (long long )i);
      __CrestLoad(29976, (unsigned long )0, (long long )1);
      __CrestApply2(29975, 0, (long long )(i + 1));
      __CrestStore(29978, (unsigned long )(& i));
# 1064 "dfa.c"
      i ++;
    }
    while_break___0: ;
    }
  } else {
    __CrestBranch(29965, 4962, 0);

  }
  }

  {
  __CrestReturn(29979);
# 1053 "dfa.c"
  return;
  }
}
}
# 1072 "dfa.c"
static int state_index(struct dfa *d , position_set *s , int newline , int letter )
{
  int hash ;
  int constraint ;
  int i ;
  int j ;
  ptr_t tmp ;
  ptr_t tmp___0 ;
  int tmp___1 ;
  int tmp___2 ;
  int tmp___3 ;
  int tmp___4 ;
  int tmp___5 ;
  int tmp___6 ;
  int tmp___7 ;
  int tmp___8 ;
  position *mem_19 ;
  position *mem_20 ;
  dfa_state *mem_21 ;
  dfa_state *mem_22 ;
  dfa_state *mem_23 ;
  dfa_state *mem_24 ;
  position *mem_25 ;
  dfa_state *mem_26 ;
  position *mem_27 ;
  position *mem_28 ;
  dfa_state *mem_29 ;
  position *mem_30 ;
  dfa_state *mem_31 ;
  dfa_state *mem_32 ;
  dfa_state *mem_33 ;
  dfa_state *mem_34 ;
  dfa_state *mem_35 ;
  dfa_state *mem_36 ;
  dfa_state *mem_37 ;
  dfa_state *mem_38 ;
  position *mem_39 ;
  token *mem_40 ;
  position *mem_41 ;
  dfa_state *mem_42 ;
  dfa_state *mem_43 ;
  dfa_state *mem_44 ;
  dfa_state *mem_45 ;
  dfa_state *mem_46 ;
  dfa_state *mem_47 ;
  dfa_state *mem_48 ;
  dfa_state *mem_49 ;
  dfa_state *mem_50 ;
  dfa_state *mem_51 ;
  position *mem_52 ;
  token *mem_53 ;
  position *mem_54 ;
  token *mem_55 ;
  dfa_state *mem_56 ;
  dfa_state *mem_57 ;
  int __retres58 ;

  {
  __CrestCall(29982, 250);
  __CrestStore(29981, (unsigned long )(& letter));
  __CrestStore(29980, (unsigned long )(& newline));
  __CrestLoad(29983, (unsigned long )0, (long long )0);
  __CrestStore(29984, (unsigned long )(& hash));
# 1079 "dfa.c"
  hash = 0;
  {
  __CrestLoad(29987, (unsigned long )(& newline), (long long )newline);
  __CrestLoad(29986, (unsigned long )0, (long long )0);
  __CrestApply2(29985, 13, (long long )(newline != 0));
# 1083 "dfa.c"
  if (newline != 0) {
    __CrestBranch(29988, 4967, 1);
    __CrestLoad(29990, (unsigned long )0, (long long )1);
    __CrestStore(29991, (unsigned long )(& newline));
# 1083 "dfa.c"
    newline = 1;
  } else {
    __CrestBranch(29989, 4968, 0);
    __CrestLoad(29992, (unsigned long )0, (long long )0);
    __CrestStore(29993, (unsigned long )(& newline));
# 1083 "dfa.c"
    newline = 0;
  }
  }
  {
  __CrestLoad(29996, (unsigned long )(& letter), (long long )letter);
  __CrestLoad(29995, (unsigned long )0, (long long )0);
  __CrestApply2(29994, 13, (long long )(letter != 0));
# 1084 "dfa.c"
  if (letter != 0) {
    __CrestBranch(29997, 4970, 1);
    __CrestLoad(29999, (unsigned long )0, (long long )1);
    __CrestStore(30000, (unsigned long )(& letter));
# 1084 "dfa.c"
    letter = 1;
  } else {
    __CrestBranch(29998, 4971, 0);
    __CrestLoad(30001, (unsigned long )0, (long long )0);
    __CrestStore(30002, (unsigned long )(& letter));
# 1084 "dfa.c"
    letter = 0;
  }
  }
  __CrestLoad(30003, (unsigned long )0, (long long )0);
  __CrestStore(30004, (unsigned long )(& i));
# 1086 "dfa.c"
  i = 0;
  {
# 1086 "dfa.c"
  while (1) {
    while_continue: ;
    {
    __CrestLoad(30007, (unsigned long )(& i), (long long )i);
    __CrestLoad(30006, (unsigned long )(& s->nelem), (long long )s->nelem);
    __CrestApply2(30005, 16, (long long )(i < s->nelem));
# 1086 "dfa.c"
    if (i < s->nelem) {
      __CrestBranch(30008, 4977, 1);

    } else {
      __CrestBranch(30009, 4978, 0);
# 1086 "dfa.c"
      goto while_break;
    }
    }
# 1087 "dfa.c"
    mem_19 = s->elems + i;
# 1087 "dfa.c"
    mem_20 = s->elems + i;
    __CrestLoad(30014, (unsigned long )(& hash), (long long )hash);
    __CrestLoad(30013, (unsigned long )(& mem_19->strchr), (long long )mem_19->strchr);
    __CrestLoad(30012, (unsigned long )(& mem_20->constraint), (long long )mem_20->constraint);
    __CrestApply2(30011, 0, (long long )(mem_19->strchr + mem_20->constraint));
    __CrestApply2(30010, 7, (long long )((unsigned int )hash ^ (mem_19->strchr + mem_20->constraint)));
    __CrestStore(30015, (unsigned long )(& hash));
# 1087 "dfa.c"
    hash = (int )((unsigned int )hash ^ (mem_19->strchr + mem_20->constraint));
    __CrestLoad(30018, (unsigned long )(& i), (long long )i);
    __CrestLoad(30017, (unsigned long )0, (long long )1);
    __CrestApply2(30016, 0, (long long )(i + 1));
    __CrestStore(30019, (unsigned long )(& i));
# 1086 "dfa.c"
    i ++;
  }
  while_break: ;
  }
  __CrestLoad(30020, (unsigned long )0, (long long )0);
  __CrestStore(30021, (unsigned long )(& i));
# 1090 "dfa.c"
  i = 0;
  {
# 1090 "dfa.c"
  while (1) {
    while_continue___0: ;
    {
    __CrestLoad(30024, (unsigned long )(& i), (long long )i);
    __CrestLoad(30023, (unsigned long )(& d->sindex), (long long )d->sindex);
    __CrestApply2(30022, 16, (long long )(i < d->sindex));
# 1090 "dfa.c"
    if (i < d->sindex) {
      __CrestBranch(30025, 4986, 1);

    } else {
      __CrestBranch(30026, 4987, 0);
# 1090 "dfa.c"
      goto while_break___0;
    }
    }
    {
# 1092 "dfa.c"
    mem_21 = d->states + i;
    {
    __CrestLoad(30029, (unsigned long )(& hash), (long long )hash);
    __CrestLoad(30028, (unsigned long )(& mem_21->hash), (long long )mem_21->hash);
    __CrestApply2(30027, 13, (long long )(hash != mem_21->hash));
# 1092 "dfa.c"
    if (hash != mem_21->hash) {
      __CrestBranch(30030, 4991, 1);
# 1094 "dfa.c"
      goto __Cont;
    } else {
      __CrestBranch(30031, 4992, 0);
      {
# 1092 "dfa.c"
      mem_22 = d->states + i;
      {
      __CrestLoad(30034, (unsigned long )(& s->nelem), (long long )s->nelem);
      __CrestLoad(30033, (unsigned long )(& mem_22->elems.nelem), (long long )mem_22->elems.nelem);
      __CrestApply2(30032, 13, (long long )(s->nelem != mem_22->elems.nelem));
# 1092 "dfa.c"
      if (s->nelem != mem_22->elems.nelem) {
        __CrestBranch(30035, 4995, 1);
# 1094 "dfa.c"
        goto __Cont;
      } else {
        __CrestBranch(30036, 4996, 0);
        {
# 1092 "dfa.c"
        mem_23 = d->states + i;
        {
        __CrestLoad(30039, (unsigned long )(& newline), (long long )newline);
        __CrestLoad(30038, (unsigned long )(& mem_23->newline), (long long )mem_23->newline);
        __CrestApply2(30037, 13, (long long )(newline != (int )mem_23->newline));
# 1092 "dfa.c"
        if (newline != (int )mem_23->newline) {
          __CrestBranch(30040, 4999, 1);
# 1094 "dfa.c"
          goto __Cont;
        } else {
          __CrestBranch(30041, 5000, 0);
          {
# 1092 "dfa.c"
          mem_24 = d->states + i;
          {
          __CrestLoad(30044, (unsigned long )(& letter), (long long )letter);
          __CrestLoad(30043, (unsigned long )(& mem_24->letter), (long long )mem_24->letter);
          __CrestApply2(30042, 13, (long long )(letter != (int )mem_24->letter));
# 1092 "dfa.c"
          if (letter != (int )mem_24->letter) {
            __CrestBranch(30045, 5003, 1);
# 1094 "dfa.c"
            goto __Cont;
          } else {
            __CrestBranch(30046, 5004, 0);

          }
          }
          }
        }
        }
        }
      }
      }
      }
    }
    }
    }
    __CrestLoad(30047, (unsigned long )0, (long long )0);
    __CrestStore(30048, (unsigned long )(& j));
# 1095 "dfa.c"
    j = 0;
    {
# 1095 "dfa.c"
    while (1) {
      while_continue___1: ;
      {
      __CrestLoad(30051, (unsigned long )(& j), (long long )j);
      __CrestLoad(30050, (unsigned long )(& s->nelem), (long long )s->nelem);
      __CrestApply2(30049, 16, (long long )(j < s->nelem));
# 1095 "dfa.c"
      if (j < s->nelem) {
        __CrestBranch(30052, 5010, 1);

      } else {
        __CrestBranch(30053, 5011, 0);
# 1095 "dfa.c"
        goto while_break___1;
      }
      }
      {
# 1096 "dfa.c"
      mem_25 = s->elems + j;
# 1096 "dfa.c"
      mem_26 = d->states + i;
# 1096 "dfa.c"
      mem_27 = mem_26->elems.elems + j;
      {
      __CrestLoad(30056, (unsigned long )(& mem_25->constraint), (long long )mem_25->constraint);
      __CrestLoad(30055, (unsigned long )(& mem_27->constraint), (long long )mem_27->constraint);
      __CrestApply2(30054, 13, (long long )(mem_25->constraint != mem_27->constraint));
# 1096 "dfa.c"
      if (mem_25->constraint != mem_27->constraint) {
        __CrestBranch(30057, 5015, 1);
# 1099 "dfa.c"
        goto while_break___1;
      } else {
        __CrestBranch(30058, 5016, 0);
        {
# 1096 "dfa.c"
        mem_28 = s->elems + j;
# 1096 "dfa.c"
        mem_29 = d->states + i;
# 1096 "dfa.c"
        mem_30 = mem_29->elems.elems + j;
        {
        __CrestLoad(30061, (unsigned long )(& mem_28->strchr), (long long )mem_28->strchr);
        __CrestLoad(30060, (unsigned long )(& mem_30->strchr), (long long )mem_30->strchr);
        __CrestApply2(30059, 13, (long long )(mem_28->strchr != mem_30->strchr));
# 1096 "dfa.c"
        if (mem_28->strchr != mem_30->strchr) {
          __CrestBranch(30062, 5019, 1);
# 1099 "dfa.c"
          goto while_break___1;
        } else {
          __CrestBranch(30063, 5020, 0);

        }
        }
        }
      }
      }
      }
      __CrestLoad(30066, (unsigned long )(& j), (long long )j);
      __CrestLoad(30065, (unsigned long )0, (long long )1);
      __CrestApply2(30064, 0, (long long )(j + 1));
      __CrestStore(30067, (unsigned long )(& j));
# 1095 "dfa.c"
      j ++;
    }
    while_break___1: ;
    }
    {
    __CrestLoad(30070, (unsigned long )(& j), (long long )j);
    __CrestLoad(30069, (unsigned long )(& s->nelem), (long long )s->nelem);
    __CrestApply2(30068, 12, (long long )(j == s->nelem));
# 1100 "dfa.c"
    if (j == s->nelem) {
      __CrestBranch(30071, 5024, 1);
      __CrestLoad(30073, (unsigned long )(& i), (long long )i);
      __CrestStore(30074, (unsigned long )(& __retres58));
# 1101 "dfa.c"
      __retres58 = i;
# 1101 "dfa.c"
      goto return_label;
    } else {
      __CrestBranch(30072, 5026, 0);

    }
    }
    __Cont:
    __CrestLoad(30077, (unsigned long )(& i), (long long )i);
    __CrestLoad(30076, (unsigned long )0, (long long )1);
    __CrestApply2(30075, 0, (long long )(i + 1));
    __CrestStore(30078, (unsigned long )(& i));
# 1090 "dfa.c"
    i ++;
  }
  while_break___0: ;
  }
  {
  __CrestLoad(30081, (unsigned long )(& d->sindex), (long long )d->sindex);
  __CrestLoad(30080, (unsigned long )(& d->salloc), (long long )d->salloc);
  __CrestApply2(30079, 17, (long long )(d->sindex >= d->salloc));
# 1105 "dfa.c"
  if (d->sindex >= d->salloc) {
    __CrestBranch(30082, 5030, 1);
    {
# 1105 "dfa.c"
    while (1) {
      while_continue___2: ;
      {
      __CrestLoad(30086, (unsigned long )(& d->sindex), (long long )d->sindex);
      __CrestLoad(30085, (unsigned long )(& d->salloc), (long long )d->salloc);
      __CrestApply2(30084, 17, (long long )(d->sindex >= d->salloc));
# 1105 "dfa.c"
      if (d->sindex >= d->salloc) {
        __CrestBranch(30087, 5034, 1);

      } else {
        __CrestBranch(30088, 5035, 0);
# 1105 "dfa.c"
        goto while_break___2;
      }
      }
      __CrestLoad(30091, (unsigned long )(& d->salloc), (long long )d->salloc);
      __CrestLoad(30090, (unsigned long )0, (long long )2);
      __CrestApply2(30089, 2, (long long )(d->salloc * 2));
      __CrestStore(30092, (unsigned long )(& d->salloc));
# 1105 "dfa.c"
      d->salloc *= 2;
    }
    while_break___2: ;
    }
    __CrestLoad(30095, (unsigned long )(& d->salloc), (long long )d->salloc);
    __CrestLoad(30094, (unsigned long )0, (long long )sizeof(dfa_state ));
    __CrestApply2(30093, 2, (long long )((unsigned long )d->salloc * sizeof(dfa_state )));
# 1105 "dfa.c"
    tmp = xrealloc((ptr_t )d->states, (unsigned long )d->salloc * sizeof(dfa_state ));
    __CrestClearStack(30096);
# 1105 "dfa.c"
    d->states = (dfa_state *)tmp;
  } else {
    __CrestBranch(30083, 5039, 0);

  }
  }
# 1106 "dfa.c"
  mem_31 = d->states + i;
  __CrestLoad(30097, (unsigned long )(& hash), (long long )hash);
  __CrestStore(30098, (unsigned long )(& mem_31->hash));
# 1106 "dfa.c"
  mem_31->hash = hash;
  __CrestLoad(30101, (unsigned long )(& s->nelem), (long long )s->nelem);
  __CrestLoad(30100, (unsigned long )0, (long long )sizeof(position ));
  __CrestApply2(30099, 2, (long long )((unsigned long )s->nelem * sizeof(position )));
# 1107 "dfa.c"
  tmp___0 = xmalloc((unsigned long )s->nelem * sizeof(position ));
  __CrestClearStack(30102);
# 1107 "dfa.c"
  mem_32 = d->states + i;
# 1107 "dfa.c"
  mem_32->elems.elems = (position *)tmp___0;
# 1108 "dfa.c"
  mem_33 = d->states + i;
# 1108 "dfa.c"
  copy(s, & mem_33->elems);
  __CrestClearStack(30103);
# 1109 "dfa.c"
  mem_34 = d->states + i;
  __CrestLoad(30104, (unsigned long )(& newline), (long long )newline);
  __CrestStore(30105, (unsigned long )(& mem_34->newline));
# 1109 "dfa.c"
  mem_34->newline = (char )newline;
# 1110 "dfa.c"
  mem_35 = d->states + i;
  __CrestLoad(30106, (unsigned long )(& letter), (long long )letter);
  __CrestStore(30107, (unsigned long )(& mem_35->letter));
# 1110 "dfa.c"
  mem_35->letter = (char )letter;
# 1111 "dfa.c"
  mem_36 = d->states + i;
  __CrestLoad(30108, (unsigned long )0, (long long )(char)0);
  __CrestStore(30109, (unsigned long )(& mem_36->backref));
# 1111 "dfa.c"
  mem_36->backref = (char)0;
# 1112 "dfa.c"
  mem_37 = d->states + i;
  __CrestLoad(30110, (unsigned long )0, (long long )(unsigned char)0);
  __CrestStore(30111, (unsigned long )(& mem_37->constraint));
# 1112 "dfa.c"
  mem_37->constraint = (unsigned char)0;
# 1113 "dfa.c"
  mem_38 = d->states + i;
  __CrestLoad(30112, (unsigned long )0, (long long )0);
  __CrestStore(30113, (unsigned long )(& mem_38->first_end));
# 1113 "dfa.c"
  mem_38->first_end = 0;
  __CrestLoad(30114, (unsigned long )0, (long long )0);
  __CrestStore(30115, (unsigned long )(& j));
# 1114 "dfa.c"
  j = 0;
  {
# 1114 "dfa.c"
  while (1) {
    while_continue___3: ;
    {
    __CrestLoad(30118, (unsigned long )(& j), (long long )j);
    __CrestLoad(30117, (unsigned long )(& s->nelem), (long long )s->nelem);
    __CrestApply2(30116, 16, (long long )(j < s->nelem));
# 1114 "dfa.c"
    if (j < s->nelem) {
      __CrestBranch(30119, 5045, 1);

    } else {
      __CrestBranch(30120, 5046, 0);
# 1114 "dfa.c"
      goto while_break___3;
    }
    }
    {
# 1115 "dfa.c"
    mem_39 = s->elems + j;
# 1115 "dfa.c"
    mem_40 = d->tokens + mem_39->strchr;
    {
    __CrestLoad(30123, (unsigned long )mem_40, (long long )*mem_40);
    __CrestLoad(30122, (unsigned long )0, (long long )0);
    __CrestApply2(30121, 16, (long long )((int )*mem_40 < 0));
# 1115 "dfa.c"
    if ((int )*mem_40 < 0) {
      __CrestBranch(30124, 5050, 1);
# 1117 "dfa.c"
      mem_41 = s->elems + j;
      __CrestLoad(30126, (unsigned long )(& mem_41->constraint), (long long )mem_41->constraint);
      __CrestStore(30127, (unsigned long )(& constraint));
# 1117 "dfa.c"
      constraint = (int )mem_41->constraint;
      {
      __CrestLoad(30130, (unsigned long )(& newline), (long long )newline);
      __CrestLoad(30129, (unsigned long )0, (long long )0);
      __CrestApply2(30128, 13, (long long )(newline != 0));
# 1118 "dfa.c"
      if (newline != 0) {
        __CrestBranch(30131, 5052, 1);
        __CrestLoad(30133, (unsigned long )0, (long long )2);
        __CrestStore(30134, (unsigned long )(& tmp___1));
# 1118 "dfa.c"
        tmp___1 = 2;
      } else {
        __CrestBranch(30132, 5053, 0);
        __CrestLoad(30135, (unsigned long )0, (long long )0);
        __CrestStore(30136, (unsigned long )(& tmp___1));
# 1118 "dfa.c"
        tmp___1 = 0;
      }
      }
      {
      __CrestLoad(30145, (unsigned long )(& constraint), (long long )constraint);
      __CrestLoad(30144, (unsigned long )0, (long long )1);
      __CrestLoad(30143, (unsigned long )(& tmp___1), (long long )tmp___1);
      __CrestLoad(30142, (unsigned long )0, (long long )4);
      __CrestApply2(30141, 0, (long long )(tmp___1 + 4));
      __CrestApply2(30140, 8, (long long )(1 << (tmp___1 + 4)));
      __CrestApply2(30139, 5, (long long )(constraint & (1 << (tmp___1 + 4))));
      __CrestLoad(30138, (unsigned long )0, (long long )0);
      __CrestApply2(30137, 13, (long long )((constraint & (1 << (tmp___1 + 4))) != 0));
# 1118 "dfa.c"
      if ((constraint & (1 << (tmp___1 + 4))) != 0) {
        __CrestBranch(30146, 5055, 1);
        {
        __CrestLoad(30150, (unsigned long )(& letter), (long long )letter);
        __CrestLoad(30149, (unsigned long )0, (long long )0);
        __CrestApply2(30148, 13, (long long )(letter != 0));
# 1118 "dfa.c"
        if (letter != 0) {
          __CrestBranch(30151, 5056, 1);
          __CrestLoad(30153, (unsigned long )0, (long long )2);
          __CrestStore(30154, (unsigned long )(& tmp___2));
# 1118 "dfa.c"
          tmp___2 = 2;
        } else {
          __CrestBranch(30152, 5057, 0);
          __CrestLoad(30155, (unsigned long )0, (long long )0);
          __CrestStore(30156, (unsigned long )(& tmp___2));
# 1118 "dfa.c"
          tmp___2 = 0;
        }
        }
        {
        __CrestLoad(30163, (unsigned long )(& constraint), (long long )constraint);
        __CrestLoad(30162, (unsigned long )0, (long long )1);
        __CrestLoad(30161, (unsigned long )(& tmp___2), (long long )tmp___2);
        __CrestApply2(30160, 8, (long long )(1 << tmp___2));
        __CrestApply2(30159, 5, (long long )(constraint & (1 << tmp___2)));
        __CrestLoad(30158, (unsigned long )0, (long long )0);
        __CrestApply2(30157, 13, (long long )((constraint & (1 << tmp___2)) != 0));
# 1118 "dfa.c"
        if ((constraint & (1 << tmp___2)) != 0) {
          __CrestBranch(30164, 5059, 1);
# 1122 "dfa.c"
          mem_42 = d->states + i;
# 1122 "dfa.c"
          mem_43 = d->states + i;
          __CrestLoad(30168, (unsigned long )(& mem_43->constraint), (long long )mem_43->constraint);
          __CrestLoad(30167, (unsigned long )(& constraint), (long long )constraint);
          __CrestApply2(30166, 6, (long long )((int )mem_43->constraint | constraint));
          __CrestStore(30169, (unsigned long )(& mem_42->constraint));
# 1122 "dfa.c"
          mem_42->constraint = (unsigned char )((int )mem_43->constraint | constraint);
        } else {
          __CrestBranch(30165, 5060, 0);
# 1118 "dfa.c"
          goto _L___1;
        }
        }
      } else {
        __CrestBranch(30147, 5061, 0);
        _L___1:
        {
        __CrestLoad(30172, (unsigned long )(& newline), (long long )newline);
        __CrestLoad(30171, (unsigned long )0, (long long )0);
        __CrestApply2(30170, 13, (long long )(newline != 0));
# 1118 "dfa.c"
        if (newline != 0) {
          __CrestBranch(30173, 5062, 1);
          __CrestLoad(30175, (unsigned long )0, (long long )2);
          __CrestStore(30176, (unsigned long )(& tmp___3));
# 1118 "dfa.c"
          tmp___3 = 2;
        } else {
          __CrestBranch(30174, 5063, 0);
          __CrestLoad(30177, (unsigned long )0, (long long )0);
          __CrestStore(30178, (unsigned long )(& tmp___3));
# 1118 "dfa.c"
          tmp___3 = 0;
        }
        }
        {
        __CrestLoad(30187, (unsigned long )(& constraint), (long long )constraint);
        __CrestLoad(30186, (unsigned long )0, (long long )1);
        __CrestLoad(30185, (unsigned long )(& tmp___3), (long long )tmp___3);
        __CrestLoad(30184, (unsigned long )0, (long long )4);
        __CrestApply2(30183, 0, (long long )(tmp___3 + 4));
        __CrestApply2(30182, 8, (long long )(1 << (tmp___3 + 4)));
        __CrestApply2(30181, 5, (long long )(constraint & (1 << (tmp___3 + 4))));
        __CrestLoad(30180, (unsigned long )0, (long long )0);
        __CrestApply2(30179, 13, (long long )((constraint & (1 << (tmp___3 + 4))) != 0));
# 1118 "dfa.c"
        if ((constraint & (1 << (tmp___3 + 4))) != 0) {
          __CrestBranch(30188, 5065, 1);
          {
          __CrestLoad(30192, (unsigned long )(& letter), (long long )letter);
          __CrestLoad(30191, (unsigned long )0, (long long )0);
          __CrestApply2(30190, 13, (long long )(letter != 0));
# 1118 "dfa.c"
          if (letter != 0) {
            __CrestBranch(30193, 5066, 1);
            __CrestLoad(30195, (unsigned long )0, (long long )2);
            __CrestStore(30196, (unsigned long )(& tmp___4));
# 1118 "dfa.c"
            tmp___4 = 2;
          } else {
            __CrestBranch(30194, 5067, 0);
            __CrestLoad(30197, (unsigned long )0, (long long )0);
            __CrestStore(30198, (unsigned long )(& tmp___4));
# 1118 "dfa.c"
            tmp___4 = 0;
          }
          }
          {
          __CrestLoad(30207, (unsigned long )(& constraint), (long long )constraint);
          __CrestLoad(30206, (unsigned long )0, (long long )1);
          __CrestLoad(30205, (unsigned long )(& tmp___4), (long long )tmp___4);
          __CrestLoad(30204, (unsigned long )0, (long long )1);
          __CrestApply2(30203, 0, (long long )(tmp___4 + 1));
          __CrestApply2(30202, 8, (long long )(1 << (tmp___4 + 1)));
          __CrestApply2(30201, 5, (long long )(constraint & (1 << (tmp___4 + 1))));
          __CrestLoad(30200, (unsigned long )0, (long long )0);
          __CrestApply2(30199, 13, (long long )((constraint & (1 << (tmp___4 + 1))) != 0));
# 1118 "dfa.c"
          if ((constraint & (1 << (tmp___4 + 1))) != 0) {
            __CrestBranch(30208, 5069, 1);
# 1122 "dfa.c"
            mem_44 = d->states + i;
# 1122 "dfa.c"
            mem_45 = d->states + i;
            __CrestLoad(30212, (unsigned long )(& mem_45->constraint), (long long )mem_45->constraint);
            __CrestLoad(30211, (unsigned long )(& constraint), (long long )constraint);
            __CrestApply2(30210, 6, (long long )((int )mem_45->constraint | constraint));
            __CrestStore(30213, (unsigned long )(& mem_44->constraint));
# 1122 "dfa.c"
            mem_44->constraint = (unsigned char )((int )mem_45->constraint | constraint);
          } else {
            __CrestBranch(30209, 5070, 0);
# 1118 "dfa.c"
            goto _L___0;
          }
          }
        } else {
          __CrestBranch(30189, 5071, 0);
          _L___0:
          {
          __CrestLoad(30216, (unsigned long )(& newline), (long long )newline);
          __CrestLoad(30215, (unsigned long )0, (long long )0);
          __CrestApply2(30214, 13, (long long )(newline != 0));
# 1118 "dfa.c"
          if (newline != 0) {
            __CrestBranch(30217, 5072, 1);
            __CrestLoad(30219, (unsigned long )0, (long long )2);
            __CrestStore(30220, (unsigned long )(& tmp___5));
# 1118 "dfa.c"
            tmp___5 = 2;
          } else {
            __CrestBranch(30218, 5073, 0);
            __CrestLoad(30221, (unsigned long )0, (long long )0);
            __CrestStore(30222, (unsigned long )(& tmp___5));
# 1118 "dfa.c"
            tmp___5 = 0;
          }
          }
          {
          __CrestLoad(30233, (unsigned long )(& constraint), (long long )constraint);
          __CrestLoad(30232, (unsigned long )0, (long long )1);
          __CrestLoad(30231, (unsigned long )(& tmp___5), (long long )tmp___5);
          __CrestLoad(30230, (unsigned long )0, (long long )1);
          __CrestApply2(30229, 0, (long long )(tmp___5 + 1));
          __CrestLoad(30228, (unsigned long )0, (long long )4);
          __CrestApply2(30227, 0, (long long )((tmp___5 + 1) + 4));
          __CrestApply2(30226, 8, (long long )(1 << ((tmp___5 + 1) + 4)));
          __CrestApply2(30225, 5, (long long )(constraint & (1 << ((tmp___5 + 1) + 4))));
          __CrestLoad(30224, (unsigned long )0, (long long )0);
          __CrestApply2(30223, 13, (long long )((constraint & (1 << ((tmp___5 + 1) + 4))) != 0));
# 1118 "dfa.c"
          if ((constraint & (1 << ((tmp___5 + 1) + 4))) != 0) {
            __CrestBranch(30234, 5075, 1);
            {
            __CrestLoad(30238, (unsigned long )(& letter), (long long )letter);
            __CrestLoad(30237, (unsigned long )0, (long long )0);
            __CrestApply2(30236, 13, (long long )(letter != 0));
# 1118 "dfa.c"
            if (letter != 0) {
              __CrestBranch(30239, 5076, 1);
              __CrestLoad(30241, (unsigned long )0, (long long )2);
              __CrestStore(30242, (unsigned long )(& tmp___6));
# 1118 "dfa.c"
              tmp___6 = 2;
            } else {
              __CrestBranch(30240, 5077, 0);
              __CrestLoad(30243, (unsigned long )0, (long long )0);
              __CrestStore(30244, (unsigned long )(& tmp___6));
# 1118 "dfa.c"
              tmp___6 = 0;
            }
            }
            {
            __CrestLoad(30251, (unsigned long )(& constraint), (long long )constraint);
            __CrestLoad(30250, (unsigned long )0, (long long )1);
            __CrestLoad(30249, (unsigned long )(& tmp___6), (long long )tmp___6);
            __CrestApply2(30248, 8, (long long )(1 << tmp___6));
            __CrestApply2(30247, 5, (long long )(constraint & (1 << tmp___6)));
            __CrestLoad(30246, (unsigned long )0, (long long )0);
            __CrestApply2(30245, 13, (long long )((constraint & (1 << tmp___6)) != 0));
# 1118 "dfa.c"
            if ((constraint & (1 << tmp___6)) != 0) {
              __CrestBranch(30252, 5079, 1);
# 1122 "dfa.c"
              mem_46 = d->states + i;
# 1122 "dfa.c"
              mem_47 = d->states + i;
              __CrestLoad(30256, (unsigned long )(& mem_47->constraint), (long long )mem_47->constraint);
              __CrestLoad(30255, (unsigned long )(& constraint), (long long )constraint);
              __CrestApply2(30254, 6, (long long )((int )mem_47->constraint | constraint));
              __CrestStore(30257, (unsigned long )(& mem_46->constraint));
# 1122 "dfa.c"
              mem_46->constraint = (unsigned char )((int )mem_47->constraint | constraint);
            } else {
              __CrestBranch(30253, 5080, 0);
# 1118 "dfa.c"
              goto _L;
            }
            }
          } else {
            __CrestBranch(30235, 5081, 0);
            _L:
            {
            __CrestLoad(30260, (unsigned long )(& newline), (long long )newline);
            __CrestLoad(30259, (unsigned long )0, (long long )0);
            __CrestApply2(30258, 13, (long long )(newline != 0));
# 1118 "dfa.c"
            if (newline != 0) {
              __CrestBranch(30261, 5082, 1);
              __CrestLoad(30263, (unsigned long )0, (long long )2);
              __CrestStore(30264, (unsigned long )(& tmp___7));
# 1118 "dfa.c"
              tmp___7 = 2;
            } else {
              __CrestBranch(30262, 5083, 0);
              __CrestLoad(30265, (unsigned long )0, (long long )0);
              __CrestStore(30266, (unsigned long )(& tmp___7));
# 1118 "dfa.c"
              tmp___7 = 0;
            }
            }
            {
            __CrestLoad(30277, (unsigned long )(& constraint), (long long )constraint);
            __CrestLoad(30276, (unsigned long )0, (long long )1);
            __CrestLoad(30275, (unsigned long )(& tmp___7), (long long )tmp___7);
            __CrestLoad(30274, (unsigned long )0, (long long )1);
            __CrestApply2(30273, 0, (long long )(tmp___7 + 1));
            __CrestLoad(30272, (unsigned long )0, (long long )4);
            __CrestApply2(30271, 0, (long long )((tmp___7 + 1) + 4));
            __CrestApply2(30270, 8, (long long )(1 << ((tmp___7 + 1) + 4)));
            __CrestApply2(30269, 5, (long long )(constraint & (1 << ((tmp___7 + 1) + 4))));
            __CrestLoad(30268, (unsigned long )0, (long long )0);
            __CrestApply2(30267, 13, (long long )((constraint & (1 << ((tmp___7 + 1) + 4))) != 0));
# 1118 "dfa.c"
            if ((constraint & (1 << ((tmp___7 + 1) + 4))) != 0) {
              __CrestBranch(30278, 5085, 1);
              {
              __CrestLoad(30282, (unsigned long )(& letter), (long long )letter);
              __CrestLoad(30281, (unsigned long )0, (long long )0);
              __CrestApply2(30280, 13, (long long )(letter != 0));
# 1118 "dfa.c"
              if (letter != 0) {
                __CrestBranch(30283, 5086, 1);
                __CrestLoad(30285, (unsigned long )0, (long long )2);
                __CrestStore(30286, (unsigned long )(& tmp___8));
# 1118 "dfa.c"
                tmp___8 = 2;
              } else {
                __CrestBranch(30284, 5087, 0);
                __CrestLoad(30287, (unsigned long )0, (long long )0);
                __CrestStore(30288, (unsigned long )(& tmp___8));
# 1118 "dfa.c"
                tmp___8 = 0;
              }
              }
              {
              __CrestLoad(30297, (unsigned long )(& constraint), (long long )constraint);
              __CrestLoad(30296, (unsigned long )0, (long long )1);
              __CrestLoad(30295, (unsigned long )(& tmp___8), (long long )tmp___8);
              __CrestLoad(30294, (unsigned long )0, (long long )1);
              __CrestApply2(30293, 0, (long long )(tmp___8 + 1));
              __CrestApply2(30292, 8, (long long )(1 << (tmp___8 + 1)));
              __CrestApply2(30291, 5, (long long )(constraint & (1 << (tmp___8 + 1))));
              __CrestLoad(30290, (unsigned long )0, (long long )0);
              __CrestApply2(30289, 13, (long long )((constraint & (1 << (tmp___8 + 1))) != 0));
# 1118 "dfa.c"
              if ((constraint & (1 << (tmp___8 + 1))) != 0) {
                __CrestBranch(30298, 5089, 1);
# 1122 "dfa.c"
                mem_48 = d->states + i;
# 1122 "dfa.c"
                mem_49 = d->states + i;
                __CrestLoad(30302, (unsigned long )(& mem_49->constraint), (long long )mem_49->constraint);
                __CrestLoad(30301, (unsigned long )(& constraint), (long long )constraint);
                __CrestApply2(30300, 6, (long long )((int )mem_49->constraint | constraint));
                __CrestStore(30303, (unsigned long )(& mem_48->constraint));
# 1122 "dfa.c"
                mem_48->constraint = (unsigned char )((int )mem_49->constraint | constraint);
              } else {
                __CrestBranch(30299, 5090, 0);

              }
              }
            } else {
              __CrestBranch(30279, 5091, 0);

            }
            }
          }
          }
        }
        }
      }
      }
      {
# 1123 "dfa.c"
      mem_50 = d->states + i;
      {
      __CrestLoad(30306, (unsigned long )(& mem_50->first_end), (long long )mem_50->first_end);
      __CrestLoad(30305, (unsigned long )0, (long long )0);
      __CrestApply2(30304, 12, (long long )(mem_50->first_end == 0));
# 1123 "dfa.c"
      if (mem_50->first_end == 0) {
        __CrestBranch(30307, 5095, 1);
# 1124 "dfa.c"
        mem_51 = d->states + i;
# 1124 "dfa.c"
        mem_52 = s->elems + j;
# 1124 "dfa.c"
        mem_53 = d->tokens + mem_52->strchr;
        __CrestLoad(30309, (unsigned long )mem_53, (long long )*mem_53);
        __CrestStore(30310, (unsigned long )(& mem_51->first_end));
# 1124 "dfa.c"
        mem_51->first_end = (int )*mem_53;
      } else {
        __CrestBranch(30308, 5096, 0);

      }
      }
      }
    } else {
      __CrestBranch(30125, 5097, 0);
      {
# 1126 "dfa.c"
      mem_54 = s->elems + j;
# 1126 "dfa.c"
      mem_55 = d->tokens + mem_54->strchr;
      {
      __CrestLoad(30313, (unsigned long )mem_55, (long long )*mem_55);
      __CrestLoad(30312, (unsigned long )0, (long long )257);
      __CrestApply2(30311, 12, (long long )((int )*mem_55 == 257));
# 1126 "dfa.c"
      if ((int )*mem_55 == 257) {
        __CrestBranch(30314, 5100, 1);
# 1128 "dfa.c"
        mem_56 = d->states + i;
        __CrestLoad(30316, (unsigned long )0, (long long )(unsigned char)255);
        __CrestStore(30317, (unsigned long )(& mem_56->constraint));
# 1128 "dfa.c"
        mem_56->constraint = (unsigned char)255;
# 1129 "dfa.c"
        mem_57 = d->states + i;
        __CrestLoad(30318, (unsigned long )0, (long long )(char)1);
        __CrestStore(30319, (unsigned long )(& mem_57->backref));
# 1129 "dfa.c"
        mem_57->backref = (char)1;
      } else {
        __CrestBranch(30315, 5101, 0);

      }
      }
      }
    }
    }
    }
    __CrestLoad(30322, (unsigned long )(& j), (long long )j);
    __CrestLoad(30321, (unsigned long )0, (long long )1);
    __CrestApply2(30320, 0, (long long )(j + 1));
    __CrestStore(30323, (unsigned long )(& j));
# 1114 "dfa.c"
    j ++;
  }
  while_break___3: ;
  }
  __CrestLoad(30326, (unsigned long )(& d->sindex), (long long )d->sindex);
  __CrestLoad(30325, (unsigned long )0, (long long )1);
  __CrestApply2(30324, 0, (long long )(d->sindex + 1));
  __CrestStore(30327, (unsigned long )(& d->sindex));
# 1132 "dfa.c"
  (d->sindex) ++;
  __CrestLoad(30328, (unsigned long )(& i), (long long )i);
  __CrestStore(30329, (unsigned long )(& __retres58));
# 1134 "dfa.c"
  __retres58 = i;
  return_label:
  {
  __CrestLoad(30330, (unsigned long )(& __retres58), (long long )__retres58);
  __CrestReturn(30331);
# 1072 "dfa.c"
  return (__retres58);
  }
}
}
# 1142 "dfa.c"
static void epsclosure(position_set *s , struct dfa *d ) ;
# 1144 "dfa.c"
static void epsclosure(position_set *s , struct dfa *d )
{
  int i ;
  int j ;
  int *visited ;
  position p ;
  position old ;
  ptr_t tmp ;
  int *mem_9 ;
  position *mem_10 ;
  token *mem_11 ;
  position *mem_12 ;
  token *mem_13 ;
  position *mem_14 ;
  token *mem_15 ;
  position *mem_16 ;
  position *mem_17 ;
  int *mem_18 ;
  int *mem_19 ;
  token *mem_20 ;
  position_set *mem_21 ;
  position_set *mem_22 ;
  position *mem_23 ;

  {
  __CrestCall(30332, 251);

  __CrestLoad(30335, (unsigned long )(& d->tindex), (long long )d->tindex);
  __CrestLoad(30334, (unsigned long )0, (long long )sizeof(int ));
  __CrestApply2(30333, 2, (long long )((unsigned long )d->tindex * sizeof(int )));
# 1153 "dfa.c"
  tmp = xmalloc((unsigned long )d->tindex * sizeof(int ));
  __CrestClearStack(30336);
# 1153 "dfa.c"
  visited = (int *)tmp;
  __CrestLoad(30337, (unsigned long )0, (long long )0);
  __CrestStore(30338, (unsigned long )(& i));
# 1154 "dfa.c"
  i = 0;
  {
# 1154 "dfa.c"
  while (1) {
    while_continue: ;
    {
    __CrestLoad(30341, (unsigned long )(& i), (long long )i);
    __CrestLoad(30340, (unsigned long )(& d->tindex), (long long )d->tindex);
    __CrestApply2(30339, 16, (long long )(i < d->tindex));
# 1154 "dfa.c"
    if (i < d->tindex) {
      __CrestBranch(30342, 5112, 1);

    } else {
      __CrestBranch(30343, 5113, 0);
# 1154 "dfa.c"
      goto while_break;
    }
    }
# 1155 "dfa.c"
    mem_9 = visited + i;
    __CrestLoad(30344, (unsigned long )0, (long long )0);
    __CrestStore(30345, (unsigned long )mem_9);
# 1155 "dfa.c"
    *mem_9 = 0;
    __CrestLoad(30348, (unsigned long )(& i), (long long )i);
    __CrestLoad(30347, (unsigned long )0, (long long )1);
    __CrestApply2(30346, 0, (long long )(i + 1));
    __CrestStore(30349, (unsigned long )(& i));
# 1154 "dfa.c"
    i ++;
  }
  while_break: ;
  }
  __CrestLoad(30350, (unsigned long )0, (long long )0);
  __CrestStore(30351, (unsigned long )(& i));
# 1157 "dfa.c"
  i = 0;
  {
# 1157 "dfa.c"
  while (1) {
    while_continue___0: ;
    {
    __CrestLoad(30354, (unsigned long )(& i), (long long )i);
    __CrestLoad(30353, (unsigned long )(& s->nelem), (long long )s->nelem);
    __CrestApply2(30352, 16, (long long )(i < s->nelem));
# 1157 "dfa.c"
    if (i < s->nelem) {
      __CrestBranch(30355, 5121, 1);

    } else {
      __CrestBranch(30356, 5122, 0);
# 1157 "dfa.c"
      goto while_break___0;
    }
    }
    {
# 1158 "dfa.c"
    mem_10 = s->elems + i;
# 1158 "dfa.c"
    mem_11 = d->tokens + mem_10->strchr;
    {
    __CrestLoad(30359, (unsigned long )mem_11, (long long )*mem_11);
    __CrestLoad(30358, (unsigned long )0, (long long )(1 << 8));
    __CrestApply2(30357, 17, (long long )((int )*mem_11 >= 1 << 8));
# 1158 "dfa.c"
    if ((int )*mem_11 >= 1 << 8) {
      __CrestBranch(30360, 5126, 1);
      {
# 1158 "dfa.c"
      mem_12 = s->elems + i;
# 1158 "dfa.c"
      mem_13 = d->tokens + mem_12->strchr;
      {
      __CrestLoad(30364, (unsigned long )mem_13, (long long )*mem_13);
      __CrestLoad(30363, (unsigned long )0, (long long )257);
      __CrestApply2(30362, 13, (long long )((int )*mem_13 != 257));
# 1158 "dfa.c"
      if ((int )*mem_13 != 257) {
        __CrestBranch(30365, 5129, 1);
        {
# 1158 "dfa.c"
        mem_14 = s->elems + i;
# 1158 "dfa.c"
        mem_15 = d->tokens + mem_14->strchr;
        {
        __CrestLoad(30369, (unsigned long )mem_15, (long long )*mem_15);
        __CrestLoad(30368, (unsigned long )0, (long long )273);
        __CrestApply2(30367, 16, (long long )((int )*mem_15 < 273));
# 1158 "dfa.c"
        if ((int )*mem_15 < 273) {
          __CrestBranch(30370, 5132, 1);
# 1162 "dfa.c"
          mem_16 = s->elems + i;
# 1162 "dfa.c"
          old = *mem_16;
          __CrestLoad(30372, (unsigned long )(& old.constraint), (long long )old.constraint);
          __CrestStore(30373, (unsigned long )(& p.constraint));
# 1163 "dfa.c"
          p.constraint = old.constraint;
# 1164 "dfa.c"
          mem_17 = s->elems + i;
# 1164 "dfa.c"
          delete(*mem_17, s);
          __CrestClearStack(30374);
          {
# 1165 "dfa.c"
          mem_18 = visited + old.strchr;
          {
          __CrestLoad(30377, (unsigned long )mem_18, (long long )*mem_18);
          __CrestLoad(30376, (unsigned long )0, (long long )0);
          __CrestApply2(30375, 13, (long long )(*mem_18 != 0));
# 1165 "dfa.c"
          if (*mem_18 != 0) {
            __CrestBranch(30378, 5136, 1);
            __CrestLoad(30382, (unsigned long )(& i), (long long )i);
            __CrestLoad(30381, (unsigned long )0, (long long )1);
            __CrestApply2(30380, 1, (long long )(i - 1));
            __CrestStore(30383, (unsigned long )(& i));
# 1167 "dfa.c"
            i --;
# 1168 "dfa.c"
            goto __Cont;
          } else {
            __CrestBranch(30379, 5138, 0);

          }
          }
          }
# 1170 "dfa.c"
          mem_19 = visited + old.strchr;
          __CrestLoad(30384, (unsigned long )0, (long long )1);
          __CrestStore(30385, (unsigned long )mem_19);
# 1170 "dfa.c"
          *mem_19 = 1;
          {
# 1171 "dfa.c"
          mem_20 = d->tokens + old.strchr;
          {
          {
          __CrestLoad(30388, (unsigned long )mem_20, (long long )*mem_20);
          __CrestLoad(30387, (unsigned long )0, (long long )258);
          __CrestApply2(30386, 12, (long long )((int )*mem_20 == 258));
# 1173 "dfa.c"
          if ((int )*mem_20 == 258) {
            __CrestBranch(30389, 5144, 1);
# 1173 "dfa.c"
            goto case_258;
          } else {
            __CrestBranch(30390, 5145, 0);

          }
          }
          {
          __CrestLoad(30393, (unsigned long )mem_20, (long long )*mem_20);
          __CrestLoad(30392, (unsigned long )0, (long long )259);
          __CrestApply2(30391, 12, (long long )((int )*mem_20 == 259));
# 1176 "dfa.c"
          if ((int )*mem_20 == 259) {
            __CrestBranch(30394, 5147, 1);
# 1176 "dfa.c"
            goto case_259;
          } else {
            __CrestBranch(30395, 5148, 0);

          }
          }
          {
          __CrestLoad(30398, (unsigned long )mem_20, (long long )*mem_20);
          __CrestLoad(30397, (unsigned long )0, (long long )260);
          __CrestApply2(30396, 12, (long long )((int )*mem_20 == 260));
# 1179 "dfa.c"
          if ((int )*mem_20 == 260) {
            __CrestBranch(30399, 5150, 1);
# 1179 "dfa.c"
            goto case_260;
          } else {
            __CrestBranch(30400, 5151, 0);

          }
          }
          {
          __CrestLoad(30403, (unsigned long )mem_20, (long long )*mem_20);
          __CrestLoad(30402, (unsigned long )0, (long long )261);
          __CrestApply2(30401, 12, (long long )((int )*mem_20 == 261));
# 1182 "dfa.c"
          if ((int )*mem_20 == 261) {
            __CrestBranch(30404, 5153, 1);
# 1182 "dfa.c"
            goto case_261;
          } else {
            __CrestBranch(30405, 5154, 0);

          }
          }
          {
          __CrestLoad(30408, (unsigned long )mem_20, (long long )*mem_20);
          __CrestLoad(30407, (unsigned long )0, (long long )262);
          __CrestApply2(30406, 12, (long long )((int )*mem_20 == 262));
# 1185 "dfa.c"
          if ((int )*mem_20 == 262) {
            __CrestBranch(30409, 5156, 1);
# 1185 "dfa.c"
            goto case_262;
          } else {
            __CrestBranch(30410, 5157, 0);

          }
          }
          {
          __CrestLoad(30413, (unsigned long )mem_20, (long long )*mem_20);
          __CrestLoad(30412, (unsigned long )0, (long long )263);
          __CrestApply2(30411, 12, (long long )((int )*mem_20 == 263));
# 1188 "dfa.c"
          if ((int )*mem_20 == 263) {
            __CrestBranch(30414, 5159, 1);
# 1188 "dfa.c"
            goto case_263;
          } else {
            __CrestBranch(30415, 5160, 0);

          }
          }
# 1191 "dfa.c"
          goto switch_default;
          case_258:
          __CrestLoad(30418, (unsigned long )(& p.constraint), (long long )p.constraint);
          __CrestLoad(30417, (unsigned long )0, (long long )207U);
          __CrestApply2(30416, 5, (long long )(p.constraint & 207U));
          __CrestStore(30419, (unsigned long )(& p.constraint));
# 1174 "dfa.c"
          p.constraint &= 207U;
# 1175 "dfa.c"
          goto switch_break;
          case_259:
          __CrestLoad(30422, (unsigned long )(& p.constraint), (long long )p.constraint);
          __CrestLoad(30421, (unsigned long )0, (long long )175U);
          __CrestApply2(30420, 5, (long long )(p.constraint & 175U));
          __CrestStore(30423, (unsigned long )(& p.constraint));
# 1177 "dfa.c"
          p.constraint &= 175U;
# 1178 "dfa.c"
          goto switch_break;
          case_260:
          __CrestLoad(30426, (unsigned long )(& p.constraint), (long long )p.constraint);
          __CrestLoad(30425, (unsigned long )0, (long long )242U);
          __CrestApply2(30424, 5, (long long )(p.constraint & 242U));
          __CrestStore(30427, (unsigned long )(& p.constraint));
# 1180 "dfa.c"
          p.constraint &= 242U;
# 1181 "dfa.c"
          goto switch_break;
          case_261:
          __CrestLoad(30430, (unsigned long )(& p.constraint), (long long )p.constraint);
          __CrestLoad(30429, (unsigned long )0, (long long )244U);
          __CrestApply2(30428, 5, (long long )(p.constraint & 244U));
          __CrestStore(30431, (unsigned long )(& p.constraint));
# 1183 "dfa.c"
          p.constraint &= 244U;
# 1184 "dfa.c"
          goto switch_break;
          case_262:
          __CrestLoad(30434, (unsigned long )(& p.constraint), (long long )p.constraint);
          __CrestLoad(30433, (unsigned long )0, (long long )246U);
          __CrestApply2(30432, 5, (long long )(p.constraint & 246U));
          __CrestStore(30435, (unsigned long )(& p.constraint));
# 1186 "dfa.c"
          p.constraint &= 246U;
# 1187 "dfa.c"
          goto switch_break;
          case_263:
          __CrestLoad(30438, (unsigned long )(& p.constraint), (long long )p.constraint);
          __CrestLoad(30437, (unsigned long )0, (long long )249U);
          __CrestApply2(30436, 5, (long long )(p.constraint & 249U));
          __CrestStore(30439, (unsigned long )(& p.constraint));
# 1189 "dfa.c"
          p.constraint &= 249U;
# 1190 "dfa.c"
          goto switch_break;
          switch_default:
# 1192 "dfa.c"
          goto switch_break;
          switch_break: ;
          }
          }
          __CrestLoad(30440, (unsigned long )0, (long long )0);
          __CrestStore(30441, (unsigned long )(& j));
# 1194 "dfa.c"
          j = 0;
          {
# 1194 "dfa.c"
          while (1) {
            while_continue___1: ;
            {
# 1194 "dfa.c"
            mem_21 = d->follows + old.strchr;
            {
            __CrestLoad(30444, (unsigned long )(& j), (long long )j);
            __CrestLoad(30443, (unsigned long )(& mem_21->nelem), (long long )mem_21->nelem);
            __CrestApply2(30442, 16, (long long )(j < mem_21->nelem));
# 1194 "dfa.c"
            if (j < mem_21->nelem) {
              __CrestBranch(30445, 5183, 1);

            } else {
              __CrestBranch(30446, 5184, 0);
# 1194 "dfa.c"
              goto while_break___1;
            }
            }
            }
# 1196 "dfa.c"
            mem_22 = d->follows + old.strchr;
# 1196 "dfa.c"
            mem_23 = mem_22->elems + j;
            __CrestLoad(30447, (unsigned long )(& mem_23->strchr), (long long )mem_23->strchr);
            __CrestStore(30448, (unsigned long )(& p.strchr));
# 1196 "dfa.c"
            p.strchr = mem_23->strchr;
# 1197 "dfa.c"
            insert(p, s);
            __CrestClearStack(30449);
            __CrestLoad(30452, (unsigned long )(& j), (long long )j);
            __CrestLoad(30451, (unsigned long )0, (long long )1);
            __CrestApply2(30450, 0, (long long )(j + 1));
            __CrestStore(30453, (unsigned long )(& j));
# 1194 "dfa.c"
            j ++;
          }
          while_break___1: ;
          }
          __CrestLoad(30454, (unsigned long )0, (long long )-1);
          __CrestStore(30455, (unsigned long )(& i));
# 1200 "dfa.c"
          i = -1;
        } else {
          __CrestBranch(30371, 5188, 0);

        }
        }
        }
      } else {
        __CrestBranch(30366, 5189, 0);

      }
      }
      }
    } else {
      __CrestBranch(30361, 5190, 0);

    }
    }
    }
    __Cont:
    __CrestLoad(30458, (unsigned long )(& i), (long long )i);
    __CrestLoad(30457, (unsigned long )0, (long long )1);
    __CrestApply2(30456, 0, (long long )(i + 1));
    __CrestStore(30459, (unsigned long )(& i));
# 1157 "dfa.c"
    i ++;
  }
  while_break___0: ;
  }
# 1203 "dfa.c"
  free((void *)visited);
  __CrestClearStack(30460);

  {
  __CrestReturn(30461);
# 1144 "dfa.c"
  return;
  }
}
}
# 1258 "dfa.c"
void dfaanalyze(struct dfa *d , int searchflag )
{
  int *nullable ;
  int *nfirstpos ;
  position *firstpos ;
  int *nlastpos ;
  position *lastpos ;
  int *nalloc ;
  position_set tmp ;
  position_set merged ;
  int wants_newline ;
  int *o_nullable ;
  int *o_nfirst ;
  int *o_nlast ;
  position *o_firstpos ;
  position *o_lastpos ;
  int i ;
  int j ;
  position *pos ;
  ptr_t tmp___0 ;
  ptr_t tmp___1 ;
  ptr_t tmp___2 ;
  ptr_t tmp___3 ;
  ptr_t tmp___4 ;
  ptr_t tmp___5 ;
  ptr_t tmp___6 ;
  ptr_t tmp___7 ;
  int *tmp___8 ;
  int *tmp___9 ;
  int *tmp___10 ;
  int tmp___11 ;
  ptr_t tmp___12 ;
  ptr_t tmp___13 ;
  int tmp___14 ;
  int tmp___15 ;
  int *tmp___16 ;
  int *tmp___17 ;
  int *tmp___18 ;
  int tmp___19 ;
  unsigned int tmp___20 ;
  unsigned int tmp___21 ;
  ptr_t tmp___22 ;
  ptr_t tmp___23 ;
  ptr_t tmp___24 ;
  int *mem_45 ;
  token *mem_46 ;
  int *mem_47 ;
  int *mem_48 ;
  position *mem_49 ;
  position *mem_50 ;
  int *mem_51 ;
  position *mem_52 ;
  int *mem_53 ;
  position *mem_54 ;
  int *mem_55 ;
  position *mem_56 ;
  int *mem_57 ;
  position *mem_58 ;
  position_set *mem_59 ;
  position *mem_60 ;
  int *mem_61 ;
  position *mem_62 ;
  position_set *mem_63 ;
  position *mem_64 ;
  token *mem_65 ;
  int *mem_66 ;
  int *mem_67 ;
  int *mem_68 ;
  int *mem_69 ;
  position *mem_70 ;
  position *mem_71 ;
  int *mem_72 ;
  position *mem_73 ;
  int *mem_74 ;
  position *mem_75 ;
  int *mem_76 ;
  position *mem_77 ;
  int *mem_78 ;
  position *mem_79 ;
  position_set *mem_80 ;
  position *mem_81 ;
  int *mem_82 ;
  position *mem_83 ;
  position_set *mem_84 ;
  position *mem_85 ;
  int *mem_86 ;
  int *mem_87 ;
  int *mem_88 ;
  int *mem_89 ;
  int *mem_90 ;
  int *mem_91 ;
  int *mem_92 ;
  int *mem_93 ;
  int *mem_94 ;
  int *mem_95 ;
  int *mem_96 ;
  position *mem_97 ;
  position *mem_98 ;
  int *mem_99 ;
  int *mem_100 ;
  int *mem_101 ;
  int *mem_102 ;
  int *mem_103 ;
  int *mem_104 ;
  int *mem_105 ;
  int *mem_106 ;
  int *mem_107 ;
  int *mem_108 ;
  int *mem_109 ;
  int *mem_110 ;
  int *mem_111 ;
  int *mem_112 ;
  int *mem_113 ;
  token *mem_114 ;
  int *mem_115 ;
  int *mem_116 ;
  position_set *mem_117 ;
  token *mem_118 ;
  token *mem_119 ;
  token *mem_120 ;
  position_set *mem_121 ;
  position_set *mem_122 ;
  position_set *mem_123 ;
  int *mem_124 ;
  position *mem_125 ;
  position *mem_126 ;
  position *mem_127 ;

  {
  __CrestCall(30463, 252);
  __CrestStore(30462, (unsigned long )(& searchflag));
  __CrestLoad(30464, (unsigned long )(& searchflag), (long long )searchflag);
  __CrestStore(30465, (unsigned long )(& d->searchflag));
# 1288 "dfa.c"
  d->searchflag = searchflag;
  __CrestLoad(30468, (unsigned long )(& d->depth), (long long )d->depth);
  __CrestLoad(30467, (unsigned long )0, (long long )sizeof(int ));
  __CrestApply2(30466, 2, (long long )((unsigned long )d->depth * sizeof(int )));
# 1290 "dfa.c"
  tmp___0 = xmalloc((unsigned long )d->depth * sizeof(int ));
  __CrestClearStack(30469);
# 1290 "dfa.c"
  nullable = (int *)tmp___0;
# 1291 "dfa.c"
  o_nullable = nullable;
  __CrestLoad(30472, (unsigned long )(& d->depth), (long long )d->depth);
  __CrestLoad(30471, (unsigned long )0, (long long )sizeof(int ));
  __CrestApply2(30470, 2, (long long )((unsigned long )d->depth * sizeof(int )));
# 1292 "dfa.c"
  tmp___1 = xmalloc((unsigned long )d->depth * sizeof(int ));
  __CrestClearStack(30473);
# 1292 "dfa.c"
  nfirstpos = (int *)tmp___1;
# 1293 "dfa.c"
  o_nfirst = nfirstpos;
  __CrestLoad(30476, (unsigned long )(& d->nleaves), (long long )d->nleaves);
  __CrestLoad(30475, (unsigned long )0, (long long )sizeof(position ));
  __CrestApply2(30474, 2, (long long )((unsigned long )d->nleaves * sizeof(position )));
# 1294 "dfa.c"
  tmp___2 = xmalloc((unsigned long )d->nleaves * sizeof(position ));
  __CrestClearStack(30477);
# 1294 "dfa.c"
  firstpos = (position *)tmp___2;
# 1295 "dfa.c"
  o_firstpos = firstpos;
# 1295 "dfa.c"
  firstpos += d->nleaves;
  __CrestLoad(30480, (unsigned long )(& d->depth), (long long )d->depth);
  __CrestLoad(30479, (unsigned long )0, (long long )sizeof(int ));
  __CrestApply2(30478, 2, (long long )((unsigned long )d->depth * sizeof(int )));
# 1296 "dfa.c"
  tmp___3 = xmalloc((unsigned long )d->depth * sizeof(int ));
  __CrestClearStack(30481);
# 1296 "dfa.c"
  nlastpos = (int *)tmp___3;
# 1297 "dfa.c"
  o_nlast = nlastpos;
  __CrestLoad(30484, (unsigned long )(& d->nleaves), (long long )d->nleaves);
  __CrestLoad(30483, (unsigned long )0, (long long )sizeof(position ));
  __CrestApply2(30482, 2, (long long )((unsigned long )d->nleaves * sizeof(position )));
# 1298 "dfa.c"
  tmp___4 = xmalloc((unsigned long )d->nleaves * sizeof(position ));
  __CrestClearStack(30485);
# 1298 "dfa.c"
  lastpos = (position *)tmp___4;
# 1299 "dfa.c"
  o_lastpos = lastpos;
# 1299 "dfa.c"
  lastpos += d->nleaves;
  __CrestLoad(30488, (unsigned long )(& d->tindex), (long long )d->tindex);
  __CrestLoad(30487, (unsigned long )0, (long long )sizeof(int ));
  __CrestApply2(30486, 2, (long long )((unsigned long )d->tindex * sizeof(int )));
# 1300 "dfa.c"
  tmp___5 = xmalloc((unsigned long )d->tindex * sizeof(int ));
  __CrestClearStack(30489);
# 1300 "dfa.c"
  nalloc = (int *)tmp___5;
  __CrestLoad(30490, (unsigned long )0, (long long )0);
  __CrestStore(30491, (unsigned long )(& i));
# 1301 "dfa.c"
  i = 0;
  {
# 1301 "dfa.c"
  while (1) {
    while_continue: ;
    {
    __CrestLoad(30494, (unsigned long )(& i), (long long )i);
    __CrestLoad(30493, (unsigned long )(& d->tindex), (long long )d->tindex);
    __CrestApply2(30492, 16, (long long )(i < d->tindex));
# 1301 "dfa.c"
    if (i < d->tindex) {
      __CrestBranch(30495, 5201, 1);

    } else {
      __CrestBranch(30496, 5202, 0);
# 1301 "dfa.c"
      goto while_break;
    }
    }
# 1302 "dfa.c"
    mem_45 = nalloc + i;
    __CrestLoad(30497, (unsigned long )0, (long long )0);
    __CrestStore(30498, (unsigned long )mem_45);
# 1302 "dfa.c"
    *mem_45 = 0;
    __CrestLoad(30501, (unsigned long )(& i), (long long )i);
    __CrestLoad(30500, (unsigned long )0, (long long )1);
    __CrestApply2(30499, 0, (long long )(i + 1));
    __CrestStore(30502, (unsigned long )(& i));
# 1301 "dfa.c"
    i ++;
  }
  while_break: ;
  }
  __CrestLoad(30505, (unsigned long )(& d->nleaves), (long long )d->nleaves);
  __CrestLoad(30504, (unsigned long )0, (long long )sizeof(position ));
  __CrestApply2(30503, 2, (long long )((unsigned long )d->nleaves * sizeof(position )));
# 1303 "dfa.c"
  tmp___6 = xmalloc((unsigned long )d->nleaves * sizeof(position ));
  __CrestClearStack(30506);
# 1303 "dfa.c"
  merged.elems = (position *)tmp___6;
  __CrestLoad(30507, (unsigned long )(& d->tindex), (long long )d->tindex);
  __CrestLoad(30508, (unsigned long )0, (long long )sizeof(position_set ));
# 1305 "dfa.c"
  tmp___7 = xcalloc((size_t )d->tindex, sizeof(position_set ));
  __CrestClearStack(30509);
# 1305 "dfa.c"
  d->follows = (position_set *)tmp___7;
  __CrestLoad(30510, (unsigned long )0, (long long )0);
  __CrestStore(30511, (unsigned long )(& i));
# 1307 "dfa.c"
  i = 0;
  {
# 1307 "dfa.c"
  while (1) {
    while_continue___0: ;
    {
    __CrestLoad(30514, (unsigned long )(& i), (long long )i);
    __CrestLoad(30513, (unsigned long )(& d->tindex), (long long )d->tindex);
    __CrestApply2(30512, 16, (long long )(i < d->tindex));
# 1307 "dfa.c"
    if (i < d->tindex) {
      __CrestBranch(30515, 5210, 1);

    } else {
      __CrestBranch(30516, 5211, 0);
# 1307 "dfa.c"
      goto while_break___0;
    }
    }
    {
# 1311 "dfa.c"
    mem_46 = d->tokens + i;
    {
    {
    __CrestLoad(30519, (unsigned long )mem_46, (long long )*mem_46);
    __CrestLoad(30518, (unsigned long )0, (long long )256);
    __CrestApply2(30517, 12, (long long )((int )*mem_46 == 256));
# 1313 "dfa.c"
    if ((int )*mem_46 == 256) {
      __CrestBranch(30520, 5216, 1);
# 1313 "dfa.c"
      goto case_256;
    } else {
      __CrestBranch(30521, 5217, 0);

    }
    }
    {
    __CrestLoad(30524, (unsigned long )mem_46, (long long )*mem_46);
    __CrestLoad(30523, (unsigned long )0, (long long )266);
    __CrestApply2(30522, 12, (long long )((int )*mem_46 == 266));
# 1322 "dfa.c"
    if ((int )*mem_46 == 266) {
      __CrestBranch(30525, 5219, 1);
# 1322 "dfa.c"
      goto case_266;
    } else {
      __CrestBranch(30526, 5220, 0);

    }
    }
    {
    __CrestLoad(30529, (unsigned long )mem_46, (long long )*mem_46);
    __CrestLoad(30528, (unsigned long )0, (long long )265);
    __CrestApply2(30527, 12, (long long )((int )*mem_46 == 265));
# 1322 "dfa.c"
    if ((int )*mem_46 == 265) {
      __CrestBranch(30530, 5222, 1);
# 1322 "dfa.c"
      goto case_266;
    } else {
      __CrestBranch(30531, 5223, 0);

    }
    }
    {
    __CrestLoad(30534, (unsigned long )mem_46, (long long )*mem_46);
    __CrestLoad(30533, (unsigned long )0, (long long )264);
    __CrestApply2(30532, 12, (long long )((int )*mem_46 == 264));
# 1336 "dfa.c"
    if ((int )*mem_46 == 264) {
      __CrestBranch(30535, 5225, 1);
# 1336 "dfa.c"
      goto case_264;
    } else {
      __CrestBranch(30536, 5226, 0);

    }
    }
    {
    __CrestLoad(30539, (unsigned long )mem_46, (long long )*mem_46);
    __CrestLoad(30538, (unsigned long )0, (long long )268);
    __CrestApply2(30537, 12, (long long )((int )*mem_46 == 268));
# 1342 "dfa.c"
    if ((int )*mem_46 == 268) {
      __CrestBranch(30540, 5228, 1);
# 1342 "dfa.c"
      goto case_268;
    } else {
      __CrestBranch(30541, 5229, 0);

    }
    }
    {
    __CrestLoad(30544, (unsigned long )mem_46, (long long )*mem_46);
    __CrestLoad(30543, (unsigned long )0, (long long )270);
    __CrestApply2(30542, 12, (long long )((int )*mem_46 == 270));
# 1384 "dfa.c"
    if ((int )*mem_46 == 270) {
      __CrestBranch(30545, 5231, 1);
# 1384 "dfa.c"
      goto case_270;
    } else {
      __CrestBranch(30546, 5232, 0);

    }
    }
    {
    __CrestLoad(30549, (unsigned long )mem_46, (long long )*mem_46);
    __CrestLoad(30548, (unsigned long )0, (long long )269);
    __CrestApply2(30547, 12, (long long )((int )*mem_46 == 269));
# 1384 "dfa.c"
    if ((int )*mem_46 == 269) {
      __CrestBranch(30550, 5234, 1);
# 1384 "dfa.c"
      goto case_270;
    } else {
      __CrestBranch(30551, 5235, 0);

    }
    }
# 1398 "dfa.c"
    goto switch_default;
    case_256:
# 1315 "dfa.c"
    tmp___8 = nullable;
# 1315 "dfa.c"
    nullable ++;
    __CrestLoad(30552, (unsigned long )0, (long long )1);
    __CrestStore(30553, (unsigned long )tmp___8);
# 1315 "dfa.c"
    *tmp___8 = 1;
# 1318 "dfa.c"
    tmp___9 = nfirstpos;
# 1318 "dfa.c"
    nfirstpos ++;
# 1318 "dfa.c"
    tmp___10 = nlastpos;
# 1318 "dfa.c"
    nlastpos ++;
    __CrestLoad(30554, (unsigned long )0, (long long )0);
    __CrestStore(30555, (unsigned long )(& tmp___11));
# 1318 "dfa.c"
    tmp___11 = 0;
    __CrestLoad(30556, (unsigned long )(& tmp___11), (long long )tmp___11);
    __CrestStore(30557, (unsigned long )tmp___10);
# 1318 "dfa.c"
    *tmp___10 = tmp___11;
    __CrestLoad(30558, (unsigned long )(& tmp___11), (long long )tmp___11);
    __CrestStore(30559, (unsigned long )tmp___9);
# 1318 "dfa.c"
    *tmp___9 = tmp___11;
# 1319 "dfa.c"
    goto switch_break;
    case_266:
    case_265:
# 1325 "dfa.c"
    mem_47 = nfirstpos + -1;
    __CrestLoad(30560, (unsigned long )mem_47, (long long )*mem_47);
    __CrestStore(30561, (unsigned long )(& tmp.nelem));
# 1325 "dfa.c"
    tmp.nelem = *mem_47;
# 1326 "dfa.c"
    tmp.elems = firstpos;
# 1327 "dfa.c"
    pos = lastpos;
    __CrestLoad(30562, (unsigned long )0, (long long )0);
    __CrestStore(30563, (unsigned long )(& j));
# 1328 "dfa.c"
    j = 0;
    {
# 1328 "dfa.c"
    while (1) {
      while_continue___1: ;
      {
# 1328 "dfa.c"
      mem_48 = nlastpos + -1;
      {
      __CrestLoad(30566, (unsigned long )(& j), (long long )j);
      __CrestLoad(30565, (unsigned long )mem_48, (long long )*mem_48);
      __CrestApply2(30564, 16, (long long )(j < *mem_48));
# 1328 "dfa.c"
      if (j < *mem_48) {
        __CrestBranch(30567, 5246, 1);

      } else {
        __CrestBranch(30568, 5247, 0);
# 1328 "dfa.c"
        goto while_break___1;
      }
      }
      }
# 1330 "dfa.c"
      mem_49 = pos + j;
# 1330 "dfa.c"
      merge(& tmp, d->follows + mem_49->strchr, & merged);
      __CrestClearStack(30569);
      {
# 1331 "dfa.c"
      mem_50 = pos + j;
# 1331 "dfa.c"
      mem_51 = nalloc + mem_50->strchr;
      {
      __CrestLoad(30574, (unsigned long )(& merged.nelem), (long long )merged.nelem);
      __CrestLoad(30573, (unsigned long )0, (long long )1);
      __CrestApply2(30572, 1, (long long )(merged.nelem - 1));
      __CrestLoad(30571, (unsigned long )mem_51, (long long )*mem_51);
      __CrestApply2(30570, 17, (long long )(merged.nelem - 1 >= *mem_51));
# 1331 "dfa.c"
      if (merged.nelem - 1 >= *mem_51) {
        __CrestBranch(30575, 5252, 1);
        {
# 1331 "dfa.c"
        while (1) {
          while_continue___2: ;
          {
# 1331 "dfa.c"
          mem_52 = pos + j;
# 1331 "dfa.c"
          mem_53 = nalloc + mem_52->strchr;
          {
          __CrestLoad(30581, (unsigned long )(& merged.nelem), (long long )merged.nelem);
          __CrestLoad(30580, (unsigned long )0, (long long )1);
          __CrestApply2(30579, 1, (long long )(merged.nelem - 1));
          __CrestLoad(30578, (unsigned long )mem_53, (long long )*mem_53);
          __CrestApply2(30577, 17, (long long )(merged.nelem - 1 >= *mem_53));
# 1331 "dfa.c"
          if (merged.nelem - 1 >= *mem_53) {
            __CrestBranch(30582, 5258, 1);

          } else {
            __CrestBranch(30583, 5259, 0);
# 1331 "dfa.c"
            goto while_break___2;
          }
          }
          }
# 1331 "dfa.c"
          mem_54 = pos + j;
# 1331 "dfa.c"
          mem_55 = nalloc + mem_54->strchr;
# 1331 "dfa.c"
          mem_56 = pos + j;
# 1331 "dfa.c"
          mem_57 = nalloc + mem_56->strchr;
          __CrestLoad(30586, (unsigned long )mem_57, (long long )*mem_57);
          __CrestLoad(30585, (unsigned long )0, (long long )2);
          __CrestApply2(30584, 2, (long long )(*mem_57 * 2));
          __CrestStore(30587, (unsigned long )mem_55);
# 1331 "dfa.c"
          *mem_55 = *mem_57 * 2;
        }
        while_break___2: ;
        }
# 1331 "dfa.c"
        mem_58 = pos + j;
# 1331 "dfa.c"
        mem_59 = d->follows + mem_58->strchr;
# 1331 "dfa.c"
        mem_60 = pos + j;
# 1331 "dfa.c"
        mem_61 = nalloc + mem_60->strchr;
        __CrestLoad(30590, (unsigned long )mem_61, (long long )*mem_61);
        __CrestLoad(30589, (unsigned long )0, (long long )sizeof(position ));
        __CrestApply2(30588, 2, (long long )((unsigned long )*mem_61 * sizeof(position )));
# 1331 "dfa.c"
        tmp___12 = xrealloc((ptr_t )mem_59->elems, (unsigned long )*mem_61 * sizeof(position ));
        __CrestClearStack(30591);
# 1331 "dfa.c"
        mem_62 = pos + j;
# 1331 "dfa.c"
        mem_63 = d->follows + mem_62->strchr;
# 1331 "dfa.c"
        mem_63->elems = (position *)tmp___12;
      } else {
        __CrestBranch(30576, 5263, 0);

      }
      }
      }
# 1333 "dfa.c"
      mem_64 = pos + j;
# 1333 "dfa.c"
      copy(& merged, d->follows + mem_64->strchr);
      __CrestClearStack(30592);
      __CrestLoad(30595, (unsigned long )(& j), (long long )j);
      __CrestLoad(30594, (unsigned long )0, (long long )1);
      __CrestApply2(30593, 0, (long long )(j + 1));
      __CrestStore(30596, (unsigned long )(& j));
# 1328 "dfa.c"
      j ++;
    }
    while_break___1: ;
    }
    case_264:
    {
# 1338 "dfa.c"
    mem_65 = d->tokens + i;
    {
    __CrestLoad(30599, (unsigned long )mem_65, (long long )*mem_65);
    __CrestLoad(30598, (unsigned long )0, (long long )266);
    __CrestApply2(30597, 13, (long long )((int )*mem_65 != 266));
# 1338 "dfa.c"
    if ((int )*mem_65 != 266) {
      __CrestBranch(30600, 5269, 1);
# 1339 "dfa.c"
      mem_66 = nullable + -1;
      __CrestLoad(30602, (unsigned long )0, (long long )1);
      __CrestStore(30603, (unsigned long )mem_66);
# 1339 "dfa.c"
      *mem_66 = 1;
    } else {
      __CrestBranch(30601, 5270, 0);

    }
    }
    }
# 1340 "dfa.c"
    goto switch_break;
    case_268:
# 1345 "dfa.c"
    mem_67 = nfirstpos + -1;
    __CrestLoad(30604, (unsigned long )mem_67, (long long )*mem_67);
    __CrestStore(30605, (unsigned long )(& tmp.nelem));
# 1345 "dfa.c"
    tmp.nelem = *mem_67;
# 1346 "dfa.c"
    tmp.elems = firstpos;
# 1347 "dfa.c"
    mem_68 = nlastpos + -1;
# 1347 "dfa.c"
    pos = lastpos + *mem_68;
    __CrestLoad(30606, (unsigned long )0, (long long )0);
    __CrestStore(30607, (unsigned long )(& j));
# 1348 "dfa.c"
    j = 0;
    {
# 1348 "dfa.c"
    while (1) {
      while_continue___3: ;
      {
# 1348 "dfa.c"
      mem_69 = nlastpos + -2;
      {
      __CrestLoad(30610, (unsigned long )(& j), (long long )j);
      __CrestLoad(30609, (unsigned long )mem_69, (long long )*mem_69);
      __CrestApply2(30608, 16, (long long )(j < *mem_69));
# 1348 "dfa.c"
      if (j < *mem_69) {
        __CrestBranch(30611, 5279, 1);

      } else {
        __CrestBranch(30612, 5280, 0);
# 1348 "dfa.c"
        goto while_break___3;
      }
      }
      }
# 1350 "dfa.c"
      mem_70 = pos + j;
# 1350 "dfa.c"
      merge(& tmp, d->follows + mem_70->strchr, & merged);
      __CrestClearStack(30613);
      {
# 1351 "dfa.c"
      mem_71 = pos + j;
# 1351 "dfa.c"
      mem_72 = nalloc + mem_71->strchr;
      {
      __CrestLoad(30618, (unsigned long )(& merged.nelem), (long long )merged.nelem);
      __CrestLoad(30617, (unsigned long )0, (long long )1);
      __CrestApply2(30616, 1, (long long )(merged.nelem - 1));
      __CrestLoad(30615, (unsigned long )mem_72, (long long )*mem_72);
      __CrestApply2(30614, 17, (long long )(merged.nelem - 1 >= *mem_72));
# 1351 "dfa.c"
      if (merged.nelem - 1 >= *mem_72) {
        __CrestBranch(30619, 5285, 1);
        {
# 1351 "dfa.c"
        while (1) {
          while_continue___4: ;
          {
# 1351 "dfa.c"
          mem_73 = pos + j;
# 1351 "dfa.c"
          mem_74 = nalloc + mem_73->strchr;
          {
          __CrestLoad(30625, (unsigned long )(& merged.nelem), (long long )merged.nelem);
          __CrestLoad(30624, (unsigned long )0, (long long )1);
          __CrestApply2(30623, 1, (long long )(merged.nelem - 1));
          __CrestLoad(30622, (unsigned long )mem_74, (long long )*mem_74);
          __CrestApply2(30621, 17, (long long )(merged.nelem - 1 >= *mem_74));
# 1351 "dfa.c"
          if (merged.nelem - 1 >= *mem_74) {
            __CrestBranch(30626, 5291, 1);

          } else {
            __CrestBranch(30627, 5292, 0);
# 1351 "dfa.c"
            goto while_break___4;
          }
          }
          }
# 1351 "dfa.c"
          mem_75 = pos + j;
# 1351 "dfa.c"
          mem_76 = nalloc + mem_75->strchr;
# 1351 "dfa.c"
          mem_77 = pos + j;
# 1351 "dfa.c"
          mem_78 = nalloc + mem_77->strchr;
          __CrestLoad(30630, (unsigned long )mem_78, (long long )*mem_78);
          __CrestLoad(30629, (unsigned long )0, (long long )2);
          __CrestApply2(30628, 2, (long long )(*mem_78 * 2));
          __CrestStore(30631, (unsigned long )mem_76);
# 1351 "dfa.c"
          *mem_76 = *mem_78 * 2;
        }
        while_break___4: ;
        }
# 1351 "dfa.c"
        mem_79 = pos + j;
# 1351 "dfa.c"
        mem_80 = d->follows + mem_79->strchr;
# 1351 "dfa.c"
        mem_81 = pos + j;
# 1351 "dfa.c"
        mem_82 = nalloc + mem_81->strchr;
        __CrestLoad(30634, (unsigned long )mem_82, (long long )*mem_82);
        __CrestLoad(30633, (unsigned long )0, (long long )sizeof(position ));
        __CrestApply2(30632, 2, (long long )((unsigned long )*mem_82 * sizeof(position )));
# 1351 "dfa.c"
        tmp___13 = xrealloc((ptr_t )mem_80->elems, (unsigned long )*mem_82 * sizeof(position ));
        __CrestClearStack(30635);
# 1351 "dfa.c"
        mem_83 = pos + j;
# 1351 "dfa.c"
        mem_84 = d->follows + mem_83->strchr;
# 1351 "dfa.c"
        mem_84->elems = (position *)tmp___13;
      } else {
        __CrestBranch(30620, 5296, 0);

      }
      }
      }
# 1353 "dfa.c"
      mem_85 = pos + j;
# 1353 "dfa.c"
      copy(& merged, d->follows + mem_85->strchr);
      __CrestClearStack(30636);
      __CrestLoad(30639, (unsigned long )(& j), (long long )j);
      __CrestLoad(30638, (unsigned long )0, (long long )1);
      __CrestApply2(30637, 0, (long long )(j + 1));
      __CrestStore(30640, (unsigned long )(& j));
# 1348 "dfa.c"
      j ++;
    }
    while_break___3: ;
    }
    {
# 1358 "dfa.c"
    mem_86 = nullable + -2;
    {
    __CrestLoad(30643, (unsigned long )mem_86, (long long )*mem_86);
    __CrestLoad(30642, (unsigned long )0, (long long )0);
    __CrestApply2(30641, 13, (long long )(*mem_86 != 0));
# 1358 "dfa.c"
    if (*mem_86 != 0) {
      __CrestBranch(30644, 5302, 1);
# 1359 "dfa.c"
      mem_87 = nfirstpos + -2;
# 1359 "dfa.c"
      mem_88 = nfirstpos + -2;
# 1359 "dfa.c"
      mem_89 = nfirstpos + -1;
      __CrestLoad(30648, (unsigned long )mem_88, (long long )*mem_88);
      __CrestLoad(30647, (unsigned long )mem_89, (long long )*mem_89);
      __CrestApply2(30646, 0, (long long )(*mem_88 + *mem_89));
      __CrestStore(30649, (unsigned long )mem_87);
# 1359 "dfa.c"
      *mem_87 = *mem_88 + *mem_89;
    } else {
      __CrestBranch(30645, 5303, 0);
# 1361 "dfa.c"
      mem_90 = nfirstpos + -1;
# 1361 "dfa.c"
      firstpos += *mem_90;
    }
    }
    }
# 1362 "dfa.c"
    nfirstpos --;
    {
# 1366 "dfa.c"
    mem_91 = nullable + -1;
    {
    __CrestLoad(30652, (unsigned long )mem_91, (long long )*mem_91);
    __CrestLoad(30651, (unsigned long )0, (long long )0);
    __CrestApply2(30650, 13, (long long )(*mem_91 != 0));
# 1366 "dfa.c"
    if (*mem_91 != 0) {
      __CrestBranch(30653, 5308, 1);
# 1367 "dfa.c"
      mem_92 = nlastpos + -2;
# 1367 "dfa.c"
      mem_93 = nlastpos + -2;
# 1367 "dfa.c"
      mem_94 = nlastpos + -1;
      __CrestLoad(30657, (unsigned long )mem_93, (long long )*mem_93);
      __CrestLoad(30656, (unsigned long )mem_94, (long long )*mem_94);
      __CrestApply2(30655, 0, (long long )(*mem_93 + *mem_94));
      __CrestStore(30658, (unsigned long )mem_92);
# 1367 "dfa.c"
      *mem_92 = *mem_93 + *mem_94;
    } else {
      __CrestBranch(30654, 5309, 0);
# 1370 "dfa.c"
      mem_95 = nlastpos + -2;
# 1370 "dfa.c"
      pos = lastpos + *mem_95;
# 1371 "dfa.c"
      mem_96 = nlastpos + -1;
      __CrestLoad(30661, (unsigned long )mem_96, (long long )*mem_96);
      __CrestLoad(30660, (unsigned long )0, (long long )1);
      __CrestApply2(30659, 1, (long long )(*mem_96 - 1));
      __CrestStore(30662, (unsigned long )(& j));
# 1371 "dfa.c"
      j = *mem_96 - 1;
      {
# 1371 "dfa.c"
      while (1) {
        while_continue___5: ;
        {
        __CrestLoad(30665, (unsigned long )(& j), (long long )j);
        __CrestLoad(30664, (unsigned long )0, (long long )0);
        __CrestApply2(30663, 17, (long long )(j >= 0));
# 1371 "dfa.c"
        if (j >= 0) {
          __CrestBranch(30666, 5314, 1);

        } else {
          __CrestBranch(30667, 5315, 0);
# 1371 "dfa.c"
          goto while_break___5;
        }
        }
# 1372 "dfa.c"
        mem_97 = pos + j;
# 1372 "dfa.c"
        mem_98 = lastpos + j;
# 1372 "dfa.c"
        *mem_97 = *mem_98;
        __CrestLoad(30670, (unsigned long )(& j), (long long )j);
        __CrestLoad(30669, (unsigned long )0, (long long )1);
        __CrestApply2(30668, 1, (long long )(j - 1));
        __CrestStore(30671, (unsigned long )(& j));
# 1371 "dfa.c"
        j --;
      }
      while_break___5: ;
      }
# 1373 "dfa.c"
      mem_99 = nlastpos + -2;
# 1373 "dfa.c"
      lastpos += *mem_99;
# 1374 "dfa.c"
      mem_100 = nlastpos + -2;
# 1374 "dfa.c"
      mem_101 = nlastpos + -1;
      __CrestLoad(30672, (unsigned long )mem_101, (long long )*mem_101);
      __CrestStore(30673, (unsigned long )mem_100);
# 1374 "dfa.c"
      *mem_100 = *mem_101;
    }
    }
    }
# 1376 "dfa.c"
    nlastpos --;
    {
# 1379 "dfa.c"
    mem_102 = nullable + -1;
    {
    __CrestLoad(30676, (unsigned long )mem_102, (long long )*mem_102);
    __CrestLoad(30675, (unsigned long )0, (long long )0);
    __CrestApply2(30674, 13, (long long )(*mem_102 != 0));
# 1379 "dfa.c"
    if (*mem_102 != 0) {
      __CrestBranch(30677, 5323, 1);
      {
# 1379 "dfa.c"
      mem_103 = nullable + -2;
      {
      __CrestLoad(30681, (unsigned long )mem_103, (long long )*mem_103);
      __CrestLoad(30680, (unsigned long )0, (long long )0);
      __CrestApply2(30679, 13, (long long )(*mem_103 != 0));
# 1379 "dfa.c"
      if (*mem_103 != 0) {
        __CrestBranch(30682, 5326, 1);
        __CrestLoad(30684, (unsigned long )0, (long long )1);
        __CrestStore(30685, (unsigned long )(& tmp___14));
# 1379 "dfa.c"
        tmp___14 = 1;
      } else {
        __CrestBranch(30683, 5327, 0);
        __CrestLoad(30686, (unsigned long )0, (long long )0);
        __CrestStore(30687, (unsigned long )(& tmp___14));
# 1379 "dfa.c"
        tmp___14 = 0;
      }
      }
      }
    } else {
      __CrestBranch(30678, 5328, 0);
      __CrestLoad(30688, (unsigned long )0, (long long )0);
      __CrestStore(30689, (unsigned long )(& tmp___14));
# 1379 "dfa.c"
      tmp___14 = 0;
    }
    }
    }
# 1379 "dfa.c"
    mem_104 = nullable + -2;
    __CrestLoad(30690, (unsigned long )(& tmp___14), (long long )tmp___14);
    __CrestStore(30691, (unsigned long )mem_104);
# 1379 "dfa.c"
    *mem_104 = tmp___14;
# 1380 "dfa.c"
    nullable --;
# 1381 "dfa.c"
    goto switch_break;
    case_270:
    case_269:
# 1386 "dfa.c"
    mem_105 = nfirstpos + -2;
# 1386 "dfa.c"
    mem_106 = nfirstpos + -2;
# 1386 "dfa.c"
    mem_107 = nfirstpos + -1;
    __CrestLoad(30694, (unsigned long )mem_106, (long long )*mem_106);
    __CrestLoad(30693, (unsigned long )mem_107, (long long )*mem_107);
    __CrestApply2(30692, 0, (long long )(*mem_106 + *mem_107));
    __CrestStore(30695, (unsigned long )mem_105);
# 1386 "dfa.c"
    *mem_105 = *mem_106 + *mem_107;
# 1387 "dfa.c"
    nfirstpos --;
# 1390 "dfa.c"
    mem_108 = nlastpos + -2;
# 1390 "dfa.c"
    mem_109 = nlastpos + -2;
# 1390 "dfa.c"
    mem_110 = nlastpos + -1;
    __CrestLoad(30698, (unsigned long )mem_109, (long long )*mem_109);
    __CrestLoad(30697, (unsigned long )mem_110, (long long )*mem_110);
    __CrestApply2(30696, 0, (long long )(*mem_109 + *mem_110));
    __CrestStore(30699, (unsigned long )mem_108);
# 1390 "dfa.c"
    *mem_108 = *mem_109 + *mem_110;
# 1391 "dfa.c"
    nlastpos --;
    {
# 1394 "dfa.c"
    mem_111 = nullable + -1;
    {
    __CrestLoad(30702, (unsigned long )mem_111, (long long )*mem_111);
    __CrestLoad(30701, (unsigned long )0, (long long )0);
    __CrestApply2(30700, 13, (long long )(*mem_111 != 0));
# 1394 "dfa.c"
    if (*mem_111 != 0) {
      __CrestBranch(30703, 5335, 1);
      __CrestLoad(30705, (unsigned long )0, (long long )1);
      __CrestStore(30706, (unsigned long )(& tmp___15));
# 1394 "dfa.c"
      tmp___15 = 1;
    } else {
      __CrestBranch(30704, 5336, 0);
      {
# 1394 "dfa.c"
      mem_112 = nullable + -2;
      {
      __CrestLoad(30709, (unsigned long )mem_112, (long long )*mem_112);
      __CrestLoad(30708, (unsigned long )0, (long long )0);
      __CrestApply2(30707, 13, (long long )(*mem_112 != 0));
# 1394 "dfa.c"
      if (*mem_112 != 0) {
        __CrestBranch(30710, 5339, 1);
        __CrestLoad(30712, (unsigned long )0, (long long )1);
        __CrestStore(30713, (unsigned long )(& tmp___15));
# 1394 "dfa.c"
        tmp___15 = 1;
      } else {
        __CrestBranch(30711, 5340, 0);
        __CrestLoad(30714, (unsigned long )0, (long long )0);
        __CrestStore(30715, (unsigned long )(& tmp___15));
# 1394 "dfa.c"
        tmp___15 = 0;
      }
      }
      }
    }
    }
    }
# 1394 "dfa.c"
    mem_113 = nullable + -2;
    __CrestLoad(30716, (unsigned long )(& tmp___15), (long long )tmp___15);
    __CrestStore(30717, (unsigned long )mem_113);
# 1394 "dfa.c"
    *mem_113 = tmp___15;
# 1395 "dfa.c"
    nullable --;
# 1396 "dfa.c"
    goto switch_break;
    switch_default:
# 1404 "dfa.c"
    tmp___16 = nullable;
# 1404 "dfa.c"
    nullable ++;
# 1404 "dfa.c"
    mem_114 = d->tokens + i;
    __CrestLoad(30720, (unsigned long )mem_114, (long long )*mem_114);
    __CrestLoad(30719, (unsigned long )0, (long long )257);
    __CrestApply2(30718, 12, (long long )((int )*mem_114 == 257));
    __CrestStore(30721, (unsigned long )tmp___16);
# 1404 "dfa.c"
    *tmp___16 = (int )*mem_114 == 257;
# 1407 "dfa.c"
    tmp___17 = nfirstpos;
# 1407 "dfa.c"
    nfirstpos ++;
# 1407 "dfa.c"
    tmp___18 = nlastpos;
# 1407 "dfa.c"
    nlastpos ++;
    __CrestLoad(30722, (unsigned long )0, (long long )1);
    __CrestStore(30723, (unsigned long )(& tmp___19));
# 1407 "dfa.c"
    tmp___19 = 1;
    __CrestLoad(30724, (unsigned long )(& tmp___19), (long long )tmp___19);
    __CrestStore(30725, (unsigned long )tmp___18);
# 1407 "dfa.c"
    *tmp___18 = tmp___19;
    __CrestLoad(30726, (unsigned long )(& tmp___19), (long long )tmp___19);
    __CrestStore(30727, (unsigned long )tmp___17);
# 1407 "dfa.c"
    *tmp___17 = tmp___19;
# 1408 "dfa.c"
    firstpos --;
# 1408 "dfa.c"
    lastpos --;
    __CrestLoad(30728, (unsigned long )(& i), (long long )i);
    __CrestStore(30729, (unsigned long )(& tmp___20));
# 1409 "dfa.c"
    tmp___20 = (unsigned int )i;
    __CrestLoad(30730, (unsigned long )(& tmp___20), (long long )tmp___20);
    __CrestStore(30731, (unsigned long )(& lastpos->strchr));
# 1409 "dfa.c"
    lastpos->strchr = tmp___20;
    __CrestLoad(30732, (unsigned long )(& tmp___20), (long long )tmp___20);
    __CrestStore(30733, (unsigned long )(& firstpos->strchr));
# 1409 "dfa.c"
    firstpos->strchr = tmp___20;
    __CrestLoad(30734, (unsigned long )0, (long long )255U);
    __CrestStore(30735, (unsigned long )(& tmp___21));
# 1410 "dfa.c"
    tmp___21 = 255U;
    __CrestLoad(30736, (unsigned long )(& tmp___21), (long long )tmp___21);
    __CrestStore(30737, (unsigned long )(& lastpos->constraint));
# 1410 "dfa.c"
    lastpos->constraint = tmp___21;
    __CrestLoad(30738, (unsigned long )(& tmp___21), (long long )tmp___21);
    __CrestStore(30739, (unsigned long )(& firstpos->constraint));
# 1410 "dfa.c"
    firstpos->constraint = tmp___21;
# 1413 "dfa.c"
    mem_115 = nalloc + i;
    __CrestLoad(30740, (unsigned long )0, (long long )1);
    __CrestStore(30741, (unsigned long )mem_115);
# 1413 "dfa.c"
    *mem_115 = 1;
# 1414 "dfa.c"
    mem_116 = nalloc + i;
    __CrestLoad(30744, (unsigned long )mem_116, (long long )*mem_116);
    __CrestLoad(30743, (unsigned long )0, (long long )sizeof(position ));
    __CrestApply2(30742, 2, (long long )((unsigned long )*mem_116 * sizeof(position )));
# 1414 "dfa.c"
    tmp___22 = xmalloc((unsigned long )*mem_116 * sizeof(position ));
    __CrestClearStack(30745);
# 1414 "dfa.c"
    mem_117 = d->follows + i;
# 1414 "dfa.c"
    mem_117->elems = (position *)tmp___22;
# 1415 "dfa.c"
    goto switch_break;
    switch_break: ;
    }
    }
    __CrestLoad(30748, (unsigned long )(& i), (long long )i);
    __CrestLoad(30747, (unsigned long )0, (long long )1);
    __CrestApply2(30746, 0, (long long )(i + 1));
    __CrestStore(30749, (unsigned long )(& i));
# 1307 "dfa.c"
    i ++;
  }
  while_break___0: ;
  }
  __CrestLoad(30750, (unsigned long )0, (long long )0);
  __CrestStore(30751, (unsigned long )(& i));
# 1441 "dfa.c"
  i = 0;
  {
# 1441 "dfa.c"
  while (1) {
    while_continue___6: ;
    {
    __CrestLoad(30754, (unsigned long )(& i), (long long )i);
    __CrestLoad(30753, (unsigned long )(& d->tindex), (long long )d->tindex);
    __CrestApply2(30752, 16, (long long )(i < d->tindex));
# 1441 "dfa.c"
    if (i < d->tindex) {
      __CrestBranch(30755, 5353, 1);

    } else {
      __CrestBranch(30756, 5354, 0);
# 1441 "dfa.c"
      goto while_break___6;
    }
    }
    {
# 1442 "dfa.c"
    mem_118 = d->tokens + i;
    {
    __CrestLoad(30759, (unsigned long )mem_118, (long long )*mem_118);
    __CrestLoad(30758, (unsigned long )0, (long long )(1 << 8));
    __CrestApply2(30757, 16, (long long )((int )*mem_118 < 1 << 8));
# 1442 "dfa.c"
    if ((int )*mem_118 < 1 << 8) {
      __CrestBranch(30760, 5358, 1);
# 1442 "dfa.c"
      goto _L;
    } else {
      __CrestBranch(30761, 5359, 0);
      {
# 1442 "dfa.c"
      mem_119 = d->tokens + i;
      {
      __CrestLoad(30764, (unsigned long )mem_119, (long long )*mem_119);
      __CrestLoad(30763, (unsigned long )0, (long long )257);
      __CrestApply2(30762, 12, (long long )((int )*mem_119 == 257));
# 1442 "dfa.c"
      if ((int )*mem_119 == 257) {
        __CrestBranch(30765, 5362, 1);
# 1442 "dfa.c"
        goto _L;
      } else {
        __CrestBranch(30766, 5363, 0);
        {
# 1442 "dfa.c"
        mem_120 = d->tokens + i;
        {
        __CrestLoad(30769, (unsigned long )mem_120, (long long )*mem_120);
        __CrestLoad(30768, (unsigned long )0, (long long )273);
        __CrestApply2(30767, 17, (long long )((int )*mem_120 >= 273));
# 1442 "dfa.c"
        if ((int )*mem_120 >= 273) {
          __CrestBranch(30770, 5366, 1);
          _L:
# 1456 "dfa.c"
          copy(d->follows + i, & merged);
          __CrestClearStack(30772);
# 1457 "dfa.c"
          epsclosure(& merged, d);
          __CrestClearStack(30773);
          {
# 1458 "dfa.c"
          mem_121 = d->follows + i;
          {
          __CrestLoad(30776, (unsigned long )(& mem_121->nelem), (long long )mem_121->nelem);
          __CrestLoad(30775, (unsigned long )(& merged.nelem), (long long )merged.nelem);
          __CrestApply2(30774, 16, (long long )(mem_121->nelem < merged.nelem));
# 1458 "dfa.c"
          if (mem_121->nelem < merged.nelem) {
            __CrestBranch(30777, 5370, 1);
# 1459 "dfa.c"
            mem_122 = d->follows + i;
            __CrestLoad(30781, (unsigned long )(& merged.nelem), (long long )merged.nelem);
            __CrestLoad(30780, (unsigned long )0, (long long )sizeof(position ));
            __CrestApply2(30779, 2, (long long )((unsigned long )merged.nelem * sizeof(position )));
# 1459 "dfa.c"
            tmp___23 = xrealloc((ptr_t )mem_122->elems, (unsigned long )merged.nelem * sizeof(position ));
            __CrestClearStack(30782);
# 1459 "dfa.c"
            mem_123 = d->follows + i;
# 1459 "dfa.c"
            mem_123->elems = (position *)tmp___23;
          } else {
            __CrestBranch(30778, 5371, 0);

          }
          }
          }
# 1460 "dfa.c"
          copy(& merged, d->follows + i);
          __CrestClearStack(30783);
        } else {
          __CrestBranch(30771, 5373, 0);

        }
        }
        }
      }
      }
      }
    }
    }
    }
    __CrestLoad(30786, (unsigned long )(& i), (long long )i);
    __CrestLoad(30785, (unsigned long )0, (long long )1);
    __CrestApply2(30784, 0, (long long )(i + 1));
    __CrestStore(30787, (unsigned long )(& i));
# 1441 "dfa.c"
    i ++;
  }
  while_break___6: ;
  }
  __CrestLoad(30788, (unsigned long )0, (long long )0);
  __CrestStore(30789, (unsigned long )(& merged.nelem));
# 1465 "dfa.c"
  merged.nelem = 0;
  __CrestLoad(30790, (unsigned long )0, (long long )0);
  __CrestStore(30791, (unsigned long )(& i));
# 1466 "dfa.c"
  i = 0;
  {
# 1466 "dfa.c"
  while (1) {
    while_continue___7: ;
    {
# 1466 "dfa.c"
    mem_124 = nfirstpos + -1;
    {
    __CrestLoad(30794, (unsigned long )(& i), (long long )i);
    __CrestLoad(30793, (unsigned long )mem_124, (long long )*mem_124);
    __CrestApply2(30792, 16, (long long )(i < *mem_124));
# 1466 "dfa.c"
    if (i < *mem_124) {
      __CrestBranch(30795, 5383, 1);

    } else {
      __CrestBranch(30796, 5384, 0);
# 1466 "dfa.c"
      goto while_break___7;
    }
    }
    }
# 1467 "dfa.c"
    mem_125 = firstpos + i;
# 1467 "dfa.c"
    insert(*mem_125, & merged);
    __CrestClearStack(30797);
    __CrestLoad(30800, (unsigned long )(& i), (long long )i);
    __CrestLoad(30799, (unsigned long )0, (long long )1);
    __CrestApply2(30798, 0, (long long )(i + 1));
    __CrestStore(30801, (unsigned long )(& i));
# 1466 "dfa.c"
    i ++;
  }
  while_break___7: ;
  }
# 1468 "dfa.c"
  epsclosure(& merged, d);
  __CrestClearStack(30802);
  __CrestLoad(30803, (unsigned long )0, (long long )0);
  __CrestStore(30804, (unsigned long )(& wants_newline));
# 1471 "dfa.c"
  wants_newline = 0;
  __CrestLoad(30805, (unsigned long )0, (long long )0);
  __CrestStore(30806, (unsigned long )(& i));
# 1472 "dfa.c"
  i = 0;
  {
# 1472 "dfa.c"
  while (1) {
    while_continue___8: ;
    {
    __CrestLoad(30809, (unsigned long )(& i), (long long )i);
    __CrestLoad(30808, (unsigned long )(& merged.nelem), (long long )merged.nelem);
    __CrestApply2(30807, 16, (long long )(i < merged.nelem));
# 1472 "dfa.c"
    if (i < merged.nelem) {
      __CrestBranch(30810, 5392, 1);

    } else {
      __CrestBranch(30811, 5393, 0);
# 1472 "dfa.c"
      goto while_break___8;
    }
    }
    {
# 1473 "dfa.c"
    mem_126 = merged.elems + i;
# 1473 "dfa.c"
    mem_127 = merged.elems + i;
    {
    __CrestLoad(30820, (unsigned long )(& mem_126->constraint), (long long )mem_126->constraint);
    __CrestLoad(30819, (unsigned long )0, (long long )192U);
    __CrestApply2(30818, 5, (long long )(mem_126->constraint & 192U));
    __CrestLoad(30817, (unsigned long )0, (long long )2);
    __CrestApply2(30816, 9, (long long )((mem_126->constraint & 192U) >> 2));
    __CrestLoad(30815, (unsigned long )(& mem_127->constraint), (long long )mem_127->constraint);
    __CrestLoad(30814, (unsigned long )0, (long long )48U);
    __CrestApply2(30813, 5, (long long )(mem_127->constraint & 48U));
    __CrestApply2(30812, 13, (long long )((mem_126->constraint & 192U) >> 2 != (mem_127->constraint & 48U)));
# 1473 "dfa.c"
    if ((mem_126->constraint & 192U) >> 2 != (mem_127->constraint & 48U)) {
      __CrestBranch(30821, 5397, 1);
      __CrestLoad(30823, (unsigned long )0, (long long )1);
      __CrestStore(30824, (unsigned long )(& wants_newline));
# 1474 "dfa.c"
      wants_newline = 1;
    } else {
      __CrestBranch(30822, 5398, 0);

    }
    }
    }
    __CrestLoad(30827, (unsigned long )(& i), (long long )i);
    __CrestLoad(30826, (unsigned long )0, (long long )1);
    __CrestApply2(30825, 0, (long long )(i + 1));
    __CrestStore(30828, (unsigned long )(& i));
# 1472 "dfa.c"
    i ++;
  }
  while_break___8: ;
  }
  __CrestLoad(30829, (unsigned long )0, (long long )1);
  __CrestStore(30830, (unsigned long )(& d->salloc));
# 1477 "dfa.c"
  d->salloc = 1;
  __CrestLoad(30831, (unsigned long )0, (long long )0);
  __CrestStore(30832, (unsigned long )(& d->sindex));
# 1478 "dfa.c"
  d->sindex = 0;
  __CrestLoad(30835, (unsigned long )(& d->salloc), (long long )d->salloc);
  __CrestLoad(30834, (unsigned long )0, (long long )sizeof(dfa_state ));
  __CrestApply2(30833, 2, (long long )((unsigned long )d->salloc * sizeof(dfa_state )));
# 1479 "dfa.c"
  tmp___24 = xmalloc((unsigned long )d->salloc * sizeof(dfa_state ));
  __CrestClearStack(30836);
# 1479 "dfa.c"
  d->states = (dfa_state *)tmp___24;
  __CrestLoad(30837, (unsigned long )(& wants_newline), (long long )wants_newline);
  __CrestLoad(30838, (unsigned long )0, (long long )0);
# 1480 "dfa.c"
  state_index(d, & merged, wants_newline, 0);
  __CrestClearStack(30839);
# 1482 "dfa.c"
  free((void *)o_nullable);
  __CrestClearStack(30840);
# 1483 "dfa.c"
  free((void *)o_nfirst);
  __CrestClearStack(30841);
# 1484 "dfa.c"
  free((void *)o_firstpos);
  __CrestClearStack(30842);
# 1485 "dfa.c"
  free((void *)o_nlast);
  __CrestClearStack(30843);
# 1486 "dfa.c"
  free((void *)o_lastpos);
  __CrestClearStack(30844);
# 1487 "dfa.c"
  free((void *)nalloc);
  __CrestClearStack(30845);
# 1488 "dfa.c"
  free((void *)merged.elems);
  __CrestClearStack(30846);

  {
  __CrestReturn(30847);
# 1258 "dfa.c"
  return;
  }
}
}
# 1537 "dfa.c"
static charclass letters ;
# 1538 "dfa.c"
static charclass newline ;
# 1546 "dfa.c"
static int initialized ;
# 1521 "dfa.c"
void dfastate(int s , struct dfa *d , int *trans )
{
  position_set grps[1 << 8] ;
  charclass labels[1 << 8] ;
  int ngrps ;
  position pos ;
  charclass matches ;
  int matchesf ;
  charclass intersect ;
  int intersectf ;
  charclass leftovers ;
  int leftoversf ;
  position_set follows ;
  position_set tmp ;
  int state ;
  int wants_newline ;
  int state_newline ;
  int wants_letter ;
  int state_letter ;
  int i ;
  int j ;
  int k ;
  unsigned short const **tmp___0 ;
  int tmp___1 ;
  int tmp___2 ;
  int tmp___3 ;
  int tmp___4 ;
  int tmp___5 ;
  int tmp___6 ;
  int match ;
  int label ;
  int tmp___7 ;
  int tmp___8 ;
  ptr_t tmp___9 ;
  int tmp___10 ;
  ptr_t tmp___11 ;
  ptr_t tmp___12 ;
  ptr_t tmp___13 ;
  unsigned short const **tmp___15 ;
  int tmp___16 ;
  int c ;
  unsigned short const **tmp___17 ;
  unsigned short const *mem_45 ;
  dfa_state *mem_46 ;
  dfa_state *mem_47 ;
  position *mem_48 ;
  token *mem_49 ;
  token *mem_50 ;
  token *mem_51 ;
  token *mem_52 ;
  token *mem_53 ;
  charclass *mem_54 ;
  dfa_state *mem_55 ;
  dfa_state *mem_56 ;
  dfa_state *mem_57 ;
  dfa_state *mem_58 ;
  token *mem_59 ;
  token *mem_60 ;
  token *mem_61 ;
  position *mem_62 ;
  position *mem_63 ;
  dfa_state *mem_64 ;
  dfa_state *mem_65 ;
  position *mem_66 ;
  dfa_state *mem_67 ;
  position *mem_68 ;
  dfa_state *mem_69 ;
  position *mem_70 ;
  dfa_state *mem_71 ;
  position *mem_72 ;
  dfa_state *mem_73 ;
  unsigned short const *mem_74 ;
  int *mem_75 ;
  int *mem_76 ;
  int *mem_77 ;
  int *mem_78 ;
  int *mem_79 ;
  position *mem_80 ;
  position_set *mem_81 ;
  position *mem_82 ;
  position_set *mem_83 ;
  position *mem_84 ;
  dfa_state *mem_85 ;
  dfa_state *mem_86 ;
  position *mem_87 ;
  position *mem_88 ;
  position *mem_89 ;
  position *mem_90 ;
  position *mem_91 ;
  int *mem_92 ;
  unsigned short const *mem_93 ;
  int *mem_94 ;
  int *mem_95 ;
  int *mem_96 ;

  {
  __CrestCall(30849, 253);
  __CrestStore(30848, (unsigned long )(& s));
  __CrestLoad(30850, (unsigned long )0, (long long )0);
  __CrestStore(30851, (unsigned long )(& ngrps));
# 1529 "dfa.c"
  ngrps = 0;
  {
  __CrestLoad(30854, (unsigned long )(& initialized), (long long )initialized);
  __CrestLoad(30853, (unsigned long )0, (long long )0);
  __CrestApply2(30852, 12, (long long )(initialized == 0));
# 1550 "dfa.c"
  if (initialized == 0) {
    __CrestBranch(30855, 5406, 1);
    __CrestLoad(30857, (unsigned long )0, (long long )1);
    __CrestStore(30858, (unsigned long )(& initialized));
# 1552 "dfa.c"
    initialized = 1;
    __CrestLoad(30859, (unsigned long )0, (long long )0);
    __CrestStore(30860, (unsigned long )(& i));
# 1553 "dfa.c"
    i = 0;
    {
# 1553 "dfa.c"
    while (1) {
      while_continue: ;
      {
      __CrestLoad(30863, (unsigned long )(& i), (long long )i);
      __CrestLoad(30862, (unsigned long )0, (long long )(1 << 8));
      __CrestApply2(30861, 16, (long long )(i < 1 << 8));
# 1553 "dfa.c"
      if (i < 1 << 8) {
        __CrestBranch(30864, 5411, 1);

      } else {
        __CrestBranch(30865, 5412, 0);
# 1553 "dfa.c"
        goto while_break;
      }
      }
# 1554 "dfa.c"
      tmp___0 = __ctype_b_loc();
      __CrestClearStack(30866);
      {
# 1554 "dfa.c"
      mem_45 = *tmp___0 + i;
      {
      __CrestLoad(30871, (unsigned long )mem_45, (long long )*mem_45);
      __CrestLoad(30870, (unsigned long )0, (long long )8);
      __CrestApply2(30869, 5, (long long )((int const )*mem_45 & 8));
      __CrestLoad(30868, (unsigned long )0, (long long )0);
      __CrestApply2(30867, 13, (long long )(((int const )*mem_45 & 8) != 0));
# 1554 "dfa.c"
      if (((int const )*mem_45 & 8) != 0) {
        __CrestBranch(30872, 5417, 1);
        __CrestLoad(30874, (unsigned long )(& i), (long long )i);
# 1555 "dfa.c"
        setbit(i, letters);
        __CrestClearStack(30875);
      } else {
        __CrestBranch(30873, 5418, 0);
        {
        __CrestLoad(30878, (unsigned long )(& i), (long long )i);
        __CrestLoad(30877, (unsigned long )0, (long long )95);
        __CrestApply2(30876, 12, (long long )(i == 95));
# 1554 "dfa.c"
        if (i == 95) {
          __CrestBranch(30879, 5419, 1);
          __CrestLoad(30881, (unsigned long )(& i), (long long )i);
# 1555 "dfa.c"
          setbit(i, letters);
          __CrestClearStack(30882);
        } else {
          __CrestBranch(30880, 5420, 0);

        }
        }
      }
      }
      }
      __CrestLoad(30885, (unsigned long )(& i), (long long )i);
      __CrestLoad(30884, (unsigned long )0, (long long )1);
      __CrestApply2(30883, 0, (long long )(i + 1));
      __CrestStore(30886, (unsigned long )(& i));
# 1553 "dfa.c"
      i ++;
    }
    while_break: ;
    }
    __CrestLoad(30887, (unsigned long )0, (long long )'\n');
# 1556 "dfa.c"
    setbit('\n', newline);
    __CrestClearStack(30888);
  } else {
    __CrestBranch(30856, 5424, 0);

  }
  }
# 1559 "dfa.c"
  zeroset(matches);
  __CrestClearStack(30889);
  __CrestLoad(30890, (unsigned long )0, (long long )0);
  __CrestStore(30891, (unsigned long )(& i));
# 1561 "dfa.c"
  i = 0;
  {
# 1561 "dfa.c"
  while (1) {
    while_continue___0: ;
    {
# 1561 "dfa.c"
    mem_46 = d->states + s;
    {
    __CrestLoad(30894, (unsigned long )(& i), (long long )i);
    __CrestLoad(30893, (unsigned long )(& mem_46->elems.nelem), (long long )mem_46->elems.nelem);
    __CrestApply2(30892, 16, (long long )(i < mem_46->elems.nelem));
# 1561 "dfa.c"
    if (i < mem_46->elems.nelem) {
      __CrestBranch(30895, 5432, 1);

    } else {
      __CrestBranch(30896, 5433, 0);
# 1561 "dfa.c"
      goto while_break___0;
    }
    }
    }
# 1563 "dfa.c"
    mem_47 = d->states + s;
# 1563 "dfa.c"
    mem_48 = mem_47->elems.elems + i;
# 1563 "dfa.c"
    pos = *mem_48;
    {
# 1564 "dfa.c"
    mem_49 = d->tokens + pos.strchr;
    {
    __CrestLoad(30899, (unsigned long )mem_49, (long long )*mem_49);
    __CrestLoad(30898, (unsigned long )0, (long long )0);
    __CrestApply2(30897, 17, (long long )((int )*mem_49 >= 0));
# 1564 "dfa.c"
    if ((int )*mem_49 >= 0) {
      __CrestBranch(30900, 5438, 1);
      {
# 1564 "dfa.c"
      mem_50 = d->tokens + pos.strchr;
      {
      __CrestLoad(30904, (unsigned long )mem_50, (long long )*mem_50);
      __CrestLoad(30903, (unsigned long )0, (long long )(1 << 8));
      __CrestApply2(30902, 16, (long long )((int )*mem_50 < 1 << 8));
# 1564 "dfa.c"
      if ((int )*mem_50 < 1 << 8) {
        __CrestBranch(30905, 5441, 1);
# 1565 "dfa.c"
        mem_51 = d->tokens + pos.strchr;
        __CrestLoad(30907, (unsigned long )mem_51, (long long )*mem_51);
# 1565 "dfa.c"
        setbit((int )*mem_51, matches);
        __CrestClearStack(30908);
      } else {
        __CrestBranch(30906, 5442, 0);
# 1564 "dfa.c"
        goto _L;
      }
      }
      }
    } else {
      __CrestBranch(30901, 5443, 0);
      _L:
      {
# 1566 "dfa.c"
      mem_52 = d->tokens + pos.strchr;
      {
      __CrestLoad(30911, (unsigned long )mem_52, (long long )*mem_52);
      __CrestLoad(30910, (unsigned long )0, (long long )273);
      __CrestApply2(30909, 17, (long long )((int )*mem_52 >= 273));
# 1566 "dfa.c"
      if ((int )*mem_52 >= 273) {
        __CrestBranch(30912, 5446, 1);
# 1567 "dfa.c"
        mem_53 = d->tokens + pos.strchr;
# 1567 "dfa.c"
        mem_54 = d->charclasses + ((int )*mem_53 - 273);
# 1567 "dfa.c"
        copyset(*mem_54, matches);
        __CrestClearStack(30914);
      } else {
        __CrestBranch(30913, 5447, 0);
# 1569 "dfa.c"
        goto __Cont;
      }
      }
      }
    }
    }
    }
    {
    __CrestLoad(30917, (unsigned long )(& pos.constraint), (long long )pos.constraint);
    __CrestLoad(30916, (unsigned long )0, (long long )255U);
    __CrestApply2(30915, 13, (long long )(pos.constraint != 255U));
# 1573 "dfa.c"
    if (pos.constraint != 255U) {
      __CrestBranch(30918, 5449, 1);
      {
# 1575 "dfa.c"
      mem_55 = d->states + s;
      {
      __CrestLoad(30922, (unsigned long )(& mem_55->newline), (long long )mem_55->newline);
      __CrestLoad(30921, (unsigned long )0, (long long )0);
      __CrestApply2(30920, 13, (long long )(mem_55->newline != 0));
# 1575 "dfa.c"
      if (mem_55->newline != 0) {
        __CrestBranch(30923, 5452, 1);
        __CrestLoad(30925, (unsigned long )0, (long long )2);
        __CrestStore(30926, (unsigned long )(& tmp___1));
# 1575 "dfa.c"
        tmp___1 = 2;
      } else {
        __CrestBranch(30924, 5453, 0);
        __CrestLoad(30927, (unsigned long )0, (long long )0);
        __CrestStore(30928, (unsigned long )(& tmp___1));
# 1575 "dfa.c"
        tmp___1 = 0;
      }
      }
      }
      {
      __CrestLoad(30939, (unsigned long )(& pos.constraint), (long long )pos.constraint);
      __CrestLoad(30938, (unsigned long )0, (long long )1);
      __CrestLoad(30937, (unsigned long )(& tmp___1), (long long )tmp___1);
      __CrestLoad(30936, (unsigned long )0, (long long )1);
      __CrestApply2(30935, 0, (long long )(tmp___1 + 1));
      __CrestLoad(30934, (unsigned long )0, (long long )4);
      __CrestApply2(30933, 0, (long long )((tmp___1 + 1) + 4));
      __CrestApply2(30932, 8, (long long )(1 << ((tmp___1 + 1) + 4)));
      __CrestApply2(30931, 5, (long long )(pos.constraint & (unsigned int )(1 << ((tmp___1 + 1) + 4))));
      __CrestLoad(30930, (unsigned long )0, (long long )0);
      __CrestApply2(30929, 13, (long long )((pos.constraint & (unsigned int )(1 << ((tmp___1 + 1) + 4))) != 0));
# 1575 "dfa.c"
      if ((pos.constraint & (unsigned int )(1 << ((tmp___1 + 1) + 4))) != 0) {
        __CrestBranch(30940, 5455, 1);

      } else {
        __CrestBranch(30941, 5456, 0);
        __CrestLoad(30942, (unsigned long )0, (long long )'\n');
# 1577 "dfa.c"
        clrbit('\n', matches);
        __CrestClearStack(30943);
      }
      }
      {
# 1578 "dfa.c"
      mem_56 = d->states + s;
      {
      __CrestLoad(30946, (unsigned long )(& mem_56->newline), (long long )mem_56->newline);
      __CrestLoad(30945, (unsigned long )0, (long long )0);
      __CrestApply2(30944, 13, (long long )(mem_56->newline != 0));
# 1578 "dfa.c"
      if (mem_56->newline != 0) {
        __CrestBranch(30947, 5460, 1);
        __CrestLoad(30949, (unsigned long )0, (long long )2);
        __CrestStore(30950, (unsigned long )(& tmp___2));
# 1578 "dfa.c"
        tmp___2 = 2;
      } else {
        __CrestBranch(30948, 5461, 0);
        __CrestLoad(30951, (unsigned long )0, (long long )0);
        __CrestStore(30952, (unsigned long )(& tmp___2));
# 1578 "dfa.c"
        tmp___2 = 0;
      }
      }
      }
      {
      __CrestLoad(30961, (unsigned long )(& pos.constraint), (long long )pos.constraint);
      __CrestLoad(30960, (unsigned long )0, (long long )1);
      __CrestLoad(30959, (unsigned long )(& tmp___2), (long long )tmp___2);
      __CrestLoad(30958, (unsigned long )0, (long long )4);
      __CrestApply2(30957, 0, (long long )(tmp___2 + 4));
      __CrestApply2(30956, 8, (long long )(1 << (tmp___2 + 4)));
      __CrestApply2(30955, 5, (long long )(pos.constraint & (unsigned int )(1 << (tmp___2 + 4))));
      __CrestLoad(30954, (unsigned long )0, (long long )0);
      __CrestApply2(30953, 13, (long long )((pos.constraint & (unsigned int )(1 << (tmp___2 + 4))) != 0));
# 1578 "dfa.c"
      if ((pos.constraint & (unsigned int )(1 << (tmp___2 + 4))) != 0) {
        __CrestBranch(30962, 5463, 1);

      } else {
        __CrestBranch(30963, 5464, 0);
        __CrestLoad(30964, (unsigned long )0, (long long )0);
        __CrestStore(30965, (unsigned long )(& j));
# 1580 "dfa.c"
        j = 0;
        {
# 1580 "dfa.c"
        while (1) {
          while_continue___1: ;
          {
          __CrestLoad(30968, (unsigned long )(& j), (long long )j);
          __CrestLoad(30967, (unsigned long )0, (long long )((((unsigned long )(1 << 8) + 8UL * sizeof(int )) - 1UL) / (8UL * sizeof(int ))));
          __CrestApply2(30966, 16, (long long )((unsigned long )j < (((unsigned long )(1 << 8) + 8UL * sizeof(int )) - 1UL) / (8UL * sizeof(int ))));
# 1580 "dfa.c"
          if ((unsigned long )j < (((unsigned long )(1 << 8) + 8UL * sizeof(int )) - 1UL) / (8UL * sizeof(int ))) {
            __CrestBranch(30969, 5469, 1);

          } else {
            __CrestBranch(30970, 5470, 0);
# 1580 "dfa.c"
            goto while_break___1;
          }
          }
          __CrestLoad(30973, (unsigned long )(& matches[j]), (long long )matches[j]);
          __CrestLoad(30972, (unsigned long )(& newline[j]), (long long )newline[j]);
          __CrestApply2(30971, 5, (long long )(matches[j] & newline[j]));
          __CrestStore(30974, (unsigned long )(& matches[j]));
# 1581 "dfa.c"
          matches[j] &= newline[j];
          __CrestLoad(30977, (unsigned long )(& j), (long long )j);
          __CrestLoad(30976, (unsigned long )0, (long long )1);
          __CrestApply2(30975, 0, (long long )(j + 1));
          __CrestStore(30978, (unsigned long )(& j));
# 1580 "dfa.c"
          j ++;
        }
        while_break___1: ;
        }
      }
      }
      {
# 1582 "dfa.c"
      mem_57 = d->states + s;
      {
      __CrestLoad(30981, (unsigned long )(& mem_57->letter), (long long )mem_57->letter);
      __CrestLoad(30980, (unsigned long )0, (long long )0);
      __CrestApply2(30979, 13, (long long )(mem_57->letter != 0));
# 1582 "dfa.c"
      if (mem_57->letter != 0) {
        __CrestBranch(30982, 5476, 1);
        __CrestLoad(30984, (unsigned long )0, (long long )2);
        __CrestStore(30985, (unsigned long )(& tmp___3));
# 1582 "dfa.c"
        tmp___3 = 2;
      } else {
        __CrestBranch(30983, 5477, 0);
        __CrestLoad(30986, (unsigned long )0, (long long )0);
        __CrestStore(30987, (unsigned long )(& tmp___3));
# 1582 "dfa.c"
        tmp___3 = 0;
      }
      }
      }
      {
      __CrestLoad(30996, (unsigned long )(& pos.constraint), (long long )pos.constraint);
      __CrestLoad(30995, (unsigned long )0, (long long )1);
      __CrestLoad(30994, (unsigned long )(& tmp___3), (long long )tmp___3);
      __CrestLoad(30993, (unsigned long )0, (long long )1);
      __CrestApply2(30992, 0, (long long )(tmp___3 + 1));
      __CrestApply2(30991, 8, (long long )(1 << (tmp___3 + 1)));
      __CrestApply2(30990, 5, (long long )(pos.constraint & (unsigned int )(1 << (tmp___3 + 1))));
      __CrestLoad(30989, (unsigned long )0, (long long )0);
      __CrestApply2(30988, 13, (long long )((pos.constraint & (unsigned int )(1 << (tmp___3 + 1))) != 0));
# 1582 "dfa.c"
      if ((pos.constraint & (unsigned int )(1 << (tmp___3 + 1))) != 0) {
        __CrestBranch(30997, 5479, 1);

      } else {
        __CrestBranch(30998, 5480, 0);
        __CrestLoad(30999, (unsigned long )0, (long long )0);
        __CrestStore(31000, (unsigned long )(& j));
# 1584 "dfa.c"
        j = 0;
        {
# 1584 "dfa.c"
        while (1) {
          while_continue___2: ;
          {
          __CrestLoad(31003, (unsigned long )(& j), (long long )j);
          __CrestLoad(31002, (unsigned long )0, (long long )((((unsigned long )(1 << 8) + 8UL * sizeof(int )) - 1UL) / (8UL * sizeof(int ))));
          __CrestApply2(31001, 16, (long long )((unsigned long )j < (((unsigned long )(1 << 8) + 8UL * sizeof(int )) - 1UL) / (8UL * sizeof(int ))));
# 1584 "dfa.c"
          if ((unsigned long )j < (((unsigned long )(1 << 8) + 8UL * sizeof(int )) - 1UL) / (8UL * sizeof(int ))) {
            __CrestBranch(31004, 5485, 1);

          } else {
            __CrestBranch(31005, 5486, 0);
# 1584 "dfa.c"
            goto while_break___2;
          }
          }
          __CrestLoad(31009, (unsigned long )(& matches[j]), (long long )matches[j]);
          __CrestLoad(31008, (unsigned long )(& letters[j]), (long long )letters[j]);
          __CrestApply1(31007, 20, (long long )(~ letters[j]));
          __CrestApply2(31006, 5, (long long )(matches[j] & ~ letters[j]));
          __CrestStore(31010, (unsigned long )(& matches[j]));
# 1585 "dfa.c"
          matches[j] &= ~ letters[j];
          __CrestLoad(31013, (unsigned long )(& j), (long long )j);
          __CrestLoad(31012, (unsigned long )0, (long long )1);
          __CrestApply2(31011, 0, (long long )(j + 1));
          __CrestStore(31014, (unsigned long )(& j));
# 1584 "dfa.c"
          j ++;
        }
        while_break___2: ;
        }
      }
      }
      {
# 1586 "dfa.c"
      mem_58 = d->states + s;
      {
      __CrestLoad(31017, (unsigned long )(& mem_58->letter), (long long )mem_58->letter);
      __CrestLoad(31016, (unsigned long )0, (long long )0);
      __CrestApply2(31015, 13, (long long )(mem_58->letter != 0));
# 1586 "dfa.c"
      if (mem_58->letter != 0) {
        __CrestBranch(31018, 5492, 1);
        __CrestLoad(31020, (unsigned long )0, (long long )2);
        __CrestStore(31021, (unsigned long )(& tmp___4));
# 1586 "dfa.c"
        tmp___4 = 2;
      } else {
        __CrestBranch(31019, 5493, 0);
        __CrestLoad(31022, (unsigned long )0, (long long )0);
        __CrestStore(31023, (unsigned long )(& tmp___4));
# 1586 "dfa.c"
        tmp___4 = 0;
      }
      }
      }
      {
      __CrestLoad(31030, (unsigned long )(& pos.constraint), (long long )pos.constraint);
      __CrestLoad(31029, (unsigned long )0, (long long )1);
      __CrestLoad(31028, (unsigned long )(& tmp___4), (long long )tmp___4);
      __CrestApply2(31027, 8, (long long )(1 << tmp___4));
      __CrestApply2(31026, 5, (long long )(pos.constraint & (unsigned int )(1 << tmp___4)));
      __CrestLoad(31025, (unsigned long )0, (long long )0);
      __CrestApply2(31024, 13, (long long )((pos.constraint & (unsigned int )(1 << tmp___4)) != 0));
# 1586 "dfa.c"
      if ((pos.constraint & (unsigned int )(1 << tmp___4)) != 0) {
        __CrestBranch(31031, 5495, 1);

      } else {
        __CrestBranch(31032, 5496, 0);
        __CrestLoad(31033, (unsigned long )0, (long long )0);
        __CrestStore(31034, (unsigned long )(& j));
# 1588 "dfa.c"
        j = 0;
        {
# 1588 "dfa.c"
        while (1) {
          while_continue___3: ;
          {
          __CrestLoad(31037, (unsigned long )(& j), (long long )j);
          __CrestLoad(31036, (unsigned long )0, (long long )((((unsigned long )(1 << 8) + 8UL * sizeof(int )) - 1UL) / (8UL * sizeof(int ))));
          __CrestApply2(31035, 16, (long long )((unsigned long )j < (((unsigned long )(1 << 8) + 8UL * sizeof(int )) - 1UL) / (8UL * sizeof(int ))));
# 1588 "dfa.c"
          if ((unsigned long )j < (((unsigned long )(1 << 8) + 8UL * sizeof(int )) - 1UL) / (8UL * sizeof(int ))) {
            __CrestBranch(31038, 5501, 1);

          } else {
            __CrestBranch(31039, 5502, 0);
# 1588 "dfa.c"
            goto while_break___3;
          }
          }
          __CrestLoad(31042, (unsigned long )(& matches[j]), (long long )matches[j]);
          __CrestLoad(31041, (unsigned long )(& letters[j]), (long long )letters[j]);
          __CrestApply2(31040, 5, (long long )(matches[j] & letters[j]));
          __CrestStore(31043, (unsigned long )(& matches[j]));
# 1589 "dfa.c"
          matches[j] &= letters[j];
          __CrestLoad(31046, (unsigned long )(& j), (long long )j);
          __CrestLoad(31045, (unsigned long )0, (long long )1);
          __CrestApply2(31044, 0, (long long )(j + 1));
          __CrestStore(31047, (unsigned long )(& j));
# 1588 "dfa.c"
          j ++;
        }
        while_break___3: ;
        }
      }
      }
      __CrestLoad(31048, (unsigned long )0, (long long )0);
      __CrestStore(31049, (unsigned long )(& j));
# 1592 "dfa.c"
      j = 0;
      {
# 1592 "dfa.c"
      while (1) {
        while_continue___4: ;
        {
        __CrestLoad(31052, (unsigned long )(& j), (long long )j);
        __CrestLoad(31051, (unsigned long )0, (long long )((((unsigned long )(1 << 8) + 8UL * sizeof(int )) - 1UL) / (8UL * sizeof(int ))));
        __CrestApply2(31050, 16, (long long )((unsigned long )j < (((unsigned long )(1 << 8) + 8UL * sizeof(int )) - 1UL) / (8UL * sizeof(int ))));
# 1592 "dfa.c"
        if ((unsigned long )j < (((unsigned long )(1 << 8) + 8UL * sizeof(int )) - 1UL) / (8UL * sizeof(int ))) {
          __CrestBranch(31053, 5510, 1);
          {
          __CrestLoad(31057, (unsigned long )(& matches[j]), (long long )matches[j]);
          __CrestLoad(31056, (unsigned long )0, (long long )0);
          __CrestApply2(31055, 12, (long long )(matches[j] == 0));
# 1592 "dfa.c"
          if (matches[j] == 0) {
            __CrestBranch(31058, 5511, 1);

          } else {
            __CrestBranch(31059, 5512, 0);
# 1592 "dfa.c"
            goto while_break___4;
          }
          }
        } else {
          __CrestBranch(31054, 5513, 0);
# 1592 "dfa.c"
          goto while_break___4;
        }
        }
# 1593 "dfa.c"
        goto __Cont___0;
        __Cont___0:
        __CrestLoad(31062, (unsigned long )(& j), (long long )j);
        __CrestLoad(31061, (unsigned long )0, (long long )1);
        __CrestApply2(31060, 0, (long long )(j + 1));
        __CrestStore(31063, (unsigned long )(& j));
# 1592 "dfa.c"
        j ++;
      }
      while_break___4: ;
      }
      {
      __CrestLoad(31066, (unsigned long )(& j), (long long )j);
      __CrestLoad(31065, (unsigned long )0, (long long )((((unsigned long )(1 << 8) + 8UL * sizeof(int )) - 1UL) / (8UL * sizeof(int ))));
      __CrestApply2(31064, 12, (long long )((unsigned long )j == (((unsigned long )(1 << 8) + 8UL * sizeof(int )) - 1UL) / (8UL * sizeof(int ))));
# 1594 "dfa.c"
      if ((unsigned long )j == (((unsigned long )(1 << 8) + 8UL * sizeof(int )) - 1UL) / (8UL * sizeof(int ))) {
        __CrestBranch(31067, 5518, 1);
# 1595 "dfa.c"
        goto __Cont;
      } else {
        __CrestBranch(31068, 5519, 0);

      }
      }
    } else {
      __CrestBranch(30919, 5520, 0);

    }
    }
    __CrestLoad(31069, (unsigned long )0, (long long )0);
    __CrestStore(31070, (unsigned long )(& j));
# 1598 "dfa.c"
    j = 0;
    {
# 1598 "dfa.c"
    while (1) {
      while_continue___5: ;
      {
      __CrestLoad(31073, (unsigned long )(& j), (long long )j);
      __CrestLoad(31072, (unsigned long )(& ngrps), (long long )ngrps);
      __CrestApply2(31071, 16, (long long )(j < ngrps));
# 1598 "dfa.c"
      if (j < ngrps) {
        __CrestBranch(31074, 5526, 1);

      } else {
        __CrestBranch(31075, 5527, 0);
# 1598 "dfa.c"
        goto while_break___5;
      }
      }
      {
# 1603 "dfa.c"
      mem_59 = d->tokens + pos.strchr;
      {
      __CrestLoad(31078, (unsigned long )mem_59, (long long )*mem_59);
      __CrestLoad(31077, (unsigned long )0, (long long )0);
      __CrestApply2(31076, 17, (long long )((int )*mem_59 >= 0));
# 1603 "dfa.c"
      if ((int )*mem_59 >= 0) {
        __CrestBranch(31079, 5531, 1);
        {
# 1603 "dfa.c"
        mem_60 = d->tokens + pos.strchr;
        {
        __CrestLoad(31083, (unsigned long )mem_60, (long long )*mem_60);
        __CrestLoad(31082, (unsigned long )0, (long long )(1 << 8));
        __CrestApply2(31081, 16, (long long )((int )*mem_60 < 1 << 8));
# 1603 "dfa.c"
        if ((int )*mem_60 < 1 << 8) {
          __CrestBranch(31084, 5534, 1);
# 1603 "dfa.c"
          mem_61 = d->tokens + pos.strchr;
          __CrestLoad(31086, (unsigned long )mem_61, (long long )*mem_61);
# 1603 "dfa.c"
          tmp___5 = tstbit((int )*mem_61, labels[j]);
          __CrestHandleReturn(31088, (long long )tmp___5);
          __CrestStore(31087, (unsigned long )(& tmp___5));
          {
          __CrestLoad(31091, (unsigned long )(& tmp___5), (long long )tmp___5);
          __CrestLoad(31090, (unsigned long )0, (long long )0);
          __CrestApply2(31089, 13, (long long )(tmp___5 != 0));
# 1603 "dfa.c"
          if (tmp___5 != 0) {
            __CrestBranch(31092, 5536, 1);

          } else {
            __CrestBranch(31093, 5537, 0);
# 1605 "dfa.c"
            goto __Cont___1;
          }
          }
        } else {
          __CrestBranch(31085, 5538, 0);

        }
        }
        }
      } else {
        __CrestBranch(31080, 5539, 0);

      }
      }
      }
      __CrestLoad(31094, (unsigned long )0, (long long )0);
      __CrestStore(31095, (unsigned long )(& intersectf));
# 1609 "dfa.c"
      intersectf = 0;
      __CrestLoad(31096, (unsigned long )0, (long long )0);
      __CrestStore(31097, (unsigned long )(& k));
# 1610 "dfa.c"
      k = 0;
      {
# 1610 "dfa.c"
      while (1) {
        while_continue___6: ;
        {
        __CrestLoad(31100, (unsigned long )(& k), (long long )k);
        __CrestLoad(31099, (unsigned long )0, (long long )((((unsigned long )(1 << 8) + 8UL * sizeof(int )) - 1UL) / (8UL * sizeof(int ))));
        __CrestApply2(31098, 16, (long long )((unsigned long )k < (((unsigned long )(1 << 8) + 8UL * sizeof(int )) - 1UL) / (8UL * sizeof(int ))));
# 1610 "dfa.c"
        if ((unsigned long )k < (((unsigned long )(1 << 8) + 8UL * sizeof(int )) - 1UL) / (8UL * sizeof(int ))) {
          __CrestBranch(31101, 5545, 1);

        } else {
          __CrestBranch(31102, 5546, 0);
# 1610 "dfa.c"
          goto while_break___6;
        }
        }
        __CrestLoad(31105, (unsigned long )(& matches[k]), (long long )matches[k]);
        __CrestLoad(31104, (unsigned long )(& labels[j][k]), (long long )labels[j][k]);
        __CrestApply2(31103, 5, (long long )(matches[k] & labels[j][k]));
        __CrestStore(31106, (unsigned long )(& tmp___6));
# 1611 "dfa.c"
        tmp___6 = matches[k] & labels[j][k];
        __CrestLoad(31107, (unsigned long )(& tmp___6), (long long )tmp___6);
        __CrestStore(31108, (unsigned long )(& intersect[k]));
# 1611 "dfa.c"
        intersect[k] = tmp___6;
        {
        __CrestLoad(31111, (unsigned long )(& tmp___6), (long long )tmp___6);
        __CrestLoad(31110, (unsigned long )0, (long long )0);
        __CrestApply2(31109, 13, (long long )(tmp___6 != 0));
# 1611 "dfa.c"
        if (tmp___6 != 0) {
          __CrestBranch(31112, 5549, 1);
          __CrestLoad(31114, (unsigned long )0, (long long )1);
          __CrestStore(31115, (unsigned long )(& intersectf));
# 1611 "dfa.c"
          intersectf = 1;
        } else {
          __CrestBranch(31113, 5550, 0);

        }
        }
        __CrestLoad(31118, (unsigned long )(& k), (long long )k);
        __CrestLoad(31117, (unsigned long )0, (long long )1);
        __CrestApply2(31116, 0, (long long )(k + 1));
        __CrestStore(31119, (unsigned long )(& k));
# 1610 "dfa.c"
        k ++;
      }
      while_break___6: ;
      }
      {
      __CrestLoad(31122, (unsigned long )(& intersectf), (long long )intersectf);
      __CrestLoad(31121, (unsigned long )0, (long long )0);
      __CrestApply2(31120, 12, (long long )(intersectf == 0));
# 1612 "dfa.c"
      if (intersectf == 0) {
        __CrestBranch(31123, 5554, 1);
# 1613 "dfa.c"
        goto __Cont___1;
      } else {
        __CrestBranch(31124, 5555, 0);

      }
      }
      __CrestLoad(31125, (unsigned long )0, (long long )0);
      __CrestStore(31126, (unsigned long )(& matchesf));
# 1616 "dfa.c"
      matchesf = 0;
      __CrestLoad(31127, (unsigned long )(& matchesf), (long long )matchesf);
      __CrestStore(31128, (unsigned long )(& leftoversf));
# 1616 "dfa.c"
      leftoversf = matchesf;
      __CrestLoad(31129, (unsigned long )0, (long long )0);
      __CrestStore(31130, (unsigned long )(& k));
# 1617 "dfa.c"
      k = 0;
      {
# 1617 "dfa.c"
      while (1) {
        while_continue___7: ;
        {
        __CrestLoad(31133, (unsigned long )(& k), (long long )k);
        __CrestLoad(31132, (unsigned long )0, (long long )((((unsigned long )(1 << 8) + 8UL * sizeof(int )) - 1UL) / (8UL * sizeof(int ))));
        __CrestApply2(31131, 16, (long long )((unsigned long )k < (((unsigned long )(1 << 8) + 8UL * sizeof(int )) - 1UL) / (8UL * sizeof(int ))));
# 1617 "dfa.c"
        if ((unsigned long )k < (((unsigned long )(1 << 8) + 8UL * sizeof(int )) - 1UL) / (8UL * sizeof(int ))) {
          __CrestBranch(31134, 5561, 1);

        } else {
          __CrestBranch(31135, 5562, 0);
# 1617 "dfa.c"
          goto while_break___7;
        }
        }
        __CrestLoad(31136, (unsigned long )(& matches[k]), (long long )matches[k]);
        __CrestStore(31137, (unsigned long )(& match));
# 1620 "dfa.c"
        match = matches[k];
        __CrestLoad(31138, (unsigned long )(& labels[j][k]), (long long )labels[j][k]);
        __CrestStore(31139, (unsigned long )(& label));
# 1620 "dfa.c"
        label = labels[j][k];
        __CrestLoad(31143, (unsigned long )(& match), (long long )match);
        __CrestApply1(31142, 20, (long long )(~ match));
        __CrestLoad(31141, (unsigned long )(& label), (long long )label);
        __CrestApply2(31140, 5, (long long )(~ match & label));
        __CrestStore(31144, (unsigned long )(& tmp___7));
# 1622 "dfa.c"
        tmp___7 = ~ match & label;
        __CrestLoad(31145, (unsigned long )(& tmp___7), (long long )tmp___7);
        __CrestStore(31146, (unsigned long )(& leftovers[k]));
# 1622 "dfa.c"
        leftovers[k] = tmp___7;
        {
        __CrestLoad(31149, (unsigned long )(& tmp___7), (long long )tmp___7);
        __CrestLoad(31148, (unsigned long )0, (long long )0);
        __CrestApply2(31147, 13, (long long )(tmp___7 != 0));
# 1622 "dfa.c"
        if (tmp___7 != 0) {
          __CrestBranch(31150, 5565, 1);
          __CrestLoad(31152, (unsigned long )0, (long long )1);
          __CrestStore(31153, (unsigned long )(& leftoversf));
# 1622 "dfa.c"
          leftoversf = 1;
        } else {
          __CrestBranch(31151, 5566, 0);

        }
        }
        __CrestLoad(31157, (unsigned long )(& match), (long long )match);
        __CrestLoad(31156, (unsigned long )(& label), (long long )label);
        __CrestApply1(31155, 20, (long long )(~ label));
        __CrestApply2(31154, 5, (long long )(match & ~ label));
        __CrestStore(31158, (unsigned long )(& tmp___8));
# 1623 "dfa.c"
        tmp___8 = match & ~ label;
        __CrestLoad(31159, (unsigned long )(& tmp___8), (long long )tmp___8);
        __CrestStore(31160, (unsigned long )(& matches[k]));
# 1623 "dfa.c"
        matches[k] = tmp___8;
        {
        __CrestLoad(31163, (unsigned long )(& tmp___8), (long long )tmp___8);
        __CrestLoad(31162, (unsigned long )0, (long long )0);
        __CrestApply2(31161, 13, (long long )(tmp___8 != 0));
# 1623 "dfa.c"
        if (tmp___8 != 0) {
          __CrestBranch(31164, 5569, 1);
          __CrestLoad(31166, (unsigned long )0, (long long )1);
          __CrestStore(31167, (unsigned long )(& matchesf));
# 1623 "dfa.c"
          matchesf = 1;
        } else {
          __CrestBranch(31165, 5570, 0);

        }
        }
        __CrestLoad(31170, (unsigned long )(& k), (long long )k);
        __CrestLoad(31169, (unsigned long )0, (long long )1);
        __CrestApply2(31168, 0, (long long )(k + 1));
        __CrestStore(31171, (unsigned long )(& k));
# 1617 "dfa.c"
        k ++;
      }
      while_break___7: ;
      }
      {
      __CrestLoad(31174, (unsigned long )(& leftoversf), (long long )leftoversf);
      __CrestLoad(31173, (unsigned long )0, (long long )0);
      __CrestApply2(31172, 13, (long long )(leftoversf != 0));
# 1627 "dfa.c"
      if (leftoversf != 0) {
        __CrestBranch(31175, 5574, 1);
# 1629 "dfa.c"
        copyset(leftovers, labels[ngrps]);
        __CrestClearStack(31177);
# 1630 "dfa.c"
        copyset(intersect, labels[j]);
        __CrestClearStack(31178);
        __CrestLoad(31181, (unsigned long )(& d->nleaves), (long long )d->nleaves);
        __CrestLoad(31180, (unsigned long )0, (long long )sizeof(position ));
        __CrestApply2(31179, 2, (long long )((unsigned long )d->nleaves * sizeof(position )));
# 1631 "dfa.c"
        tmp___9 = xmalloc((unsigned long )d->nleaves * sizeof(position ));
        __CrestClearStack(31182);
# 1631 "dfa.c"
        grps[ngrps].elems = (position *)tmp___9;
# 1632 "dfa.c"
        copy(& grps[j], & grps[ngrps]);
        __CrestClearStack(31183);
        __CrestLoad(31186, (unsigned long )(& ngrps), (long long )ngrps);
        __CrestLoad(31185, (unsigned long )0, (long long )1);
        __CrestApply2(31184, 0, (long long )(ngrps + 1));
        __CrestStore(31187, (unsigned long )(& ngrps));
# 1633 "dfa.c"
        ngrps ++;
      } else {
        __CrestBranch(31176, 5575, 0);

      }
      }
      __CrestLoad(31188, (unsigned long )(& grps[j].nelem), (long long )grps[j].nelem);
      __CrestStore(31189, (unsigned long )(& tmp___10));
# 1638 "dfa.c"
      tmp___10 = grps[j].nelem;
      __CrestLoad(31192, (unsigned long )(& grps[j].nelem), (long long )grps[j].nelem);
      __CrestLoad(31191, (unsigned long )0, (long long )1);
      __CrestApply2(31190, 0, (long long )(grps[j].nelem + 1));
      __CrestStore(31193, (unsigned long )(& grps[j].nelem));
# 1638 "dfa.c"
      (grps[j].nelem) ++;
# 1638 "dfa.c"
      mem_62 = grps[j].elems + tmp___10;
# 1638 "dfa.c"
      *mem_62 = pos;
      {
      __CrestLoad(31196, (unsigned long )(& matchesf), (long long )matchesf);
      __CrestLoad(31195, (unsigned long )0, (long long )0);
      __CrestApply2(31194, 12, (long long )(matchesf == 0));
# 1642 "dfa.c"
      if (matchesf == 0) {
        __CrestBranch(31197, 5578, 1);
# 1643 "dfa.c"
        goto while_break___5;
      } else {
        __CrestBranch(31198, 5579, 0);

      }
      }
      __Cont___1:
      __CrestLoad(31201, (unsigned long )(& j), (long long )j);
      __CrestLoad(31200, (unsigned long )0, (long long )1);
      __CrestApply2(31199, 0, (long long )(j + 1));
      __CrestStore(31202, (unsigned long )(& j));
# 1598 "dfa.c"
      j ++;
    }
    while_break___5: ;
    }
    {
    __CrestLoad(31205, (unsigned long )(& j), (long long )j);
    __CrestLoad(31204, (unsigned long )(& ngrps), (long long )ngrps);
    __CrestApply2(31203, 12, (long long )(j == ngrps));
# 1648 "dfa.c"
    if (j == ngrps) {
      __CrestBranch(31206, 5583, 1);
# 1650 "dfa.c"
      copyset(matches, labels[ngrps]);
      __CrestClearStack(31208);
# 1651 "dfa.c"
      zeroset(matches);
      __CrestClearStack(31209);
      __CrestLoad(31212, (unsigned long )(& d->nleaves), (long long )d->nleaves);
      __CrestLoad(31211, (unsigned long )0, (long long )sizeof(position ));
      __CrestApply2(31210, 2, (long long )((unsigned long )d->nleaves * sizeof(position )));
# 1652 "dfa.c"
      tmp___11 = xmalloc((unsigned long )d->nleaves * sizeof(position ));
      __CrestClearStack(31213);
# 1652 "dfa.c"
      grps[ngrps].elems = (position *)tmp___11;
      __CrestLoad(31214, (unsigned long )0, (long long )1);
      __CrestStore(31215, (unsigned long )(& grps[ngrps].nelem));
# 1653 "dfa.c"
      grps[ngrps].nelem = 1;
# 1654 "dfa.c"
      mem_63 = grps[ngrps].elems + 0;
# 1654 "dfa.c"
      *mem_63 = pos;
      __CrestLoad(31218, (unsigned long )(& ngrps), (long long )ngrps);
      __CrestLoad(31217, (unsigned long )0, (long long )1);
      __CrestApply2(31216, 0, (long long )(ngrps + 1));
      __CrestStore(31219, (unsigned long )(& ngrps));
# 1655 "dfa.c"
      ngrps ++;
    } else {
      __CrestBranch(31207, 5584, 0);

    }
    }
    __Cont:
    __CrestLoad(31222, (unsigned long )(& i), (long long )i);
    __CrestLoad(31221, (unsigned long )0, (long long )1);
    __CrestApply2(31220, 0, (long long )(i + 1));
    __CrestStore(31223, (unsigned long )(& i));
# 1561 "dfa.c"
    i ++;
  }
  while_break___0: ;
  }
  __CrestLoad(31226, (unsigned long )(& d->nleaves), (long long )d->nleaves);
  __CrestLoad(31225, (unsigned long )0, (long long )sizeof(position ));
  __CrestApply2(31224, 2, (long long )((unsigned long )d->nleaves * sizeof(position )));
# 1659 "dfa.c"
  tmp___12 = xmalloc((unsigned long )d->nleaves * sizeof(position ));
  __CrestClearStack(31227);
# 1659 "dfa.c"
  follows.elems = (position *)tmp___12;
  __CrestLoad(31230, (unsigned long )(& d->nleaves), (long long )d->nleaves);
  __CrestLoad(31229, (unsigned long )0, (long long )sizeof(position ));
  __CrestApply2(31228, 2, (long long )((unsigned long )d->nleaves * sizeof(position )));
# 1660 "dfa.c"
  tmp___13 = xmalloc((unsigned long )d->nleaves * sizeof(position ));
  __CrestClearStack(31231);
# 1660 "dfa.c"
  tmp.elems = (position *)tmp___13;
  {
  __CrestLoad(31234, (unsigned long )(& d->searchflag), (long long )d->searchflag);
  __CrestLoad(31233, (unsigned long )0, (long long )0);
  __CrestApply2(31232, 13, (long long )(d->searchflag != 0));
# 1665 "dfa.c"
  if (d->searchflag != 0) {
    __CrestBranch(31235, 5589, 1);
    __CrestLoad(31237, (unsigned long )0, (long long )0);
    __CrestStore(31238, (unsigned long )(& wants_newline));
# 1667 "dfa.c"
    wants_newline = 0;
    __CrestLoad(31239, (unsigned long )0, (long long )0);
    __CrestStore(31240, (unsigned long )(& wants_letter));
# 1668 "dfa.c"
    wants_letter = 0;
    __CrestLoad(31241, (unsigned long )0, (long long )0);
    __CrestStore(31242, (unsigned long )(& i));
# 1669 "dfa.c"
    i = 0;
    {
# 1669 "dfa.c"
    while (1) {
      while_continue___8: ;
      {
# 1669 "dfa.c"
      mem_64 = d->states + 0;
      {
      __CrestLoad(31245, (unsigned long )(& i), (long long )i);
      __CrestLoad(31244, (unsigned long )(& mem_64->elems.nelem), (long long )mem_64->elems.nelem);
      __CrestApply2(31243, 16, (long long )(i < mem_64->elems.nelem));
# 1669 "dfa.c"
      if (i < mem_64->elems.nelem) {
        __CrestBranch(31246, 5596, 1);

      } else {
        __CrestBranch(31247, 5597, 0);
# 1669 "dfa.c"
        goto while_break___8;
      }
      }
      }
      {
# 1671 "dfa.c"
      mem_65 = d->states + 0;
# 1671 "dfa.c"
      mem_66 = mem_65->elems.elems + i;
# 1671 "dfa.c"
      mem_67 = d->states + 0;
# 1671 "dfa.c"
      mem_68 = mem_67->elems.elems + i;
      {
      __CrestLoad(31256, (unsigned long )(& mem_66->constraint), (long long )mem_66->constraint);
      __CrestLoad(31255, (unsigned long )0, (long long )192U);
      __CrestApply2(31254, 5, (long long )(mem_66->constraint & 192U));
      __CrestLoad(31253, (unsigned long )0, (long long )2);
      __CrestApply2(31252, 9, (long long )((mem_66->constraint & 192U) >> 2));
      __CrestLoad(31251, (unsigned long )(& mem_68->constraint), (long long )mem_68->constraint);
      __CrestLoad(31250, (unsigned long )0, (long long )48U);
      __CrestApply2(31249, 5, (long long )(mem_68->constraint & 48U));
      __CrestApply2(31248, 13, (long long )((mem_66->constraint & 192U) >> 2 != (mem_68->constraint & 48U)));
# 1671 "dfa.c"
      if ((mem_66->constraint & 192U) >> 2 != (mem_68->constraint & 48U)) {
        __CrestBranch(31257, 5601, 1);
        __CrestLoad(31259, (unsigned long )0, (long long )1);
        __CrestStore(31260, (unsigned long )(& wants_newline));
# 1672 "dfa.c"
        wants_newline = 1;
      } else {
        __CrestBranch(31258, 5602, 0);

      }
      }
      }
      {
# 1673 "dfa.c"
      mem_69 = d->states + 0;
# 1673 "dfa.c"
      mem_70 = mem_69->elems.elems + i;
# 1673 "dfa.c"
      mem_71 = d->states + 0;
# 1673 "dfa.c"
      mem_72 = mem_71->elems.elems + i;
      {
      __CrestLoad(31269, (unsigned long )(& mem_70->constraint), (long long )mem_70->constraint);
      __CrestLoad(31268, (unsigned long )0, (long long )12U);
      __CrestApply2(31267, 5, (long long )(mem_70->constraint & 12U));
      __CrestLoad(31266, (unsigned long )0, (long long )2);
      __CrestApply2(31265, 9, (long long )((mem_70->constraint & 12U) >> 2));
      __CrestLoad(31264, (unsigned long )(& mem_72->constraint), (long long )mem_72->constraint);
      __CrestLoad(31263, (unsigned long )0, (long long )3U);
      __CrestApply2(31262, 5, (long long )(mem_72->constraint & 3U));
      __CrestApply2(31261, 13, (long long )((mem_70->constraint & 12U) >> 2 != (mem_72->constraint & 3U)));
# 1673 "dfa.c"
      if ((mem_70->constraint & 12U) >> 2 != (mem_72->constraint & 3U)) {
        __CrestBranch(31270, 5606, 1);
        __CrestLoad(31272, (unsigned long )0, (long long )1);
        __CrestStore(31273, (unsigned long )(& wants_letter));
# 1674 "dfa.c"
        wants_letter = 1;
      } else {
        __CrestBranch(31271, 5607, 0);

      }
      }
      }
      __CrestLoad(31276, (unsigned long )(& i), (long long )i);
      __CrestLoad(31275, (unsigned long )0, (long long )1);
      __CrestApply2(31274, 0, (long long )(i + 1));
      __CrestStore(31277, (unsigned long )(& i));
# 1669 "dfa.c"
      i ++;
    }
    while_break___8: ;
    }
# 1676 "dfa.c"
    mem_73 = d->states + 0;
# 1676 "dfa.c"
    copy(& mem_73->elems, & follows);
    __CrestClearStack(31278);
    __CrestLoad(31279, (unsigned long )0, (long long )0);
    __CrestLoad(31280, (unsigned long )0, (long long )0);
# 1677 "dfa.c"
    state = state_index(d, & follows, 0, 0);
    __CrestHandleReturn(31282, (long long )state);
    __CrestStore(31281, (unsigned long )(& state));
    {
    __CrestLoad(31285, (unsigned long )(& wants_newline), (long long )wants_newline);
    __CrestLoad(31284, (unsigned long )0, (long long )0);
    __CrestApply2(31283, 13, (long long )(wants_newline != 0));
# 1678 "dfa.c"
    if (wants_newline != 0) {
      __CrestBranch(31286, 5612, 1);
      __CrestLoad(31288, (unsigned long )0, (long long )1);
      __CrestLoad(31289, (unsigned long )0, (long long )0);
# 1679 "dfa.c"
      state_newline = state_index(d, & follows, 1, 0);
      __CrestHandleReturn(31291, (long long )state_newline);
      __CrestStore(31290, (unsigned long )(& state_newline));
    } else {
      __CrestBranch(31287, 5613, 0);
      __CrestLoad(31292, (unsigned long )(& state), (long long )state);
      __CrestStore(31293, (unsigned long )(& state_newline));
# 1681 "dfa.c"
      state_newline = state;
    }
    }
    {
    __CrestLoad(31296, (unsigned long )(& wants_letter), (long long )wants_letter);
    __CrestLoad(31295, (unsigned long )0, (long long )0);
    __CrestApply2(31294, 13, (long long )(wants_letter != 0));
# 1682 "dfa.c"
    if (wants_letter != 0) {
      __CrestBranch(31297, 5615, 1);
      __CrestLoad(31299, (unsigned long )0, (long long )0);
      __CrestLoad(31300, (unsigned long )0, (long long )1);
# 1683 "dfa.c"
      state_letter = state_index(d, & follows, 0, 1);
      __CrestHandleReturn(31302, (long long )state_letter);
      __CrestStore(31301, (unsigned long )(& state_letter));
    } else {
      __CrestBranch(31298, 5616, 0);
      __CrestLoad(31303, (unsigned long )(& state), (long long )state);
      __CrestStore(31304, (unsigned long )(& state_letter));
# 1685 "dfa.c"
      state_letter = state;
    }
    }
    __CrestLoad(31305, (unsigned long )0, (long long )0);
    __CrestStore(31306, (unsigned long )(& i));
# 1686 "dfa.c"
    i = 0;
    {
# 1686 "dfa.c"
    while (1) {
      while_continue___9: ;
      {
      __CrestLoad(31309, (unsigned long )(& i), (long long )i);
      __CrestLoad(31308, (unsigned long )0, (long long )(1 << 8));
      __CrestApply2(31307, 16, (long long )(i < 1 << 8));
# 1686 "dfa.c"
      if (i < 1 << 8) {
        __CrestBranch(31310, 5622, 1);

      } else {
        __CrestBranch(31311, 5623, 0);
# 1686 "dfa.c"
        goto while_break___9;
      }
      }
# 1687 "dfa.c"
      tmp___15 = __ctype_b_loc();
      __CrestClearStack(31312);
      {
# 1687 "dfa.c"
      mem_74 = *tmp___15 + i;
      {
      __CrestLoad(31317, (unsigned long )mem_74, (long long )*mem_74);
      __CrestLoad(31316, (unsigned long )0, (long long )8);
      __CrestApply2(31315, 5, (long long )((int const )*mem_74 & 8));
      __CrestLoad(31314, (unsigned long )0, (long long )0);
      __CrestApply2(31313, 13, (long long )(((int const )*mem_74 & 8) != 0));
# 1687 "dfa.c"
      if (((int const )*mem_74 & 8) != 0) {
        __CrestBranch(31318, 5628, 1);
# 1687 "dfa.c"
        mem_75 = trans + i;
        __CrestLoad(31320, (unsigned long )(& state_letter), (long long )state_letter);
        __CrestStore(31321, (unsigned long )mem_75);
# 1687 "dfa.c"
        *mem_75 = state_letter;
      } else {
        __CrestBranch(31319, 5629, 0);
        {
        __CrestLoad(31324, (unsigned long )(& i), (long long )i);
        __CrestLoad(31323, (unsigned long )0, (long long )95);
        __CrestApply2(31322, 12, (long long )(i == 95));
# 1687 "dfa.c"
        if (i == 95) {
          __CrestBranch(31325, 5630, 1);
# 1687 "dfa.c"
          mem_76 = trans + i;
          __CrestLoad(31327, (unsigned long )(& state_letter), (long long )state_letter);
          __CrestStore(31328, (unsigned long )mem_76);
# 1687 "dfa.c"
          *mem_76 = state_letter;
        } else {
          __CrestBranch(31326, 5631, 0);
# 1687 "dfa.c"
          mem_77 = trans + i;
          __CrestLoad(31329, (unsigned long )(& state), (long long )state);
          __CrestStore(31330, (unsigned long )mem_77);
# 1687 "dfa.c"
          *mem_77 = state;
        }
        }
      }
      }
      }
      __CrestLoad(31333, (unsigned long )(& i), (long long )i);
      __CrestLoad(31332, (unsigned long )0, (long long )1);
      __CrestApply2(31331, 0, (long long )(i + 1));
      __CrestStore(31334, (unsigned long )(& i));
# 1686 "dfa.c"
      i ++;
    }
    while_break___9: ;
    }
# 1688 "dfa.c"
    mem_78 = trans + '\n';
    __CrestLoad(31335, (unsigned long )(& state_newline), (long long )state_newline);
    __CrestStore(31336, (unsigned long )mem_78);
# 1688 "dfa.c"
    *mem_78 = state_newline;
  } else {
    __CrestBranch(31236, 5635, 0);
    __CrestLoad(31337, (unsigned long )0, (long long )0);
    __CrestStore(31338, (unsigned long )(& i));
# 1691 "dfa.c"
    i = 0;
    {
# 1691 "dfa.c"
    while (1) {
      while_continue___10: ;
      {
      __CrestLoad(31341, (unsigned long )(& i), (long long )i);
      __CrestLoad(31340, (unsigned long )0, (long long )(1 << 8));
      __CrestApply2(31339, 16, (long long )(i < 1 << 8));
# 1691 "dfa.c"
      if (i < 1 << 8) {
        __CrestBranch(31342, 5640, 1);

      } else {
        __CrestBranch(31343, 5641, 0);
# 1691 "dfa.c"
        goto while_break___10;
      }
      }
# 1692 "dfa.c"
      mem_79 = trans + i;
      __CrestLoad(31344, (unsigned long )0, (long long )-1);
      __CrestStore(31345, (unsigned long )mem_79);
# 1692 "dfa.c"
      *mem_79 = -1;
      __CrestLoad(31348, (unsigned long )(& i), (long long )i);
      __CrestLoad(31347, (unsigned long )0, (long long )1);
      __CrestApply2(31346, 0, (long long )(i + 1));
      __CrestStore(31349, (unsigned long )(& i));
# 1691 "dfa.c"
      i ++;
    }
    while_break___10: ;
    }
  }
  }
  __CrestLoad(31350, (unsigned long )0, (long long )0);
  __CrestStore(31351, (unsigned long )(& i));
# 1694 "dfa.c"
  i = 0;
  {
# 1694 "dfa.c"
  while (1) {
    while_continue___11: ;
    {
    __CrestLoad(31354, (unsigned long )(& i), (long long )i);
    __CrestLoad(31353, (unsigned long )(& ngrps), (long long )ngrps);
    __CrestApply2(31352, 16, (long long )(i < ngrps));
# 1694 "dfa.c"
    if (i < ngrps) {
      __CrestBranch(31355, 5649, 1);

    } else {
      __CrestBranch(31356, 5650, 0);
# 1694 "dfa.c"
      goto while_break___11;
    }
    }
    __CrestLoad(31357, (unsigned long )0, (long long )0);
    __CrestStore(31358, (unsigned long )(& follows.nelem));
# 1696 "dfa.c"
    follows.nelem = 0;
    __CrestLoad(31359, (unsigned long )0, (long long )0);
    __CrestStore(31360, (unsigned long )(& j));
# 1700 "dfa.c"
    j = 0;
    {
# 1700 "dfa.c"
    while (1) {
      while_continue___12: ;
      {
      __CrestLoad(31363, (unsigned long )(& j), (long long )j);
      __CrestLoad(31362, (unsigned long )(& grps[i].nelem), (long long )grps[i].nelem);
      __CrestApply2(31361, 16, (long long )(j < grps[i].nelem));
# 1700 "dfa.c"
      if (j < grps[i].nelem) {
        __CrestBranch(31364, 5656, 1);

      } else {
        __CrestBranch(31365, 5657, 0);
# 1700 "dfa.c"
        goto while_break___12;
      }
      }
      __CrestLoad(31366, (unsigned long )0, (long long )0);
      __CrestStore(31367, (unsigned long )(& k));
# 1701 "dfa.c"
      k = 0;
      {
# 1701 "dfa.c"
      while (1) {
        while_continue___13: ;
        {
# 1701 "dfa.c"
        mem_80 = grps[i].elems + j;
# 1701 "dfa.c"
        mem_81 = d->follows + mem_80->strchr;
        {
        __CrestLoad(31370, (unsigned long )(& k), (long long )k);
        __CrestLoad(31369, (unsigned long )(& mem_81->nelem), (long long )mem_81->nelem);
        __CrestApply2(31368, 16, (long long )(k < mem_81->nelem));
# 1701 "dfa.c"
        if (k < mem_81->nelem) {
          __CrestBranch(31371, 5665, 1);

        } else {
          __CrestBranch(31372, 5666, 0);
# 1701 "dfa.c"
          goto while_break___13;
        }
        }
        }
# 1702 "dfa.c"
        mem_82 = grps[i].elems + j;
# 1702 "dfa.c"
        mem_83 = d->follows + mem_82->strchr;
# 1702 "dfa.c"
        mem_84 = mem_83->elems + k;
# 1702 "dfa.c"
        insert(*mem_84, & follows);
        __CrestClearStack(31373);
        __CrestLoad(31376, (unsigned long )(& k), (long long )k);
        __CrestLoad(31375, (unsigned long )0, (long long )1);
        __CrestApply2(31374, 0, (long long )(k + 1));
        __CrestStore(31377, (unsigned long )(& k));
# 1701 "dfa.c"
        k ++;
      }
      while_break___13: ;
      }
      __CrestLoad(31380, (unsigned long )(& j), (long long )j);
      __CrestLoad(31379, (unsigned long )0, (long long )1);
      __CrestApply2(31378, 0, (long long )(j + 1));
      __CrestStore(31381, (unsigned long )(& j));
# 1700 "dfa.c"
      j ++;
    }
    while_break___12: ;
    }
    {
    __CrestLoad(31384, (unsigned long )(& d->searchflag), (long long )d->searchflag);
    __CrestLoad(31383, (unsigned long )0, (long long )0);
    __CrestApply2(31382, 13, (long long )(d->searchflag != 0));
# 1706 "dfa.c"
    if (d->searchflag != 0) {
      __CrestBranch(31385, 5672, 1);
      __CrestLoad(31387, (unsigned long )0, (long long )0);
      __CrestStore(31388, (unsigned long )(& j));
# 1707 "dfa.c"
      j = 0;
      {
# 1707 "dfa.c"
      while (1) {
        while_continue___14: ;
        {
# 1707 "dfa.c"
        mem_85 = d->states + 0;
        {
        __CrestLoad(31391, (unsigned long )(& j), (long long )j);
        __CrestLoad(31390, (unsigned long )(& mem_85->elems.nelem), (long long )mem_85->elems.nelem);
        __CrestApply2(31389, 16, (long long )(j < mem_85->elems.nelem));
# 1707 "dfa.c"
        if (j < mem_85->elems.nelem) {
          __CrestBranch(31392, 5679, 1);

        } else {
          __CrestBranch(31393, 5680, 0);
# 1707 "dfa.c"
          goto while_break___14;
        }
        }
        }
# 1708 "dfa.c"
        mem_86 = d->states + 0;
# 1708 "dfa.c"
        mem_87 = mem_86->elems.elems + j;
# 1708 "dfa.c"
        insert(*mem_87, & follows);
        __CrestClearStack(31394);
        __CrestLoad(31397, (unsigned long )(& j), (long long )j);
        __CrestLoad(31396, (unsigned long )0, (long long )1);
        __CrestApply2(31395, 0, (long long )(j + 1));
        __CrestStore(31398, (unsigned long )(& j));
# 1707 "dfa.c"
        j ++;
      }
      while_break___14: ;
      }
    } else {
      __CrestBranch(31386, 5683, 0);

    }
    }
    __CrestLoad(31399, (unsigned long )0, (long long )0);
    __CrestStore(31400, (unsigned long )(& wants_newline));
# 1711 "dfa.c"
    wants_newline = 0;
    __CrestLoad(31401, (unsigned long )0, (long long )'\n');
# 1712 "dfa.c"
    tmp___16 = tstbit('\n', labels[i]);
    __CrestHandleReturn(31403, (long long )tmp___16);
    __CrestStore(31402, (unsigned long )(& tmp___16));
    {
    __CrestLoad(31406, (unsigned long )(& tmp___16), (long long )tmp___16);
    __CrestLoad(31405, (unsigned long )0, (long long )0);
    __CrestApply2(31404, 13, (long long )(tmp___16 != 0));
# 1712 "dfa.c"
    if (tmp___16 != 0) {
      __CrestBranch(31407, 5686, 1);
      __CrestLoad(31409, (unsigned long )0, (long long )0);
      __CrestStore(31410, (unsigned long )(& j));
# 1713 "dfa.c"
      j = 0;
      {
# 1713 "dfa.c"
      while (1) {
        while_continue___15: ;
        {
        __CrestLoad(31413, (unsigned long )(& j), (long long )j);
        __CrestLoad(31412, (unsigned long )(& follows.nelem), (long long )follows.nelem);
        __CrestApply2(31411, 16, (long long )(j < follows.nelem));
# 1713 "dfa.c"
        if (j < follows.nelem) {
          __CrestBranch(31414, 5691, 1);

        } else {
          __CrestBranch(31415, 5692, 0);
# 1713 "dfa.c"
          goto while_break___15;
        }
        }
        {
# 1714 "dfa.c"
        mem_88 = follows.elems + j;
# 1714 "dfa.c"
        mem_89 = follows.elems + j;
        {
        __CrestLoad(31424, (unsigned long )(& mem_88->constraint), (long long )mem_88->constraint);
        __CrestLoad(31423, (unsigned long )0, (long long )192U);
        __CrestApply2(31422, 5, (long long )(mem_88->constraint & 192U));
        __CrestLoad(31421, (unsigned long )0, (long long )2);
        __CrestApply2(31420, 9, (long long )((mem_88->constraint & 192U) >> 2));
        __CrestLoad(31419, (unsigned long )(& mem_89->constraint), (long long )mem_89->constraint);
        __CrestLoad(31418, (unsigned long )0, (long long )48U);
        __CrestApply2(31417, 5, (long long )(mem_89->constraint & 48U));
        __CrestApply2(31416, 13, (long long )((mem_88->constraint & 192U) >> 2 != (mem_89->constraint & 48U)));
# 1714 "dfa.c"
        if ((mem_88->constraint & 192U) >> 2 != (mem_89->constraint & 48U)) {
          __CrestBranch(31425, 5696, 1);
          __CrestLoad(31427, (unsigned long )0, (long long )1);
          __CrestStore(31428, (unsigned long )(& wants_newline));
# 1715 "dfa.c"
          wants_newline = 1;
        } else {
          __CrestBranch(31426, 5697, 0);

        }
        }
        }
        __CrestLoad(31431, (unsigned long )(& j), (long long )j);
        __CrestLoad(31430, (unsigned long )0, (long long )1);
        __CrestApply2(31429, 0, (long long )(j + 1));
        __CrestStore(31432, (unsigned long )(& j));
# 1713 "dfa.c"
        j ++;
      }
      while_break___15: ;
      }
    } else {
      __CrestBranch(31408, 5700, 0);

    }
    }
    __CrestLoad(31433, (unsigned long )0, (long long )0);
    __CrestStore(31434, (unsigned long )(& wants_letter));
# 1717 "dfa.c"
    wants_letter = 0;
    __CrestLoad(31435, (unsigned long )0, (long long )0);
    __CrestStore(31436, (unsigned long )(& j));
# 1718 "dfa.c"
    j = 0;
    {
# 1718 "dfa.c"
    while (1) {
      while_continue___16: ;
      {
      __CrestLoad(31439, (unsigned long )(& j), (long long )j);
      __CrestLoad(31438, (unsigned long )0, (long long )((((unsigned long )(1 << 8) + 8UL * sizeof(int )) - 1UL) / (8UL * sizeof(int ))));
      __CrestApply2(31437, 16, (long long )((unsigned long )j < (((unsigned long )(1 << 8) + 8UL * sizeof(int )) - 1UL) / (8UL * sizeof(int ))));
# 1718 "dfa.c"
      if ((unsigned long )j < (((unsigned long )(1 << 8) + 8UL * sizeof(int )) - 1UL) / (8UL * sizeof(int ))) {
        __CrestBranch(31440, 5706, 1);

      } else {
        __CrestBranch(31441, 5707, 0);
# 1718 "dfa.c"
        goto while_break___16;
      }
      }
      {
      __CrestLoad(31446, (unsigned long )(& labels[i][j]), (long long )labels[i][j]);
      __CrestLoad(31445, (unsigned long )(& letters[j]), (long long )letters[j]);
      __CrestApply2(31444, 5, (long long )(labels[i][j] & letters[j]));
      __CrestLoad(31443, (unsigned long )0, (long long )0);
      __CrestApply2(31442, 13, (long long )((labels[i][j] & letters[j]) != 0));
# 1719 "dfa.c"
      if ((labels[i][j] & letters[j]) != 0) {
        __CrestBranch(31447, 5709, 1);
# 1720 "dfa.c"
        goto while_break___16;
      } else {
        __CrestBranch(31448, 5710, 0);

      }
      }
      __CrestLoad(31451, (unsigned long )(& j), (long long )j);
      __CrestLoad(31450, (unsigned long )0, (long long )1);
      __CrestApply2(31449, 0, (long long )(j + 1));
      __CrestStore(31452, (unsigned long )(& j));
# 1718 "dfa.c"
      j ++;
    }
    while_break___16: ;
    }
    {
    __CrestLoad(31455, (unsigned long )(& j), (long long )j);
    __CrestLoad(31454, (unsigned long )0, (long long )((((unsigned long )(1 << 8) + 8UL * sizeof(int )) - 1UL) / (8UL * sizeof(int ))));
    __CrestApply2(31453, 16, (long long )((unsigned long )j < (((unsigned long )(1 << 8) + 8UL * sizeof(int )) - 1UL) / (8UL * sizeof(int ))));
# 1721 "dfa.c"
    if ((unsigned long )j < (((unsigned long )(1 << 8) + 8UL * sizeof(int )) - 1UL) / (8UL * sizeof(int ))) {
      __CrestBranch(31456, 5714, 1);
      __CrestLoad(31458, (unsigned long )0, (long long )0);
      __CrestStore(31459, (unsigned long )(& j));
# 1722 "dfa.c"
      j = 0;
      {
# 1722 "dfa.c"
      while (1) {
        while_continue___17: ;
        {
        __CrestLoad(31462, (unsigned long )(& j), (long long )j);
        __CrestLoad(31461, (unsigned long )(& follows.nelem), (long long )follows.nelem);
        __CrestApply2(31460, 16, (long long )(j < follows.nelem));
# 1722 "dfa.c"
        if (j < follows.nelem) {
          __CrestBranch(31463, 5719, 1);

        } else {
          __CrestBranch(31464, 5720, 0);
# 1722 "dfa.c"
          goto while_break___17;
        }
        }
        {
# 1723 "dfa.c"
        mem_90 = follows.elems + j;
# 1723 "dfa.c"
        mem_91 = follows.elems + j;
        {
        __CrestLoad(31473, (unsigned long )(& mem_90->constraint), (long long )mem_90->constraint);
        __CrestLoad(31472, (unsigned long )0, (long long )12U);
        __CrestApply2(31471, 5, (long long )(mem_90->constraint & 12U));
        __CrestLoad(31470, (unsigned long )0, (long long )2);
        __CrestApply2(31469, 9, (long long )((mem_90->constraint & 12U) >> 2));
        __CrestLoad(31468, (unsigned long )(& mem_91->constraint), (long long )mem_91->constraint);
        __CrestLoad(31467, (unsigned long )0, (long long )3U);
        __CrestApply2(31466, 5, (long long )(mem_91->constraint & 3U));
        __CrestApply2(31465, 13, (long long )((mem_90->constraint & 12U) >> 2 != (mem_91->constraint & 3U)));
# 1723 "dfa.c"
        if ((mem_90->constraint & 12U) >> 2 != (mem_91->constraint & 3U)) {
          __CrestBranch(31474, 5724, 1);
          __CrestLoad(31476, (unsigned long )0, (long long )1);
          __CrestStore(31477, (unsigned long )(& wants_letter));
# 1724 "dfa.c"
          wants_letter = 1;
        } else {
          __CrestBranch(31475, 5725, 0);

        }
        }
        }
        __CrestLoad(31480, (unsigned long )(& j), (long long )j);
        __CrestLoad(31479, (unsigned long )0, (long long )1);
        __CrestApply2(31478, 0, (long long )(j + 1));
        __CrestStore(31481, (unsigned long )(& j));
# 1722 "dfa.c"
        j ++;
      }
      while_break___17: ;
      }
    } else {
      __CrestBranch(31457, 5728, 0);

    }
    }
    __CrestLoad(31482, (unsigned long )0, (long long )0);
    __CrestLoad(31483, (unsigned long )0, (long long )0);
# 1727 "dfa.c"
    state = state_index(d, & follows, 0, 0);
    __CrestHandleReturn(31485, (long long )state);
    __CrestStore(31484, (unsigned long )(& state));
    {
    __CrestLoad(31488, (unsigned long )(& wants_newline), (long long )wants_newline);
    __CrestLoad(31487, (unsigned long )0, (long long )0);
    __CrestApply2(31486, 13, (long long )(wants_newline != 0));
# 1728 "dfa.c"
    if (wants_newline != 0) {
      __CrestBranch(31489, 5731, 1);
      __CrestLoad(31491, (unsigned long )0, (long long )1);
      __CrestLoad(31492, (unsigned long )0, (long long )0);
# 1729 "dfa.c"
      state_newline = state_index(d, & follows, 1, 0);
      __CrestHandleReturn(31494, (long long )state_newline);
      __CrestStore(31493, (unsigned long )(& state_newline));
    } else {
      __CrestBranch(31490, 5732, 0);
      __CrestLoad(31495, (unsigned long )(& state), (long long )state);
      __CrestStore(31496, (unsigned long )(& state_newline));
# 1731 "dfa.c"
      state_newline = state;
    }
    }
    {
    __CrestLoad(31499, (unsigned long )(& wants_letter), (long long )wants_letter);
    __CrestLoad(31498, (unsigned long )0, (long long )0);
    __CrestApply2(31497, 13, (long long )(wants_letter != 0));
# 1732 "dfa.c"
    if (wants_letter != 0) {
      __CrestBranch(31500, 5734, 1);
      __CrestLoad(31502, (unsigned long )0, (long long )0);
      __CrestLoad(31503, (unsigned long )0, (long long )1);
# 1733 "dfa.c"
      state_letter = state_index(d, & follows, 0, 1);
      __CrestHandleReturn(31505, (long long )state_letter);
      __CrestStore(31504, (unsigned long )(& state_letter));
    } else {
      __CrestBranch(31501, 5735, 0);
      __CrestLoad(31506, (unsigned long )(& state), (long long )state);
      __CrestStore(31507, (unsigned long )(& state_letter));
# 1735 "dfa.c"
      state_letter = state;
    }
    }
    __CrestLoad(31508, (unsigned long )0, (long long )0);
    __CrestStore(31509, (unsigned long )(& j));
# 1738 "dfa.c"
    j = 0;
    {
# 1738 "dfa.c"
    while (1) {
      while_continue___18: ;
      {
      __CrestLoad(31512, (unsigned long )(& j), (long long )j);
      __CrestLoad(31511, (unsigned long )0, (long long )((((unsigned long )(1 << 8) + 8UL * sizeof(int )) - 1UL) / (8UL * sizeof(int ))));
      __CrestApply2(31510, 16, (long long )((unsigned long )j < (((unsigned long )(1 << 8) + 8UL * sizeof(int )) - 1UL) / (8UL * sizeof(int ))));
# 1738 "dfa.c"
      if ((unsigned long )j < (((unsigned long )(1 << 8) + 8UL * sizeof(int )) - 1UL) / (8UL * sizeof(int ))) {
        __CrestBranch(31513, 5741, 1);

      } else {
        __CrestBranch(31514, 5742, 0);
# 1738 "dfa.c"
        goto while_break___18;
      }
      }
      __CrestLoad(31515, (unsigned long )0, (long long )0);
      __CrestStore(31516, (unsigned long )(& k));
# 1739 "dfa.c"
      k = 0;
      {
# 1739 "dfa.c"
      while (1) {
        while_continue___19: ;
        {
        __CrestLoad(31519, (unsigned long )(& k), (long long )k);
        __CrestLoad(31518, (unsigned long )0, (long long )(8UL * sizeof(int )));
        __CrestApply2(31517, 16, (long long )((unsigned long )k < 8UL * sizeof(int )));
# 1739 "dfa.c"
        if ((unsigned long )k < 8UL * sizeof(int )) {
          __CrestBranch(31520, 5748, 1);

        } else {
          __CrestBranch(31521, 5749, 0);
# 1739 "dfa.c"
          goto while_break___19;
        }
        }
        {
        __CrestLoad(31528, (unsigned long )(& labels[i][j]), (long long )labels[i][j]);
        __CrestLoad(31527, (unsigned long )0, (long long )1);
        __CrestLoad(31526, (unsigned long )(& k), (long long )k);
        __CrestApply2(31525, 8, (long long )(1 << k));
        __CrestApply2(31524, 5, (long long )(labels[i][j] & (1 << k)));
        __CrestLoad(31523, (unsigned long )0, (long long )0);
        __CrestApply2(31522, 13, (long long )((labels[i][j] & (1 << k)) != 0));
# 1740 "dfa.c"
        if ((labels[i][j] & (1 << k)) != 0) {
          __CrestBranch(31529, 5751, 1);
          __CrestLoad(31535, (unsigned long )(& j), (long long )j);
          __CrestLoad(31534, (unsigned long )0, (long long )(8UL * sizeof(int )));
          __CrestApply2(31533, 2, (long long )((unsigned long )j * (8UL * sizeof(int ))));
          __CrestLoad(31532, (unsigned long )(& k), (long long )k);
          __CrestApply2(31531, 0, (long long )((unsigned long )j * (8UL * sizeof(int )) + (unsigned long )k));
          __CrestStore(31536, (unsigned long )(& c));
# 1742 "dfa.c"
          c = (int )((unsigned long )j * (8UL * sizeof(int )) + (unsigned long )k);
          {
          __CrestLoad(31539, (unsigned long )(& c), (long long )c);
          __CrestLoad(31538, (unsigned long )0, (long long )10);
          __CrestApply2(31537, 12, (long long )(c == 10));
# 1744 "dfa.c"
          if (c == 10) {
            __CrestBranch(31540, 5753, 1);
# 1745 "dfa.c"
            mem_92 = trans + c;
            __CrestLoad(31542, (unsigned long )(& state_newline), (long long )state_newline);
            __CrestStore(31543, (unsigned long )mem_92);
# 1745 "dfa.c"
            *mem_92 = state_newline;
          } else {
            __CrestBranch(31541, 5754, 0);
# 1746 "dfa.c"
            tmp___17 = __ctype_b_loc();
            __CrestClearStack(31544);
            {
# 1746 "dfa.c"
            mem_93 = *tmp___17 + c;
            {
            __CrestLoad(31549, (unsigned long )mem_93, (long long )*mem_93);
            __CrestLoad(31548, (unsigned long )0, (long long )8);
            __CrestApply2(31547, 5, (long long )((int const )*mem_93 & 8));
            __CrestLoad(31546, (unsigned long )0, (long long )0);
            __CrestApply2(31545, 13, (long long )(((int const )*mem_93 & 8) != 0));
# 1746 "dfa.c"
            if (((int const )*mem_93 & 8) != 0) {
              __CrestBranch(31550, 5758, 1);
# 1747 "dfa.c"
              mem_94 = trans + c;
              __CrestLoad(31552, (unsigned long )(& state_letter), (long long )state_letter);
              __CrestStore(31553, (unsigned long )mem_94);
# 1747 "dfa.c"
              *mem_94 = state_letter;
            } else {
              __CrestBranch(31551, 5759, 0);
              {
              __CrestLoad(31556, (unsigned long )(& c), (long long )c);
              __CrestLoad(31555, (unsigned long )0, (long long )95);
              __CrestApply2(31554, 12, (long long )(c == 95));
# 1746 "dfa.c"
              if (c == 95) {
                __CrestBranch(31557, 5760, 1);
# 1747 "dfa.c"
                mem_95 = trans + c;
                __CrestLoad(31559, (unsigned long )(& state_letter), (long long )state_letter);
                __CrestStore(31560, (unsigned long )mem_95);
# 1747 "dfa.c"
                *mem_95 = state_letter;
              } else {
                __CrestBranch(31558, 5761, 0);
                {
                __CrestLoad(31563, (unsigned long )(& c), (long long )c);
                __CrestLoad(31562, (unsigned long )0, (long long )(1 << 8));
                __CrestApply2(31561, 16, (long long )(c < 1 << 8));
# 1748 "dfa.c"
                if (c < 1 << 8) {
                  __CrestBranch(31564, 5762, 1);
# 1749 "dfa.c"
                  mem_96 = trans + c;
                  __CrestLoad(31566, (unsigned long )(& state), (long long )state);
                  __CrestStore(31567, (unsigned long )mem_96);
# 1749 "dfa.c"
                  *mem_96 = state;
                } else {
                  __CrestBranch(31565, 5763, 0);

                }
                }
              }
              }
            }
            }
            }
          }
          }
        } else {
          __CrestBranch(31530, 5764, 0);

        }
        }
        __CrestLoad(31570, (unsigned long )(& k), (long long )k);
        __CrestLoad(31569, (unsigned long )0, (long long )1);
        __CrestApply2(31568, 0, (long long )(k + 1));
        __CrestStore(31571, (unsigned long )(& k));
# 1739 "dfa.c"
        k ++;
      }
      while_break___19: ;
      }
      __CrestLoad(31574, (unsigned long )(& j), (long long )j);
      __CrestLoad(31573, (unsigned long )0, (long long )1);
      __CrestApply2(31572, 0, (long long )(j + 1));
      __CrestStore(31575, (unsigned long )(& j));
# 1738 "dfa.c"
      j ++;
    }
    while_break___18: ;
    }
    __CrestLoad(31578, (unsigned long )(& i), (long long )i);
    __CrestLoad(31577, (unsigned long )0, (long long )1);
    __CrestApply2(31576, 0, (long long )(i + 1));
    __CrestStore(31579, (unsigned long )(& i));
# 1694 "dfa.c"
    i ++;
  }
  while_break___11: ;
  }
  __CrestLoad(31580, (unsigned long )0, (long long )0);
  __CrestStore(31581, (unsigned long )(& i));
# 1753 "dfa.c"
  i = 0;
  {
# 1753 "dfa.c"
  while (1) {
    while_continue___20: ;
    {
    __CrestLoad(31584, (unsigned long )(& i), (long long )i);
    __CrestLoad(31583, (unsigned long )(& ngrps), (long long )ngrps);
    __CrestApply2(31582, 16, (long long )(i < ngrps));
# 1753 "dfa.c"
    if (i < ngrps) {
      __CrestBranch(31585, 5776, 1);

    } else {
      __CrestBranch(31586, 5777, 0);
# 1753 "dfa.c"
      goto while_break___20;
    }
    }
# 1754 "dfa.c"
    free((void *)grps[i].elems);
    __CrestClearStack(31587);
    __CrestLoad(31590, (unsigned long )(& i), (long long )i);
    __CrestLoad(31589, (unsigned long )0, (long long )1);
    __CrestApply2(31588, 0, (long long )(i + 1));
    __CrestStore(31591, (unsigned long )(& i));
# 1753 "dfa.c"
    i ++;
  }
  while_break___20: ;
  }
# 1755 "dfa.c"
  free((void *)follows.elems);
  __CrestClearStack(31592);
# 1756 "dfa.c"
  free((void *)tmp.elems);
  __CrestClearStack(31593);

  {
  __CrestReturn(31594);
# 1521 "dfa.c"
  return;
  }
}
}
# 1766 "dfa.c"
static void build_state(int s , struct dfa *d )
{
  int *trans ;
  int i ;
  int tmp ;
  int tmp___0 ;
  int tmp___1 ;
  int tmp___2 ;
  int tmp___3 ;
  int tmp___4 ;
  ptr_t tmp___5 ;
  int oldalloc ;
  ptr_t tmp___6 ;
  ptr_t tmp___7 ;
  ptr_t tmp___8 ;
  ptr_t tmp___9 ;
  int tmp___10 ;
  int **mem_18 ;
  int **mem_19 ;
  int **mem_20 ;
  int **mem_21 ;
  int **mem_22 ;
  int **mem_23 ;
  int *mem_24 ;
  dfa_state *mem_25 ;
  dfa_state *mem_26 ;
  dfa_state *mem_27 ;
  dfa_state *mem_28 ;
  int *mem_29 ;
  int *mem_30 ;
  dfa_state *mem_31 ;
  dfa_state *mem_32 ;
  dfa_state *mem_33 ;
  dfa_state *mem_34 ;
  int *mem_35 ;
  int *mem_36 ;
  dfa_state *mem_37 ;
  dfa_state *mem_38 ;
  dfa_state *mem_39 ;
  dfa_state *mem_40 ;
  int *mem_41 ;
  int *mem_42 ;
  int *mem_43 ;
  int *mem_44 ;
  int **mem_45 ;
  int **mem_46 ;
  int *mem_47 ;
  int *mem_48 ;
  int *mem_49 ;
  dfa_state *mem_50 ;
  int **mem_51 ;
  int **mem_52 ;

  {
  __CrestCall(31596, 254);
  __CrestStore(31595, (unsigned long )(& s));
  {
  __CrestLoad(31599, (unsigned long )(& d->trcount), (long long )d->trcount);
  __CrestLoad(31598, (unsigned long )0, (long long )1024);
  __CrestApply2(31597, 17, (long long )(d->trcount >= 1024));
# 1778 "dfa.c"
  if (d->trcount >= 1024) {
    __CrestBranch(31600, 5784, 1);
    __CrestLoad(31602, (unsigned long )0, (long long )0);
    __CrestStore(31603, (unsigned long )(& i));
# 1780 "dfa.c"
    i = 0;
    {
# 1780 "dfa.c"
    while (1) {
      while_continue: ;
      {
      __CrestLoad(31606, (unsigned long )(& i), (long long )i);
      __CrestLoad(31605, (unsigned long )(& d->tralloc), (long long )d->tralloc);
      __CrestApply2(31604, 16, (long long )(i < d->tralloc));
# 1780 "dfa.c"
      if (i < d->tralloc) {
        __CrestBranch(31607, 5789, 1);

      } else {
        __CrestBranch(31608, 5790, 0);
# 1780 "dfa.c"
        goto while_break;
      }
      }
      {
# 1781 "dfa.c"
      mem_18 = d->trans + i;
      {
      __CrestLoad(31611, (unsigned long )mem_18, (long long )((unsigned long )*mem_18));
      __CrestLoad(31610, (unsigned long )0, (long long )0);
      __CrestApply2(31609, 13, (long long )(*mem_18 != 0));
# 1781 "dfa.c"
      if (*mem_18 != 0) {
        __CrestBranch(31612, 5794, 1);
# 1783 "dfa.c"
        mem_19 = d->trans + i;
# 1783 "dfa.c"
        free((ptr_t )*mem_19);
        __CrestClearStack(31614);
# 1784 "dfa.c"
        mem_20 = d->trans + i;
# 1784 "dfa.c"
        *mem_20 = (int *)((void *)0);
      } else {
        __CrestBranch(31613, 5795, 0);
        {
# 1786 "dfa.c"
        mem_21 = d->fails + i;
        {
        __CrestLoad(31617, (unsigned long )mem_21, (long long )((unsigned long )*mem_21));
        __CrestLoad(31616, (unsigned long )0, (long long )0);
        __CrestApply2(31615, 13, (long long )(*mem_21 != 0));
# 1786 "dfa.c"
        if (*mem_21 != 0) {
          __CrestBranch(31618, 5798, 1);
# 1788 "dfa.c"
          mem_22 = d->fails + i;
# 1788 "dfa.c"
          free((ptr_t )*mem_22);
          __CrestClearStack(31620);
# 1789 "dfa.c"
          mem_23 = d->fails + i;
# 1789 "dfa.c"
          *mem_23 = (int *)((void *)0);
        } else {
          __CrestBranch(31619, 5799, 0);

        }
        }
        }
      }
      }
      }
      __CrestLoad(31623, (unsigned long )(& i), (long long )i);
      __CrestLoad(31622, (unsigned long )0, (long long )1);
      __CrestApply2(31621, 0, (long long )(i + 1));
      __CrestStore(31624, (unsigned long )(& i));
# 1780 "dfa.c"
      i ++;
    }
    while_break: ;
    }
    __CrestLoad(31625, (unsigned long )0, (long long )0);
    __CrestStore(31626, (unsigned long )(& d->trcount));
# 1791 "dfa.c"
    d->trcount = 0;
  } else {
    __CrestBranch(31601, 5803, 0);

  }
  }
  __CrestLoad(31629, (unsigned long )(& d->trcount), (long long )d->trcount);
  __CrestLoad(31628, (unsigned long )0, (long long )1);
  __CrestApply2(31627, 0, (long long )(d->trcount + 1));
  __CrestStore(31630, (unsigned long )(& d->trcount));
# 1794 "dfa.c"
  (d->trcount) ++;
# 1797 "dfa.c"
  mem_24 = d->success + s;
  __CrestLoad(31631, (unsigned long )0, (long long )0);
  __CrestStore(31632, (unsigned long )mem_24);
# 1797 "dfa.c"
  *mem_24 = 0;
  {
# 1798 "dfa.c"
  mem_25 = d->states + s;
  {
  __CrestLoad(31635, (unsigned long )(& mem_25->newline), (long long )mem_25->newline);
  __CrestLoad(31634, (unsigned long )0, (long long )0);
  __CrestApply2(31633, 13, (long long )(mem_25->newline != 0));
# 1798 "dfa.c"
  if (mem_25->newline != 0) {
    __CrestBranch(31636, 5808, 1);
    __CrestLoad(31638, (unsigned long )0, (long long )2);
    __CrestStore(31639, (unsigned long )(& tmp));
# 1798 "dfa.c"
    tmp = 2;
  } else {
    __CrestBranch(31637, 5809, 0);
    __CrestLoad(31640, (unsigned long )0, (long long )0);
    __CrestStore(31641, (unsigned long )(& tmp));
# 1798 "dfa.c"
    tmp = 0;
  }
  }
  }
  {
# 1798 "dfa.c"
  mem_26 = d->states + s;
  {
  __CrestLoad(31652, (unsigned long )(& mem_26->constraint), (long long )mem_26->constraint);
  __CrestLoad(31651, (unsigned long )0, (long long )1);
  __CrestLoad(31650, (unsigned long )(& tmp), (long long )tmp);
  __CrestLoad(31649, (unsigned long )0, (long long )1);
  __CrestApply2(31648, 0, (long long )(tmp + 1));
  __CrestLoad(31647, (unsigned long )0, (long long )4);
  __CrestApply2(31646, 0, (long long )((tmp + 1) + 4));
  __CrestApply2(31645, 8, (long long )(1 << ((tmp + 1) + 4)));
  __CrestApply2(31644, 5, (long long )((int )mem_26->constraint & (1 << ((tmp + 1) + 4))));
  __CrestLoad(31643, (unsigned long )0, (long long )0);
  __CrestApply2(31642, 13, (long long )(((int )mem_26->constraint & (1 << ((tmp + 1) + 4))) != 0));
# 1798 "dfa.c"
  if (((int )mem_26->constraint & (1 << ((tmp + 1) + 4))) != 0) {
    __CrestBranch(31653, 5813, 1);
    {
# 1798 "dfa.c"
    mem_27 = d->states + s;
    {
    __CrestLoad(31657, (unsigned long )(& mem_27->letter), (long long )mem_27->letter);
    __CrestLoad(31656, (unsigned long )0, (long long )0);
    __CrestApply2(31655, 13, (long long )(mem_27->letter != 0));
# 1798 "dfa.c"
    if (mem_27->letter != 0) {
      __CrestBranch(31658, 5816, 1);
      __CrestLoad(31660, (unsigned long )0, (long long )2);
      __CrestStore(31661, (unsigned long )(& tmp___0));
# 1798 "dfa.c"
      tmp___0 = 2;
    } else {
      __CrestBranch(31659, 5817, 0);
      __CrestLoad(31662, (unsigned long )0, (long long )0);
      __CrestStore(31663, (unsigned long )(& tmp___0));
# 1798 "dfa.c"
      tmp___0 = 0;
    }
    }
    }
    {
# 1798 "dfa.c"
    mem_28 = d->states + s;
    {
    __CrestLoad(31670, (unsigned long )(& mem_28->constraint), (long long )mem_28->constraint);
    __CrestLoad(31669, (unsigned long )0, (long long )1);
    __CrestLoad(31668, (unsigned long )(& tmp___0), (long long )tmp___0);
    __CrestApply2(31667, 8, (long long )(1 << tmp___0));
    __CrestApply2(31666, 5, (long long )((int )mem_28->constraint & (1 << tmp___0)));
    __CrestLoad(31665, (unsigned long )0, (long long )0);
    __CrestApply2(31664, 13, (long long )(((int )mem_28->constraint & (1 << tmp___0)) != 0));
# 1798 "dfa.c"
    if (((int )mem_28->constraint & (1 << tmp___0)) != 0) {
      __CrestBranch(31671, 5821, 1);
# 1800 "dfa.c"
      mem_29 = d->success + s;
# 1800 "dfa.c"
      mem_30 = d->success + s;
      __CrestLoad(31675, (unsigned long )mem_30, (long long )*mem_30);
      __CrestLoad(31674, (unsigned long )0, (long long )4);
      __CrestApply2(31673, 6, (long long )(*mem_30 | 4));
      __CrestStore(31676, (unsigned long )mem_29);
# 1800 "dfa.c"
      *mem_29 = *mem_30 | 4;
    } else {
      __CrestBranch(31672, 5822, 0);

    }
    }
    }
  } else {
    __CrestBranch(31654, 5823, 0);

  }
  }
  }
  {
# 1801 "dfa.c"
  mem_31 = d->states + s;
  {
  __CrestLoad(31679, (unsigned long )(& mem_31->newline), (long long )mem_31->newline);
  __CrestLoad(31678, (unsigned long )0, (long long )0);
  __CrestApply2(31677, 13, (long long )(mem_31->newline != 0));
# 1801 "dfa.c"
  if (mem_31->newline != 0) {
    __CrestBranch(31680, 5827, 1);
    __CrestLoad(31682, (unsigned long )0, (long long )2);
    __CrestStore(31683, (unsigned long )(& tmp___1));
# 1801 "dfa.c"
    tmp___1 = 2;
  } else {
    __CrestBranch(31681, 5828, 0);
    __CrestLoad(31684, (unsigned long )0, (long long )0);
    __CrestStore(31685, (unsigned long )(& tmp___1));
# 1801 "dfa.c"
    tmp___1 = 0;
  }
  }
  }
  {
# 1801 "dfa.c"
  mem_32 = d->states + s;
  {
  __CrestLoad(31694, (unsigned long )(& mem_32->constraint), (long long )mem_32->constraint);
  __CrestLoad(31693, (unsigned long )0, (long long )1);
  __CrestLoad(31692, (unsigned long )(& tmp___1), (long long )tmp___1);
  __CrestLoad(31691, (unsigned long )0, (long long )4);
  __CrestApply2(31690, 0, (long long )(tmp___1 + 4));
  __CrestApply2(31689, 8, (long long )(1 << (tmp___1 + 4)));
  __CrestApply2(31688, 5, (long long )((int )mem_32->constraint & (1 << (tmp___1 + 4))));
  __CrestLoad(31687, (unsigned long )0, (long long )0);
  __CrestApply2(31686, 13, (long long )(((int )mem_32->constraint & (1 << (tmp___1 + 4))) != 0));
# 1801 "dfa.c"
  if (((int )mem_32->constraint & (1 << (tmp___1 + 4))) != 0) {
    __CrestBranch(31695, 5832, 1);
    {
# 1801 "dfa.c"
    mem_33 = d->states + s;
    {
    __CrestLoad(31699, (unsigned long )(& mem_33->letter), (long long )mem_33->letter);
    __CrestLoad(31698, (unsigned long )0, (long long )0);
    __CrestApply2(31697, 13, (long long )(mem_33->letter != 0));
# 1801 "dfa.c"
    if (mem_33->letter != 0) {
      __CrestBranch(31700, 5835, 1);
      __CrestLoad(31702, (unsigned long )0, (long long )2);
      __CrestStore(31703, (unsigned long )(& tmp___2));
# 1801 "dfa.c"
      tmp___2 = 2;
    } else {
      __CrestBranch(31701, 5836, 0);
      __CrestLoad(31704, (unsigned long )0, (long long )0);
      __CrestStore(31705, (unsigned long )(& tmp___2));
# 1801 "dfa.c"
      tmp___2 = 0;
    }
    }
    }
    {
# 1801 "dfa.c"
    mem_34 = d->states + s;
    {
    __CrestLoad(31714, (unsigned long )(& mem_34->constraint), (long long )mem_34->constraint);
    __CrestLoad(31713, (unsigned long )0, (long long )1);
    __CrestLoad(31712, (unsigned long )(& tmp___2), (long long )tmp___2);
    __CrestLoad(31711, (unsigned long )0, (long long )1);
    __CrestApply2(31710, 0, (long long )(tmp___2 + 1));
    __CrestApply2(31709, 8, (long long )(1 << (tmp___2 + 1)));
    __CrestApply2(31708, 5, (long long )((int )mem_34->constraint & (1 << (tmp___2 + 1))));
    __CrestLoad(31707, (unsigned long )0, (long long )0);
    __CrestApply2(31706, 13, (long long )(((int )mem_34->constraint & (1 << (tmp___2 + 1))) != 0));
# 1801 "dfa.c"
    if (((int )mem_34->constraint & (1 << (tmp___2 + 1))) != 0) {
      __CrestBranch(31715, 5840, 1);
# 1803 "dfa.c"
      mem_35 = d->success + s;
# 1803 "dfa.c"
      mem_36 = d->success + s;
      __CrestLoad(31719, (unsigned long )mem_36, (long long )*mem_36);
      __CrestLoad(31718, (unsigned long )0, (long long )2);
      __CrestApply2(31717, 6, (long long )(*mem_36 | 2));
      __CrestStore(31720, (unsigned long )mem_35);
# 1803 "dfa.c"
      *mem_35 = *mem_36 | 2;
    } else {
      __CrestBranch(31716, 5841, 0);

    }
    }
    }
  } else {
    __CrestBranch(31696, 5842, 0);

  }
  }
  }
  {
# 1804 "dfa.c"
  mem_37 = d->states + s;
  {
  __CrestLoad(31723, (unsigned long )(& mem_37->newline), (long long )mem_37->newline);
  __CrestLoad(31722, (unsigned long )0, (long long )0);
  __CrestApply2(31721, 13, (long long )(mem_37->newline != 0));
# 1804 "dfa.c"
  if (mem_37->newline != 0) {
    __CrestBranch(31724, 5846, 1);
    __CrestLoad(31726, (unsigned long )0, (long long )2);
    __CrestStore(31727, (unsigned long )(& tmp___3));
# 1804 "dfa.c"
    tmp___3 = 2;
  } else {
    __CrestBranch(31725, 5847, 0);
    __CrestLoad(31728, (unsigned long )0, (long long )0);
    __CrestStore(31729, (unsigned long )(& tmp___3));
# 1804 "dfa.c"
    tmp___3 = 0;
  }
  }
  }
  {
# 1804 "dfa.c"
  mem_38 = d->states + s;
  {
  __CrestLoad(31738, (unsigned long )(& mem_38->constraint), (long long )mem_38->constraint);
  __CrestLoad(31737, (unsigned long )0, (long long )1);
  __CrestLoad(31736, (unsigned long )(& tmp___3), (long long )tmp___3);
  __CrestLoad(31735, (unsigned long )0, (long long )4);
  __CrestApply2(31734, 0, (long long )(tmp___3 + 4));
  __CrestApply2(31733, 8, (long long )(1 << (tmp___3 + 4)));
  __CrestApply2(31732, 5, (long long )((int )mem_38->constraint & (1 << (tmp___3 + 4))));
  __CrestLoad(31731, (unsigned long )0, (long long )0);
  __CrestApply2(31730, 13, (long long )(((int )mem_38->constraint & (1 << (tmp___3 + 4))) != 0));
# 1804 "dfa.c"
  if (((int )mem_38->constraint & (1 << (tmp___3 + 4))) != 0) {
    __CrestBranch(31739, 5851, 1);
    {
# 1804 "dfa.c"
    mem_39 = d->states + s;
    {
    __CrestLoad(31743, (unsigned long )(& mem_39->letter), (long long )mem_39->letter);
    __CrestLoad(31742, (unsigned long )0, (long long )0);
    __CrestApply2(31741, 13, (long long )(mem_39->letter != 0));
# 1804 "dfa.c"
    if (mem_39->letter != 0) {
      __CrestBranch(31744, 5854, 1);
      __CrestLoad(31746, (unsigned long )0, (long long )2);
      __CrestStore(31747, (unsigned long )(& tmp___4));
# 1804 "dfa.c"
      tmp___4 = 2;
    } else {
      __CrestBranch(31745, 5855, 0);
      __CrestLoad(31748, (unsigned long )0, (long long )0);
      __CrestStore(31749, (unsigned long )(& tmp___4));
# 1804 "dfa.c"
      tmp___4 = 0;
    }
    }
    }
    {
# 1804 "dfa.c"
    mem_40 = d->states + s;
    {
    __CrestLoad(31756, (unsigned long )(& mem_40->constraint), (long long )mem_40->constraint);
    __CrestLoad(31755, (unsigned long )0, (long long )1);
    __CrestLoad(31754, (unsigned long )(& tmp___4), (long long )tmp___4);
    __CrestApply2(31753, 8, (long long )(1 << tmp___4));
    __CrestApply2(31752, 5, (long long )((int )mem_40->constraint & (1 << tmp___4)));
    __CrestLoad(31751, (unsigned long )0, (long long )0);
    __CrestApply2(31750, 13, (long long )(((int )mem_40->constraint & (1 << tmp___4)) != 0));
# 1804 "dfa.c"
    if (((int )mem_40->constraint & (1 << tmp___4)) != 0) {
      __CrestBranch(31757, 5859, 1);
# 1806 "dfa.c"
      mem_41 = d->success + s;
# 1806 "dfa.c"
      mem_42 = d->success + s;
      __CrestLoad(31761, (unsigned long )mem_42, (long long )*mem_42);
      __CrestLoad(31760, (unsigned long )0, (long long )1);
      __CrestApply2(31759, 6, (long long )(*mem_42 | 1));
      __CrestStore(31762, (unsigned long )mem_41);
# 1806 "dfa.c"
      *mem_41 = *mem_42 | 1;
    } else {
      __CrestBranch(31758, 5860, 0);

    }
    }
    }
  } else {
    __CrestBranch(31740, 5861, 0);

  }
  }
  }
  __CrestLoad(31763, (unsigned long )0, (long long )((unsigned long )(1 << 8) * sizeof(int )));
# 1808 "dfa.c"
  tmp___5 = xmalloc((unsigned long )(1 << 8) * sizeof(int ));
  __CrestClearStack(31764);
# 1808 "dfa.c"
  trans = (int *)tmp___5;
  __CrestLoad(31765, (unsigned long )(& s), (long long )s);
# 1809 "dfa.c"
  dfastate(s, d, trans);
  __CrestClearStack(31766);
  __CrestLoad(31767, (unsigned long )0, (long long )0);
  __CrestStore(31768, (unsigned long )(& i));
# 1814 "dfa.c"
  i = 0;
  {
# 1814 "dfa.c"
  while (1) {
    while_continue___0: ;
    {
    __CrestLoad(31771, (unsigned long )(& i), (long long )i);
    __CrestLoad(31770, (unsigned long )0, (long long )(1 << 8));
    __CrestApply2(31769, 16, (long long )(i < 1 << 8));
# 1814 "dfa.c"
    if (i < 1 << 8) {
      __CrestBranch(31772, 5867, 1);

    } else {
      __CrestBranch(31773, 5868, 0);
# 1814 "dfa.c"
      goto while_break___0;
    }
    }
    {
# 1815 "dfa.c"
    mem_43 = trans + i;
    {
    __CrestLoad(31776, (unsigned long )mem_43, (long long )*mem_43);
    __CrestLoad(31775, (unsigned long )(& d->tralloc), (long long )d->tralloc);
    __CrestApply2(31774, 17, (long long )(*mem_43 >= d->tralloc));
# 1815 "dfa.c"
    if (*mem_43 >= d->tralloc) {
      __CrestBranch(31777, 5872, 1);
      __CrestLoad(31779, (unsigned long )(& d->tralloc), (long long )d->tralloc);
      __CrestStore(31780, (unsigned long )(& oldalloc));
# 1817 "dfa.c"
      oldalloc = d->tralloc;
      {
# 1819 "dfa.c"
      while (1) {
        while_continue___1: ;
        {
# 1819 "dfa.c"
        mem_44 = trans + i;
        {
        __CrestLoad(31783, (unsigned long )mem_44, (long long )*mem_44);
        __CrestLoad(31782, (unsigned long )(& d->tralloc), (long long )d->tralloc);
        __CrestApply2(31781, 17, (long long )(*mem_44 >= d->tralloc));
# 1819 "dfa.c"
        if (*mem_44 >= d->tralloc) {
          __CrestBranch(31784, 5879, 1);

        } else {
          __CrestBranch(31785, 5880, 0);
# 1819 "dfa.c"
          goto while_break___1;
        }
        }
        }
        __CrestLoad(31788, (unsigned long )(& d->tralloc), (long long )d->tralloc);
        __CrestLoad(31787, (unsigned long )0, (long long )2);
        __CrestApply2(31786, 2, (long long )(d->tralloc * 2));
        __CrestStore(31789, (unsigned long )(& d->tralloc));
# 1820 "dfa.c"
        d->tralloc *= 2;
      }
      while_break___1: ;
      }
      __CrestLoad(31794, (unsigned long )(& d->tralloc), (long long )d->tralloc);
      __CrestLoad(31793, (unsigned long )0, (long long )1);
      __CrestApply2(31792, 0, (long long )(d->tralloc + 1));
      __CrestLoad(31791, (unsigned long )0, (long long )sizeof(int *));
      __CrestApply2(31790, 2, (long long )((unsigned long )(d->tralloc + 1) * sizeof(int *)));
# 1821 "dfa.c"
      tmp___6 = xrealloc((ptr_t )d->realtrans, (unsigned long )(d->tralloc + 1) * sizeof(int *));
      __CrestClearStack(31795);
# 1821 "dfa.c"
      d->realtrans = (int **)tmp___6;
# 1822 "dfa.c"
      d->trans = d->realtrans + 1;
      __CrestLoad(31798, (unsigned long )(& d->tralloc), (long long )d->tralloc);
      __CrestLoad(31797, (unsigned long )0, (long long )sizeof(int *));
      __CrestApply2(31796, 2, (long long )((unsigned long )d->tralloc * sizeof(int *)));
# 1823 "dfa.c"
      tmp___7 = xrealloc((ptr_t )d->fails, (unsigned long )d->tralloc * sizeof(int *));
      __CrestClearStack(31799);
# 1823 "dfa.c"
      d->fails = (int **)tmp___7;
      __CrestLoad(31802, (unsigned long )(& d->tralloc), (long long )d->tralloc);
      __CrestLoad(31801, (unsigned long )0, (long long )sizeof(int ));
      __CrestApply2(31800, 2, (long long )((unsigned long )d->tralloc * sizeof(int )));
# 1824 "dfa.c"
      tmp___8 = xrealloc((ptr_t )d->success, (unsigned long )d->tralloc * sizeof(int ));
      __CrestClearStack(31803);
# 1824 "dfa.c"
      d->success = (int *)tmp___8;
      __CrestLoad(31806, (unsigned long )(& d->tralloc), (long long )d->tralloc);
      __CrestLoad(31805, (unsigned long )0, (long long )sizeof(int ));
      __CrestApply2(31804, 2, (long long )((unsigned long )d->tralloc * sizeof(int )));
# 1825 "dfa.c"
      tmp___9 = xrealloc((ptr_t )d->newlines, (unsigned long )d->tralloc * sizeof(int ));
      __CrestClearStack(31807);
# 1825 "dfa.c"
      d->newlines = (int *)tmp___9;
      {
# 1826 "dfa.c"
      while (1) {
        while_continue___2: ;
        {
        __CrestLoad(31810, (unsigned long )(& oldalloc), (long long )oldalloc);
        __CrestLoad(31809, (unsigned long )(& d->tralloc), (long long )d->tralloc);
        __CrestApply2(31808, 16, (long long )(oldalloc < d->tralloc));
# 1826 "dfa.c"
        if (oldalloc < d->tralloc) {
          __CrestBranch(31811, 5888, 1);

        } else {
          __CrestBranch(31812, 5889, 0);
# 1826 "dfa.c"
          goto while_break___2;
        }
        }
# 1828 "dfa.c"
        mem_45 = d->trans + oldalloc;
# 1828 "dfa.c"
        *mem_45 = (int *)((void *)0);
        __CrestLoad(31813, (unsigned long )(& oldalloc), (long long )oldalloc);
        __CrestStore(31814, (unsigned long )(& tmp___10));
# 1829 "dfa.c"
        tmp___10 = oldalloc;
        __CrestLoad(31817, (unsigned long )(& oldalloc), (long long )oldalloc);
        __CrestLoad(31816, (unsigned long )0, (long long )1);
        __CrestApply2(31815, 0, (long long )(oldalloc + 1));
        __CrestStore(31818, (unsigned long )(& oldalloc));
# 1829 "dfa.c"
        oldalloc ++;
# 1829 "dfa.c"
        mem_46 = d->fails + tmp___10;
# 1829 "dfa.c"
        *mem_46 = (int *)((void *)0);
      }
      while_break___2: ;
      }
    } else {
      __CrestBranch(31778, 5892, 0);

    }
    }
    }
    __CrestLoad(31821, (unsigned long )(& i), (long long )i);
    __CrestLoad(31820, (unsigned long )0, (long long )1);
    __CrestApply2(31819, 0, (long long )(i + 1));
    __CrestStore(31822, (unsigned long )(& i));
# 1814 "dfa.c"
    i ++;
  }
  while_break___0: ;
  }
# 1835 "dfa.c"
  mem_47 = d->newlines + s;
# 1835 "dfa.c"
  mem_48 = trans + '\n';
  __CrestLoad(31823, (unsigned long )mem_48, (long long )*mem_48);
  __CrestStore(31824, (unsigned long )mem_47);
# 1835 "dfa.c"
  *mem_47 = *mem_48;
# 1836 "dfa.c"
  mem_49 = trans + '\n';
  __CrestLoad(31825, (unsigned long )0, (long long )-1);
  __CrestStore(31826, (unsigned long )mem_49);
# 1836 "dfa.c"
  *mem_49 = -1;
  {
# 1838 "dfa.c"
  mem_50 = d->states + s;
  {
  __CrestLoad(31829, (unsigned long )(& mem_50->constraint), (long long )mem_50->constraint);
  __CrestLoad(31828, (unsigned long )0, (long long )0);
  __CrestApply2(31827, 13, (long long )(mem_50->constraint != 0));
# 1838 "dfa.c"
  if (mem_50->constraint != 0) {
    __CrestBranch(31830, 5899, 1);
# 1839 "dfa.c"
    mem_51 = d->fails + s;
# 1839 "dfa.c"
    *mem_51 = trans;
  } else {
    __CrestBranch(31831, 5900, 0);
# 1841 "dfa.c"
    mem_52 = d->trans + s;
# 1841 "dfa.c"
    *mem_52 = trans;
  }
  }
  }

  {
  __CrestReturn(31832);
# 1766 "dfa.c"
  return;
  }
}
}
# 1844 "dfa.c"
static void build_state_zero(struct dfa *d )
{
  ptr_t tmp ;
  ptr_t tmp___0 ;
  ptr_t tmp___1 ;
  ptr_t tmp___2 ;

  {
  __CrestCall(31833, 255);

  __CrestLoad(31834, (unsigned long )0, (long long )1);
  __CrestStore(31835, (unsigned long )(& d->tralloc));
# 1848 "dfa.c"
  d->tralloc = 1;
  __CrestLoad(31836, (unsigned long )0, (long long )0);
  __CrestStore(31837, (unsigned long )(& d->trcount));
# 1849 "dfa.c"
  d->trcount = 0;
  __CrestLoad(31840, (unsigned long )(& d->tralloc), (long long )d->tralloc);
  __CrestLoad(31839, (unsigned long )0, (long long )1);
  __CrestApply2(31838, 0, (long long )(d->tralloc + 1));
  __CrestLoad(31841, (unsigned long )0, (long long )sizeof(int *));
# 1850 "dfa.c"
  tmp = xcalloc((size_t )(d->tralloc + 1), sizeof(int *));
  __CrestClearStack(31842);
# 1850 "dfa.c"
  d->realtrans = (int **)tmp;
# 1851 "dfa.c"
  d->trans = d->realtrans + 1;
  __CrestLoad(31843, (unsigned long )(& d->tralloc), (long long )d->tralloc);
  __CrestLoad(31844, (unsigned long )0, (long long )sizeof(int *));
# 1852 "dfa.c"
  tmp___0 = xcalloc((size_t )d->tralloc, sizeof(int *));
  __CrestClearStack(31845);
# 1852 "dfa.c"
  d->fails = (int **)tmp___0;
  __CrestLoad(31848, (unsigned long )(& d->tralloc), (long long )d->tralloc);
  __CrestLoad(31847, (unsigned long )0, (long long )sizeof(int ));
  __CrestApply2(31846, 2, (long long )((unsigned long )d->tralloc * sizeof(int )));
# 1853 "dfa.c"
  tmp___1 = xmalloc((unsigned long )d->tralloc * sizeof(int ));
  __CrestClearStack(31849);
# 1853 "dfa.c"
  d->success = (int *)tmp___1;
  __CrestLoad(31852, (unsigned long )(& d->tralloc), (long long )d->tralloc);
  __CrestLoad(31851, (unsigned long )0, (long long )sizeof(int ));
  __CrestApply2(31850, 2, (long long )((unsigned long )d->tralloc * sizeof(int )));
# 1854 "dfa.c"
  tmp___2 = xmalloc((unsigned long )d->tralloc * sizeof(int ));
  __CrestClearStack(31853);
# 1854 "dfa.c"
  d->newlines = (int *)tmp___2;
  __CrestLoad(31854, (unsigned long )0, (long long )0);
# 1855 "dfa.c"
  build_state(0, d);
  __CrestClearStack(31855);

  {
  __CrestReturn(31856);
# 1844 "dfa.c"
  return;
  }
}
}
# 1884 "dfa.c"
static int sbit[256] ;
# 1885 "dfa.c"
static int sbit_init ;
# 1871 "dfa.c"
char *dfaexec(struct dfa *d , char *begin , char *end , int newline___0 , int *count ,
              int *backref )
{
  int s ;
  int s1 ;
  int tmp ;
  unsigned char *p ;
  register int **trans ;
  int *t ;
  int i ;
  unsigned short const **tmp___1 ;
  unsigned char *tmp___2 ;
  unsigned char *tmp___3 ;
  unsigned char *tmp___4 ;
  unsigned short const *mem_19 ;
  int **mem_20 ;
  int *mem_21 ;
  int **mem_22 ;
  int *mem_23 ;
  int **mem_24 ;
  int *mem_25 ;
  dfa_state *mem_26 ;
  int **mem_27 ;
  int *mem_28 ;
  unsigned char *mem_29 ;
  unsigned char *mem_30 ;
  int *mem_31 ;
  char *__retres32 ;

  {
  __CrestCall(31858, 256);
  __CrestStore(31857, (unsigned long )(& newline___0));
  {
  __CrestLoad(31861, (unsigned long )(& sbit_init), (long long )sbit_init);
  __CrestLoad(31860, (unsigned long )0, (long long )0);
  __CrestApply2(31859, 12, (long long )(sbit_init == 0));
# 1887 "dfa.c"
  if (sbit_init == 0) {
    __CrestBranch(31862, 5907, 1);
    __CrestLoad(31864, (unsigned long )0, (long long )1);
    __CrestStore(31865, (unsigned long )(& sbit_init));
# 1891 "dfa.c"
    sbit_init = 1;
    __CrestLoad(31866, (unsigned long )0, (long long )0);
    __CrestStore(31867, (unsigned long )(& i));
# 1892 "dfa.c"
    i = 0;
    {
# 1892 "dfa.c"
    while (1) {
      while_continue: ;
      {
      __CrestLoad(31870, (unsigned long )(& i), (long long )i);
      __CrestLoad(31869, (unsigned long )0, (long long )(1 << 8));
      __CrestApply2(31868, 16, (long long )(i < 1 << 8));
# 1892 "dfa.c"
      if (i < 1 << 8) {
        __CrestBranch(31871, 5912, 1);

      } else {
        __CrestBranch(31872, 5913, 0);
# 1892 "dfa.c"
        goto while_break;
      }
      }
# 1893 "dfa.c"
      tmp___1 = __ctype_b_loc();
      __CrestClearStack(31873);
      {
# 1893 "dfa.c"
      mem_19 = *tmp___1 + i;
      {
      __CrestLoad(31878, (unsigned long )mem_19, (long long )*mem_19);
      __CrestLoad(31877, (unsigned long )0, (long long )8);
      __CrestApply2(31876, 5, (long long )((int const )*mem_19 & 8));
      __CrestLoad(31875, (unsigned long )0, (long long )0);
      __CrestApply2(31874, 13, (long long )(((int const )*mem_19 & 8) != 0));
# 1893 "dfa.c"
      if (((int const )*mem_19 & 8) != 0) {
        __CrestBranch(31879, 5918, 1);
        __CrestLoad(31881, (unsigned long )0, (long long )2);
        __CrestStore(31882, (unsigned long )(& sbit[i]));
# 1893 "dfa.c"
        sbit[i] = 2;
      } else {
        __CrestBranch(31880, 5919, 0);
        {
        __CrestLoad(31885, (unsigned long )(& i), (long long )i);
        __CrestLoad(31884, (unsigned long )0, (long long )95);
        __CrestApply2(31883, 12, (long long )(i == 95));
# 1893 "dfa.c"
        if (i == 95) {
          __CrestBranch(31886, 5920, 1);
          __CrestLoad(31888, (unsigned long )0, (long long )2);
          __CrestStore(31889, (unsigned long )(& sbit[i]));
# 1893 "dfa.c"
          sbit[i] = 2;
        } else {
          __CrestBranch(31887, 5921, 0);
          __CrestLoad(31890, (unsigned long )0, (long long )1);
          __CrestStore(31891, (unsigned long )(& sbit[i]));
# 1893 "dfa.c"
          sbit[i] = 1;
        }
        }
      }
      }
      }
      __CrestLoad(31894, (unsigned long )(& i), (long long )i);
      __CrestLoad(31893, (unsigned long )0, (long long )1);
      __CrestApply2(31892, 0, (long long )(i + 1));
      __CrestStore(31895, (unsigned long )(& i));
# 1892 "dfa.c"
      i ++;
    }
    while_break: ;
    }
    __CrestLoad(31896, (unsigned long )0, (long long )4);
    __CrestStore(31897, (unsigned long )(& sbit['\n']));
# 1894 "dfa.c"
    sbit['\n'] = 4;
  } else {
    __CrestBranch(31863, 5925, 0);

  }
  }
  {
  __CrestLoad(31900, (unsigned long )(& d->tralloc), (long long )d->tralloc);
  __CrestLoad(31899, (unsigned long )0, (long long )0);
  __CrestApply2(31898, 12, (long long )(d->tralloc == 0));
# 1897 "dfa.c"
  if (d->tralloc == 0) {
    __CrestBranch(31901, 5927, 1);
# 1898 "dfa.c"
    build_state_zero(d);
    __CrestClearStack(31903);
  } else {
    __CrestBranch(31902, 5928, 0);

  }
  }
  __CrestLoad(31904, (unsigned long )0, (long long )0);
  __CrestStore(31905, (unsigned long )(& s1));
# 1900 "dfa.c"
  s1 = 0;
  __CrestLoad(31906, (unsigned long )(& s1), (long long )s1);
  __CrestStore(31907, (unsigned long )(& s));
# 1900 "dfa.c"
  s = s1;
# 1901 "dfa.c"
  p = (unsigned char *)begin;
# 1902 "dfa.c"
  trans = d->trans;
  __CrestLoad(31908, (unsigned long )0, (long long )((char )'\n'));
  __CrestStore(31909, (unsigned long )end);
# 1903 "dfa.c"
  *end = (char )'\n';
  {
# 1905 "dfa.c"
  while (1) {
    while_continue___0: ;
    {
# 1907 "dfa.c"
    while (1) {
      while_continue___1: ;
# 1907 "dfa.c"
      mem_20 = trans + s;
# 1907 "dfa.c"
      t = *mem_20;
      {
      __CrestLoad(31912, (unsigned long )(& t), (long long )((unsigned long )t));
      __CrestLoad(31911, (unsigned long )0, (long long )((unsigned long )((int *)0)));
      __CrestApply2(31910, 13, (long long )((unsigned long )t != (unsigned long )((int *)0)));
# 1907 "dfa.c"
      if ((unsigned long )t != (unsigned long )((int *)0)) {
        __CrestBranch(31913, 5938, 1);

      } else {
        __CrestBranch(31914, 5939, 0);
# 1907 "dfa.c"
        goto while_break___1;
      }
      }
# 1908 "dfa.c"
      tmp___2 = p;
# 1908 "dfa.c"
      p ++;
# 1908 "dfa.c"
      mem_21 = t + *tmp___2;
      __CrestLoad(31915, (unsigned long )mem_21, (long long )*mem_21);
      __CrestStore(31916, (unsigned long )(& s1));
# 1908 "dfa.c"
      s1 = *mem_21;
# 1909 "dfa.c"
      mem_22 = trans + s1;
# 1909 "dfa.c"
      t = *mem_22;
      {
      __CrestLoad(31919, (unsigned long )(& t), (long long )((unsigned long )t));
      __CrestLoad(31918, (unsigned long )0, (long long )((unsigned long )((int *)0)));
      __CrestApply2(31917, 12, (long long )((unsigned long )t == (unsigned long )((int *)0)));
# 1909 "dfa.c"
      if ((unsigned long )t == (unsigned long )((int *)0)) {
        __CrestBranch(31920, 5942, 1);
        __CrestLoad(31922, (unsigned long )(& s), (long long )s);
        __CrestStore(31923, (unsigned long )(& tmp));
# 1910 "dfa.c"
        tmp = s;
        __CrestLoad(31924, (unsigned long )(& s1), (long long )s1);
        __CrestStore(31925, (unsigned long )(& s));
# 1910 "dfa.c"
        s = s1;
        __CrestLoad(31926, (unsigned long )(& tmp), (long long )tmp);
        __CrestStore(31927, (unsigned long )(& s1));
# 1910 "dfa.c"
        s1 = tmp;
# 1911 "dfa.c"
        goto while_break___1;
      } else {
        __CrestBranch(31921, 5944, 0);

      }
      }
# 1913 "dfa.c"
      tmp___3 = p;
# 1913 "dfa.c"
      p ++;
# 1913 "dfa.c"
      mem_23 = t + *tmp___3;
      __CrestLoad(31928, (unsigned long )mem_23, (long long )*mem_23);
      __CrestStore(31929, (unsigned long )(& s));
# 1913 "dfa.c"
      s = *mem_23;
    }
    while_break___1: ;
    }
    {
    __CrestLoad(31932, (unsigned long )(& s), (long long )s);
    __CrestLoad(31931, (unsigned long )0, (long long )0);
    __CrestApply2(31930, 17, (long long )(s >= 0));
# 1916 "dfa.c"
    if (s >= 0) {
      __CrestBranch(31933, 5948, 1);
      {
      __CrestLoad(31937, (unsigned long )(& p), (long long )((unsigned long )p));
      __CrestLoad(31936, (unsigned long )(& end), (long long )((unsigned long )end));
      __CrestApply2(31935, 15, (long long )((unsigned long )p <= (unsigned long )((unsigned char *)end)));
# 1916 "dfa.c"
      if ((unsigned long )p <= (unsigned long )((unsigned char *)end)) {
        __CrestBranch(31938, 5949, 1);
        {
# 1916 "dfa.c"
        mem_24 = d->fails + s;
        {
        __CrestLoad(31942, (unsigned long )mem_24, (long long )((unsigned long )*mem_24));
        __CrestLoad(31941, (unsigned long )0, (long long )0);
        __CrestApply2(31940, 13, (long long )(*mem_24 != 0));
# 1916 "dfa.c"
        if (*mem_24 != 0) {
          __CrestBranch(31943, 5952, 1);
          {
# 1918 "dfa.c"
          mem_25 = d->success + s;
          {
          __CrestLoad(31949, (unsigned long )mem_25, (long long )*mem_25);
          __CrestLoad(31948, (unsigned long )(& sbit[*p]), (long long )sbit[*p]);
          __CrestApply2(31947, 5, (long long )(*mem_25 & sbit[*p]));
          __CrestLoad(31946, (unsigned long )0, (long long )0);
          __CrestApply2(31945, 13, (long long )((*mem_25 & sbit[*p]) != 0));
# 1918 "dfa.c"
          if ((*mem_25 & sbit[*p]) != 0) {
            __CrestBranch(31950, 5955, 1);
            {
            __CrestLoad(31954, (unsigned long )(& backref), (long long )((unsigned long )backref));
            __CrestLoad(31953, (unsigned long )0, (long long )0);
            __CrestApply2(31952, 13, (long long )(backref != 0));
# 1920 "dfa.c"
            if (backref != 0) {
              __CrestBranch(31955, 5956, 1);
# 1921 "dfa.c"
              mem_26 = d->states + s;
              __CrestLoad(31959, (unsigned long )(& mem_26->backref), (long long )mem_26->backref);
              __CrestLoad(31958, (unsigned long )0, (long long )0);
              __CrestApply2(31957, 13, (long long )((int )mem_26->backref != 0));
              __CrestStore(31960, (unsigned long )backref);
# 1921 "dfa.c"
              *backref = (int )mem_26->backref != 0;
            } else {
              __CrestBranch(31956, 5957, 0);

            }
            }
# 1922 "dfa.c"
            __retres32 = (char *)p;
# 1922 "dfa.c"
            goto return_label;
          } else {
            __CrestBranch(31951, 5960, 0);

          }
          }
          }
          __CrestLoad(31961, (unsigned long )(& s), (long long )s);
          __CrestStore(31962, (unsigned long )(& s1));
# 1925 "dfa.c"
          s1 = s;
# 1926 "dfa.c"
          tmp___4 = p;
# 1926 "dfa.c"
          p ++;
# 1926 "dfa.c"
          mem_27 = d->fails + s;
# 1926 "dfa.c"
          mem_28 = *mem_27 + *tmp___4;
          __CrestLoad(31963, (unsigned long )mem_28, (long long )*mem_28);
          __CrestStore(31964, (unsigned long )(& s));
# 1926 "dfa.c"
          s = *mem_28;
# 1927 "dfa.c"
          goto __Cont;
        } else {
          __CrestBranch(31944, 5963, 0);

        }
        }
        }
      } else {
        __CrestBranch(31939, 5964, 0);

      }
      }
    } else {
      __CrestBranch(31934, 5965, 0);

    }
    }
    {
    __CrestLoad(31967, (unsigned long )(& count), (long long )((unsigned long )count));
    __CrestLoad(31966, (unsigned long )0, (long long )0);
    __CrestApply2(31965, 13, (long long )(count != 0));
# 1931 "dfa.c"
    if (count != 0) {
      __CrestBranch(31968, 5967, 1);
      {
      __CrestLoad(31972, (unsigned long )(& p), (long long )((unsigned long )p));
      __CrestLoad(31971, (unsigned long )(& end), (long long )((unsigned long )end));
      __CrestApply2(31970, 15, (long long )((unsigned long )((char *)p) <= (unsigned long )end));
# 1931 "dfa.c"
      if ((unsigned long )((char *)p) <= (unsigned long )end) {
        __CrestBranch(31973, 5968, 1);
        {
# 1931 "dfa.c"
        mem_29 = p + -1;
        {
        __CrestLoad(31977, (unsigned long )mem_29, (long long )*mem_29);
        __CrestLoad(31976, (unsigned long )0, (long long )10);
        __CrestApply2(31975, 12, (long long )((int )*mem_29 == 10));
# 1931 "dfa.c"
        if ((int )*mem_29 == 10) {
          __CrestBranch(31978, 5971, 1);
          __CrestLoad(31982, (unsigned long )count, (long long )*count);
          __CrestLoad(31981, (unsigned long )0, (long long )1);
          __CrestApply2(31980, 0, (long long )(*count + 1));
          __CrestStore(31983, (unsigned long )count);
# 1932 "dfa.c"
          (*count) ++;
        } else {
          __CrestBranch(31979, 5972, 0);

        }
        }
        }
      } else {
        __CrestBranch(31974, 5973, 0);

      }
      }
    } else {
      __CrestBranch(31969, 5974, 0);

    }
    }
    {
    __CrestLoad(31986, (unsigned long )(& p), (long long )((unsigned long )p));
    __CrestLoad(31985, (unsigned long )(& end), (long long )((unsigned long )end));
    __CrestApply2(31984, 14, (long long )((unsigned long )((char *)p) > (unsigned long )end));
# 1935 "dfa.c"
    if ((unsigned long )((char *)p) > (unsigned long )end) {
      __CrestBranch(31987, 5976, 1);
# 1936 "dfa.c"
      __retres32 = (char *)((void *)0);
# 1936 "dfa.c"
      goto return_label;
    } else {
      __CrestBranch(31988, 5978, 0);

    }
    }
    {
    __CrestLoad(31991, (unsigned long )(& s), (long long )s);
    __CrestLoad(31990, (unsigned long )0, (long long )0);
    __CrestApply2(31989, 17, (long long )(s >= 0));
# 1938 "dfa.c"
    if (s >= 0) {
      __CrestBranch(31992, 5980, 1);
      __CrestLoad(31994, (unsigned long )(& s), (long long )s);
# 1940 "dfa.c"
      build_state(s, d);
      __CrestClearStack(31995);
# 1941 "dfa.c"
      trans = d->trans;
# 1942 "dfa.c"
      goto __Cont;
    } else {
      __CrestBranch(31993, 5982, 0);

    }
    }
    {
# 1945 "dfa.c"
    mem_30 = p + -1;
    {
    __CrestLoad(31998, (unsigned long )mem_30, (long long )*mem_30);
    __CrestLoad(31997, (unsigned long )0, (long long )10);
    __CrestApply2(31996, 12, (long long )((int )*mem_30 == 10));
# 1945 "dfa.c"
    if ((int )*mem_30 == 10) {
      __CrestBranch(31999, 5986, 1);
      {
      __CrestLoad(32003, (unsigned long )(& newline___0), (long long )newline___0);
      __CrestLoad(32002, (unsigned long )0, (long long )0);
      __CrestApply2(32001, 13, (long long )(newline___0 != 0));
# 1945 "dfa.c"
      if (newline___0 != 0) {
        __CrestBranch(32004, 5987, 1);
# 1947 "dfa.c"
        mem_31 = d->newlines + s1;
        __CrestLoad(32006, (unsigned long )mem_31, (long long )*mem_31);
        __CrestStore(32007, (unsigned long )(& s));
# 1947 "dfa.c"
        s = *mem_31;
# 1948 "dfa.c"
        goto __Cont;
      } else {
        __CrestBranch(32005, 5989, 0);

      }
      }
    } else {
      __CrestBranch(32000, 5990, 0);

    }
    }
    }
    __CrestLoad(32008, (unsigned long )0, (long long )0);
    __CrestStore(32009, (unsigned long )(& s));
# 1951 "dfa.c"
    s = 0;
    __Cont: ;
  }
  while_break___0: ;
  }
  return_label:
  {
  __CrestReturn(32010);
# 1871 "dfa.c"
  return (__retres32);
  }
}
}
# 1957 "dfa.c"
void dfainit(struct dfa *d )
{
  ptr_t tmp ;
  ptr_t tmp___0 ;
  int tmp___1 ;
  int tmp___2 ;
  int tmp___3 ;

  {
  __CrestCall(32011, 257);

  __CrestLoad(32012, (unsigned long )0, (long long )1);
  __CrestStore(32013, (unsigned long )(& d->calloc));
# 1961 "dfa.c"
  d->calloc = 1;
  __CrestLoad(32016, (unsigned long )(& d->calloc), (long long )d->calloc);
  __CrestLoad(32015, (unsigned long )0, (long long )sizeof(charclass ));
  __CrestApply2(32014, 2, (long long )((unsigned long )d->calloc * sizeof(charclass )));
# 1962 "dfa.c"
  tmp = xmalloc((unsigned long )d->calloc * sizeof(charclass ));
  __CrestClearStack(32017);
# 1962 "dfa.c"
  d->charclasses = (charclass *)tmp;
  __CrestLoad(32018, (unsigned long )0, (long long )0);
  __CrestStore(32019, (unsigned long )(& d->cindex));
# 1963 "dfa.c"
  d->cindex = 0;
  __CrestLoad(32020, (unsigned long )0, (long long )1);
  __CrestStore(32021, (unsigned long )(& d->talloc));
# 1965 "dfa.c"
  d->talloc = 1;
  __CrestLoad(32024, (unsigned long )(& d->talloc), (long long )d->talloc);
  __CrestLoad(32023, (unsigned long )0, (long long )sizeof(token ));
  __CrestApply2(32022, 2, (long long )((unsigned long )d->talloc * sizeof(token )));
# 1966 "dfa.c"
  tmp___0 = xmalloc((unsigned long )d->talloc * sizeof(token ));
  __CrestClearStack(32025);
# 1966 "dfa.c"
  d->tokens = (token *)tmp___0;
  __CrestLoad(32026, (unsigned long )0, (long long )0);
  __CrestStore(32027, (unsigned long )(& tmp___3));
# 1967 "dfa.c"
  tmp___3 = 0;
  __CrestLoad(32028, (unsigned long )(& tmp___3), (long long )tmp___3);
  __CrestStore(32029, (unsigned long )(& d->nregexps));
# 1967 "dfa.c"
  d->nregexps = tmp___3;
  __CrestLoad(32030, (unsigned long )(& tmp___3), (long long )tmp___3);
  __CrestStore(32031, (unsigned long )(& tmp___2));
# 1967 "dfa.c"
  tmp___2 = tmp___3;
  __CrestLoad(32032, (unsigned long )(& tmp___2), (long long )tmp___2);
  __CrestStore(32033, (unsigned long )(& d->nleaves));
# 1967 "dfa.c"
  d->nleaves = tmp___2;
  __CrestLoad(32034, (unsigned long )(& tmp___2), (long long )tmp___2);
  __CrestStore(32035, (unsigned long )(& tmp___1));
# 1967 "dfa.c"
  tmp___1 = tmp___2;
  __CrestLoad(32036, (unsigned long )(& tmp___1), (long long )tmp___1);
  __CrestStore(32037, (unsigned long )(& d->depth));
# 1967 "dfa.c"
  d->depth = tmp___1;
  __CrestLoad(32038, (unsigned long )(& tmp___1), (long long )tmp___1);
  __CrestStore(32039, (unsigned long )(& d->tindex));
# 1967 "dfa.c"
  d->tindex = tmp___1;
  __CrestLoad(32040, (unsigned long )0, (long long )0);
  __CrestStore(32041, (unsigned long )(& d->searchflag));
# 1969 "dfa.c"
  d->searchflag = 0;
  __CrestLoad(32042, (unsigned long )0, (long long )0);
  __CrestStore(32043, (unsigned long )(& d->tralloc));
# 1970 "dfa.c"
  d->tralloc = 0;
# 1972 "dfa.c"
  d->musts = (struct dfamust *)0;

  {
  __CrestReturn(32044);
# 1957 "dfa.c"
  return;
  }
}
}
# 1976 "dfa.c"
void dfacomp(char *s , size_t len , struct dfa *d , int searchflag )
{
  char *lcopy ;
  int i ;
  void *tmp ;
  char *tmp___0 ;
  int tmp___1 ;
  unsigned short const **tmp___2 ;
  int tmp___3 ;
  int tmp___4 ;
  int tmp___5 ;
  int tmp___6 ;
  char *mem_15 ;
  unsigned short const *mem_16 ;
  char *mem_17 ;
  char *mem_18 ;
  char *mem_19 ;
  char *mem_20 ;

  {
  __CrestCall(32047, 258);
  __CrestStore(32046, (unsigned long )(& searchflag));
  __CrestStore(32045, (unsigned long )(& len));
  {
  __CrestLoad(32050, (unsigned long )(& case_fold), (long long )case_fold);
  __CrestLoad(32049, (unsigned long )0, (long long )0);
  __CrestApply2(32048, 13, (long long )(case_fold != 0));
# 1983 "dfa.c"
  if (case_fold != 0) {
    __CrestBranch(32051, 5999, 1);
    __CrestLoad(32053, (unsigned long )(& len), (long long )len);
# 1988 "dfa.c"
    tmp = malloc(len);
    __CrestClearStack(32054);
# 1988 "dfa.c"
    lcopy = (char *)tmp;
    {
    __CrestLoad(32057, (unsigned long )(& lcopy), (long long )((unsigned long )lcopy));
    __CrestLoad(32056, (unsigned long )0, (long long )0);
    __CrestApply2(32055, 12, (long long )(lcopy == 0));
# 1989 "dfa.c"
    if (lcopy == 0) {
      __CrestBranch(32058, 6001, 1);
      __CrestLoad(32060, (unsigned long )0, (long long )5);
# 1990 "dfa.c"
      tmp___0 = dcgettext((char const *)((void *)0), "out of memory", 5);
      __CrestClearStack(32061);
# 1990 "dfa.c"
      dfaerror((char const *)tmp___0);
      __CrestClearStack(32062);
    } else {
      __CrestBranch(32059, 6002, 0);

    }
    }
    __CrestLoad(32063, (unsigned long )0, (long long )0);
    __CrestStore(32064, (unsigned long )(& case_fold));
# 1993 "dfa.c"
    case_fold = 0;
    __CrestLoad(32065, (unsigned long )0, (long long )0);
    __CrestStore(32066, (unsigned long )(& i));
# 1994 "dfa.c"
    i = 0;
    {
# 1994 "dfa.c"
    while (1) {
      while_continue: ;
      {
      __CrestLoad(32069, (unsigned long )(& i), (long long )i);
      __CrestLoad(32068, (unsigned long )(& len), (long long )len);
      __CrestApply2(32067, 16, (long long )((size_t )i < len));
# 1994 "dfa.c"
      if ((size_t )i < len) {
        __CrestBranch(32070, 6008, 1);

      } else {
        __CrestBranch(32071, 6009, 0);
# 1994 "dfa.c"
        goto while_break;
      }
      }
# 1995 "dfa.c"
      tmp___2 = __ctype_b_loc();
      __CrestClearStack(32072);
      {
# 1995 "dfa.c"
      mem_15 = s + i;
# 1995 "dfa.c"
      mem_16 = *tmp___2 + (int )*mem_15;
      {
      __CrestLoad(32077, (unsigned long )mem_16, (long long )*mem_16);
      __CrestLoad(32076, (unsigned long )0, (long long )256);
      __CrestApply2(32075, 5, (long long )((int const )*mem_16 & 256));
      __CrestLoad(32074, (unsigned long )0, (long long )0);
      __CrestApply2(32073, 13, (long long )(((int const )*mem_16 & 256) != 0));
# 1995 "dfa.c"
      if (((int const )*mem_16 & 256) != 0) {
        __CrestBranch(32078, 6014, 1);
# 1996 "dfa.c"
        mem_17 = s + i;
        __CrestLoad(32080, (unsigned long )mem_17, (long long )*mem_17);
# 1996 "dfa.c"
        tmp___1 = tolower((int )*mem_17);
        __CrestHandleReturn(32082, (long long )tmp___1);
        __CrestStore(32081, (unsigned long )(& tmp___1));
# 1996 "dfa.c"
        mem_18 = lcopy + i;
        __CrestLoad(32083, (unsigned long )(& tmp___1), (long long )tmp___1);
        __CrestStore(32084, (unsigned long )mem_18);
# 1996 "dfa.c"
        *mem_18 = (char )tmp___1;
      } else {
        __CrestBranch(32079, 6015, 0);
# 1998 "dfa.c"
        mem_19 = lcopy + i;
# 1998 "dfa.c"
        mem_20 = s + i;
        __CrestLoad(32085, (unsigned long )mem_20, (long long )*mem_20);
        __CrestStore(32086, (unsigned long )mem_19);
# 1998 "dfa.c"
        *mem_19 = *mem_20;
      }
      }
      }
      __CrestLoad(32089, (unsigned long )(& i), (long long )i);
      __CrestLoad(32088, (unsigned long )0, (long long )1);
      __CrestApply2(32087, 0, (long long )(i + 1));
      __CrestStore(32090, (unsigned long )(& i));
# 1994 "dfa.c"
      i ++;
    }
    while_break: ;
    }
# 2000 "dfa.c"
    dfainit(d);
    __CrestClearStack(32091);
    __CrestLoad(32092, (unsigned long )(& len), (long long )len);
# 2001 "dfa.c"
    dfaparse(lcopy, len, d);
    __CrestClearStack(32093);
# 2002 "dfa.c"
    free((void *)lcopy);
    __CrestClearStack(32094);
# 2003 "dfa.c"
    dfamust(d);
    __CrestClearStack(32095);
    __CrestLoad(32096, (unsigned long )0, (long long )0);
    __CrestStore(32097, (unsigned long )(& tmp___6));
# 2004 "dfa.c"
    tmp___6 = 0;
    __CrestLoad(32098, (unsigned long )(& tmp___6), (long long )tmp___6);
    __CrestStore(32099, (unsigned long )(& d->nregexps));
# 2004 "dfa.c"
    d->nregexps = tmp___6;
    __CrestLoad(32100, (unsigned long )(& tmp___6), (long long )tmp___6);
    __CrestStore(32101, (unsigned long )(& tmp___5));
# 2004 "dfa.c"
    tmp___5 = tmp___6;
    __CrestLoad(32102, (unsigned long )(& tmp___5), (long long )tmp___5);
    __CrestStore(32103, (unsigned long )(& d->nleaves));
# 2004 "dfa.c"
    d->nleaves = tmp___5;
    __CrestLoad(32104, (unsigned long )(& tmp___5), (long long )tmp___5);
    __CrestStore(32105, (unsigned long )(& tmp___4));
# 2004 "dfa.c"
    tmp___4 = tmp___5;
    __CrestLoad(32106, (unsigned long )(& tmp___4), (long long )tmp___4);
    __CrestStore(32107, (unsigned long )(& d->depth));
# 2004 "dfa.c"
    d->depth = tmp___4;
    __CrestLoad(32108, (unsigned long )(& tmp___4), (long long )tmp___4);
    __CrestStore(32109, (unsigned long )(& tmp___3));
# 2004 "dfa.c"
    tmp___3 = tmp___4;
    __CrestLoad(32110, (unsigned long )(& tmp___3), (long long )tmp___3);
    __CrestStore(32111, (unsigned long )(& d->tindex));
# 2004 "dfa.c"
    d->tindex = tmp___3;
    __CrestLoad(32112, (unsigned long )(& tmp___3), (long long )tmp___3);
    __CrestStore(32113, (unsigned long )(& d->cindex));
# 2004 "dfa.c"
    d->cindex = tmp___3;
    __CrestLoad(32114, (unsigned long )0, (long long )1);
    __CrestStore(32115, (unsigned long )(& case_fold));
# 2005 "dfa.c"
    case_fold = 1;
    __CrestLoad(32116, (unsigned long )(& len), (long long )len);
# 2006 "dfa.c"
    dfaparse(s, len, d);
    __CrestClearStack(32117);
    __CrestLoad(32118, (unsigned long )(& searchflag), (long long )searchflag);
# 2007 "dfa.c"
    dfaanalyze(d, searchflag);
    __CrestClearStack(32119);
  } else {
    __CrestBranch(32052, 6019, 0);
# 2011 "dfa.c"
    dfainit(d);
    __CrestClearStack(32120);
    __CrestLoad(32121, (unsigned long )(& len), (long long )len);
# 2012 "dfa.c"
    dfaparse(s, len, d);
    __CrestClearStack(32122);
# 2013 "dfa.c"
    dfamust(d);
    __CrestClearStack(32123);
    __CrestLoad(32124, (unsigned long )(& searchflag), (long long )searchflag);
# 2014 "dfa.c"
    dfaanalyze(d, searchflag);
    __CrestClearStack(32125);
  }
  }

  {
  __CrestReturn(32126);
# 1976 "dfa.c"
  return;
  }
}
}
# 2019 "dfa.c"
void dfafree(struct dfa *d )
{
  int i ;
  struct dfamust *dm ;
  struct dfamust *ndm ;
  dfa_state *mem_5 ;
  position_set *mem_6 ;
  position_set *mem_7 ;
  int **mem_8 ;
  int **mem_9 ;
  int **mem_10 ;
  int **mem_11 ;

  {
  __CrestCall(32127, 259);
# 2026 "dfa.c"
  free((ptr_t )d->charclasses);
  __CrestClearStack(32128);
# 2027 "dfa.c"
  free((ptr_t )d->tokens);
  __CrestClearStack(32129);
  __CrestLoad(32130, (unsigned long )0, (long long )0);
  __CrestStore(32131, (unsigned long )(& i));
# 2028 "dfa.c"
  i = 0;
  {
# 2028 "dfa.c"
  while (1) {
    while_continue: ;
    {
    __CrestLoad(32134, (unsigned long )(& i), (long long )i);
    __CrestLoad(32133, (unsigned long )(& d->sindex), (long long )d->sindex);
    __CrestApply2(32132, 16, (long long )(i < d->sindex));
# 2028 "dfa.c"
    if (i < d->sindex) {
      __CrestBranch(32135, 6027, 1);

    } else {
      __CrestBranch(32136, 6028, 0);
# 2028 "dfa.c"
      goto while_break;
    }
    }
# 2029 "dfa.c"
    mem_5 = d->states + i;
# 2029 "dfa.c"
    free((ptr_t )mem_5->elems.elems);
    __CrestClearStack(32137);
    __CrestLoad(32140, (unsigned long )(& i), (long long )i);
    __CrestLoad(32139, (unsigned long )0, (long long )1);
    __CrestApply2(32138, 0, (long long )(i + 1));
    __CrestStore(32141, (unsigned long )(& i));
# 2028 "dfa.c"
    i ++;
  }
  while_break: ;
  }
# 2030 "dfa.c"
  free((ptr_t )d->states);
  __CrestClearStack(32142);
  __CrestLoad(32143, (unsigned long )0, (long long )0);
  __CrestStore(32144, (unsigned long )(& i));
# 2031 "dfa.c"
  i = 0;
  {
# 2031 "dfa.c"
  while (1) {
    while_continue___0: ;
    {
    __CrestLoad(32147, (unsigned long )(& i), (long long )i);
    __CrestLoad(32146, (unsigned long )(& d->tindex), (long long )d->tindex);
    __CrestApply2(32145, 16, (long long )(i < d->tindex));
# 2031 "dfa.c"
    if (i < d->tindex) {
      __CrestBranch(32148, 6036, 1);

    } else {
      __CrestBranch(32149, 6037, 0);
# 2031 "dfa.c"
      goto while_break___0;
    }
    }
    {
# 2032 "dfa.c"
    mem_6 = d->follows + i;
    {
    __CrestLoad(32152, (unsigned long )(& mem_6->elems), (long long )((unsigned long )mem_6->elems));
    __CrestLoad(32151, (unsigned long )0, (long long )0);
    __CrestApply2(32150, 13, (long long )(mem_6->elems != 0));
# 2032 "dfa.c"
    if (mem_6->elems != 0) {
      __CrestBranch(32153, 6041, 1);
# 2033 "dfa.c"
      mem_7 = d->follows + i;
# 2033 "dfa.c"
      free((ptr_t )mem_7->elems);
      __CrestClearStack(32155);
    } else {
      __CrestBranch(32154, 6042, 0);

    }
    }
    }
    __CrestLoad(32158, (unsigned long )(& i), (long long )i);
    __CrestLoad(32157, (unsigned long )0, (long long )1);
    __CrestApply2(32156, 0, (long long )(i + 1));
    __CrestStore(32159, (unsigned long )(& i));
# 2031 "dfa.c"
    i ++;
  }
  while_break___0: ;
  }
# 2034 "dfa.c"
  free((ptr_t )d->follows);
  __CrestClearStack(32160);
  __CrestLoad(32161, (unsigned long )0, (long long )0);
  __CrestStore(32162, (unsigned long )(& i));
# 2035 "dfa.c"
  i = 0;
  {
# 2035 "dfa.c"
  while (1) {
    while_continue___1: ;
    {
    __CrestLoad(32165, (unsigned long )(& i), (long long )i);
    __CrestLoad(32164, (unsigned long )(& d->tralloc), (long long )d->tralloc);
    __CrestApply2(32163, 16, (long long )(i < d->tralloc));
# 2035 "dfa.c"
    if (i < d->tralloc) {
      __CrestBranch(32166, 6050, 1);

    } else {
      __CrestBranch(32167, 6051, 0);
# 2035 "dfa.c"
      goto while_break___1;
    }
    }
    {
# 2036 "dfa.c"
    mem_8 = d->trans + i;
    {
    __CrestLoad(32170, (unsigned long )mem_8, (long long )((unsigned long )*mem_8));
    __CrestLoad(32169, (unsigned long )0, (long long )0);
    __CrestApply2(32168, 13, (long long )(*mem_8 != 0));
# 2036 "dfa.c"
    if (*mem_8 != 0) {
      __CrestBranch(32171, 6055, 1);
# 2037 "dfa.c"
      mem_9 = d->trans + i;
# 2037 "dfa.c"
      free((ptr_t )*mem_9);
      __CrestClearStack(32173);
    } else {
      __CrestBranch(32172, 6056, 0);
      {
# 2038 "dfa.c"
      mem_10 = d->fails + i;
      {
      __CrestLoad(32176, (unsigned long )mem_10, (long long )((unsigned long )*mem_10));
      __CrestLoad(32175, (unsigned long )0, (long long )0);
      __CrestApply2(32174, 13, (long long )(*mem_10 != 0));
# 2038 "dfa.c"
      if (*mem_10 != 0) {
        __CrestBranch(32177, 6059, 1);
# 2039 "dfa.c"
        mem_11 = d->fails + i;
# 2039 "dfa.c"
        free((ptr_t )*mem_11);
        __CrestClearStack(32179);
      } else {
        __CrestBranch(32178, 6060, 0);

      }
      }
      }
    }
    }
    }
    __CrestLoad(32182, (unsigned long )(& i), (long long )i);
    __CrestLoad(32181, (unsigned long )0, (long long )1);
    __CrestApply2(32180, 0, (long long )(i + 1));
    __CrestStore(32183, (unsigned long )(& i));
# 2035 "dfa.c"
    i ++;
  }
  while_break___1: ;
  }
  {
  __CrestLoad(32186, (unsigned long )(& d->realtrans), (long long )((unsigned long )d->realtrans));
  __CrestLoad(32185, (unsigned long )0, (long long )0);
  __CrestApply2(32184, 13, (long long )(d->realtrans != 0));
# 2040 "dfa.c"
  if (d->realtrans != 0) {
    __CrestBranch(32187, 6064, 1);
# 2040 "dfa.c"
    free((ptr_t )d->realtrans);
    __CrestClearStack(32189);
  } else {
    __CrestBranch(32188, 6065, 0);

  }
  }
  {
  __CrestLoad(32192, (unsigned long )(& d->fails), (long long )((unsigned long )d->fails));
  __CrestLoad(32191, (unsigned long )0, (long long )0);
  __CrestApply2(32190, 13, (long long )(d->fails != 0));
# 2041 "dfa.c"
  if (d->fails != 0) {
    __CrestBranch(32193, 6067, 1);
# 2041 "dfa.c"
    free((ptr_t )d->fails);
    __CrestClearStack(32195);
  } else {
    __CrestBranch(32194, 6068, 0);

  }
  }
  {
  __CrestLoad(32198, (unsigned long )(& d->newlines), (long long )((unsigned long )d->newlines));
  __CrestLoad(32197, (unsigned long )0, (long long )0);
  __CrestApply2(32196, 13, (long long )(d->newlines != 0));
# 2042 "dfa.c"
  if (d->newlines != 0) {
    __CrestBranch(32199, 6070, 1);
# 2042 "dfa.c"
    free((ptr_t )d->newlines);
    __CrestClearStack(32201);
  } else {
    __CrestBranch(32200, 6071, 0);

  }
  }
  {
  __CrestLoad(32204, (unsigned long )(& d->success), (long long )((unsigned long )d->success));
  __CrestLoad(32203, (unsigned long )0, (long long )0);
  __CrestApply2(32202, 13, (long long )(d->success != 0));
# 2043 "dfa.c"
  if (d->success != 0) {
    __CrestBranch(32205, 6073, 1);
# 2043 "dfa.c"
    free((ptr_t )d->success);
    __CrestClearStack(32207);
  } else {
    __CrestBranch(32206, 6074, 0);

  }
  }
# 2044 "dfa.c"
  dm = d->musts;
  {
# 2044 "dfa.c"
  while (1) {
    while_continue___2: ;
    {
    __CrestLoad(32210, (unsigned long )(& dm), (long long )((unsigned long )dm));
    __CrestLoad(32209, (unsigned long )0, (long long )0);
    __CrestApply2(32208, 13, (long long )(dm != 0));
# 2044 "dfa.c"
    if (dm != 0) {
      __CrestBranch(32211, 6080, 1);

    } else {
      __CrestBranch(32212, 6081, 0);
# 2044 "dfa.c"
      goto while_break___2;
    }
    }
# 2046 "dfa.c"
    ndm = dm->next;
# 2047 "dfa.c"
    free((void *)dm->must);
    __CrestClearStack(32213);
# 2048 "dfa.c"
    free((ptr_t )dm);
    __CrestClearStack(32214);
# 2044 "dfa.c"
    dm = ndm;
  }
  while_break___2: ;
  }

  {
  __CrestReturn(32215);
# 2019 "dfa.c"
  return;
  }
}
}
# 2132 "dfa.c"
static char *icatalloc(char *old , char *new )
{
  char *result ;
  size_t oldsize ;
  size_t newsize ;
  size_t tmp ;
  void *tmp___0 ;
  void *tmp___1 ;
  char *__retres9 ;

  {
  __CrestCall(32216, 260);

  {
  __CrestLoad(32219, (unsigned long )(& new), (long long )((unsigned long )new));
  __CrestLoad(32218, (unsigned long )0, (long long )((unsigned long )((void *)0)));
  __CrestApply2(32217, 12, (long long )((unsigned long )new == (unsigned long )((void *)0)));
# 2140 "dfa.c"
  if ((unsigned long )new == (unsigned long )((void *)0)) {
    __CrestBranch(32220, 6087, 1);
    __CrestLoad(32222, (unsigned long )0, (long long )((size_t )0));
    __CrestStore(32223, (unsigned long )(& newsize));
# 2140 "dfa.c"
    newsize = (size_t )0;
  } else {
    __CrestBranch(32221, 6088, 0);
# 2140 "dfa.c"
    tmp = strlen((char const *)new);
    __CrestHandleReturn(32225, (long long )tmp);
    __CrestStore(32224, (unsigned long )(& tmp));
    __CrestLoad(32226, (unsigned long )(& tmp), (long long )tmp);
    __CrestStore(32227, (unsigned long )(& newsize));
# 2140 "dfa.c"
    newsize = tmp;
  }
  }
  {
  __CrestLoad(32230, (unsigned long )(& old), (long long )((unsigned long )old));
  __CrestLoad(32229, (unsigned long )0, (long long )((unsigned long )((void *)0)));
  __CrestApply2(32228, 12, (long long )((unsigned long )old == (unsigned long )((void *)0)));
# 2141 "dfa.c"
  if ((unsigned long )old == (unsigned long )((void *)0)) {
    __CrestBranch(32231, 6090, 1);
    __CrestLoad(32233, (unsigned long )0, (long long )((size_t )0));
    __CrestStore(32234, (unsigned long )(& oldsize));
# 2142 "dfa.c"
    oldsize = (size_t )0;
  } else {
    __CrestBranch(32232, 6091, 0);
    {
    __CrestLoad(32237, (unsigned long )(& newsize), (long long )newsize);
    __CrestLoad(32236, (unsigned long )0, (long long )0UL);
    __CrestApply2(32235, 12, (long long )(newsize == 0UL));
# 2143 "dfa.c"
    if (newsize == 0UL) {
      __CrestBranch(32238, 6092, 1);
# 2144 "dfa.c"
      __retres9 = old;
# 2144 "dfa.c"
      goto return_label;
    } else {
      __CrestBranch(32239, 6094, 0);
# 2145 "dfa.c"
      oldsize = strlen((char const *)old);
      __CrestHandleReturn(32241, (long long )oldsize);
      __CrestStore(32240, (unsigned long )(& oldsize));
    }
    }
  }
  }
  {
  __CrestLoad(32244, (unsigned long )(& old), (long long )((unsigned long )old));
  __CrestLoad(32243, (unsigned long )0, (long long )((unsigned long )((void *)0)));
  __CrestApply2(32242, 12, (long long )((unsigned long )old == (unsigned long )((void *)0)));
# 2146 "dfa.c"
  if ((unsigned long )old == (unsigned long )((void *)0)) {
    __CrestBranch(32245, 6096, 1);
    __CrestLoad(32249, (unsigned long )(& newsize), (long long )newsize);
    __CrestLoad(32248, (unsigned long )0, (long long )1UL);
    __CrestApply2(32247, 0, (long long )(newsize + 1UL));
# 2147 "dfa.c"
    tmp___0 = malloc(newsize + 1UL);
    __CrestClearStack(32250);
# 2147 "dfa.c"
    result = (char *)tmp___0;
  } else {
    __CrestBranch(32246, 6097, 0);
    __CrestLoad(32255, (unsigned long )(& oldsize), (long long )oldsize);
    __CrestLoad(32254, (unsigned long )(& newsize), (long long )newsize);
    __CrestApply2(32253, 0, (long long )(oldsize + newsize));
    __CrestLoad(32252, (unsigned long )0, (long long )1UL);
    __CrestApply2(32251, 0, (long long )((oldsize + newsize) + 1UL));
# 2149 "dfa.c"
    tmp___1 = realloc((void *)old, (oldsize + newsize) + 1UL);
    __CrestClearStack(32256);
# 2149 "dfa.c"
    result = (char *)tmp___1;
  }
  }
  {
  __CrestLoad(32259, (unsigned long )(& result), (long long )((unsigned long )result));
  __CrestLoad(32258, (unsigned long )0, (long long )((unsigned long )((void *)0)));
  __CrestApply2(32257, 13, (long long )((unsigned long )result != (unsigned long )((void *)0)));
# 2150 "dfa.c"
  if ((unsigned long )result != (unsigned long )((void *)0)) {
    __CrestBranch(32260, 6099, 1);
    {
    __CrestLoad(32264, (unsigned long )(& new), (long long )((unsigned long )new));
    __CrestLoad(32263, (unsigned long )0, (long long )((unsigned long )((void *)0)));
    __CrestApply2(32262, 13, (long long )((unsigned long )new != (unsigned long )((void *)0)));
# 2150 "dfa.c"
    if ((unsigned long )new != (unsigned long )((void *)0)) {
      __CrestBranch(32265, 6100, 1);
# 2151 "dfa.c"
      strcpy((char * __restrict )(result + oldsize), (char const * __restrict )new);
      __CrestClearStack(32267);
    } else {
      __CrestBranch(32266, 6101, 0);

    }
    }
  } else {
    __CrestBranch(32261, 6102, 0);

  }
  }
# 2152 "dfa.c"
  __retres9 = result;
  return_label:
  {
  __CrestReturn(32268);
# 2132 "dfa.c"
  return (__retres9);
  }
}
}
# 2155 "dfa.c"
static char *icpyalloc(char *string )
{
  char *tmp ;

  {
  __CrestCall(32269, 261);
# 2159 "dfa.c"
  tmp = icatalloc((char *)((void *)0), string);
  __CrestClearStack(32270);
  {
  __CrestReturn(32271);
# 2159 "dfa.c"
  return (tmp);
  }
}
}
# 2162 "dfa.c"
static char *istrstr(char *lookin , char *lookfor )
{
  char *cp ;
  size_t len ;
  int tmp ;
  char *__retres6 ;

  {
  __CrestCall(32272, 262);
# 2170 "dfa.c"
  len = strlen((char const *)lookfor);
  __CrestHandleReturn(32274, (long long )len);
  __CrestStore(32273, (unsigned long )(& len));
# 2171 "dfa.c"
  cp = lookin;
  {
# 2171 "dfa.c"
  while (1) {
    while_continue: ;
    {
    __CrestLoad(32277, (unsigned long )cp, (long long )*cp);
    __CrestLoad(32276, (unsigned long )0, (long long )0);
    __CrestApply2(32275, 13, (long long )((int )*cp != 0));
# 2171 "dfa.c"
    if ((int )*cp != 0) {
      __CrestBranch(32278, 6112, 1);

    } else {
      __CrestBranch(32279, 6113, 0);
# 2171 "dfa.c"
      goto while_break;
    }
    }
    __CrestLoad(32280, (unsigned long )(& len), (long long )len);
# 2172 "dfa.c"
    tmp = strncmp((char const *)cp, (char const *)lookfor, len);
    __CrestHandleReturn(32282, (long long )tmp);
    __CrestStore(32281, (unsigned long )(& tmp));
    {
    __CrestLoad(32285, (unsigned long )(& tmp), (long long )tmp);
    __CrestLoad(32284, (unsigned long )0, (long long )0);
    __CrestApply2(32283, 12, (long long )(tmp == 0));
# 2172 "dfa.c"
    if (tmp == 0) {
      __CrestBranch(32286, 6116, 1);
# 2173 "dfa.c"
      __retres6 = cp;
# 2173 "dfa.c"
      goto return_label;
    } else {
      __CrestBranch(32287, 6118, 0);

    }
    }
# 2171 "dfa.c"
    cp ++;
  }
  while_break: ;
  }
# 2174 "dfa.c"
  __retres6 = (char *)((void *)0);
  return_label:
  {
  __CrestReturn(32288);
# 2162 "dfa.c"
  return (__retres6);
  }
}
}
# 2177 "dfa.c"
static void ifree(char *cp )
{


  {
  __CrestCall(32289, 263);

  {
  __CrestLoad(32292, (unsigned long )(& cp), (long long )((unsigned long )cp));
  __CrestLoad(32291, (unsigned long )0, (long long )((unsigned long )((void *)0)));
  __CrestApply2(32290, 13, (long long )((unsigned long )cp != (unsigned long )((void *)0)));
# 2181 "dfa.c"
  if ((unsigned long )cp != (unsigned long )((void *)0)) {
    __CrestBranch(32293, 6124, 1);
# 2182 "dfa.c"
    free((void *)cp);
    __CrestClearStack(32295);
  } else {
    __CrestBranch(32294, 6125, 0);

  }
  }

  {
  __CrestReturn(32296);
# 2177 "dfa.c"
  return;
  }
}
}
# 2185 "dfa.c"
static void freelist(char **cpp )
{
  int i ;
  char **mem_3 ;
  char **mem_4 ;
  char **mem_5 ;

  {
  __CrestCall(32297, 264);

  {
  __CrestLoad(32300, (unsigned long )(& cpp), (long long )((unsigned long )cpp));
  __CrestLoad(32299, (unsigned long )0, (long long )((unsigned long )((void *)0)));
  __CrestApply2(32298, 12, (long long )((unsigned long )cpp == (unsigned long )((void *)0)));
# 2191 "dfa.c"
  if ((unsigned long )cpp == (unsigned long )((void *)0)) {
    __CrestBranch(32301, 6129, 1);
# 2192 "dfa.c"
    goto return_label;
  } else {
    __CrestBranch(32302, 6131, 0);

  }
  }
  __CrestLoad(32303, (unsigned long )0, (long long )0);
  __CrestStore(32304, (unsigned long )(& i));
# 2193 "dfa.c"
  i = 0;
  {
# 2193 "dfa.c"
  while (1) {
    while_continue: ;
    {
# 2193 "dfa.c"
    mem_3 = cpp + i;
    {
    __CrestLoad(32307, (unsigned long )mem_3, (long long )((unsigned long )*mem_3));
    __CrestLoad(32306, (unsigned long )0, (long long )((unsigned long )((void *)0)));
    __CrestApply2(32305, 13, (long long )((unsigned long )*mem_3 != (unsigned long )((void *)0)));
# 2193 "dfa.c"
    if ((unsigned long )*mem_3 != (unsigned long )((void *)0)) {
      __CrestBranch(32308, 6139, 1);

    } else {
      __CrestBranch(32309, 6140, 0);
# 2193 "dfa.c"
      goto while_break;
    }
    }
    }
# 2195 "dfa.c"
    mem_4 = cpp + i;
# 2195 "dfa.c"
    free((void *)*mem_4);
    __CrestClearStack(32310);
# 2196 "dfa.c"
    mem_5 = cpp + i;
# 2196 "dfa.c"
    *mem_5 = (char *)((void *)0);
    __CrestLoad(32313, (unsigned long )(& i), (long long )i);
    __CrestLoad(32312, (unsigned long )0, (long long )1);
    __CrestApply2(32311, 0, (long long )(i + 1));
    __CrestStore(32314, (unsigned long )(& i));
# 2193 "dfa.c"
    i ++;
  }
  while_break: ;
  }

  return_label:
  {
  __CrestReturn(32315);
# 2185 "dfa.c"
  return;
  }
}
}
# 2200 "dfa.c"
static char **enlist(char **cpp , char *new , size_t len )
{
  int i ;
  int j ;
  char *tmp ;
  char *tmp___0 ;
  void *tmp___1 ;
  char *mem_9 ;
  char **mem_10 ;
  char **mem_11 ;
  char **mem_12 ;
  char **mem_13 ;
  char **mem_14 ;
  char **mem_15 ;
  char **mem_16 ;
  char **mem_17 ;
  char **mem_18 ;
  char **mem_19 ;
  char **__retres20 ;

  {
  __CrestCall(32317, 265);
  __CrestStore(32316, (unsigned long )(& len));
  {
  __CrestLoad(32320, (unsigned long )(& cpp), (long long )((unsigned long )cpp));
  __CrestLoad(32319, (unsigned long )0, (long long )((unsigned long )((void *)0)));
  __CrestApply2(32318, 12, (long long )((unsigned long )cpp == (unsigned long )((void *)0)));
# 2208 "dfa.c"
  if ((unsigned long )cpp == (unsigned long )((void *)0)) {
    __CrestBranch(32321, 6146, 1);
# 2209 "dfa.c"
    __retres20 = (char **)((void *)0);
# 2209 "dfa.c"
    goto return_label;
  } else {
    __CrestBranch(32322, 6148, 0);

  }
  }
# 2210 "dfa.c"
  new = icpyalloc(new);
  __CrestClearStack(32323);
  {
  __CrestLoad(32326, (unsigned long )(& new), (long long )((unsigned long )new));
  __CrestLoad(32325, (unsigned long )0, (long long )((unsigned long )((void *)0)));
  __CrestApply2(32324, 12, (long long )((unsigned long )new == (unsigned long )((void *)0)));
# 2210 "dfa.c"
  if ((unsigned long )new == (unsigned long )((void *)0)) {
    __CrestBranch(32327, 6151, 1);
# 2212 "dfa.c"
    freelist(cpp);
    __CrestClearStack(32329);
# 2213 "dfa.c"
    __retres20 = (char **)((void *)0);
# 2213 "dfa.c"
    goto return_label;
  } else {
    __CrestBranch(32328, 6154, 0);

  }
  }
# 2215 "dfa.c"
  mem_9 = new + len;
  __CrestLoad(32330, (unsigned long )0, (long long )((char )'\000'));
  __CrestStore(32331, (unsigned long )mem_9);
# 2215 "dfa.c"
  *mem_9 = (char )'\000';
  __CrestLoad(32332, (unsigned long )0, (long long )0);
  __CrestStore(32333, (unsigned long )(& i));
# 2217 "dfa.c"
  i = 0;
  {
# 2217 "dfa.c"
  while (1) {
    while_continue: ;
    {
# 2217 "dfa.c"
    mem_10 = cpp + i;
    {
    __CrestLoad(32336, (unsigned long )mem_10, (long long )((unsigned long )*mem_10));
    __CrestLoad(32335, (unsigned long )0, (long long )((unsigned long )((void *)0)));
    __CrestApply2(32334, 13, (long long )((unsigned long )*mem_10 != (unsigned long )((void *)0)));
# 2217 "dfa.c"
    if ((unsigned long )*mem_10 != (unsigned long )((void *)0)) {
      __CrestBranch(32337, 6162, 1);

    } else {
      __CrestBranch(32338, 6163, 0);
# 2217 "dfa.c"
      goto while_break;
    }
    }
    }
# 2218 "dfa.c"
    mem_11 = cpp + i;
# 2218 "dfa.c"
    tmp = istrstr(*mem_11, new);
    __CrestClearStack(32339);
    {
    __CrestLoad(32342, (unsigned long )(& tmp), (long long )((unsigned long )tmp));
    __CrestLoad(32341, (unsigned long )0, (long long )((unsigned long )((void *)0)));
    __CrestApply2(32340, 13, (long long )((unsigned long )tmp != (unsigned long )((void *)0)));
# 2218 "dfa.c"
    if ((unsigned long )tmp != (unsigned long )((void *)0)) {
      __CrestBranch(32343, 6166, 1);
# 2220 "dfa.c"
      free((void *)new);
      __CrestClearStack(32345);
# 2221 "dfa.c"
      __retres20 = cpp;
# 2221 "dfa.c"
      goto return_label;
    } else {
      __CrestBranch(32344, 6169, 0);

    }
    }
    __CrestLoad(32348, (unsigned long )(& i), (long long )i);
    __CrestLoad(32347, (unsigned long )0, (long long )1);
    __CrestApply2(32346, 0, (long long )(i + 1));
    __CrestStore(32349, (unsigned long )(& i));
# 2217 "dfa.c"
    i ++;
  }
  while_break: ;
  }
  __CrestLoad(32350, (unsigned long )0, (long long )0);
  __CrestStore(32351, (unsigned long )(& j));
# 2224 "dfa.c"
  j = 0;
  {
# 2225 "dfa.c"
  while (1) {
    while_continue___0: ;
    {
# 2225 "dfa.c"
    mem_12 = cpp + j;
    {
    __CrestLoad(32354, (unsigned long )mem_12, (long long )((unsigned long )*mem_12));
    __CrestLoad(32353, (unsigned long )0, (long long )((unsigned long )((void *)0)));
    __CrestApply2(32352, 13, (long long )((unsigned long )*mem_12 != (unsigned long )((void *)0)));
# 2225 "dfa.c"
    if ((unsigned long )*mem_12 != (unsigned long )((void *)0)) {
      __CrestBranch(32355, 6179, 1);

    } else {
      __CrestBranch(32356, 6180, 0);
# 2225 "dfa.c"
      goto while_break___0;
    }
    }
    }
# 2226 "dfa.c"
    mem_13 = cpp + j;
# 2226 "dfa.c"
    tmp___0 = istrstr(new, *mem_13);
    __CrestClearStack(32357);
    {
    __CrestLoad(32360, (unsigned long )(& tmp___0), (long long )((unsigned long )tmp___0));
    __CrestLoad(32359, (unsigned long )0, (long long )((unsigned long )((void *)0)));
    __CrestApply2(32358, 12, (long long )((unsigned long )tmp___0 == (unsigned long )((void *)0)));
# 2226 "dfa.c"
    if ((unsigned long )tmp___0 == (unsigned long )((void *)0)) {
      __CrestBranch(32361, 6183, 1);
      __CrestLoad(32365, (unsigned long )(& j), (long long )j);
      __CrestLoad(32364, (unsigned long )0, (long long )1);
      __CrestApply2(32363, 0, (long long )(j + 1));
      __CrestStore(32366, (unsigned long )(& j));
# 2227 "dfa.c"
      j ++;
    } else {
      __CrestBranch(32362, 6184, 0);
# 2230 "dfa.c"
      mem_14 = cpp + j;
# 2230 "dfa.c"
      free((void *)*mem_14);
      __CrestClearStack(32367);
      __CrestLoad(32370, (unsigned long )(& i), (long long )i);
      __CrestLoad(32369, (unsigned long )0, (long long )1);
      __CrestApply2(32368, 1, (long long )(i - 1));
      __CrestStore(32371, (unsigned long )(& i));
# 2231 "dfa.c"
      i --;
      {
      __CrestLoad(32374, (unsigned long )(& i), (long long )i);
      __CrestLoad(32373, (unsigned long )(& j), (long long )j);
      __CrestApply2(32372, 12, (long long )(i == j));
# 2231 "dfa.c"
      if (i == j) {
        __CrestBranch(32375, 6186, 1);
# 2232 "dfa.c"
        goto while_break___0;
      } else {
        __CrestBranch(32376, 6187, 0);

      }
      }
# 2233 "dfa.c"
      mem_15 = cpp + j;
# 2233 "dfa.c"
      mem_16 = cpp + i;
# 2233 "dfa.c"
      *mem_15 = *mem_16;
# 2234 "dfa.c"
      mem_17 = cpp + i;
# 2234 "dfa.c"
      *mem_17 = (char *)((void *)0);
    }
    }
  }
  while_break___0: ;
  }
  __CrestLoad(32381, (unsigned long )(& i), (long long )i);
  __CrestLoad(32380, (unsigned long )0, (long long )2);
  __CrestApply2(32379, 0, (long long )(i + 2));
  __CrestLoad(32378, (unsigned long )0, (long long )sizeof(*cpp));
  __CrestApply2(32377, 2, (long long )((unsigned long )(i + 2) * sizeof(*cpp)));
# 2237 "dfa.c"
  tmp___1 = realloc((void *)((char *)cpp), (unsigned long )(i + 2) * sizeof(*cpp));
  __CrestClearStack(32382);
# 2237 "dfa.c"
  cpp = (char **)tmp___1;
  {
  __CrestLoad(32385, (unsigned long )(& cpp), (long long )((unsigned long )cpp));
  __CrestLoad(32384, (unsigned long )0, (long long )((unsigned long )((void *)0)));
  __CrestApply2(32383, 12, (long long )((unsigned long )cpp == (unsigned long )((void *)0)));
# 2238 "dfa.c"
  if ((unsigned long )cpp == (unsigned long )((void *)0)) {
    __CrestBranch(32386, 6192, 1);
# 2239 "dfa.c"
    __retres20 = (char **)((void *)0);
# 2239 "dfa.c"
    goto return_label;
  } else {
    __CrestBranch(32387, 6194, 0);

  }
  }
# 2240 "dfa.c"
  mem_18 = cpp + i;
# 2240 "dfa.c"
  *mem_18 = new;
# 2241 "dfa.c"
  mem_19 = cpp + (i + 1);
# 2241 "dfa.c"
  *mem_19 = (char *)((void *)0);
# 2242 "dfa.c"
  __retres20 = cpp;
  return_label:
  {
  __CrestReturn(32388);
# 2200 "dfa.c"
  return (__retres20);
  }
}
}
# 2248 "dfa.c"
static char **comsubs(char *left , char *right )
{
  char **cpp ;
  char *lcp ;
  char *rcp ;
  size_t i ;
  size_t len ;
  void *tmp ;
  char **mem_9 ;
  char *mem_10 ;
  char *mem_11 ;
  char *mem_12 ;
  char **__retres13 ;

  {
  __CrestCall(32389, 266);

  {
  __CrestLoad(32392, (unsigned long )(& left), (long long )((unsigned long )left));
  __CrestLoad(32391, (unsigned long )0, (long long )((unsigned long )((void *)0)));
  __CrestApply2(32390, 12, (long long )((unsigned long )left == (unsigned long )((void *)0)));
# 2258 "dfa.c"
  if ((unsigned long )left == (unsigned long )((void *)0)) {
    __CrestBranch(32393, 6199, 1);
# 2259 "dfa.c"
    __retres13 = (char **)((void *)0);
# 2259 "dfa.c"
    goto return_label;
  } else {
    __CrestBranch(32394, 6201, 0);
    {
    __CrestLoad(32397, (unsigned long )(& right), (long long )((unsigned long )right));
    __CrestLoad(32396, (unsigned long )0, (long long )((unsigned long )((void *)0)));
    __CrestApply2(32395, 12, (long long )((unsigned long )right == (unsigned long )((void *)0)));
# 2258 "dfa.c"
    if ((unsigned long )right == (unsigned long )((void *)0)) {
      __CrestBranch(32398, 6202, 1);
# 2259 "dfa.c"
      __retres13 = (char **)((void *)0);
# 2259 "dfa.c"
      goto return_label;
    } else {
      __CrestBranch(32399, 6204, 0);

    }
    }
  }
  }
  __CrestLoad(32400, (unsigned long )0, (long long )sizeof(*cpp));
# 2260 "dfa.c"
  tmp = malloc(sizeof(*cpp));
  __CrestClearStack(32401);
# 2260 "dfa.c"
  cpp = (char **)tmp;
  {
  __CrestLoad(32404, (unsigned long )(& cpp), (long long )((unsigned long )cpp));
  __CrestLoad(32403, (unsigned long )0, (long long )((unsigned long )((void *)0)));
  __CrestApply2(32402, 12, (long long )((unsigned long )cpp == (unsigned long )((void *)0)));
# 2261 "dfa.c"
  if ((unsigned long )cpp == (unsigned long )((void *)0)) {
    __CrestBranch(32405, 6207, 1);
# 2262 "dfa.c"
    __retres13 = (char **)((void *)0);
# 2262 "dfa.c"
    goto return_label;
  } else {
    __CrestBranch(32406, 6209, 0);

  }
  }
# 2263 "dfa.c"
  mem_9 = cpp + 0;
# 2263 "dfa.c"
  *mem_9 = (char *)((void *)0);
# 2264 "dfa.c"
  lcp = left;
  {
# 2264 "dfa.c"
  while (1) {
    while_continue: ;
    {
    __CrestLoad(32409, (unsigned long )lcp, (long long )*lcp);
    __CrestLoad(32408, (unsigned long )0, (long long )0);
    __CrestApply2(32407, 13, (long long )((int )*lcp != 0));
# 2264 "dfa.c"
    if ((int )*lcp != 0) {
      __CrestBranch(32410, 6215, 1);

    } else {
      __CrestBranch(32411, 6216, 0);
# 2264 "dfa.c"
      goto while_break;
    }
    }
    __CrestLoad(32412, (unsigned long )0, (long long )((size_t )0));
    __CrestStore(32413, (unsigned long )(& len));
# 2266 "dfa.c"
    len = (size_t )0;
    __CrestLoad(32414, (unsigned long )lcp, (long long )*lcp);
# 2267 "dfa.c"
    rcp = strchr((char const *)right, (int )*lcp);
    __CrestClearStack(32415);
    {
# 2268 "dfa.c"
    while (1) {
      while_continue___0: ;
      {
      __CrestLoad(32418, (unsigned long )(& rcp), (long long )((unsigned long )rcp));
      __CrestLoad(32417, (unsigned long )0, (long long )((unsigned long )((void *)0)));
      __CrestApply2(32416, 13, (long long )((unsigned long )rcp != (unsigned long )((void *)0)));
# 2268 "dfa.c"
      if ((unsigned long )rcp != (unsigned long )((void *)0)) {
        __CrestBranch(32419, 6222, 1);

      } else {
        __CrestBranch(32420, 6223, 0);
# 2268 "dfa.c"
        goto while_break___0;
      }
      }
      __CrestLoad(32421, (unsigned long )0, (long long )((size_t )1));
      __CrestStore(32422, (unsigned long )(& i));
# 2270 "dfa.c"
      i = (size_t )1;
      {
# 2270 "dfa.c"
      while (1) {
        while_continue___1: ;
        {
# 2270 "dfa.c"
        mem_10 = lcp + i;
        {
        __CrestLoad(32425, (unsigned long )mem_10, (long long )*mem_10);
        __CrestLoad(32424, (unsigned long )0, (long long )0);
        __CrestApply2(32423, 13, (long long )((int )*mem_10 != 0));
# 2270 "dfa.c"
        if ((int )*mem_10 != 0) {
          __CrestBranch(32426, 6231, 1);
          {
# 2270 "dfa.c"
          mem_11 = lcp + i;
# 2270 "dfa.c"
          mem_12 = rcp + i;
          {
          __CrestLoad(32430, (unsigned long )mem_11, (long long )*mem_11);
          __CrestLoad(32429, (unsigned long )mem_12, (long long )*mem_12);
          __CrestApply2(32428, 12, (long long )((int )*mem_11 == (int )*mem_12));
# 2270 "dfa.c"
          if ((int )*mem_11 == (int )*mem_12) {
            __CrestBranch(32431, 6234, 1);

          } else {
            __CrestBranch(32432, 6235, 0);
# 2270 "dfa.c"
            goto while_break___1;
          }
          }
          }
        } else {
          __CrestBranch(32427, 6236, 0);
# 2270 "dfa.c"
          goto while_break___1;
        }
        }
        }
# 2271 "dfa.c"
        goto __Cont;
        __Cont:
        __CrestLoad(32435, (unsigned long )(& i), (long long )i);
        __CrestLoad(32434, (unsigned long )0, (long long )1UL);
        __CrestApply2(32433, 0, (long long )(i + 1UL));
        __CrestStore(32436, (unsigned long )(& i));
# 2270 "dfa.c"
        i ++;
      }
      while_break___1: ;
      }
      {
      __CrestLoad(32439, (unsigned long )(& i), (long long )i);
      __CrestLoad(32438, (unsigned long )(& len), (long long )len);
      __CrestApply2(32437, 14, (long long )(i > len));
# 2272 "dfa.c"
      if (i > len) {
        __CrestBranch(32440, 6241, 1);
        __CrestLoad(32442, (unsigned long )(& i), (long long )i);
        __CrestStore(32443, (unsigned long )(& len));
# 2273 "dfa.c"
        len = i;
      } else {
        __CrestBranch(32441, 6242, 0);

      }
      }
      __CrestLoad(32444, (unsigned long )lcp, (long long )*lcp);
# 2274 "dfa.c"
      rcp = strchr((char const *)(rcp + 1), (int )*lcp);
      __CrestClearStack(32445);
    }
    while_break___0: ;
    }
    {
    __CrestLoad(32448, (unsigned long )(& len), (long long )len);
    __CrestLoad(32447, (unsigned long )0, (long long )0UL);
    __CrestApply2(32446, 12, (long long )(len == 0UL));
# 2276 "dfa.c"
    if (len == 0UL) {
      __CrestBranch(32449, 6246, 1);
# 2277 "dfa.c"
      goto __Cont___0;
    } else {
      __CrestBranch(32450, 6247, 0);

    }
    }
    __CrestLoad(32451, (unsigned long )(& len), (long long )len);
# 2278 "dfa.c"
    cpp = enlist(cpp, lcp, len);
    __CrestClearStack(32452);
    {
    __CrestLoad(32455, (unsigned long )(& cpp), (long long )((unsigned long )cpp));
    __CrestLoad(32454, (unsigned long )0, (long long )((unsigned long )((void *)0)));
    __CrestApply2(32453, 12, (long long )((unsigned long )cpp == (unsigned long )((void *)0)));
# 2278 "dfa.c"
    if ((unsigned long )cpp == (unsigned long )((void *)0)) {
      __CrestBranch(32456, 6250, 1);
# 2279 "dfa.c"
      goto while_break;
    } else {
      __CrestBranch(32457, 6251, 0);

    }
    }
    __Cont___0:
# 2264 "dfa.c"
    lcp ++;
  }
  while_break: ;
  }
# 2281 "dfa.c"
  __retres13 = cpp;
  return_label:
  {
  __CrestReturn(32458);
# 2248 "dfa.c"
  return (__retres13);
  }
}
}
# 2284 "dfa.c"
static char **addlists(char **old , char **new )
{
  int i ;
  size_t tmp ;
  char **mem_5 ;
  char **mem_6 ;
  char **mem_7 ;
  char **__retres8 ;

  {
  __CrestCall(32459, 267);

  {
  __CrestLoad(32462, (unsigned long )(& old), (long long )((unsigned long )old));
  __CrestLoad(32461, (unsigned long )0, (long long )((unsigned long )((void *)0)));
  __CrestApply2(32460, 12, (long long )((unsigned long )old == (unsigned long )((void *)0)));
# 2291 "dfa.c"
  if ((unsigned long )old == (unsigned long )((void *)0)) {
    __CrestBranch(32463, 6257, 1);
# 2292 "dfa.c"
    __retres8 = (char **)((void *)0);
# 2292 "dfa.c"
    goto return_label;
  } else {
    __CrestBranch(32464, 6259, 0);
    {
    __CrestLoad(32467, (unsigned long )(& new), (long long )((unsigned long )new));
    __CrestLoad(32466, (unsigned long )0, (long long )((unsigned long )((void *)0)));
    __CrestApply2(32465, 12, (long long )((unsigned long )new == (unsigned long )((void *)0)));
# 2291 "dfa.c"
    if ((unsigned long )new == (unsigned long )((void *)0)) {
      __CrestBranch(32468, 6260, 1);
# 2292 "dfa.c"
      __retres8 = (char **)((void *)0);
# 2292 "dfa.c"
      goto return_label;
    } else {
      __CrestBranch(32469, 6262, 0);

    }
    }
  }
  }
  __CrestLoad(32470, (unsigned long )0, (long long )0);
  __CrestStore(32471, (unsigned long )(& i));
# 2293 "dfa.c"
  i = 0;
  {
# 2293 "dfa.c"
  while (1) {
    while_continue: ;
    {
# 2293 "dfa.c"
    mem_5 = new + i;
    {
    __CrestLoad(32474, (unsigned long )mem_5, (long long )((unsigned long )*mem_5));
    __CrestLoad(32473, (unsigned long )0, (long long )((unsigned long )((void *)0)));
    __CrestApply2(32472, 13, (long long )((unsigned long )*mem_5 != (unsigned long )((void *)0)));
# 2293 "dfa.c"
    if ((unsigned long )*mem_5 != (unsigned long )((void *)0)) {
      __CrestBranch(32475, 6270, 1);

    } else {
      __CrestBranch(32476, 6271, 0);
# 2293 "dfa.c"
      goto while_break;
    }
    }
    }
# 2295 "dfa.c"
    mem_6 = new + i;
# 2295 "dfa.c"
    tmp = strlen((char const *)*mem_6);
    __CrestHandleReturn(32478, (long long )tmp);
    __CrestStore(32477, (unsigned long )(& tmp));
# 2295 "dfa.c"
    mem_7 = new + i;
    __CrestLoad(32479, (unsigned long )(& tmp), (long long )tmp);
# 2295 "dfa.c"
    old = enlist(old, *mem_7, tmp);
    __CrestClearStack(32480);
    {
    __CrestLoad(32483, (unsigned long )(& old), (long long )((unsigned long )old));
    __CrestLoad(32482, (unsigned long )0, (long long )((unsigned long )((void *)0)));
    __CrestApply2(32481, 12, (long long )((unsigned long )old == (unsigned long )((void *)0)));
# 2296 "dfa.c"
    if ((unsigned long )old == (unsigned long )((void *)0)) {
      __CrestBranch(32484, 6274, 1);
# 2297 "dfa.c"
      goto while_break;
    } else {
      __CrestBranch(32485, 6275, 0);

    }
    }
    __CrestLoad(32488, (unsigned long )(& i), (long long )i);
    __CrestLoad(32487, (unsigned long )0, (long long )1);
    __CrestApply2(32486, 0, (long long )(i + 1));
    __CrestStore(32489, (unsigned long )(& i));
# 2293 "dfa.c"
    i ++;
  }
  while_break: ;
  }
# 2299 "dfa.c"
  __retres8 = old;
  return_label:
  {
  __CrestReturn(32490);
# 2284 "dfa.c"
  return (__retres8);
  }
}
}
# 2304 "dfa.c"
static char **inboth(char **left , char **right )
{
  char **both ;
  char **temp ;
  int lnum ;
  int rnum ;
  void *tmp ;
  char **mem_8 ;
  char **mem_9 ;
  char **mem_10 ;
  char **mem_11 ;
  char **mem_12 ;
  char **__retres13 ;

  {
  __CrestCall(32491, 268);

  {
  __CrestLoad(32494, (unsigned long )(& left), (long long )((unsigned long )left));
  __CrestLoad(32493, (unsigned long )0, (long long )((unsigned long )((void *)0)));
  __CrestApply2(32492, 12, (long long )((unsigned long )left == (unsigned long )((void *)0)));
# 2313 "dfa.c"
  if ((unsigned long )left == (unsigned long )((void *)0)) {
    __CrestBranch(32495, 6281, 1);
# 2314 "dfa.c"
    __retres13 = (char **)((void *)0);
# 2314 "dfa.c"
    goto return_label;
  } else {
    __CrestBranch(32496, 6283, 0);
    {
    __CrestLoad(32499, (unsigned long )(& right), (long long )((unsigned long )right));
    __CrestLoad(32498, (unsigned long )0, (long long )((unsigned long )((void *)0)));
    __CrestApply2(32497, 12, (long long )((unsigned long )right == (unsigned long )((void *)0)));
# 2313 "dfa.c"
    if ((unsigned long )right == (unsigned long )((void *)0)) {
      __CrestBranch(32500, 6284, 1);
# 2314 "dfa.c"
      __retres13 = (char **)((void *)0);
# 2314 "dfa.c"
      goto return_label;
    } else {
      __CrestBranch(32501, 6286, 0);

    }
    }
  }
  }
  __CrestLoad(32502, (unsigned long )0, (long long )sizeof(*both));
# 2315 "dfa.c"
  tmp = malloc(sizeof(*both));
  __CrestClearStack(32503);
# 2315 "dfa.c"
  both = (char **)tmp;
  {
  __CrestLoad(32506, (unsigned long )(& both), (long long )((unsigned long )both));
  __CrestLoad(32505, (unsigned long )0, (long long )((unsigned long )((void *)0)));
  __CrestApply2(32504, 12, (long long )((unsigned long )both == (unsigned long )((void *)0)));
# 2316 "dfa.c"
  if ((unsigned long )both == (unsigned long )((void *)0)) {
    __CrestBranch(32507, 6289, 1);
# 2317 "dfa.c"
    __retres13 = (char **)((void *)0);
# 2317 "dfa.c"
    goto return_label;
  } else {
    __CrestBranch(32508, 6291, 0);

  }
  }
# 2318 "dfa.c"
  mem_8 = both + 0;
# 2318 "dfa.c"
  *mem_8 = (char *)((void *)0);
  __CrestLoad(32509, (unsigned long )0, (long long )0);
  __CrestStore(32510, (unsigned long )(& lnum));
# 2319 "dfa.c"
  lnum = 0;
  {
# 2319 "dfa.c"
  while (1) {
    while_continue: ;
    {
# 2319 "dfa.c"
    mem_9 = left + lnum;
    {
    __CrestLoad(32513, (unsigned long )mem_9, (long long )((unsigned long )*mem_9));
    __CrestLoad(32512, (unsigned long )0, (long long )((unsigned long )((void *)0)));
    __CrestApply2(32511, 13, (long long )((unsigned long )*mem_9 != (unsigned long )((void *)0)));
# 2319 "dfa.c"
    if ((unsigned long )*mem_9 != (unsigned long )((void *)0)) {
      __CrestBranch(32514, 6299, 1);

    } else {
      __CrestBranch(32515, 6300, 0);
# 2319 "dfa.c"
      goto while_break;
    }
    }
    }
    __CrestLoad(32516, (unsigned long )0, (long long )0);
    __CrestStore(32517, (unsigned long )(& rnum));
# 2321 "dfa.c"
    rnum = 0;
    {
# 2321 "dfa.c"
    while (1) {
      while_continue___0: ;
      {
# 2321 "dfa.c"
      mem_10 = right + rnum;
      {
      __CrestLoad(32520, (unsigned long )mem_10, (long long )((unsigned long )*mem_10));
      __CrestLoad(32519, (unsigned long )0, (long long )((unsigned long )((void *)0)));
      __CrestApply2(32518, 13, (long long )((unsigned long )*mem_10 != (unsigned long )((void *)0)));
# 2321 "dfa.c"
      if ((unsigned long )*mem_10 != (unsigned long )((void *)0)) {
        __CrestBranch(32521, 6308, 1);

      } else {
        __CrestBranch(32522, 6309, 0);
# 2321 "dfa.c"
        goto while_break___0;
      }
      }
      }
# 2323 "dfa.c"
      mem_11 = left + lnum;
# 2323 "dfa.c"
      mem_12 = right + rnum;
# 2323 "dfa.c"
      temp = comsubs(*mem_11, *mem_12);
      __CrestClearStack(32523);
      {
      __CrestLoad(32526, (unsigned long )(& temp), (long long )((unsigned long )temp));
      __CrestLoad(32525, (unsigned long )0, (long long )((unsigned long )((void *)0)));
      __CrestApply2(32524, 12, (long long )((unsigned long )temp == (unsigned long )((void *)0)));
# 2324 "dfa.c"
      if ((unsigned long )temp == (unsigned long )((void *)0)) {
        __CrestBranch(32527, 6312, 1);
# 2326 "dfa.c"
        freelist(both);
        __CrestClearStack(32529);
# 2327 "dfa.c"
        __retres13 = (char **)((void *)0);
# 2327 "dfa.c"
        goto return_label;
      } else {
        __CrestBranch(32528, 6315, 0);

      }
      }
# 2329 "dfa.c"
      both = addlists(both, temp);
      __CrestClearStack(32530);
# 2330 "dfa.c"
      freelist(temp);
      __CrestClearStack(32531);
# 2331 "dfa.c"
      free((void *)temp);
      __CrestClearStack(32532);
      {
      __CrestLoad(32535, (unsigned long )(& both), (long long )((unsigned long )both));
      __CrestLoad(32534, (unsigned long )0, (long long )((unsigned long )((void *)0)));
      __CrestApply2(32533, 12, (long long )((unsigned long )both == (unsigned long )((void *)0)));
# 2332 "dfa.c"
      if ((unsigned long )both == (unsigned long )((void *)0)) {
        __CrestBranch(32536, 6318, 1);
# 2333 "dfa.c"
        __retres13 = (char **)((void *)0);
# 2333 "dfa.c"
        goto return_label;
      } else {
        __CrestBranch(32537, 6320, 0);

      }
      }
      __CrestLoad(32540, (unsigned long )(& rnum), (long long )rnum);
      __CrestLoad(32539, (unsigned long )0, (long long )1);
      __CrestApply2(32538, 0, (long long )(rnum + 1));
      __CrestStore(32541, (unsigned long )(& rnum));
# 2321 "dfa.c"
      rnum ++;
    }
    while_break___0: ;
    }
    __CrestLoad(32544, (unsigned long )(& lnum), (long long )lnum);
    __CrestLoad(32543, (unsigned long )0, (long long )1);
    __CrestApply2(32542, 0, (long long )(lnum + 1));
    __CrestStore(32545, (unsigned long )(& lnum));
# 2319 "dfa.c"
    lnum ++;
  }
  while_break: ;
  }
# 2336 "dfa.c"
  __retres13 = both;
  return_label:
  {
  __CrestReturn(32546);
# 2304 "dfa.c"
  return (__retres13);
  }
}
}
# 2347 "dfa.c"
static void resetmust(must *mp )
{
  char tmp ;
  char tmp___0 ;
  char *mem_4 ;
  char *mem_5 ;
  char *mem_6 ;

  {
  __CrestCall(32547, 269);

  __CrestLoad(32548, (unsigned long )0, (long long )((char )'\000'));
  __CrestStore(32549, (unsigned long )(& tmp___0));
# 2351 "dfa.c"
  tmp___0 = (char )'\000';
# 2351 "dfa.c"
  mem_4 = mp->is + 0;
  __CrestLoad(32550, (unsigned long )(& tmp___0), (long long )tmp___0);
  __CrestStore(32551, (unsigned long )mem_4);
# 2351 "dfa.c"
  *mem_4 = tmp___0;
  __CrestLoad(32552, (unsigned long )(& tmp___0), (long long )tmp___0);
  __CrestStore(32553, (unsigned long )(& tmp));
# 2351 "dfa.c"
  tmp = tmp___0;
# 2351 "dfa.c"
  mem_5 = mp->right + 0;
  __CrestLoad(32554, (unsigned long )(& tmp), (long long )tmp);
  __CrestStore(32555, (unsigned long )mem_5);
# 2351 "dfa.c"
  *mem_5 = tmp;
# 2351 "dfa.c"
  mem_6 = mp->left + 0;
  __CrestLoad(32556, (unsigned long )(& tmp), (long long )tmp);
  __CrestStore(32557, (unsigned long )mem_6);
# 2351 "dfa.c"
  *mem_6 = tmp;
# 2352 "dfa.c"
  freelist(mp->in);
  __CrestClearStack(32558);

  {
  __CrestReturn(32559);
# 2347 "dfa.c"
  return;
  }
}
}
# 2366 "dfa.c"
static must must0 ;
# 2368 "dfa.c"
static void dfamust(struct dfa *dfa___0 ) ;
# 2368 "dfa.c"
static char empty_string[1] = { (char )'\000'};
# 2355 "dfa.c"
static void dfamust(struct dfa *dfa___0 )
{
  must *musts ;
  must *mp ;
  char *result ;
  int ri ;
  int i ;
  int exact ;
  token t ;
  struct dfamust *dm ;
  void *tmp ;
  void *tmp___0 ;
  void *tmp___1 ;
  void *tmp___2 ;
  void *tmp___3 ;
  char tmp___4 ;
  char tmp___5 ;
  char **new ;
  must *lmp ;
  must *rmp ;
  int j ;
  int ln ;
  int rn ;
  int n ;
  int tmp___6 ;
  size_t tmp___7 ;
  size_t tmp___8 ;
  size_t tmp___9 ;
  size_t tmp___10 ;
  int tmp___11 ;
  must *lmp___0 ;
  must *rmp___0 ;
  char *tp ;
  size_t tmp___12 ;
  char tmp___13 ;
  char tmp___14 ;
  char tmp___15 ;
  char tmp___16 ;
  void *tmp___17 ;
  size_t tmp___18 ;
  void *tmp___19 ;
  size_t tmp___20 ;
  must *mem_42 ;
  must *mem_43 ;
  char **mem_44 ;
  must *mem_45 ;
  must *mem_46 ;
  must *mem_47 ;
  must *mem_48 ;
  must *mem_49 ;
  must *mem_50 ;
  must *mem_51 ;
  must *mem_52 ;
  must *mem_53 ;
  char *mem_54 ;
  must *mem_55 ;
  char *mem_56 ;
  must *mem_57 ;
  char *mem_58 ;
  must *mem_59 ;
  char **mem_60 ;
  token *mem_61 ;
  char *mem_62 ;
  char *mem_63 ;
  char *mem_64 ;
  char *mem_65 ;
  char *mem_66 ;
  char *mem_67 ;
  char *mem_68 ;
  char *mem_69 ;
  char *mem_70 ;
  char *mem_71 ;
  char *mem_72 ;
  must *mem_73 ;
  char **mem_74 ;
  must *mem_75 ;
  char **mem_76 ;
  must *mem_77 ;
  char **mem_78 ;
  must *mem_79 ;
  char *mem_80 ;
  char *mem_81 ;
  char *mem_82 ;
  char *mem_83 ;
  char *mem_84 ;
  char *mem_85 ;
  char *mem_86 ;
  char *mem_87 ;
  char *mem_88 ;
  char *mem_89 ;
  char *mem_90 ;
  char *mem_91 ;
  char *mem_92 ;
  char *mem_93 ;
  char *mem_94 ;
  must *mem_95 ;
  must *mem_96 ;
  must *mem_97 ;
  must *mem_98 ;
  must *mem_99 ;

  {
  __CrestCall(32560, 270);
# 2370 "dfa.c"
  result = empty_string;
  __CrestLoad(32561, (unsigned long )0, (long long )0);
  __CrestStore(32562, (unsigned long )(& exact));
# 2371 "dfa.c"
  exact = 0;
  __CrestLoad(32567, (unsigned long )(& dfa___0->tindex), (long long )dfa___0->tindex);
  __CrestLoad(32566, (unsigned long )0, (long long )1);
  __CrestApply2(32565, 0, (long long )(dfa___0->tindex + 1));
  __CrestLoad(32564, (unsigned long )0, (long long )sizeof(*musts));
  __CrestApply2(32563, 2, (long long )((unsigned long )(dfa___0->tindex + 1) * sizeof(*musts)));
# 2372 "dfa.c"
  tmp = malloc((unsigned long )(dfa___0->tindex + 1) * sizeof(*musts));
  __CrestClearStack(32568);
# 2372 "dfa.c"
  musts = (must *)tmp;
  {
  __CrestLoad(32571, (unsigned long )(& musts), (long long )((unsigned long )musts));
  __CrestLoad(32570, (unsigned long )0, (long long )((unsigned long )((void *)0)));
  __CrestApply2(32569, 12, (long long )((unsigned long )musts == (unsigned long )((void *)0)));
# 2373 "dfa.c"
  if ((unsigned long )musts == (unsigned long )((void *)0)) {
    __CrestBranch(32572, 6332, 1);
# 2374 "dfa.c"
    goto return_label;
  } else {
    __CrestBranch(32573, 6334, 0);

  }
  }
# 2375 "dfa.c"
  mp = musts;
  __CrestLoad(32574, (unsigned long )0, (long long )0);
  __CrestStore(32575, (unsigned long )(& i));
# 2376 "dfa.c"
  i = 0;
  {
# 2376 "dfa.c"
  while (1) {
    while_continue: ;
    {
    __CrestLoad(32578, (unsigned long )(& i), (long long )i);
    __CrestLoad(32577, (unsigned long )(& dfa___0->tindex), (long long )dfa___0->tindex);
    __CrestApply2(32576, 15, (long long )(i <= dfa___0->tindex));
# 2376 "dfa.c"
    if (i <= dfa___0->tindex) {
      __CrestBranch(32579, 6340, 1);

    } else {
      __CrestBranch(32580, 6341, 0);
# 2376 "dfa.c"
      goto while_break;
    }
    }
# 2377 "dfa.c"
    mem_42 = mp + i;
# 2377 "dfa.c"
    *mem_42 = must0;
    __CrestLoad(32583, (unsigned long )(& i), (long long )i);
    __CrestLoad(32582, (unsigned long )0, (long long )1);
    __CrestApply2(32581, 0, (long long )(i + 1));
    __CrestStore(32584, (unsigned long )(& i));
# 2376 "dfa.c"
    i ++;
  }
  while_break: ;
  }
  __CrestLoad(32585, (unsigned long )0, (long long )0);
  __CrestStore(32586, (unsigned long )(& i));
# 2378 "dfa.c"
  i = 0;
  {
# 2378 "dfa.c"
  while (1) {
    while_continue___0: ;
    {
    __CrestLoad(32589, (unsigned long )(& i), (long long )i);
    __CrestLoad(32588, (unsigned long )(& dfa___0->tindex), (long long )dfa___0->tindex);
    __CrestApply2(32587, 15, (long long )(i <= dfa___0->tindex));
# 2378 "dfa.c"
    if (i <= dfa___0->tindex) {
      __CrestBranch(32590, 6349, 1);

    } else {
      __CrestBranch(32591, 6350, 0);
# 2378 "dfa.c"
      goto while_break___0;
    }
    }
# 2380 "dfa.c"
    mem_43 = mp + i;
# 2380 "dfa.c"
    mem_44 = mem_43->in;
    __CrestLoad(32592, (unsigned long )0, (long long )sizeof(*mem_44));
# 2380 "dfa.c"
    tmp___0 = malloc(sizeof(*mem_44));
    __CrestClearStack(32593);
# 2380 "dfa.c"
    mem_45 = mp + i;
# 2380 "dfa.c"
    mem_45->in = (char **)tmp___0;
    __CrestLoad(32594, (unsigned long )0, (long long )((size_t )2));
# 2381 "dfa.c"
    tmp___1 = malloc((size_t )2);
    __CrestClearStack(32595);
# 2381 "dfa.c"
    mem_46 = mp + i;
# 2381 "dfa.c"
    mem_46->left = (char *)tmp___1;
    __CrestLoad(32596, (unsigned long )0, (long long )((size_t )2));
# 2382 "dfa.c"
    tmp___2 = malloc((size_t )2);
    __CrestClearStack(32597);
# 2382 "dfa.c"
    mem_47 = mp + i;
# 2382 "dfa.c"
    mem_47->right = (char *)tmp___2;
    __CrestLoad(32598, (unsigned long )0, (long long )((size_t )2));
# 2383 "dfa.c"
    tmp___3 = malloc((size_t )2);
    __CrestClearStack(32599);
# 2383 "dfa.c"
    mem_48 = mp + i;
# 2383 "dfa.c"
    mem_48->is = (char *)tmp___3;
    {
# 2384 "dfa.c"
    mem_49 = mp + i;
    {
    __CrestLoad(32602, (unsigned long )(& mem_49->in), (long long )((unsigned long )mem_49->in));
    __CrestLoad(32601, (unsigned long )0, (long long )((unsigned long )((void *)0)));
    __CrestApply2(32600, 12, (long long )((unsigned long )mem_49->in == (unsigned long )((void *)0)));
# 2384 "dfa.c"
    if ((unsigned long )mem_49->in == (unsigned long )((void *)0)) {
      __CrestBranch(32603, 6355, 1);
# 2386 "dfa.c"
      goto done;
    } else {
      __CrestBranch(32604, 6356, 0);
      {
# 2384 "dfa.c"
      mem_50 = mp + i;
      {
      __CrestLoad(32607, (unsigned long )(& mem_50->left), (long long )((unsigned long )mem_50->left));
      __CrestLoad(32606, (unsigned long )0, (long long )((unsigned long )((void *)0)));
      __CrestApply2(32605, 12, (long long )((unsigned long )mem_50->left == (unsigned long )((void *)0)));
# 2384 "dfa.c"
      if ((unsigned long )mem_50->left == (unsigned long )((void *)0)) {
        __CrestBranch(32608, 6359, 1);
# 2386 "dfa.c"
        goto done;
      } else {
        __CrestBranch(32609, 6360, 0);
        {
# 2384 "dfa.c"
        mem_51 = mp + i;
        {
        __CrestLoad(32612, (unsigned long )(& mem_51->right), (long long )((unsigned long )mem_51->right));
        __CrestLoad(32611, (unsigned long )0, (long long )((unsigned long )((void *)0)));
        __CrestApply2(32610, 12, (long long )((unsigned long )mem_51->right == (unsigned long )((void *)0)));
# 2384 "dfa.c"
        if ((unsigned long )mem_51->right == (unsigned long )((void *)0)) {
          __CrestBranch(32613, 6363, 1);
# 2386 "dfa.c"
          goto done;
        } else {
          __CrestBranch(32614, 6364, 0);
          {
# 2384 "dfa.c"
          mem_52 = mp + i;
          {
          __CrestLoad(32617, (unsigned long )(& mem_52->is), (long long )((unsigned long )mem_52->is));
          __CrestLoad(32616, (unsigned long )0, (long long )((unsigned long )((void *)0)));
          __CrestApply2(32615, 12, (long long )((unsigned long )mem_52->is == (unsigned long )((void *)0)));
# 2384 "dfa.c"
          if ((unsigned long )mem_52->is == (unsigned long )((void *)0)) {
            __CrestBranch(32618, 6367, 1);
# 2386 "dfa.c"
            goto done;
          } else {
            __CrestBranch(32619, 6368, 0);

          }
          }
          }
        }
        }
        }
      }
      }
      }
    }
    }
    }
    __CrestLoad(32620, (unsigned long )0, (long long )((char )'\000'));
    __CrestStore(32621, (unsigned long )(& tmp___5));
# 2387 "dfa.c"
    tmp___5 = (char )'\000';
# 2387 "dfa.c"
    mem_53 = mp + i;
# 2387 "dfa.c"
    mem_54 = mem_53->is + 0;
    __CrestLoad(32622, (unsigned long )(& tmp___5), (long long )tmp___5);
    __CrestStore(32623, (unsigned long )mem_54);
# 2387 "dfa.c"
    *mem_54 = tmp___5;
    __CrestLoad(32624, (unsigned long )(& tmp___5), (long long )tmp___5);
    __CrestStore(32625, (unsigned long )(& tmp___4));
# 2387 "dfa.c"
    tmp___4 = tmp___5;
# 2387 "dfa.c"
    mem_55 = mp + i;
# 2387 "dfa.c"
    mem_56 = mem_55->right + 0;
    __CrestLoad(32626, (unsigned long )(& tmp___4), (long long )tmp___4);
    __CrestStore(32627, (unsigned long )mem_56);
# 2387 "dfa.c"
    *mem_56 = tmp___4;
# 2387 "dfa.c"
    mem_57 = mp + i;
# 2387 "dfa.c"
    mem_58 = mem_57->left + 0;
    __CrestLoad(32628, (unsigned long )(& tmp___4), (long long )tmp___4);
    __CrestStore(32629, (unsigned long )mem_58);
# 2387 "dfa.c"
    *mem_58 = tmp___4;
# 2388 "dfa.c"
    mem_59 = mp + i;
# 2388 "dfa.c"
    mem_60 = mem_59->in + 0;
# 2388 "dfa.c"
    *mem_60 = (char *)((void *)0);
    __CrestLoad(32632, (unsigned long )(& i), (long long )i);
    __CrestLoad(32631, (unsigned long )0, (long long )1);
    __CrestApply2(32630, 0, (long long )(i + 1));
    __CrestStore(32633, (unsigned long )(& i));
# 2378 "dfa.c"
    i ++;
  }
  while_break___0: ;
  }
  __CrestLoad(32634, (unsigned long )0, (long long )0);
  __CrestStore(32635, (unsigned long )(& ri));
# 2399 "dfa.c"
  ri = 0;
  {
# 2399 "dfa.c"
  while (1) {
    while_continue___1: ;
    {
    __CrestLoad(32638, (unsigned long )(& ri), (long long )ri);
    __CrestLoad(32637, (unsigned long )(& dfa___0->tindex), (long long )dfa___0->tindex);
    __CrestApply2(32636, 16, (long long )(ri < dfa___0->tindex));
# 2399 "dfa.c"
    if (ri < dfa___0->tindex) {
      __CrestBranch(32639, 6376, 1);

    } else {
      __CrestBranch(32640, 6377, 0);
# 2399 "dfa.c"
      goto while_break___1;
    }
    }
# 2401 "dfa.c"
    mem_61 = dfa___0->tokens + ri;
    __CrestLoad(32641, (unsigned long )mem_61, (long long )*mem_61);
    __CrestStore(32642, (unsigned long )(& t));
# 2401 "dfa.c"
    t = *mem_61;
    {
    {
    __CrestLoad(32645, (unsigned long )(& t), (long long )t);
    __CrestLoad(32644, (unsigned long )0, (long long )272);
    __CrestApply2(32643, 12, (long long )((int )t == 272));
# 2404 "dfa.c"
    if ((int )t == 272) {
      __CrestBranch(32646, 6381, 1);
# 2404 "dfa.c"
      goto case_272;
    } else {
      __CrestBranch(32647, 6382, 0);

    }
    }
    {
    __CrestLoad(32650, (unsigned long )(& t), (long long )t);
    __CrestLoad(32649, (unsigned long )0, (long long )271);
    __CrestApply2(32648, 12, (long long )((int )t == 271));
# 2404 "dfa.c"
    if ((int )t == 271) {
      __CrestBranch(32651, 6384, 1);
# 2404 "dfa.c"
      goto case_272;
    } else {
      __CrestBranch(32652, 6385, 0);

    }
    }
    {
    __CrestLoad(32655, (unsigned long )(& t), (long long )t);
    __CrestLoad(32654, (unsigned long )0, (long long )257);
    __CrestApply2(32653, 12, (long long )((int )t == 257));
# 2413 "dfa.c"
    if ((int )t == 257) {
      __CrestBranch(32656, 6387, 1);
# 2413 "dfa.c"
      goto case_257;
    } else {
      __CrestBranch(32657, 6388, 0);

    }
    }
    {
    __CrestLoad(32660, (unsigned long )(& t), (long long )t);
    __CrestLoad(32659, (unsigned long )0, (long long )263);
    __CrestApply2(32658, 12, (long long )((int )t == 263));
# 2413 "dfa.c"
    if ((int )t == 263) {
      __CrestBranch(32661, 6390, 1);
# 2413 "dfa.c"
      goto case_257;
    } else {
      __CrestBranch(32662, 6391, 0);

    }
    }
    {
    __CrestLoad(32665, (unsigned long )(& t), (long long )t);
    __CrestLoad(32664, (unsigned long )0, (long long )262);
    __CrestApply2(32663, 12, (long long )((int )t == 262));
# 2413 "dfa.c"
    if ((int )t == 262) {
      __CrestBranch(32666, 6393, 1);
# 2413 "dfa.c"
      goto case_257;
    } else {
      __CrestBranch(32667, 6394, 0);

    }
    }
    {
    __CrestLoad(32670, (unsigned long )(& t), (long long )t);
    __CrestLoad(32669, (unsigned long )0, (long long )261);
    __CrestApply2(32668, 12, (long long )((int )t == 261));
# 2413 "dfa.c"
    if ((int )t == 261) {
      __CrestBranch(32671, 6396, 1);
# 2413 "dfa.c"
      goto case_257;
    } else {
      __CrestBranch(32672, 6397, 0);

    }
    }
    {
    __CrestLoad(32675, (unsigned long )(& t), (long long )t);
    __CrestLoad(32674, (unsigned long )0, (long long )260);
    __CrestApply2(32673, 12, (long long )((int )t == 260));
# 2413 "dfa.c"
    if ((int )t == 260) {
      __CrestBranch(32676, 6399, 1);
# 2413 "dfa.c"
      goto case_257;
    } else {
      __CrestBranch(32677, 6400, 0);

    }
    }
    {
    __CrestLoad(32680, (unsigned long )(& t), (long long )t);
    __CrestLoad(32679, (unsigned long )0, (long long )259);
    __CrestApply2(32678, 12, (long long )((int )t == 259));
# 2413 "dfa.c"
    if ((int )t == 259) {
      __CrestBranch(32681, 6402, 1);
# 2413 "dfa.c"
      goto case_257;
    } else {
      __CrestBranch(32682, 6403, 0);

    }
    }
    {
    __CrestLoad(32685, (unsigned long )(& t), (long long )t);
    __CrestLoad(32684, (unsigned long )0, (long long )258);
    __CrestApply2(32683, 12, (long long )((int )t == 258));
# 2413 "dfa.c"
    if ((int )t == 258) {
      __CrestBranch(32686, 6405, 1);
# 2413 "dfa.c"
      goto case_257;
    } else {
      __CrestBranch(32687, 6406, 0);

    }
    }
    {
    __CrestLoad(32690, (unsigned long )(& t), (long long )t);
    __CrestLoad(32689, (unsigned long )0, (long long )256);
    __CrestApply2(32688, 12, (long long )((int )t == 256));
# 2413 "dfa.c"
    if ((int )t == 256) {
      __CrestBranch(32691, 6408, 1);
# 2413 "dfa.c"
      goto case_257;
    } else {
      __CrestBranch(32692, 6409, 0);

    }
    }
    {
    __CrestLoad(32695, (unsigned long )(& t), (long long )t);
    __CrestLoad(32694, (unsigned long )0, (long long )264);
    __CrestApply2(32693, 12, (long long )((int )t == 264));
# 2417 "dfa.c"
    if ((int )t == 264) {
      __CrestBranch(32696, 6411, 1);
# 2417 "dfa.c"
      goto case_264;
    } else {
      __CrestBranch(32697, 6412, 0);

    }
    }
    {
    __CrestLoad(32700, (unsigned long )(& t), (long long )t);
    __CrestLoad(32699, (unsigned long )0, (long long )265);
    __CrestApply2(32698, 12, (long long )((int )t == 265));
# 2417 "dfa.c"
    if ((int )t == 265) {
      __CrestBranch(32701, 6414, 1);
# 2417 "dfa.c"
      goto case_264;
    } else {
      __CrestBranch(32702, 6415, 0);

    }
    }
    {
    __CrestLoad(32705, (unsigned long )(& t), (long long )t);
    __CrestLoad(32704, (unsigned long )0, (long long )270);
    __CrestApply2(32703, 12, (long long )((int )t == 270));
# 2424 "dfa.c"
    if ((int )t == 270) {
      __CrestBranch(32706, 6417, 1);
# 2424 "dfa.c"
      goto case_270;
    } else {
      __CrestBranch(32707, 6418, 0);

    }
    }
    {
    __CrestLoad(32710, (unsigned long )(& t), (long long )t);
    __CrestLoad(32709, (unsigned long )0, (long long )269);
    __CrestApply2(32708, 12, (long long )((int )t == 269));
# 2424 "dfa.c"
    if ((int )t == 269) {
      __CrestBranch(32711, 6420, 1);
# 2424 "dfa.c"
      goto case_270;
    } else {
      __CrestBranch(32712, 6421, 0);

    }
    }
    {
    __CrestLoad(32715, (unsigned long )(& t), (long long )t);
    __CrestLoad(32714, (unsigned long )0, (long long )266);
    __CrestApply2(32713, 12, (long long )((int )t == 266));
# 2463 "dfa.c"
    if ((int )t == 266) {
      __CrestBranch(32716, 6423, 1);
# 2463 "dfa.c"
      goto case_266;
    } else {
      __CrestBranch(32717, 6424, 0);

    }
    }
    {
    __CrestLoad(32720, (unsigned long )(& t), (long long )t);
    __CrestLoad(32719, (unsigned long )0, (long long )-1);
    __CrestApply2(32718, 12, (long long )((int )t == -1));
# 2469 "dfa.c"
    if ((int )t == -1) {
      __CrestBranch(32721, 6426, 1);
# 2469 "dfa.c"
      goto case_neg_1;
    } else {
      __CrestBranch(32722, 6427, 0);

    }
    }
    {
    __CrestLoad(32725, (unsigned long )(& t), (long long )t);
    __CrestLoad(32724, (unsigned long )0, (long long )268);
    __CrestApply2(32723, 12, (long long )((int )t == 268));
# 2478 "dfa.c"
    if ((int )t == 268) {
      __CrestBranch(32726, 6429, 1);
# 2478 "dfa.c"
      goto case_268;
    } else {
      __CrestBranch(32727, 6430, 0);

    }
    }
# 2535 "dfa.c"
    goto switch_default;
    case_272:
    case_271:
# 2405 "dfa.c"
    goto done;
    case_257:
    case_263:
    case_262:
    case_261:
    case_260:
    case_259:
    case_258:
    case_256:
# 2414 "dfa.c"
    resetmust(mp);
    __CrestClearStack(32728);
# 2415 "dfa.c"
    goto switch_break;
    case_264:
    case_265:
    {
    __CrestLoad(32731, (unsigned long )(& mp), (long long )((unsigned long )mp));
    __CrestLoad(32730, (unsigned long )(& musts), (long long )((unsigned long )musts));
    __CrestApply2(32729, 15, (long long )((unsigned long )mp <= (unsigned long )musts));
# 2418 "dfa.c"
    if ((unsigned long )mp <= (unsigned long )musts) {
      __CrestBranch(32732, 6436, 1);
# 2419 "dfa.c"
      goto done;
    } else {
      __CrestBranch(32733, 6437, 0);

    }
    }
# 2420 "dfa.c"
    mp --;
# 2421 "dfa.c"
    resetmust(mp);
    __CrestClearStack(32734);
# 2422 "dfa.c"
    goto switch_break;
    case_270:
    case_269:
    {
    __CrestLoad(32739, (unsigned long )(& mp), (long long )((unsigned long )mp));
    __CrestLoad(32738, (unsigned long )(& musts), (long long )((unsigned long )musts));
    __CrestLoad(32737, (unsigned long )0, (long long )2);
    __CrestApply2(32736, 18, (long long )((unsigned long )(musts + 2)));
    __CrestApply2(32735, 16, (long long )((unsigned long )mp < (unsigned long )(musts + 2)));
# 2425 "dfa.c"
    if ((unsigned long )mp < (unsigned long )(musts + 2)) {
      __CrestBranch(32740, 6441, 1);
# 2426 "dfa.c"
      goto done;
    } else {
      __CrestBranch(32741, 6442, 0);

    }
    }
# 2433 "dfa.c"
    mp --;
# 2433 "dfa.c"
    rmp = mp;
# 2434 "dfa.c"
    mp --;
# 2434 "dfa.c"
    lmp = mp;
# 2436 "dfa.c"
    tmp___6 = strcmp((char const *)lmp->is, (char const *)rmp->is);
    __CrestHandleReturn(32743, (long long )tmp___6);
    __CrestStore(32742, (unsigned long )(& tmp___6));
    {
    __CrestLoad(32746, (unsigned long )(& tmp___6), (long long )tmp___6);
    __CrestLoad(32745, (unsigned long )0, (long long )0);
    __CrestApply2(32744, 13, (long long )(tmp___6 != 0));
# 2436 "dfa.c"
    if (tmp___6 != 0) {
      __CrestBranch(32747, 6445, 1);
# 2437 "dfa.c"
      mem_62 = lmp->is + 0;
      __CrestLoad(32749, (unsigned long )0, (long long )((char )'\000'));
      __CrestStore(32750, (unsigned long )mem_62);
# 2437 "dfa.c"
      *mem_62 = (char )'\000';
    } else {
      __CrestBranch(32748, 6446, 0);

    }
    }
    __CrestLoad(32751, (unsigned long )0, (long long )0);
    __CrestStore(32752, (unsigned long )(& i));
# 2439 "dfa.c"
    i = 0;
    {
# 2440 "dfa.c"
    while (1) {
      while_continue___2: ;
      {
# 2440 "dfa.c"
      mem_63 = lmp->left + i;
      {
      __CrestLoad(32755, (unsigned long )mem_63, (long long )*mem_63);
      __CrestLoad(32754, (unsigned long )0, (long long )0);
      __CrestApply2(32753, 13, (long long )((int )*mem_63 != 0));
# 2440 "dfa.c"
      if ((int )*mem_63 != 0) {
        __CrestBranch(32756, 6454, 1);
        {
# 2440 "dfa.c"
        mem_64 = lmp->left + i;
# 2440 "dfa.c"
        mem_65 = rmp->left + i;
        {
        __CrestLoad(32760, (unsigned long )mem_64, (long long )*mem_64);
        __CrestLoad(32759, (unsigned long )mem_65, (long long )*mem_65);
        __CrestApply2(32758, 12, (long long )((int )*mem_64 == (int )*mem_65));
# 2440 "dfa.c"
        if ((int )*mem_64 == (int )*mem_65) {
          __CrestBranch(32761, 6457, 1);

        } else {
          __CrestBranch(32762, 6458, 0);
# 2440 "dfa.c"
          goto while_break___2;
        }
        }
        }
      } else {
        __CrestBranch(32757, 6459, 0);
# 2440 "dfa.c"
        goto while_break___2;
      }
      }
      }
      __CrestLoad(32765, (unsigned long )(& i), (long long )i);
      __CrestLoad(32764, (unsigned long )0, (long long )1);
      __CrestApply2(32763, 0, (long long )(i + 1));
      __CrestStore(32766, (unsigned long )(& i));
# 2441 "dfa.c"
      i ++;
    }
    while_break___2: ;
    }
# 2442 "dfa.c"
    mem_66 = lmp->left + i;
    __CrestLoad(32767, (unsigned long )0, (long long )((char )'\000'));
    __CrestStore(32768, (unsigned long )mem_66);
# 2442 "dfa.c"
    *mem_66 = (char )'\000';
# 2444 "dfa.c"
    tmp___7 = strlen((char const *)lmp->right);
    __CrestHandleReturn(32770, (long long )tmp___7);
    __CrestStore(32769, (unsigned long )(& tmp___7));
    __CrestLoad(32771, (unsigned long )(& tmp___7), (long long )tmp___7);
    __CrestStore(32772, (unsigned long )(& ln));
# 2444 "dfa.c"
    ln = (int )tmp___7;
# 2445 "dfa.c"
    tmp___8 = strlen((char const *)rmp->right);
    __CrestHandleReturn(32774, (long long )tmp___8);
    __CrestStore(32773, (unsigned long )(& tmp___8));
    __CrestLoad(32775, (unsigned long )(& tmp___8), (long long )tmp___8);
    __CrestStore(32776, (unsigned long )(& rn));
# 2445 "dfa.c"
    rn = (int )tmp___8;
    __CrestLoad(32777, (unsigned long )(& ln), (long long )ln);
    __CrestStore(32778, (unsigned long )(& n));
# 2446 "dfa.c"
    n = ln;
    {
    __CrestLoad(32781, (unsigned long )(& n), (long long )n);
    __CrestLoad(32780, (unsigned long )(& rn), (long long )rn);
    __CrestApply2(32779, 14, (long long )(n > rn));
# 2447 "dfa.c"
    if (n > rn) {
      __CrestBranch(32782, 6464, 1);
      __CrestLoad(32784, (unsigned long )(& rn), (long long )rn);
      __CrestStore(32785, (unsigned long )(& n));
# 2448 "dfa.c"
      n = rn;
    } else {
      __CrestBranch(32783, 6465, 0);

    }
    }
    __CrestLoad(32786, (unsigned long )0, (long long )0);
    __CrestStore(32787, (unsigned long )(& i));
# 2449 "dfa.c"
    i = 0;
    {
# 2449 "dfa.c"
    while (1) {
      while_continue___3: ;
      {
      __CrestLoad(32790, (unsigned long )(& i), (long long )i);
      __CrestLoad(32789, (unsigned long )(& n), (long long )n);
      __CrestApply2(32788, 16, (long long )(i < n));
# 2449 "dfa.c"
      if (i < n) {
        __CrestBranch(32791, 6471, 1);

      } else {
        __CrestBranch(32792, 6472, 0);
# 2449 "dfa.c"
        goto while_break___3;
      }
      }
      {
# 2450 "dfa.c"
      mem_67 = lmp->right + ((ln - i) - 1);
# 2450 "dfa.c"
      mem_68 = rmp->right + ((rn - i) - 1);
      {
      __CrestLoad(32795, (unsigned long )mem_67, (long long )*mem_67);
      __CrestLoad(32794, (unsigned long )mem_68, (long long )*mem_68);
      __CrestApply2(32793, 13, (long long )((int )*mem_67 != (int )*mem_68));
# 2450 "dfa.c"
      if ((int )*mem_67 != (int )*mem_68) {
        __CrestBranch(32796, 6476, 1);
# 2451 "dfa.c"
        goto while_break___3;
      } else {
        __CrestBranch(32797, 6477, 0);

      }
      }
      }
      __CrestLoad(32800, (unsigned long )(& i), (long long )i);
      __CrestLoad(32799, (unsigned long )0, (long long )1);
      __CrestApply2(32798, 0, (long long )(i + 1));
      __CrestStore(32801, (unsigned long )(& i));
# 2449 "dfa.c"
      i ++;
    }
    while_break___3: ;
    }
    __CrestLoad(32802, (unsigned long )0, (long long )0);
    __CrestStore(32803, (unsigned long )(& j));
# 2452 "dfa.c"
    j = 0;
    {
# 2452 "dfa.c"
    while (1) {
      while_continue___4: ;
      {
      __CrestLoad(32806, (unsigned long )(& j), (long long )j);
      __CrestLoad(32805, (unsigned long )(& i), (long long )i);
      __CrestApply2(32804, 16, (long long )(j < i));
# 2452 "dfa.c"
      if (j < i) {
        __CrestBranch(32807, 6485, 1);

      } else {
        __CrestBranch(32808, 6486, 0);
# 2452 "dfa.c"
        goto while_break___4;
      }
      }
# 2453 "dfa.c"
      mem_69 = lmp->right + j;
# 2453 "dfa.c"
      mem_70 = lmp->right + ((ln - i) + j);
      __CrestLoad(32809, (unsigned long )mem_70, (long long )*mem_70);
      __CrestStore(32810, (unsigned long )mem_69);
# 2453 "dfa.c"
      *mem_69 = *mem_70;
      __CrestLoad(32813, (unsigned long )(& j), (long long )j);
      __CrestLoad(32812, (unsigned long )0, (long long )1);
      __CrestApply2(32811, 0, (long long )(j + 1));
      __CrestStore(32814, (unsigned long )(& j));
# 2452 "dfa.c"
      j ++;
    }
    while_break___4: ;
    }
# 2454 "dfa.c"
    mem_71 = lmp->right + j;
    __CrestLoad(32815, (unsigned long )0, (long long )((char )'\000'));
    __CrestStore(32816, (unsigned long )mem_71);
# 2454 "dfa.c"
    *mem_71 = (char )'\000';
# 2455 "dfa.c"
    new = inboth(lmp->in, rmp->in);
    __CrestClearStack(32817);
    {
    __CrestLoad(32820, (unsigned long )(& new), (long long )((unsigned long )new));
    __CrestLoad(32819, (unsigned long )0, (long long )((unsigned long )((void *)0)));
    __CrestApply2(32818, 12, (long long )((unsigned long )new == (unsigned long )((void *)0)));
# 2456 "dfa.c"
    if ((unsigned long )new == (unsigned long )((void *)0)) {
      __CrestBranch(32821, 6491, 1);
# 2457 "dfa.c"
      goto done;
    } else {
      __CrestBranch(32822, 6492, 0);

    }
    }
# 2458 "dfa.c"
    freelist(lmp->in);
    __CrestClearStack(32823);
# 2459 "dfa.c"
    free((void *)((char *)lmp->in));
    __CrestClearStack(32824);
# 2460 "dfa.c"
    lmp->in = new;
# 2462 "dfa.c"
    goto switch_break;
    case_266:
    {
    __CrestLoad(32827, (unsigned long )(& mp), (long long )((unsigned long )mp));
    __CrestLoad(32826, (unsigned long )(& musts), (long long )((unsigned long )musts));
    __CrestApply2(32825, 15, (long long )((unsigned long )mp <= (unsigned long )musts));
# 2464 "dfa.c"
    if ((unsigned long )mp <= (unsigned long )musts) {
      __CrestBranch(32828, 6496, 1);
# 2465 "dfa.c"
      goto done;
    } else {
      __CrestBranch(32829, 6497, 0);

    }
    }
# 2466 "dfa.c"
    mp --;
# 2467 "dfa.c"
    mem_72 = mp->is + 0;
    __CrestLoad(32830, (unsigned long )0, (long long )((char )'\000'));
    __CrestStore(32831, (unsigned long )mem_72);
# 2467 "dfa.c"
    *mem_72 = (char )'\000';
# 2468 "dfa.c"
    goto switch_break;
    case_neg_1:
    {
    __CrestLoad(32836, (unsigned long )(& mp), (long long )((unsigned long )mp));
    __CrestLoad(32835, (unsigned long )(& musts), (long long )((unsigned long )musts));
    __CrestLoad(32834, (unsigned long )0, (long long )1);
    __CrestApply2(32833, 18, (long long )((unsigned long )(musts + 1)));
    __CrestApply2(32832, 13, (long long )((unsigned long )mp != (unsigned long )(musts + 1)));
# 2470 "dfa.c"
    if ((unsigned long )mp != (unsigned long )(musts + 1)) {
      __CrestBranch(32837, 6501, 1);
# 2471 "dfa.c"
      goto done;
    } else {
      __CrestBranch(32838, 6502, 0);

    }
    }
    __CrestLoad(32839, (unsigned long )0, (long long )0);
    __CrestStore(32840, (unsigned long )(& i));
# 2472 "dfa.c"
    i = 0;
    {
# 2472 "dfa.c"
    while (1) {
      while_continue___5: ;
      {
# 2472 "dfa.c"
      mem_73 = musts + 0;
# 2472 "dfa.c"
      mem_74 = mem_73->in + i;
      {
      __CrestLoad(32843, (unsigned long )mem_74, (long long )((unsigned long )*mem_74));
      __CrestLoad(32842, (unsigned long )0, (long long )((unsigned long )((void *)0)));
      __CrestApply2(32841, 13, (long long )((unsigned long )*mem_74 != (unsigned long )((void *)0)));
# 2472 "dfa.c"
      if ((unsigned long )*mem_74 != (unsigned long )((void *)0)) {
        __CrestBranch(32844, 6510, 1);

      } else {
        __CrestBranch(32845, 6511, 0);
# 2472 "dfa.c"
        goto while_break___5;
      }
      }
      }
# 2473 "dfa.c"
      mem_75 = musts + 0;
# 2473 "dfa.c"
      mem_76 = mem_75->in + i;
# 2473 "dfa.c"
      tmp___9 = strlen((char const *)*mem_76);
      __CrestHandleReturn(32847, (long long )tmp___9);
      __CrestStore(32846, (unsigned long )(& tmp___9));
# 2473 "dfa.c"
      tmp___10 = strlen((char const *)result);
      __CrestHandleReturn(32849, (long long )tmp___10);
      __CrestStore(32848, (unsigned long )(& tmp___10));
      {
      __CrestLoad(32852, (unsigned long )(& tmp___9), (long long )tmp___9);
      __CrestLoad(32851, (unsigned long )(& tmp___10), (long long )tmp___10);
      __CrestApply2(32850, 14, (long long )(tmp___9 > tmp___10));
# 2473 "dfa.c"
      if (tmp___9 > tmp___10) {
        __CrestBranch(32853, 6514, 1);
# 2474 "dfa.c"
        mem_77 = musts + 0;
# 2474 "dfa.c"
        mem_78 = mem_77->in + i;
# 2474 "dfa.c"
        result = *mem_78;
      } else {
        __CrestBranch(32854, 6515, 0);

      }
      }
      __CrestLoad(32857, (unsigned long )(& i), (long long )i);
      __CrestLoad(32856, (unsigned long )0, (long long )1);
      __CrestApply2(32855, 0, (long long )(i + 1));
      __CrestStore(32858, (unsigned long )(& i));
# 2472 "dfa.c"
      i ++;
    }
    while_break___5: ;
    }
# 2475 "dfa.c"
    mem_79 = musts + 0;
# 2475 "dfa.c"
    tmp___11 = strcmp((char const *)result, (char const *)mem_79->is);
    __CrestHandleReturn(32860, (long long )tmp___11);
    __CrestStore(32859, (unsigned long )(& tmp___11));
    {
    __CrestLoad(32863, (unsigned long )(& tmp___11), (long long )tmp___11);
    __CrestLoad(32862, (unsigned long )0, (long long )0);
    __CrestApply2(32861, 12, (long long )(tmp___11 == 0));
# 2475 "dfa.c"
    if (tmp___11 == 0) {
      __CrestBranch(32864, 6520, 1);
      __CrestLoad(32866, (unsigned long )0, (long long )1);
      __CrestStore(32867, (unsigned long )(& exact));
# 2476 "dfa.c"
      exact = 1;
    } else {
      __CrestBranch(32865, 6521, 0);

    }
    }
# 2477 "dfa.c"
    goto done;
    case_268:
    {
    __CrestLoad(32872, (unsigned long )(& mp), (long long )((unsigned long )mp));
    __CrestLoad(32871, (unsigned long )(& musts), (long long )((unsigned long )musts));
    __CrestLoad(32870, (unsigned long )0, (long long )2);
    __CrestApply2(32869, 18, (long long )((unsigned long )(musts + 2)));
    __CrestApply2(32868, 16, (long long )((unsigned long )mp < (unsigned long )(musts + 2)));
# 2479 "dfa.c"
    if ((unsigned long )mp < (unsigned long )(musts + 2)) {
      __CrestBranch(32873, 6524, 1);
# 2480 "dfa.c"
      goto done;
    } else {
      __CrestBranch(32874, 6525, 0);

    }
    }
# 2485 "dfa.c"
    mp --;
# 2485 "dfa.c"
    rmp___0 = mp;
# 2486 "dfa.c"
    mp --;
# 2486 "dfa.c"
    lmp___0 = mp;
# 2490 "dfa.c"
    lmp___0->in = addlists(lmp___0->in, rmp___0->in);
    __CrestClearStack(32875);
    {
    __CrestLoad(32878, (unsigned long )(& lmp___0->in), (long long )((unsigned long )lmp___0->in));
    __CrestLoad(32877, (unsigned long )0, (long long )((unsigned long )((void *)0)));
    __CrestApply2(32876, 12, (long long )((unsigned long )lmp___0->in == (unsigned long )((void *)0)));
# 2491 "dfa.c"
    if ((unsigned long )lmp___0->in == (unsigned long )((void *)0)) {
      __CrestBranch(32879, 6528, 1);
# 2492 "dfa.c"
      goto done;
    } else {
      __CrestBranch(32880, 6529, 0);

    }
    }
    {
# 2493 "dfa.c"
    mem_80 = lmp___0->right + 0;
    {
    __CrestLoad(32883, (unsigned long )mem_80, (long long )*mem_80);
    __CrestLoad(32882, (unsigned long )0, (long long )0);
    __CrestApply2(32881, 13, (long long )((int )*mem_80 != 0));
# 2493 "dfa.c"
    if ((int )*mem_80 != 0) {
      __CrestBranch(32884, 6533, 1);
      {
# 2493 "dfa.c"
      mem_81 = rmp___0->left + 0;
      {
      __CrestLoad(32888, (unsigned long )mem_81, (long long )*mem_81);
      __CrestLoad(32887, (unsigned long )0, (long long )0);
      __CrestApply2(32886, 13, (long long )((int )*mem_81 != 0));
# 2493 "dfa.c"
      if ((int )*mem_81 != 0) {
        __CrestBranch(32889, 6536, 1);
# 2498 "dfa.c"
        tp = icpyalloc(lmp___0->right);
        __CrestClearStack(32891);
        {
        __CrestLoad(32894, (unsigned long )(& tp), (long long )((unsigned long )tp));
        __CrestLoad(32893, (unsigned long )0, (long long )((unsigned long )((void *)0)));
        __CrestApply2(32892, 12, (long long )((unsigned long )tp == (unsigned long )((void *)0)));
# 2499 "dfa.c"
        if ((unsigned long )tp == (unsigned long )((void *)0)) {
          __CrestBranch(32895, 6538, 1);
# 2500 "dfa.c"
          goto done;
        } else {
          __CrestBranch(32896, 6539, 0);

        }
        }
# 2501 "dfa.c"
        tp = icatalloc(tp, rmp___0->left);
        __CrestClearStack(32897);
        {
        __CrestLoad(32900, (unsigned long )(& tp), (long long )((unsigned long )tp));
        __CrestLoad(32899, (unsigned long )0, (long long )((unsigned long )((void *)0)));
        __CrestApply2(32898, 12, (long long )((unsigned long )tp == (unsigned long )((void *)0)));
# 2502 "dfa.c"
        if ((unsigned long )tp == (unsigned long )((void *)0)) {
          __CrestBranch(32901, 6542, 1);
# 2503 "dfa.c"
          goto done;
        } else {
          __CrestBranch(32902, 6543, 0);

        }
        }
# 2504 "dfa.c"
        tmp___12 = strlen((char const *)tp);
        __CrestHandleReturn(32904, (long long )tmp___12);
        __CrestStore(32903, (unsigned long )(& tmp___12));
        __CrestLoad(32905, (unsigned long )(& tmp___12), (long long )tmp___12);
# 2504 "dfa.c"
        lmp___0->in = enlist(lmp___0->in, tp, tmp___12);
        __CrestClearStack(32906);
# 2506 "dfa.c"
        free((void *)tp);
        __CrestClearStack(32907);
        {
        __CrestLoad(32910, (unsigned long )(& lmp___0->in), (long long )((unsigned long )lmp___0->in));
        __CrestLoad(32909, (unsigned long )0, (long long )((unsigned long )((void *)0)));
        __CrestApply2(32908, 12, (long long )((unsigned long )lmp___0->in == (unsigned long )((void *)0)));
# 2507 "dfa.c"
        if ((unsigned long )lmp___0->in == (unsigned long )((void *)0)) {
          __CrestBranch(32911, 6546, 1);
# 2508 "dfa.c"
          goto done;
        } else {
          __CrestBranch(32912, 6547, 0);

        }
        }
      } else {
        __CrestBranch(32890, 6548, 0);

      }
      }
      }
    } else {
      __CrestBranch(32885, 6549, 0);

    }
    }
    }
    {
# 2511 "dfa.c"
    mem_82 = lmp___0->is + 0;
    {
    __CrestLoad(32915, (unsigned long )mem_82, (long long )*mem_82);
    __CrestLoad(32914, (unsigned long )0, (long long )0);
    __CrestApply2(32913, 13, (long long )((int )*mem_82 != 0));
# 2511 "dfa.c"
    if ((int )*mem_82 != 0) {
      __CrestBranch(32916, 6553, 1);
# 2513 "dfa.c"
      lmp___0->left = icatalloc(lmp___0->left, rmp___0->left);
      __CrestClearStack(32918);
      {
      __CrestLoad(32921, (unsigned long )(& lmp___0->left), (long long )((unsigned long )lmp___0->left));
      __CrestLoad(32920, (unsigned long )0, (long long )((unsigned long )((void *)0)));
      __CrestApply2(32919, 12, (long long )((unsigned long )lmp___0->left == (unsigned long )((void *)0)));
# 2515 "dfa.c"
      if ((unsigned long )lmp___0->left == (unsigned long )((void *)0)) {
        __CrestBranch(32922, 6555, 1);
# 2516 "dfa.c"
        goto done;
      } else {
        __CrestBranch(32923, 6556, 0);

      }
      }
    } else {
      __CrestBranch(32917, 6557, 0);

    }
    }
    }
    {
# 2519 "dfa.c"
    mem_83 = rmp___0->is + 0;
    {
    __CrestLoad(32926, (unsigned long )mem_83, (long long )*mem_83);
    __CrestLoad(32925, (unsigned long )0, (long long )0);
    __CrestApply2(32924, 12, (long long )((int )*mem_83 == 0));
# 2519 "dfa.c"
    if ((int )*mem_83 == 0) {
      __CrestBranch(32927, 6561, 1);
# 2520 "dfa.c"
      mem_84 = lmp___0->right + 0;
      __CrestLoad(32929, (unsigned long )0, (long long )((char )'\000'));
      __CrestStore(32930, (unsigned long )mem_84);
# 2520 "dfa.c"
      *mem_84 = (char )'\000';
    } else {
      __CrestBranch(32928, 6562, 0);

    }
    }
    }
# 2521 "dfa.c"
    lmp___0->right = icatalloc(lmp___0->right, rmp___0->right);
    __CrestClearStack(32931);
    {
    __CrestLoad(32934, (unsigned long )(& lmp___0->right), (long long )((unsigned long )lmp___0->right));
    __CrestLoad(32933, (unsigned long )0, (long long )((unsigned long )((void *)0)));
    __CrestApply2(32932, 12, (long long )((unsigned long )lmp___0->right == (unsigned long )((void *)0)));
# 2522 "dfa.c"
    if ((unsigned long )lmp___0->right == (unsigned long )((void *)0)) {
      __CrestBranch(32935, 6565, 1);
# 2523 "dfa.c"
      goto done;
    } else {
      __CrestBranch(32936, 6566, 0);

    }
    }
    {
# 2525 "dfa.c"
    mem_85 = lmp___0->is + 0;
    {
    __CrestLoad(32939, (unsigned long )mem_85, (long long )*mem_85);
    __CrestLoad(32938, (unsigned long )0, (long long )0);
    __CrestApply2(32937, 13, (long long )((int )*mem_85 != 0));
# 2525 "dfa.c"
    if ((int )*mem_85 != 0) {
      __CrestBranch(32940, 6570, 1);
      {
# 2525 "dfa.c"
      mem_86 = rmp___0->is + 0;
      {
      __CrestLoad(32944, (unsigned long )mem_86, (long long )*mem_86);
      __CrestLoad(32943, (unsigned long )0, (long long )0);
      __CrestApply2(32942, 13, (long long )((int )*mem_86 != 0));
# 2525 "dfa.c"
      if ((int )*mem_86 != 0) {
        __CrestBranch(32945, 6573, 1);
# 2527 "dfa.c"
        lmp___0->is = icatalloc(lmp___0->is, rmp___0->is);
        __CrestClearStack(32947);
        {
        __CrestLoad(32950, (unsigned long )(& lmp___0->is), (long long )((unsigned long )lmp___0->is));
        __CrestLoad(32949, (unsigned long )0, (long long )((unsigned long )((void *)0)));
        __CrestApply2(32948, 12, (long long )((unsigned long )lmp___0->is == (unsigned long )((void *)0)));
# 2528 "dfa.c"
        if ((unsigned long )lmp___0->is == (unsigned long )((void *)0)) {
          __CrestBranch(32951, 6575, 1);
# 2529 "dfa.c"
          goto done;
        } else {
          __CrestBranch(32952, 6576, 0);

        }
        }
      } else {
        __CrestBranch(32946, 6577, 0);
# 2532 "dfa.c"
        mem_87 = lmp___0->is + 0;
        __CrestLoad(32953, (unsigned long )0, (long long )((char )'\000'));
        __CrestStore(32954, (unsigned long )mem_87);
# 2532 "dfa.c"
        *mem_87 = (char )'\000';
      }
      }
      }
    } else {
      __CrestBranch(32941, 6578, 0);
# 2532 "dfa.c"
      mem_88 = lmp___0->is + 0;
      __CrestLoad(32955, (unsigned long )0, (long long )((char )'\000'));
      __CrestStore(32956, (unsigned long )mem_88);
# 2532 "dfa.c"
      *mem_88 = (char )'\000';
    }
    }
    }
# 2534 "dfa.c"
    goto switch_break;
    switch_default:
    {
    __CrestLoad(32959, (unsigned long )(& t), (long long )t);
    __CrestLoad(32958, (unsigned long )0, (long long )-1);
    __CrestApply2(32957, 16, (long long )((int )t < -1));
# 2536 "dfa.c"
    if ((int )t < -1) {
      __CrestBranch(32960, 6581, 1);
# 2539 "dfa.c"
      goto done;
    } else {
      __CrestBranch(32961, 6582, 0);
      {
      __CrestLoad(32964, (unsigned long )(& t), (long long )t);
      __CrestLoad(32963, (unsigned long )0, (long long )0);
      __CrestApply2(32962, 12, (long long )((int )t == 0));
# 2541 "dfa.c"
      if ((int )t == 0) {
        __CrestBranch(32965, 6583, 1);
# 2544 "dfa.c"
        goto done;
      } else {
        __CrestBranch(32966, 6584, 0);
        {
        __CrestLoad(32969, (unsigned long )(& t), (long long )t);
        __CrestLoad(32968, (unsigned long )0, (long long )273);
        __CrestApply2(32967, 17, (long long )((int )t >= 273));
# 2546 "dfa.c"
        if ((int )t >= 273) {
          __CrestBranch(32970, 6585, 1);
# 2549 "dfa.c"
          resetmust(mp);
          __CrestClearStack(32972);
        } else {
          __CrestBranch(32971, 6586, 0);
# 2554 "dfa.c"
          resetmust(mp);
          __CrestClearStack(32973);
          __CrestLoad(32974, (unsigned long )(& t), (long long )t);
          __CrestStore(32975, (unsigned long )(& tmp___14));
# 2555 "dfa.c"
          tmp___14 = (char )t;
# 2555 "dfa.c"
          mem_89 = mp->right + 0;
          __CrestLoad(32976, (unsigned long )(& tmp___14), (long long )tmp___14);
          __CrestStore(32977, (unsigned long )mem_89);
# 2555 "dfa.c"
          *mem_89 = tmp___14;
          __CrestLoad(32978, (unsigned long )(& tmp___14), (long long )tmp___14);
          __CrestStore(32979, (unsigned long )(& tmp___13));
# 2555 "dfa.c"
          tmp___13 = tmp___14;
# 2555 "dfa.c"
          mem_90 = mp->left + 0;
          __CrestLoad(32980, (unsigned long )(& tmp___13), (long long )tmp___13);
          __CrestStore(32981, (unsigned long )mem_90);
# 2555 "dfa.c"
          *mem_90 = tmp___13;
# 2555 "dfa.c"
          mem_91 = mp->is + 0;
          __CrestLoad(32982, (unsigned long )(& tmp___13), (long long )tmp___13);
          __CrestStore(32983, (unsigned long )mem_91);
# 2555 "dfa.c"
          *mem_91 = tmp___13;
          __CrestLoad(32984, (unsigned long )0, (long long )((char )'\000'));
          __CrestStore(32985, (unsigned long )(& tmp___16));
# 2556 "dfa.c"
          tmp___16 = (char )'\000';
# 2556 "dfa.c"
          mem_92 = mp->right + 1;
          __CrestLoad(32986, (unsigned long )(& tmp___16), (long long )tmp___16);
          __CrestStore(32987, (unsigned long )mem_92);
# 2556 "dfa.c"
          *mem_92 = tmp___16;
          __CrestLoad(32988, (unsigned long )(& tmp___16), (long long )tmp___16);
          __CrestStore(32989, (unsigned long )(& tmp___15));
# 2556 "dfa.c"
          tmp___15 = tmp___16;
# 2556 "dfa.c"
          mem_93 = mp->left + 1;
          __CrestLoad(32990, (unsigned long )(& tmp___15), (long long )tmp___15);
          __CrestStore(32991, (unsigned long )mem_93);
# 2556 "dfa.c"
          *mem_93 = tmp___15;
# 2556 "dfa.c"
          mem_94 = mp->is + 1;
          __CrestLoad(32992, (unsigned long )(& tmp___15), (long long )tmp___15);
          __CrestStore(32993, (unsigned long )mem_94);
# 2556 "dfa.c"
          *mem_94 = tmp___15;
          __CrestLoad(32994, (unsigned long )0, (long long )((size_t )1));
# 2557 "dfa.c"
          mp->in = enlist(mp->in, mp->is, (size_t )1);
          __CrestClearStack(32995);
          {
          __CrestLoad(32998, (unsigned long )(& mp->in), (long long )((unsigned long )mp->in));
          __CrestLoad(32997, (unsigned long )0, (long long )((unsigned long )((void *)0)));
          __CrestApply2(32996, 12, (long long )((unsigned long )mp->in == (unsigned long )((void *)0)));
# 2558 "dfa.c"
          if ((unsigned long )mp->in == (unsigned long )((void *)0)) {
            __CrestBranch(32999, 6588, 1);
# 2559 "dfa.c"
            goto done;
          } else {
            __CrestBranch(33000, 6589, 0);

          }
          }
        }
        }
      }
      }
    }
    }
# 2561 "dfa.c"
    goto switch_break;
    switch_break: ;
    }
# 2573 "dfa.c"
    mp ++;
    __CrestLoad(33003, (unsigned long )(& ri), (long long )ri);
    __CrestLoad(33002, (unsigned long )0, (long long )1);
    __CrestApply2(33001, 0, (long long )(ri + 1));
    __CrestStore(33004, (unsigned long )(& ri));
# 2399 "dfa.c"
    ri ++;
  }
  while_break___1: ;
  }
  done:
# 2576 "dfa.c"
  tmp___20 = strlen((char const *)result);
  __CrestHandleReturn(33006, (long long )tmp___20);
  __CrestStore(33005, (unsigned long )(& tmp___20));
  {
  __CrestLoad(33009, (unsigned long )(& tmp___20), (long long )tmp___20);
  __CrestLoad(33008, (unsigned long )0, (long long )0);
  __CrestApply2(33007, 13, (long long )(tmp___20 != 0));
# 2576 "dfa.c"
  if (tmp___20 != 0) {
    __CrestBranch(33010, 6596, 1);
    __CrestLoad(33012, (unsigned long )0, (long long )sizeof(struct dfamust ));
# 2578 "dfa.c"
    tmp___17 = malloc(sizeof(struct dfamust ));
    __CrestClearStack(33013);
# 2578 "dfa.c"
    dm = (struct dfamust *)tmp___17;
    __CrestLoad(33014, (unsigned long )(& exact), (long long )exact);
    __CrestStore(33015, (unsigned long )(& dm->exact));
# 2579 "dfa.c"
    dm->exact = exact;
# 2580 "dfa.c"
    tmp___18 = strlen((char const *)result);
    __CrestHandleReturn(33017, (long long )tmp___18);
    __CrestStore(33016, (unsigned long )(& tmp___18));
    __CrestLoad(33020, (unsigned long )(& tmp___18), (long long )tmp___18);
    __CrestLoad(33019, (unsigned long )0, (long long )1UL);
    __CrestApply2(33018, 0, (long long )(tmp___18 + 1UL));
# 2580 "dfa.c"
    tmp___19 = malloc(tmp___18 + 1UL);
    __CrestClearStack(33021);
# 2580 "dfa.c"
    dm->must = (char *)tmp___19;
# 2581 "dfa.c"
    strcpy((char * __restrict )dm->must, (char const * __restrict )result);
    __CrestClearStack(33022);
# 2582 "dfa.c"
    dm->next = dfa___0->musts;
# 2583 "dfa.c"
    dfa___0->musts = dm;
  } else {
    __CrestBranch(33011, 6597, 0);

  }
  }
# 2585 "dfa.c"
  mp = musts;
  __CrestLoad(33023, (unsigned long )0, (long long )0);
  __CrestStore(33024, (unsigned long )(& i));
# 2586 "dfa.c"
  i = 0;
  {
# 2586 "dfa.c"
  while (1) {
    while_continue___6: ;
    {
    __CrestLoad(33027, (unsigned long )(& i), (long long )i);
    __CrestLoad(33026, (unsigned long )(& dfa___0->tindex), (long long )dfa___0->tindex);
    __CrestApply2(33025, 15, (long long )(i <= dfa___0->tindex));
# 2586 "dfa.c"
    if (i <= dfa___0->tindex) {
      __CrestBranch(33028, 6603, 1);

    } else {
      __CrestBranch(33029, 6604, 0);
# 2586 "dfa.c"
      goto while_break___6;
    }
    }
# 2588 "dfa.c"
    mem_95 = mp + i;
# 2588 "dfa.c"
    freelist(mem_95->in);
    __CrestClearStack(33030);
# 2589 "dfa.c"
    mem_96 = mp + i;
# 2589 "dfa.c"
    ifree((char *)mem_96->in);
    __CrestClearStack(33031);
# 2590 "dfa.c"
    mem_97 = mp + i;
# 2590 "dfa.c"
    ifree(mem_97->left);
    __CrestClearStack(33032);
# 2591 "dfa.c"
    mem_98 = mp + i;
# 2591 "dfa.c"
    ifree(mem_98->right);
    __CrestClearStack(33033);
# 2592 "dfa.c"
    mem_99 = mp + i;
# 2592 "dfa.c"
    ifree(mem_99->is);
    __CrestClearStack(33034);
    __CrestLoad(33037, (unsigned long )(& i), (long long )i);
    __CrestLoad(33036, (unsigned long )0, (long long )1);
    __CrestApply2(33035, 0, (long long )(i + 1));
    __CrestStore(33038, (unsigned long )(& i));
# 2586 "dfa.c"
    i ++;
  }
  while_break___6: ;
  }
# 2594 "dfa.c"
  free((void *)((char *)mp));
  __CrestClearStack(33039);

  return_label:
  {
  __CrestReturn(33040);
# 2355 "dfa.c"
  return;
  }
}
}
void __globinit_dfa(void)
{


  {
  __CrestInit();
}
}
