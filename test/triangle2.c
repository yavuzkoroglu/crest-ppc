/* Author: Yavuz Köroğlu
 * Date: 2014 
 *
 * Categorizes a triangle.
 * 
 */

#include <crest.h>
#include <stdio.h>

#define not_a_triangle  "NOT A TRIANGLE\n"
#define equilateral     "EQUILATERAL\n"
#define right_isosceles "RIGHT ISOSCELES\n"
#define isosceles       "ISOSCELES\n"
#define right_scalene   "RIGHT SCALENE\n"
#define scalene         "SCALENE\n"

int main(void) {
    char a;
    CREST_char(a);
    
    while(a > 0) {
		a--;
	}

	return 0;
}
