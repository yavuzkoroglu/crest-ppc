# 1 "./triangle.cil.c"
# 1 "/home/yavuz/crest-ppc3/test//"
# 1 "<command-line>"
# 1 "/usr/include/stdc-predef.h" 1 3 4
# 1 "<command-line>" 2
# 1 "./triangle.cil.c"
# 18 "triangle.c"
void __globinit_triangle(void) ;
extern void __CrestInit(void) __attribute__((__crest_skip__)) ;
extern void __CrestHandleReturn(int id , long long val ) __attribute__((__crest_skip__)) ;
extern void __CrestReturn(int id ) __attribute__((__crest_skip__)) ;
extern void __CrestCall(int id , unsigned int fid ) __attribute__((__crest_skip__)) ;
extern void __CrestBranch(int id , int bid , unsigned char b ) __attribute__((__crest_skip__)) ;
extern void __CrestApply2(int id , int op , long long val ) __attribute__((__crest_skip__)) ;
extern void __CrestApply1(int id , int op , long long val ) __attribute__((__crest_skip__)) ;
extern void __CrestClearStack(int id ) __attribute__((__crest_skip__)) ;
extern void __CrestStore(int id , unsigned long addr ) __attribute__((__crest_skip__)) ;
extern void __CrestLoad(int id , unsigned long addr , long long val ) __attribute__((__crest_skip__)) ;
# 212 "/usr/lib/gcc/x86_64-linux-gnu/4.8/include/stddef.h"
typedef unsigned long size_t;
# 131 "/usr/include/x86_64-linux-gnu/bits/types.h"
typedef long __off_t;
# 132 "/usr/include/x86_64-linux-gnu/bits/types.h"
typedef long __off64_t;
# 44 "/usr/include/stdio.h"
struct _IO_FILE;
# 44 "/usr/include/stdio.h"
struct _IO_FILE;
# 48 "/usr/include/stdio.h"
typedef struct _IO_FILE FILE;
# 144 "/usr/include/libio.h"
struct _IO_FILE;
# 154 "/usr/include/libio.h"
typedef void _IO_lock_t;
# 160 "/usr/include/libio.h"
struct _IO_marker {
   struct _IO_marker *_next ;
   struct _IO_FILE *_sbuf ;
   int _pos ;
};
# 245 "/usr/include/libio.h"
struct _IO_FILE {
   int _flags ;
   char *_IO_read_ptr ;
   char *_IO_read_end ;
   char *_IO_read_base ;
   char *_IO_write_base ;
   char *_IO_write_ptr ;
   char *_IO_write_end ;
   char *_IO_buf_base ;
   char *_IO_buf_end ;
   char *_IO_save_base ;
   char *_IO_backup_base ;
   char *_IO_save_end ;
   struct _IO_marker *_markers ;
   struct _IO_FILE *_chain ;
   int _fileno ;
   int _flags2 ;
   __off_t _old_offset ;
   unsigned short _cur_column ;
   signed char _vtable_offset ;
   char _shortbuf[1] ;
   _IO_lock_t *_lock ;
   __off64_t _offset ;
   void *__pad1 ;
   void *__pad2 ;
   void *__pad3 ;
   void *__pad4 ;
   size_t __pad5 ;
   int _mode ;
   char _unused2[(15UL * sizeof(int ) - 4UL * sizeof(void *)) - sizeof(size_t )] ;
};
# 202 "../bin/../include/crest.h"
extern void __CrestInt(int *x ) __attribute__((__crest_skip__)) ;
# 170 "/usr/include/stdio.h"
extern struct _IO_FILE *stderr ;
# 356 "/usr/include/stdio.h"
extern int fprintf(FILE * __restrict __stream , char const * __restrict __format
                   , ...) ;
# 18 "triangle.c"
int main(void)
{
  int a ;
  int b ;
  int c ;
  int __retres4 ;

  {
  __globinit_triangle();
  __CrestCall(1, 1);
# 20 "triangle.c"
  __CrestInt(& a);
# 21 "triangle.c"
  __CrestInt(& b);
# 22 "triangle.c"
  __CrestInt(& c);
  __CrestLoad(4, (unsigned long )(& a), (long long )a);
  __CrestLoad(3, (unsigned long )0, (long long )0);
  __CrestApply2(2, 15, (long long )(a <= 0));
# 24 "triangle.c"
  if (a <= 0) {
    __CrestBranch(5, 3, 1);
# 25 "triangle.c"
    fprintf((FILE * __restrict )stderr, (char const * __restrict )"NOT A TRIANGLE\n");
    __CrestClearStack(7);
    __CrestLoad(8, (unsigned long )0, (long long )1);
    __CrestStore(9, (unsigned long )(& __retres4));
# 26 "triangle.c"
    __retres4 = 1;
# 26 "triangle.c"
    goto return_label;
  } else {
    __CrestBranch(6, 6, 0);
    {
    __CrestLoad(12, (unsigned long )(& b), (long long )b);
    __CrestLoad(11, (unsigned long )0, (long long )0);
    __CrestApply2(10, 15, (long long )(b <= 0));
# 27 "triangle.c"
    if (b <= 0) {
      __CrestBranch(13, 7, 1);
# 28 "triangle.c"
      fprintf((FILE * __restrict )stderr, (char const * __restrict )"NOT A TRIANGLE\n");
      __CrestClearStack(15);
      __CrestLoad(16, (unsigned long )0, (long long )1);
      __CrestStore(17, (unsigned long )(& __retres4));
# 29 "triangle.c"
      __retres4 = 1;
# 29 "triangle.c"
      goto return_label;
    } else {
      __CrestBranch(14, 10, 0);
      {
      __CrestLoad(20, (unsigned long )(& c), (long long )c);
      __CrestLoad(19, (unsigned long )0, (long long )0);
      __CrestApply2(18, 15, (long long )(c <= 0));
# 30 "triangle.c"
      if (c <= 0) {
        __CrestBranch(21, 11, 1);
# 31 "triangle.c"
        fprintf((FILE * __restrict )stderr, (char const * __restrict )"NOT A TRIANGLE\n");
        __CrestClearStack(23);
        __CrestLoad(24, (unsigned long )0, (long long )1);
        __CrestStore(25, (unsigned long )(& __retres4));
# 32 "triangle.c"
        __retres4 = 1;
# 32 "triangle.c"
        goto return_label;
      } else {
        __CrestBranch(22, 14, 0);
        {
        __CrestLoad(30, (unsigned long )(& a), (long long )a);
        __CrestLoad(29, (unsigned long )(& b), (long long )b);
        __CrestApply2(28, 0, (long long )(a + b));
        __CrestLoad(27, (unsigned long )(& c), (long long )c);
        __CrestApply2(26, 15, (long long )(a + b <= c));
# 33 "triangle.c"
        if (a + b <= c) {
          __CrestBranch(31, 15, 1);
# 34 "triangle.c"
          fprintf((FILE * __restrict )stderr, (char const * __restrict )"NOT A TRIANGLE\n");
          __CrestClearStack(33);
          __CrestLoad(34, (unsigned long )0, (long long )1);
          __CrestStore(35, (unsigned long )(& __retres4));
# 35 "triangle.c"
          __retres4 = 1;
# 35 "triangle.c"
          goto return_label;
        } else {
          __CrestBranch(32, 18, 0);
          {
          __CrestLoad(40, (unsigned long )(& a), (long long )a);
          __CrestLoad(39, (unsigned long )(& c), (long long )c);
          __CrestApply2(38, 0, (long long )(a + c));
          __CrestLoad(37, (unsigned long )(& b), (long long )b);
          __CrestApply2(36, 15, (long long )(a + c <= b));
# 36 "triangle.c"
          if (a + c <= b) {
            __CrestBranch(41, 19, 1);
# 37 "triangle.c"
            fprintf((FILE * __restrict )stderr, (char const * __restrict )"NOT A TRIANGLE\n");
            __CrestClearStack(43);
            __CrestLoad(44, (unsigned long )0, (long long )1);
            __CrestStore(45, (unsigned long )(& __retres4));
# 38 "triangle.c"
            __retres4 = 1;
# 38 "triangle.c"
            goto return_label;
          } else {
            __CrestBranch(42, 22, 0);
            {
            __CrestLoad(50, (unsigned long )(& b), (long long )b);
            __CrestLoad(49, (unsigned long )(& c), (long long )c);
            __CrestApply2(48, 0, (long long )(b + c));
            __CrestLoad(47, (unsigned long )(& a), (long long )a);
            __CrestApply2(46, 15, (long long )(b + c <= a));
# 39 "triangle.c"
            if (b + c <= a) {
              __CrestBranch(51, 23, 1);
# 40 "triangle.c"
              fprintf((FILE * __restrict )stderr, (char const * __restrict )"NOT A TRIANGLE\n");
              __CrestClearStack(53);
              __CrestLoad(54, (unsigned long )0, (long long )1);
              __CrestStore(55, (unsigned long )(& __retres4));
# 41 "triangle.c"
              __retres4 = 1;
# 41 "triangle.c"
              goto return_label;
            } else {
              __CrestBranch(52, 26, 0);

            }
            }
          }
          }
        }
        }
      }
      }
    }
    }
  }
  __CrestLoad(58, (unsigned long )(& a), (long long )a);
  __CrestLoad(57, (unsigned long )(& b), (long long )b);
  __CrestApply2(56, 12, (long long )(a == b));
# 44 "triangle.c"
  if (a == b) {
    __CrestBranch(59, 28, 1);
    {
    __CrestLoad(63, (unsigned long )(& b), (long long )b);
    __CrestLoad(62, (unsigned long )(& c), (long long )c);
    __CrestApply2(61, 12, (long long )(b == c));
# 45 "triangle.c"
    if (b == c) {
      __CrestBranch(64, 29, 1);
# 46 "triangle.c"
      fprintf((FILE * __restrict )stderr, (char const * __restrict )"EQUILATERAL\n");
      __CrestClearStack(66);
      __CrestLoad(67, (unsigned long )0, (long long )0);
      __CrestStore(68, (unsigned long )(& __retres4));
# 47 "triangle.c"
      __retres4 = 0;
# 47 "triangle.c"
      goto return_label;
    } else {
      __CrestBranch(65, 32, 0);
# 49 "triangle.c"
      fprintf((FILE * __restrict )stderr, (char const * __restrict )"ISOSCELES\n");
      __CrestClearStack(69);
      __CrestLoad(70, (unsigned long )0, (long long )0);
      __CrestStore(71, (unsigned long )(& __retres4));
# 50 "triangle.c"
      __retres4 = 0;
# 50 "triangle.c"
      goto return_label;
    }
    }
  } else {
    __CrestBranch(60, 35, 0);
    {
    __CrestLoad(74, (unsigned long )(& a), (long long )a);
    __CrestLoad(73, (unsigned long )(& c), (long long )c);
    __CrestApply2(72, 12, (long long )(a == c));
# 52 "triangle.c"
    if (a == c) {
      __CrestBranch(75, 36, 1);
# 53 "triangle.c"
      fprintf((FILE * __restrict )stderr, (char const * __restrict )"ISOSCELES\n");
      __CrestClearStack(77);
      __CrestLoad(78, (unsigned long )0, (long long )0);
      __CrestStore(79, (unsigned long )(& __retres4));
# 54 "triangle.c"
      __retres4 = 0;
# 54 "triangle.c"
      goto return_label;
    } else {
      __CrestBranch(76, 39, 0);
      {
      __CrestLoad(82, (unsigned long )(& b), (long long )b);
      __CrestLoad(81, (unsigned long )(& c), (long long )c);
      __CrestApply2(80, 12, (long long )(b == c));
# 56 "triangle.c"
      if (b == c) {
        __CrestBranch(83, 40, 1);
# 57 "triangle.c"
        fprintf((FILE * __restrict )stderr, (char const * __restrict )"ISOSCELES\n");
        __CrestClearStack(85);
        __CrestLoad(86, (unsigned long )0, (long long )0);
        __CrestStore(87, (unsigned long )(& __retres4));
# 58 "triangle.c"
        __retres4 = 0;
# 58 "triangle.c"
        goto return_label;
      } else {
        __CrestBranch(84, 43, 0);
# 60 "triangle.c"
        fprintf((FILE * __restrict )stderr, (char const * __restrict )"SCALENE\n");
        __CrestClearStack(88);
        __CrestLoad(89, (unsigned long )0, (long long )0);
        __CrestStore(90, (unsigned long )(& __retres4));
# 61 "triangle.c"
        __retres4 = 0;
# 61 "triangle.c"
        goto return_label;
      }
      }
    }
    }
  }
  return_label:
  {
  __CrestLoad(91, (unsigned long )(& __retres4), (long long )__retres4);
  __CrestReturn(92);
# 18 "triangle.c"
  return (__retres4);
  }
}
}
void __globinit_triangle(void)
{


  {
  __CrestInit();
}
}
