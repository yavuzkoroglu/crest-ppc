// Program to print all prime factors
# include <crest.h>
# include <stdio.h>
 
int main(void)
{
	int n;
    CREST_int(n);

	if (n > 10)
		return 0;

	int i;
	fprintf(stderr, "n = %d\n", n);
	for (i = 0; i < n;) {
		fprintf(stderr, "i = %d\n", i);
		n = n - 2;
	}

    return 1;
}
