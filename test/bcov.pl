#!/usr/bin/perl

open TestSuite, "<$ARGV[0]";

`gcc -Wall -fprofile-arcs -ftest-coverage -g $ARGV[1].c -o $ARGV[1].out`;

$count = 0;
while (<TestSuite>) {
	$count++;
	`$ARGV[1].out $_`;
}

print "By using $count test cases;\n";
print `gcov -b $ARGV[1].c | grep Taken`;

close TestSuite;
